\documentclass[presentation]{beamer}
%\documentclass[handout]{beamer}

\usecolortheme{Imperial}
 
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{epstopdf}

\usepackage{algpseudocode}
\usepackage{algorithm}

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\usepackage{fancybox}

\usepackage{tikz}
\usepackage{pgf}
\usepackage{pgfplots}
\pgfplotsset{compat=1.13}
\usepgfplotslibrary{groupplots}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{tikz}
\usetikzlibrary{shapes.multipart, shapes.geometric}
\usetikzlibrary{positioning, calc}

% complying UK date format, i.e. 1 January 2001
\usepackage{datetime}
\let\dateUKenglish\relax
\newdateformat{dateUKenglish}{\THEDAY~\monthname[\THEMONTH] \THEYEAR}

% Imperial College Logo, not to be changed!
\institute{\includegraphics[height=0.7cm]{Imperial_1_Pantone_solid.eps}
           \includegraphics[height=0.7cm]{hipeds.png}}

% Add the slide number to the footer
\addtobeamertemplate{footline}{}{%
\usebeamerfont{footline}%
\usebeamercolor{footline}%
\hfill\raisebox{5pt}[0pt][0pt]{\insertframenumber\quad}}

% -----------------------------------------------------------------------------

%Information to be included in the title page:
\title{\LARGE A Survey of the Implementation of Linear Model Predictive Control on FPGAs}
\author{Ian McInerney\\
   Supervisors: Eric C.\ Kerrigan, George A.\ Constantinides}
\date{May 31, 2018}

\begin{document}
 
\frame{\titlepage}

\begin{frame}
	\frametitle{Outline}
    \tableofcontents
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Model Predictive Control Overview}

\input{slides/predictionOverview/predictionOverview.tex}

% No need to discuss the generic problem
%\begin{frame}
%    \frametitle{MPC - Problem}
%    
%    \begin{block}{Generic Problem}
%        \begin{align*}
%        \underset{\bar{u}, \bar{x}}{\text{min}} \quad & g(\bar{x}, \bar{u}, x_0)\\
%        \text{s.t.} \quad & x_{k+1} = f(x_k, u_k)  & k=0, \dots N-1 \\
%        & h(u_k, x_k, x_0) \leq 0 & k=0, \dots N 
%        \end{align*}
%    \end{block}
%    \begin{align*}
%        \bar{x} &= \begin{bmatrix}
%        x_1 & x_2 & x_3 & \cdots & x_N
%        \end{bmatrix}^T \\
%        \bar{u} &= \begin{bmatrix}
%        u_0 & u_1 & u_2 & \cdots & u_{N-1}
%        \end{bmatrix}^T
%    \end{align*}
%\end{frame}


\begin{frame}
    \frametitle{Optimization Problem}

    \begin{block}{LQR Problem}
        \begin{align*}
        \underset{\bar{u}, \bar{x}}{\text{min}} \quad & \norm{x_N}^2_P + \sum_{k=0}^{N-1} \norm{x_k}^2_Q + \norm{u_k}^2_R \\
        \text{s.t.} \quad & x_{k+1} = A x_k + B u_k & k=0, \dots N-1 \\
        & F u_k \leq 0  & k=0, \dots N-1 \\
        & J x_k \leq 0  & k=1, \dots N 
        \end{align*}
    \end{block}

    \begin{itemize}
        \item If $P,Q, R \succeq 0$, this is a convex Quadratic Program (QP)
    \end{itemize}
\end{frame}

\input{slides/solutionMethods/solutionMethods.tex}

\begin{frame}
    \frametitle{MPC Formulations}
    
    \begin{block}{Sparse}
        \begin{itemize}
            \item Original optimization problem
        \end{itemize}
        \begin{tabular}{lll}
            \underline{Advantages:} & & \underline{Disadvantages:} \\
            Banded matrices & & More optimization variables \\
            MVM constant size & & May include equality constraints
        \end{tabular}
    \end{block}
    
    \begin{block}{Condensed}
        \begin{itemize}
            \item Remove the state variables from the optimization
        \end{itemize}
        \begin{tabular}{lll}
            \underline{Advantages:} & & \underline{Disadvantages:} \\
            Fewer variables & & Dense Hessian matrix \\
            No equality constraints & & MVM data-path grows order $N$
        \end{tabular}
    \end{block}
\end{frame}

\input{slides/chipComparison/chipComparison.tex}

%\begin{frame}
%    \frametitle{Real-Time Implementation}
%    
%    \begin{block}{CPU}
%        \begin{itemize}
%            \item Fixed data-path sizes \& representations
%            \item Inherently sequential operations
%            \newline
%            \item Architecture agnostic code generation for solvers
%        \end{itemize}
%    \end{block}
%
%    \begin{block}{GPU}
%    \begin{itemize}
%        \item High level of parallelism for supported operations
%        \item Usually only supports double/single precision floating-point
%        \item Very power \& thermal intensive
%        \newline
%        \item Limited Research from MPC community
%    \end{itemize}
%\end{block}
%
%
%\end{frame}
%
%\begin{frame}
%    \frametitle{Real-Time Implementation}
%
%    \begin{block}{FPGA}
%        \begin{itemize}
%            \item Reconfigurable computational fabric
%            \item Custom datapath \& representations
%            \item Low power
%            \newline
%            \item Intensive research in the MPC community
%            \begin{itemize}
%                \item Parallelizations
%                \item Number representations
%                \item Memory Allocation/Usage
%            \end{itemize}
%            \item Results showed sampling times of 1 MHz are possible
%        \end{itemize}
%        
%    \end{block}
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Quadratic Programming Algorithms}

\begin{frame}
    \frametitle{Algorithm Taxonomy}
    
    \scalebox{0.55}{
    \centering
    \input{images/algTaxon.pgf}
    }
\end{frame}

\begin{frame}
    \frametitle{Computation Times}
    \input{comparisonParts/comparisonFigure.tex}
\end{frame}

\input{slides/interiorPoint/interiorPoint.tex}

%\begin{frame}
%    \frametitle{IPM - Scaling}
%    
%    \begin{block}{Computational Scaling}
%        \centering
%        \begin{tabular}{cc}
%            General Solver & $O(N^3 (n+m)^3)$ \\
%            MINRES & $O(N^2(n+m)^3)$ \\
%            Specialized Cholesky & $O(N (n+m)^3)$ \\
%        \end{tabular}
%        
%    \end{block}
%\end{frame}

%\begin{frame}
%    \frametitle{Active-Set Methods}
%    
%    \begin{block}{General Idea}
%        Find the set of constraints that are active at the optimal point by iteratively trying different sets until all constraints are satisfied.
%    \end{block}
%
%    \begin{block}{Implementation}
%        \begin{itemize}
%            \item Finite convergence - Number of possible iterations grows exponentially with constraints
%            \item Utilize smart update rules for adding/removing from the active-set
%            \item Experiments show number of iterations grows $O(N)$ with problem size and number of constraints
%        \end{itemize}
%    \end{block}
%    
%\end{frame}

\input{slides/firstOrder/firstOrder.tex}

\begin{frame}
    \frametitle{Number Representation}
    
    \begin{block}{Interior-Point Methods}
        \begin{itemize}
            \item No generic bounds on the variable size in IP iterations
            \item Most IPM implementations utilize floating-point
            \item MINRES Linear Solver can use fixed-point
            \begin{itemize}
                \item Use a preconditioner to bound the variables
            \end{itemize}
        \end{itemize}
    \end{block}
    
    \begin{block}{First-Order Methods}
        \begin{itemize}
            \item Most implementations use fixed-point
            \item Many proved stable under presence of round-off errors
            \item Design-time formulas to determine number of bits given a specified error bound
        \end{itemize}
    \end{block}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Formulations of MPC}

%\begin{frame}
%    \frametitle{MPC Problem Formulations}
%    
%    3 possible MPC problem formulations
%    \begin{itemize}
%        \item Condensed - Remove the states from the problem
%        \item Sparse - All variables are in the problem
%        \item Variable-Sparsity - In between the two others
%    \end{itemize}
%\end{frame}

%\begin{frame}
%    \frametitle{Condensed MPC}
%    
%    Reformulate prediction matrix to remove the states from the optimization variables
%    \begin{equation*}
%    \label{eq:mpc:cond:pred}
%    \bar{x} = 
%    \underbrace{
%        \begin{bmatrix}
%        B & 0 & 0 & & 0\\
%        AB & B & 0 & & 0\\
%        A^2b & AB & B & & 0 \\
%        \vdots & & & \ddots & \vdots\\
%        A^{N-1}B & A^{N-2}B & A^{N-3}B & \cdots & B
%        \end{bmatrix}}_{=\Gamma}  \bar{u}
%    + \underbrace{
%        \begin{bmatrix}
%        A \\
%        A^2 \\
%        A^3 \\
%        \vdots \\
%        A^{N}
%        \end{bmatrix}}_{=\Phi} x_0
%    \end{equation*}
%
%\end{frame}

%\begin{frame}
%    \frametitle{Variable-Sparsity MPC}
%    
%    \begin{block}{}
%        Induce a banded matrix in the condensed formulation
%    \end{block}
%
%    \begin{columns}
%        \begin{column}{0.48\textwidth}
%        \begin{block}{Deadbeat Controller}
%            \begin{itemize}
%                \item Use a linear controller that makes $A$ nilpotent
%                \item $H$ is then banded, with bandwidth of the controllability index
%            \end{itemize}
%        \end{block}
%        \end{column}
%        \begin{column}{0.48\textwidth}
%            \begin{block}{Variable Condensing}
%                \begin{itemize}
%                    \item Split the horizon into subintervals
%                    \item Condense inside the subintervals, not between
%                    \item Can vary between fully sparse and fully condensed
%                \end{itemize}
%            \end{block}
%        \end{column}
%    \end{columns}
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parallelization Opportunities}

\begin{frame}
    \frametitle{Parallelization Opportunities}
     
    \begin{block}{Algorithmic Level}
        Exploit data-dependencies in the algorithm steps to parallelize the iterations/iteration steps
    \end{block}


    \begin{block}{Computational Level}
        Parallelize the low-level arithmetic computations of the algorithm
        \begin{itemize}
            \item Linear System Solution
            \item Matrix-Vector Multiplication
            \item Cholesky Factorization
        \end{itemize}
    \end{block}
    
%    \begin{block}{Interior-Point Methods}
%        Usually parallelize the linear system solution
%        
%        Leave the update \& system setup as sequential
%    \end{block}
    
\end{frame}
 
\begin{frame}
    \frametitle{FGM Parallelism}

    \begin{block}{}
        Parallelization highly dependent on the constraint set
    \end{block}

    \begin{figure}
        \centering
        \includegraphics[width=0.8\linewidth]{images/par_fgmJerez2014.png}
        \caption*{FGM implementation from Jerez et.\ al.\ 2014}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{MVM Parallelism}

    \begin{minipage}{0.48\linewidth}
            \centering
            \includegraphics[width=\textwidth]{images/MVM_GPD.png}
            
            Column-Sweep Implementation\footnotemark
    \end{minipage}%
    \begin{minipage}{0.48\linewidth}
            \centering
            \includegraphics[height=12em]{images/MVM_ColMajor.png}
            
            Row-Sweep Implementation\footnotemark
    \end{minipage}
    
    \footnotetext[1]{Rubagotti et.\ al.\ 2016}
    \footnotetext[2]{Wills et.\ al.\ 2011}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Memory Architectures}

\input{slides/fpgaRegions/fpgaRegions.tex}

\begin{frame}
    \frametitle{Matrix Storage}
    
    \begin{block}{Structure Explotation}
        Exploit banded structure to reduce the memory footprint
    \end{block}

    \begin{figure}
        \includegraphics[width=0.7\textwidth]{images/CDS_Jerez2012.png}
    \end{figure}

\footnotetext[1]{Jerez et.\ al. 2012}
\end{frame}

%\begin{frame}
%    \frametitle{Matrix Storage}
%    
%    \begin{block}{Linear MPC Structure Exploitation}
%        A further savings of 75\% can be attained by also exploiting the structure of the LTI MPC problem
%        \begin{itemize}
%            \item Similar rows - Many rows of the matrix will be repeated
%            \item Similar blocks - Many of the blocks will be repeated in the matrix
%        \end{itemize}
%    \end{block}
%\end{frame}

\section{Conclusion}

\begin{frame}
    \frametitle{Conclusion}

    \begin{block}{Key Points}
        \begin{itemize}
            \item Algorithm choice drives implementation
            \item Effects of implementation on algorithm must be considered
            \item Mainly parallelize low-level computations
            \item Structure exploitation saves memory
        \end{itemize}
    \end{block}

    \begin{block}{Future Directions}
        \begin{itemize}
            \item How can the FPGA design be streamlined?
            \item How do these results extend to the nonlinear MPC problem?
        \end{itemize}
        
    \end{block}
    
\end{frame}
 
\end{document}

