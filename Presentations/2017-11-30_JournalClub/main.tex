\documentclass[presentation]{beamer}
%\documentclass[handout]{beamer}

\usecolortheme{Imperial}
 
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{epstopdf}

\usepackage{im-lib-beamer}

% complying UK date format, i.e. 1 January 2001
\usepackage{datetime}
\let\dateUKenglish\relax
\newdateformat{dateUKenglish}{\THEDAY~\monthname[\THEMONTH] \THEYEAR}

% Imperial College Logo, not to be changed!
\institute{\includegraphics[height=0.7cm]{Imperial_1_Pantone_solid.eps}}

% Add the slide number to the footer
\addtobeamertemplate{footline}{}{%
\usebeamerfont{footline}%
\usebeamercolor{footline}%
\hfill\raisebox{5pt}[0pt][0pt]{\insertframenumber\quad}}

% -----------------------------------------------------------------------------

%Information to be included in the title page:
\title{Journal Club Paper Review}
\subtitle{Event-Triggered Real-Time Scheduling of Stabilizing Control Tasks}
\author{Ian McInerney}
\date{November 30, 2017}

\begin{document}
 
\frame{\titlepage}

\begin{frame}
	\frametitle{Paper Overview}
	
    \begin{minipage}{0.49\textwidth}
        \begin{block}{}
            \includegraphics[scale=0.23]{PaperAbstract.png}
        \end{block}
    \end{minipage}
    \begin{minipage}{0.49\textwidth}
        \begin{itemize}
            \item Note in IEEE Transactions on Automatic Controls
            \item Published in 2007 (submitted 2006)
            \item 1029 paper citations
        \end{itemize}
    \end{minipage}
\end{frame}


\begin{frame}
	\frametitle{Paper Claims}
    
    \begin{block}{Main Idea}
        Examine the problem of scheduling stabilizing control tasks on embedded processors with other tasks
    \end{block}

    \begin{block}{Contributions}
        \begin{itemize}
            \item Present event-trigger criteria to guarantee system stability
            \item Derive lower bound for interexecution times of controller
            \item Present test for if a set of tasks can be coscheduled with an event-triggered controller
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Discrete-Time Controllers}
    
    \begin{itemize}
        \item Physical world is continuous, computers are discrete
        \item Continuous-time signals must be sampled for computer-based controllers
    \end{itemize}
    
    \begin{block}{Discrete-Time CL System}
        \begin{center}
            \includegraphics[scale=0.17]{SD_System.png}
        \end{center}
        {\tiny From: M. Seron and J. Braslavsky and G. Goodwin, \textit{Fundamental Limitations in Filtering and Control}, 1997}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Sampling Theory}
    
    Two ways to sample continuous-time signals:
    
    \begin{block}{Periodic}
        \begin{itemize}
            \item Fixed time between samples
            \item Usually used in practice due to simplicity of design and stability guarantees
        \end{itemize}
    \end{block}
    
    \begin{block}{Aperiodic}
        \begin{itemize}
            \item Variable time between samples
            \item Sample either randomly, or event-triggered
        \end{itemize}
    \end{block}
\end{frame}


\begin{frame}
	\frametitle{Technical Preliminaries}

    \begin{block}{Input-to-State Stability}
        \begin{itemize}
            \item Intuitive definition: \newline
            The system will have bounded states for a bounded input
            
            \item Technical definition: \newline
            \includegraphics[scale=0.3]{ISS.png}
        \end{itemize}
    \end{block}
\end{frame}


\begin{frame}
	\frametitle{Sampling Criteria (Contribution 1)}

    \begin{block}{Criteria}
        Sample the system when $\gamma ( \abs{e} ) = \sigma \alpha (\abs{x})$ where $ 1 > \sigma > 0$
    \end{block}

    \begin{block}{}
        This bounds the dynamics of $V$ so that the ISS definition is met, e.g.
        $$ \frac{\delta V}{\delta x}f(x, k(x+e)) \leq (\sigma - 1) \alpha( \abs{x})$$
        when 
        $\gamma ( \abs{e} ) \leq \sigma \alpha (\abs{x})$
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Interexecution Time Lower Bound (Contribution 2)}
    
    \begin{minipage}{0.49\textwidth}
        \begin{block}{Result}
            \includegraphics[scale=0.20]{LowerBoundTheorem.png}
        \end{block}
    \end{minipage}
    \begin{minipage}{0.49\textwidth}
        \begin{itemize}
            \item Required to show controller task won't hog the CPU
            \item Theorem applies to a large set of systems
            \item Requires finding a Lyapunov Function $V$ for $f(x,u)$
        \end{itemize}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Interexecution Time Lower Bound (Contribution 2)}

    \begin{minipage}{0.49\textwidth}
        \begin{block}{Result}
            \begin{center}
                \includegraphics[scale=0.17]{LinearCase.png}
            \end{center}
        \end{block}
    \end{minipage}
    \hspace{0.03\textwidth}
    \begin{minipage}{0.45\textwidth}
        For the linear system case:
        \begin{itemize}
            \item Can derive a numerical estimate of the interexecution time
            \item Computationally tractable
        \end{itemize}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Coschedulability of Tasks (Contribution 3)}
    \begin{block}{Result}
        Given a set of tasks $T = \{T_i\}_{i\in \mathbf{I}}$ with execution times $\Delta_i$, and an ISS controller meeting the previous theorem with computation time $\Delta$.
        \newline
        
        The tasks T are coschedulable with the controller if they are still schedulable with execution times
        $$ \Delta_i' = \Delta_i + \left[\frac{\Delta_i}{\tau}\right]\Delta$$
        
    \end{block}
    \begin{itemize}
        \item Each task can be interrupted $\frac{\Delta_i}{\tau}$ times by the controller
        \item Inflate the scheduled duration of a task to incorporate worst-case estimate of interruption by the controller
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Numerical Example}

    \begin{block}{}
        Use event-based triggering for a 2-state linear system:
        \begin{center}
            \includegraphics[scale=0.23]{Example.png}
        \end{center}
    \end{block}

    \begin{minipage}{0.43\textwidth}
        \begin{block}{System State Error}
            \includegraphics[scale=0.12]{ResultGraph.png}
        \end{block}

    \end{minipage}
    \hspace{0.02\textwidth}
    \begin{minipage}{0.50\textwidth}
        \begin{block}{Interexecution Time Bound}
            \includegraphics[scale=0.17]{LowerBounds.png}
        \end{block}

    \end{minipage}
    
\end{frame}

\begin{frame}
    \frametitle{Paper's Contributions}
    
    \begin{block}{Specific}
        Derived system stability conditions under event-triggered control
        \begin{itemize}
            \item Used general assumptions that hold with a lot of systems
        \end{itemize}
    \end{block}
    \begin{block}{General}
        Examined event-triggered controllers from a process scheduling perspective
        \begin{itemize}
            \item Suggests co-design of the controller and the software system
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Critique - Numerical Example}
    
    \begin{block}{System is boring}
        It would be better if the system:
        \begin{itemize}
            \item Had a physical meaning
            \item Had multiple time-scales
        \end{itemize}
    \end{block}

    
    \begin{block}{Other Thoughts}
        \begin{itemize}
            \item How does the controller choice affect the result?
            \item Theory derived in section III is more general than linear systems
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Critique - Abstract}
    
    \begin{center}
        \begin{minipage}{0.49\textwidth}
            \begin{block}{Abstract}
                \includegraphics[scale=0.20]{Abstract.png}
            \end{block}
        \end{minipage}
    \end{center}
    
    \begin{block}{Missing Result}
        The abstract does not mention the derivation of the interexecution time lower bound
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Minor Critique - Notation \& Definitions}
    
    \begin{block}{}
        \begin{itemize}
            \item Uses $\abs{e}$ in stability definition before actually defining the term properly
            \item Introduction of $\sigma'$ is buried inside a proof
            \begin{itemize}
                \item Its usage in the example is somewhat confusing
            \end{itemize}
        \end{itemize}
    \end{block}
\end{frame}

 
\end{document}

