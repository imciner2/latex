\documentclass[presentation]{beamer}
%\documentclass[handout]{beamer}

\usecolortheme{Imperial}
 
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{epstopdf}

% complying UK date format, i.e. 1 January 2001
\usepackage{datetime}
\let\dateUKenglish\relax
\newdateformat{dateUKenglish}{\THEDAY~\monthname[\THEMONTH] \THEYEAR}

% Imperial College Logo, not to be changed!
\institute{\includegraphics[height=0.7cm]{Imperial_1_Pantone_solid.eps}}

% Add the slide number to the footer
\addtobeamertemplate{footline}{}{%
\usebeamerfont{footline}%
\usebeamercolor{footline}%
\hfill\raisebox{5pt}[0pt][0pt]{\insertframenumber\quad}}

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

% -----------------------------------------------------------------------------

\AtBeginSection[]
{
\begin{frame}
    \frametitle{Outline}
    {\footnotesize\tableofcontents[currentsection]}
\end{frame}
}

\usepackage[backend=bibtex,style=authortitle]{biblatex}
\bibliography{references}

%Information to be included in the title page:
\title{The Koopman Operator}
\subtitle{An overview}
\author{Ian McInerney}
\date{January 12, 2018}

\begin{document}
 
\frame{\titlepage}

\begin{frame}
    \frametitle{Outline}
    {\footnotesize\tableofcontents}
\end{frame}

%%%%%%%%%%%%%%%%%%%
% Overview
%%%%%%%%%%%%%%%%%%%
\section{Overview}
\subsection{Nonlinear Systems}
\begin{frame}
	\frametitle{Nonlinear Systems}
    
    \begin{block}{Technical Definition}
        A nonlinear system can be defined by a transformation $T: \mathcal{M} \to \mathcal{M}$ that maps a point inside a manifold to another point inside a manifold. E.g.
        \begin{equation*}
            x_{i+1} = T(x_i)
        \end{equation*}
    \end{block}
    
    Examples: Basically every real-world system...

    \begin{block}{Definition}
        Define an observer function $\phi: \mathcal{M} \to \mathbb{C}$ which exists in a function space $\mathcal{F}$.
    
        Used to observe the behavior of the mapping $T$ (e.g. a sensor probe)
    \end{block}
    
\end{frame}

\begin{frame}
    \frametitle{Nonlinear Systems}

    \begin{itemize}
        \item Can be computationally intensive to evaluate $T$
        \item Direct analytical analysis is very difficult (e.g. equilibrium properties, spectral properties, etc.)
        \item Instead, transform into the linear domain to do analysis (e.g. linearization)
        \item Transformation is only valid under certain assumptions
        \begin{itemize}
            \item No poles on the $j\omega$ axis
            \item Only representative inside a region of the manifold
        \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Koopman Operator}
\begin{frame}
    \frametitle{Koopman Operator}
    
    Normal analysis focuses on seeing how $T$ affects the movement through $\mathit{M}$.
    \newline
    E.g. the trajectory \{ $p$, $T(p)$, $T^2(p)$, \dots\}

    \vspace{1em}
    Instead, track the trajectory of the observable.
    \newline
    E.g.  \{ $\phi(p)$, $\phi(T(p))$, $\phi(T^2(p))$, \dots\}
    
    \begin{block}{Koopman Operator (generally)}
        The Koopman Operator, $U_T$, is a composition operator that is composition of the observable $\phi$ with the mapping $T$. It represents how the trajectory behaves in the observable space.
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Koopman Operator}
    
    \begin{block}{Koopman Operator (formally)}
        The Koopman Operator, $U_T: \mathcal{F} \to \mathcal{F}$ is defined as
        \begin{equation*}
        [U_Tf](p) = \phi(T(p))
        \end{equation*}
    \end{block}

    \begin{block}{Important Points}
        \begin{itemize}
            \item If the observable space, $\mathcal{F}$, is a vector space, then $U_T$ is linear.
            \item If $\mathcal{M}$ is finite, then $U_T$ is a finite-dimensional operator
            \item Otherwise, $U_T$ is an infinite-dimensional operator
        \end{itemize}

    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Koopman Operator\footcite{Brunton2016}}
    
    \begin{figure}
        \includegraphics[width=\framewidth]{figures/KoopmanOperatorMechanism.png}
    \end{figure}
    
\end{frame}


\subsection{Perron-Frobenius Operator}
\begin{frame}
    \frametitle{Perron-Frobenius Operator}
    
    \begin{block}{}
        \begin{itemize}
            \item Set-valued operator
            \item Propagates densities/uncertainty about the dynamical system along the system trajectories
            \item Is the adjoint of the Koopman operator
        \end{itemize}
    \end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%
% Approximating
%%%%%%%%%%%%%%%%%%%%%%%
\section{Approximating the Koopman Operator}
\subsection{Dynamic Mode Decomposition}
\begin{frame}
    \frametitle{Dynamic Mode Decomposition}
    
    \begin{block}{Problem}
        Given a time-series of data from a dynamical system, approximate the linear operator that describes it.
    \end{block}

    \begin{block}{Solution}
        Create an over-determined system and use least-squares to approximate the system.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Dynamic Mode Decomposition}
    
    \begin{block}{Data Representation}
        Let $ y_{i} = f(x_{i})$ represent the evolution of a dynamical system.
        \newline
        
        Create the matrices:
        \begin{equation*}
            X =
            \begin{bmatrix}
            \vline & \vline & & \vline\\
            x_1 & x_2 & \cdots & x_m\\
            \vline & \vline & & \vline
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            Y =
            \begin{bmatrix}
            \vline & \vline & & \vline\\
            y_1 & y_2 & \cdots & y_m\\
            \vline & \vline & & \vline
            \end{bmatrix}
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Dynamic Mode Decomposition}
    
    \begin{block}{Approximation Technique}
        Solve the matrix-valued least-squares problem (in the Frobenius norm)
        \begin{equation*}
        \begin{matrix}
            \underset{A}{min} & \norm{Y - AX}_F
        \end{matrix}
        \end{equation*}
        
        Using the normal equation:
        \begin{equation*}
            A = YX^{\dagger}
        \end{equation*}
    \end{block}
\end{frame}

\subsection{Operator Approximation}

\begin{frame}
    \frametitle{Koopman Approximation}
    
    \begin{block}{Problem}
        \begin{itemize}
            \item Analytical description of Koopman operator is not usually available
            \item Usually $\mathcal{M}$ is not finite, so the Koopman operator is infinite
            \item Applications need finite-dimensional approximation of the operator
        \end{itemize}
    \end{block}

    \begin{block}{Solution}
        Modify the Dynamic Mode Decomposition algorithm to use the observable space
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Koopman Approximation}
    
    \begin{block}{}
        Matrices become:
        \begin{equation*}
            X_U =
            \begin{bmatrix}
            \vline & \vline & & \vline\\
            \phi(x_1) & \phi(x_2) & \cdots & \phi(x_m)\\
            \vline & \vline & & \vline
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            Y_U =
            \begin{bmatrix}
            \vline & \vline & & \vline\\
            \phi(y_1) & \phi(y_2) & \cdots & \phi(y_m)\\
            \vline & \vline & & \vline
            \end{bmatrix}
        \end{equation*}
        
        Koopman operator is then:
        \begin{equation*}
            U = Y_U X_U^{\dagger}
        \end{equation*}
    \end{block}
\end{frame}

\subsection{Observable Functions}
\begin{frame}
    \frametitle{Observable Functions}
    
    \begin{block}{}
        Choice of observable functions can be crucial to the approximation of the Koopman operator
    \end{block}

    \begin{block}{Function Choices}
        \begin{itemize}
            \item Gaussian Radial Basis
            \item Thin Plate Spline Radial Basis
            \item System-specific Choice
            \item etc.
        \end{itemize}
    \end{block}

    \begin{block}{}
        The selection of the basis functions to use is an area of ongoing research.
    \end{block}
\end{frame}

\section{Applications: Nonlinear System Analysis}
\begin{frame}
    \frametitle{Koopman Eigenfunctions}

    \begin{block}{Definition}
        An eigenfunction of the Koopman operator $U$ is defined by
        \begin{equation*}
            U \phi_{\lambda} = e^{\lambda t} \phi_{\lambda}
        \end{equation*}
        where $\phi_{\lambda} \in \mathcal{F} \neq 0$.
    \end{block}

    \begin{block}{Remarks}
        \begin{itemize}
            \item The eigenfunctions are constant along the trajectories of the original system.
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Global Stability Analysis}
    
    The Koopman eigenfunctions and eigenvalues can be used to get information about the global stability of a system.\footcite{Mauroy2013}
    
    \begin{block}{}
        For the Koopman eigenfunctions with $\text{Re}\{\lambda\} < 0$, the zero-level set $M_0 = \{ x \in \mathcal{M} | \phi_{\lambda}(x) = 0 \}$ is globally asymptotically stable.
        \newline
        
        To prove the stability of an attractor, find the intersection of all the zero level sets and see if it completely covers the attractor
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Hyperbolic Fixed Point Attractor}
    
    When the attractor is a fixed point of the system, the Koopman operator point spectrum contains the eigenvalues of the Jacobian at the fixed point.
    
    \begin{block}{Global Stability Result}
        If the Jacobian matrix has $N$ distinct eigenvalues with $\text{Re}\{\lambda\} < 0$, then the fixed point is globally stable iff the Koopman operator has $N$ distinct eigenfunctions $\phi_{\lambda} \in C^{1}$ with $\text{Re}\{\lambda\}$ and $\nabla \phi_{\lambda}(x^*) \neq 0$
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Hyperbolic Fixed Point Attractor}
    
    \begin{block}{Remarks}
        \begin{itemize}
            \item Jacobian linearization can only talk about the local stability based upon the eigenvalues of the Jacobian
            \item Existence of the $N$ $C^1$ eigenfunctions can guarantee global stability of the fixed point
            \item The support of the eigenfunctions is the region of attraction for the fixed point
        \end{itemize}
    \end{block}
\end{frame}

\section{Applications: Model Predictive Control}
\begin{frame}
    \frametitle{Nonlinear MPC}
    \begin{itemize}
        \item NMPC is computationally difficult due to the non-linearity of the equality constraint
        \item One approach to overcome this is to simply linearize the nonlinear dynamics to use in the prediction
    \end{itemize}

    \begin{block}{Idea}
        What about using the Koopman operator as the predictor?\footcite{Korda2016}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Controlled Koopman Operators}
    Previous Koopman operator theory was derived for uncontrolled nonlinear system analysis.
    \begin{block}{Control Modification}
        Modify the observable space to include the input sequences in its space
    \end{block}

    \begin{block}{Estimation}
        Observable functions becomes $\phi(x, u) = \phi(x) + \mathcal{L}(u)$ where $\mathcal{L}$ is linear and maps from the control sequence to a scalar.
        \begin{equation*}
            \begin{matrix}
            \underset{A,B}{min} & \norm{Y - AX - BU}_F
            \end{matrix}
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Koopman MPC: Overview}
    
    \begin{block}{Main Points}
        \begin{itemize}
            \item Utilize the Koopman operator system as the linear predictor
            \item Use the dense formulation of the MPC problem to remove the observables from the optimization
        \end{itemize}
    \end{block}

    \begin{block}{Advantages}
        \begin{itemize}
            \item Dense MPC problem is on same scale as equivalent linear MPC problem
            \item Nonlinear constraints and penalties can be introduced by making them an observable function, then constraining the observable
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Koopman MPC: Comparison\footcite{Korda2016}}
    
    \begin{figure}
        \includegraphics[width=0.7\framewidth]{figures/PredictionComparison.png}
    \end{figure}
\end{frame}

\begin{frame}
\frametitle{Koopman MPC: Comparison\footcite{Korda2016}}

    \begin{figure}
        \includegraphics[width=0.7\framewidth]{figures/ControllerComparison.png}
    \end{figure}
\end{frame}

\end{document}

