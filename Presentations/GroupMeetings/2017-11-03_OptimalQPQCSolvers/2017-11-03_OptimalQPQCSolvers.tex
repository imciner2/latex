\documentclass[handout]{beamer}
%\documentclass[presentation]{beamer}

\usecolortheme{Imperial}
 
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{epstopdf}

% Available from https://github.com/imciner2/texmf
\usepackage{im-lib-beamer}

% complying UK date format, i.e. 1 January 2001
\usepackage{datetime}
\let\dateUKenglish\relax
\newdateformat{dateUKenglish}{\THEDAY~\monthname[\THEMONTH] \THEYEAR}

% Imperial College Logo, not to be changed!
\institute{\includegraphics[height=0.7cm]{Imperial_1_Pantone_solid.eps}}

\addtobeamertemplate{footline}{}{%
\usebeamerfont{footline}%
\usebeamercolor{footline}%
\hfill\raisebox{5pt}[0pt][0pt]{\insertframenumber\quad}}

% -----------------------------------------------------------------------------

%Information to be included in the title page:
\title{Optimal QPQC Solvers}
\subtitle{Introduction and future work}
\author{Ian McInerney}
\date{\today}


\begin{document}
 
\frame{\titlepage}


\begin{frame}
	\frametitle{Introduction}
	
	\begin{itemize}
		\item Algorithms presented here developed by Dostal et. al.
		\item Claim optimal in the sense that the matrix-vector multiplications grow at \textbf{O}(1)
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Equality Constrained Problems}
	\begin{itemize}
	\item Solve the problem
	{\footnotesize
	\begin{align*}
	\underset{x \in \mathbb{R}^n}{\text{minimize}}
	&
	\quad \frac{1}{2} x^{T} A x - b^{T} x
	&
	\text{subject to}
	&
	\quad Cx = d
	\end{align*}}
	\item Use the unconstrained Augmented Lagrangian
	\begin{equation*}
		L(x, \mu^k, \rho_k) = \frac{1}{2} x^{T} A x - b^{T} x + (\mu^k)^T(Cx-d) + \frac{\rho_k}{2} \norm{Cx-d}^2_2
	\end{equation*}
    \item $\mu^k$ is the vector of Lagrangian multiplers, $\rho_k$ is the penalty parameter

	\end{itemize}

\end{frame}

\begin{frame}
    \frametitle{SMALE-M Algorithm}
    
    Note: $\rho$, $\beta$ and $\eta$ are fixed at run-time
    
    \begin{enumerate}
        \item[Step 1] Find $x^k$ such that
        \begin{equation*}
            \norm{g(x^k, \mu^k, \rho)} \leq \min{\left\{M_k\norm{Cx-d}, \eta\right\}}
        \end{equation*}
        
        \item[Step 2] Update $\mu$
        \begin{equation*}
            \mu^{k+1} = \mu^k + \rho(Cx^k - d)
        \end{equation*}
        
        \item[Step 3] Update M
        if Lagrangian cost hasn't increased sufficiently:\\
        \begin{center}
            $ M_{k+1} = \beta M_k $ else $ M_{k+1} = M_k $
        \end{center}
        
        \item[Step 4] Goto step 1 and repeat until termination condition met
        
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{SMALE-M Algorithm}
    
    \begin{itemize}
        \item Augmented Lagrangian algorithm with adaptive precision control of inner problem (step 1)
        \item R-linear convergence of feasibility error ($\norm{Cx}$) for cases when $d=0$ (e.g. constraints are homogenous)
    \end{itemize}
    
\end{frame}


\begin{frame}
	\frametitle{In-Equality Constrained Problems}
	\begin{itemize}
        \item Solve the problem
        {\footnotesize
            \begin{align*}
            \underset{x \in \Omega_E}{\text{minimize}}
            &
            \quad \frac{1}{2} x^{T} A x - b^{T} x
            \end{align*}}
        with $\Omega_E = \{x \in \mathbb{R}^n : h_i(x_i) \leq 0\}$
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{MPGP Algorithm}
    
    \begin{itemize}
        \item Iterative algorithm based upon 2 steps:
        \begin{itemize}
            \item Conjugate Gradient Update: $x^{k+1} = x^{k} - \alpha_{cg} p^{k+1}$
            \item Gradient Projection Update: $x^{k+1} = P_{\Omega_E}\left(x^k - \alpha g(x^k)\right)$
        \end{itemize}
        \item Choose next point using these rules:
        \begin{itemize}
            \item If $g^p(x^k) = 0$, set $x^{k+1} = x^{K}$
            \item If $x^{k}$ is not proportional, generate $x^{k+1}$ using gradient projection
            \item If $x^{k}$ is proportional, generate $x^{k+1}$ using conjugate gradient if resulting $x^{k+1}$ is feasible, otherwise use gradient projection
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{MPGP Algorithm}
    
    \begin{itemize}
        \item Constraints defining $\Omega_E$ define computational complexity of Gradient projection step
        \item Matrix-vector multiplications by Hessian grow at \textbf{O}(1)
        \item Other optimal algorithms exist (such as MPRGP)
        \item Performance improvement can be made by doing feasible half-steps or preconditioning of the problem
    \end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Equality \& In-Equality Constrained Problems}
	\begin{itemize}
        \item Solve the problem
        {\footnotesize
            \begin{align*}
            \underset{x \in \Omega_E}{\text{minimize}}
            &
            \quad \frac{1}{2} x^{T} A x - b^{T} x
            &
            \text{subject to}
            &
            \quad Cx = 0
            \end{align*}}
        
        with $\Omega_E = \{x \in \mathbb{R}^n : h_i(x_i) \leq 0\}$
        
        \item Use the Augmented Lagrangian for the equality constraints
        \begin{equation*}
        L(x, \mu^k, \rho_k) = \frac{1}{2} x^{T} A x - b^{T} x + (\mu^k)^T(Cx-d) + \frac{\rho_k}{2} \norm{Cx-d}^2_2
        \end{equation*}
        \item $\mu^k$ is the vector of Lagrangian multiplers, $\rho_k$ is the penalty parameter
        
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{SMALSE-M}

    Note: $\rho$, $\beta$ and $\eta$ are fixed at run-time
    
    \begin{enumerate}
        \item[Step 1] Find $x^k$ such that
        \begin{equation*}
        \norm{g^p(x^k, \mu^k, \rho)} \leq \min{\left\{M_k\norm{Cx}, \eta\right\}}
        \end{equation*}
        
        \item[Step 2] Update $\mu$
        \begin{equation*}
        \mu^{k+1} = \mu^k + \rho(Cx^k)
        \end{equation*}
        
        \item[Step 3] Update M
        if Lagrangian cost hasn't increased sufficiently:\\
        \begin{center}
            $ M_{k+1} = \beta M_k $ else $ M_{k+1} = M_k $
        \end{center}
        
        \item[Step 4] Goto step 1 and repeat until termination condition met
        
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{SMALSE-M}
    
    \vspace{0.5em}
    Growth of matrix-vector multiplications by the Hessian inside step 1 is \textbf{O}(1) against problem dimension
    \begin{figure}
        \includegraphics[scale=0.2]{Iterations_Dostal2009}
        \caption{Inner loop iterations (Dostal 2009)}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{SMALSE-M}
    \vspace{0.5em}
    R-linear convergence of feasibility error \& R-linear decrease in projected gradient
    
    \begin{figure}
        \includegraphics[scale=0.2]{Error_Dostal2009}
        \caption{Error decrease (Dostal 2009)}
    \end{figure}
\end{frame}
        
\begin{frame}
    \frametitle{SMALSE-M}
    \begin{itemize}
        \item Inner loop solver must be optimal solver for SMALSE-M to meet optimality guarantees
        \item Recent work by Dostal et. al. on adapting SMALSE-M to MPC problems
        \begin{itemize}
            \item Uses direct Cholesky solver for inner step instead of iterative conjugate gradient solver
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Objectives and Future Work}
    
    \textbf{Objective:} Examine the SMALSE-M algorithm's application to MPC problems
    \vspace{0.5em}
    
    Future Steps:
    \begin{itemize}
        \item Confirm convergence results numerically in MATLAB simulation
        \item Examine applicability of SMALSE-M to non-homogenous constraints
        \item Develop implementation of algorithm in C for use with ProtoIP
        \item Perform comparisons
        \begin{itemize}
            \item Against existing solvers
            \item Software versus Hardware architectures
        \end{itemize}
    \end{itemize}
\end{frame}
 
\end{document}

