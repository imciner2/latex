\documentclass[presentation]{beamer}
%\documentclass[handout]{beamer}

\usecolortheme{Imperial}
 
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{xfrac}

\usepackage{tikz}
\usepackage{pgf}
\usepackage{pgfplots}
\pgfplotsset{compat=1.13}
\usepgfplotslibrary{groupplots}


% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

%% Create some mathematical operations/delimiters
% To get an auto-sizing delimiter, use * after the name (e.g. \norm*{})
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
\DeclarePairedDelimiter{\Htwonorm}{\lVert}{\rVert_{H_2}}
\DeclarePairedDelimiter{\Hinfnorm}{\lVert}{\rVert_{H_\infty}}
\DeclarePairedDelimiter{\Fronorm}{\lVert}{\rVert_{F}}
\DeclarePairedDelimiter{\Twonorm}{\lVert}{\rVert_{2}}
\DeclarePairedDelimiter{\Onenorm}{\lVert}{\rVert_{1}}
\DeclarePairedDelimiter{\Infnorm}{\lVert}{\rVert_{\infty}}
\DeclarePairedDelimiter{\Maxnorm}{\lVert}{\rVert_{\text{max}}}
\DeclarePairedDelimiter{\inv}{(}{)^{-1}}

%% Custom mathematical operations
\newcommand{\invnb}[1]{#1^{-1}}
\newcommand{\sqrtm}[1]{#1^{\sfrac{1}{2}}}
\newcommand{\trans}[1]{#1'}
\newcommand{\ctrans}[1]{#1^{*}}
\newcommand{\trace}[1]{\text{Tr}\left(#1\right)}
\newcommand{\Linf}{\mathcal{L}^{\infty}}
\newcommand{\Cpi}{\mathcal{\tilde{C}}_{2\pi}}

% complying UK date format, i.e. 1 January 2001
\usepackage{datetime}
\let\dateUKenglish\relax
\newdateformat{dateUKenglish}{\THEDAY~\monthname[\THEMONTH] \THEYEAR}

% Imperial College Logo, not to be changed!
\institute{\includegraphics[height=0.7cm]{Imperial_1_Pantone_solid.eps}}

% Add the slide number to the footer
\addtobeamertemplate{footline}{}{%
\usebeamerfont{footline}%
\usebeamercolor{footline}%
\hfill\raisebox{5pt}[0pt][0pt]{\insertframenumber\quad}}

\AtBeginSubsection[]{
    \frame<beamer>{   
    \tableofcontents[currentsection,currentsubsection,sectionstyle=show/hide,subsectionstyle=show/shaded/hide,subsubsectionstyle=show/show/shaded/shaded]}
}

% -----------------------------------------------------------------------------

%Information to be included in the title page:
\title{Analyzing Linear MPC Algorithms}
\subtitle{Computational complexity bounds and finite precision arithmetic using Toeplitz operators}
\author{Ian McInerney}
\date{March 26, 2019}

\begin{document}
 
\frame{\titlepage}

\frame{\frametitle{Outline} \tableofcontents}

\section{Overview}

\subsection{MPC}

\begin{frame}
    \frametitle{Model Predictive Control - Overview}
    
    \begin{block}{}
        \begin{itemize}
            \item Predict the future \\
            \item Optimize control input \\
            \item Constrain the system
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{MPC: Optimization Problem}
    
    \begin{block}{CLQR Problem}
        \begin{align*}
        \underset{\bar{u}, \bar{x}}{\text{min}} \quad & \norm{x_N}^2_P + \sum_{k=0}^{N-1} \norm{x_k}^2_Q + \norm{u_k}^2_R \\
        \text{s.t.} \quad & x_{k+1} = A x_k + B u_k & k=0, \dots N-1 \\
        & E u_k \leq c_{u}  & k=0, \dots N-1 \\
        & D x_k \leq c_{x}  & k=1, \dots N 
        \end{align*}
    \end{block}
    
    \begin{itemize}
        \item If $P,Q, R \succeq 0$, this is a convex Quadratic Program (QP)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{MPC: Optimization Problem}
    
    \begin{block}{Condensed Form}
        Remove the states to ``condense'' the problem to only the inputs.
        \begin{align*}
            \underset{u}{\text{min}}\   & \frac{1}{2} \trans{u} H_c u + \trans{\hat{x}}_0 \trans{J} u\\
            \text{s.t.\ }  & G u \leq F \hat{x_0} + g
        \end{align*}
    \end{block}

    \begin{block}{Dual Form}
        Convert problem to the dual domain to more easily handle state constraints,
        with $H_d \coloneqq G H_c^{-1} \trans{G}$ and 
        $J_d \coloneqq G H_c^{-1} J + F $.
        \begin{align*}
            \underset{y}{\text{min}}\   & \frac{1}{2} y^T H_d y + (J_d \hat{x}_0 + g)^{T} y\\
            \text{s.t.\ }  & y \geq 0
        \end{align*}
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{First-Order Solution Methods}
    
    \begin{minipage}{0.48\textwidth}
        \scalebox{0.8}{
            \hspace*{-3em}
            \includegraphics[scale=0.4]{firstOrderSchematic.png}
        }
    \end{minipage}
    \begin{minipage}{0.45\textwidth}
        
        \begin{tikzpicture}[remember picture,
        outer/.style={align=center,rounded corners,minimum width=13em,minimum height=13em,draw=blue,thick},
        inner/.style={minimum width=1em,minimum height=2em,draw,rounded corners}
        ]
        \node[outer,name=alg]
        {   
            \begin{tikzpicture}
            \node[inner,name=direc]  {\footnotesize Compute Direction};
            
            \node[inner,name=proj] at ($(direc.south)-(-2.0,0.8)$) {\footnotesize Project into set}
            edge[<-, bend right=30, thick] (direc.east);
            
            \node[inner,name=accel] at ($(proj.south)-(0.5,2)$) {\footnotesize Accelerate}
            edge[<-, bend right=25, thick] (proj.south);
            
            \path (accel.west) edge[->, bend left=45, thick] (direc.west);  
            \end{tikzpicture}
        };
        \end{tikzpicture}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Fast Gradient Method}
    \centering
    \includegraphics[width=0.75\framewidth]{FGM_Algorithm.png}
\end{frame}


\subsection{Toeplitz Operators}

\begin{frame}
	\frametitle{Toeplitz Operators}

    \begin{block}{Overview}
        Toeplitz operators are infinite dimensional linear operators that are found in many applications including: PDEs, signal processing, and control theory
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Toeplitz Operator: Matrix Representation}
    
    \begin{block}{}
        The matrix representation has constant values down every diagonal. 
    \end{block}
    
    \begin{equation*}
    T = \begin{bmatrix}
    F_{0} & F_{1} & F_{2} & F_{3} & \cdots\\
    F_{-1} & F_{0} & F_{1} & F_{2} & \cdots\\
    F_{-2} & F_{-1} & F_{0} & F_{1} & \cdots \\
    F_{-3} & F_{-2} & F_{-1} & F_{0} & \cdots \\
    \vdots & \vdots & \vdots & \vdots & \ddots
    \end{bmatrix}
    \end{equation*}
    
%    \begin{block}{Banded Matrices}
%        If there is an integer $n$ such that $F_i = 0 \quad \forall i > n$, then the matrix is called a banded matrix.
%    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Toeplitz Operator: Matrix Representation}

    \begin{block}{Matrix Symbol}
        \begin{itemize}
            \item Toeplitz matrices are associated with a matrix symbol (or generating function) $f(x)$.
            \item The values on the diagonals of the matrix are the Fourier coefficients of the generating function.
        \end{itemize}
    \end{block}
    
    \begin{equation*}
    T = \begin{bmatrix}
    F_{0} & F_{1} & F_{2} & F_{3} & \cdots\\
    F_{-1} & F_{0} & F_{1} & F_{2} & \cdots\\
    F_{-2} & F_{-1} & F_{0} & F_{1} & \cdots \\
    F_{-3} & F_{-2} & F_{-1} & F_{0} & \cdots \\
    \vdots & \vdots & \vdots & \vdots & \ddots
    \end{bmatrix}
    \iff
    F_i = \frac{1}{2\pi}\int_{-\pi}^{\pi} \mathcal{F}(z) e^{-i j\omega} dz
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Spectral Properties}
    
    \begin{block}{}
        The spectrum of the Toeplitz matrix $T$ can be estimated and bounded using the spectrum of its matrix symbol $\mathcal{F}(z)$.
    \end{block}

    \begin{block}{Eigenvalue Bounds}
        \begin{equation*}
            \min \lambda(\mathcal{F}(z)) \leq \lambda(T) \leq \max \lambda(\mathcal{F}(z)) \qquad \forall \{ z \in \mathbb{C} : \abs{z} = 1\}
        \end{equation*}
    \end{block}

\end{frame}

%\begin{frame}
%    \frametitle{Spectral Properties}
%    
%    \begin{block}{Eigenvalue Estimate}
%        As the size of the matrix goes to infinity, the eigenvalues of $T$ are asymptotically equal to those of the Circulant matrix which is also generated by $f(x)$.
%    \end{block}
%
%    \begin{block}{Circulant Matrix Eigenvalues}
%        The eigenvalues of a circulant matrix can be found using
%        \begin{equation*}
%            \lambda(C) = \bigcup_{\omega \in S} \lambda( f(e^{j\omega}) ) 
%        \end{equation*}
%        with
%        \begin{equation*}
%            S = \left\{ s: s=-\frac{\pi}{2} + \frac{2\pi}{N}i \quad \forall i=1,...,N-1 \right\}
%        \end{equation*}
%    \end{block}
%\end{frame}

\section{Analysis Using Toplitz Operators}

\subsection{Relation between transfer functions and MPC matrices}

\begin{frame}
    \frametitle{Dense Prediction Matrix}

    \begin{equation*}
    \label{eq:dense:predMatrix}
    \Gamma = \begin{bmatrix}
    B & 0 & 0 & & 0\\
    AB & B & 0 & & 0\\
    A^2b & AB & B & & 0 \\
    \vdots & & & \ddots & \vdots\\
    A^{N-1}B & A^{N-2}B & A^{N-3}B & \cdots & B
    \end{bmatrix}
    \end{equation*}

    \begin{block}{}
        This is Toeplitz with the matrix symbol
        \begin{equation*}
            \mathcal{P}_{\Gamma}(z) = \sum_{i=0}^{\infty} A^iBz^{-i} = z(zI - A)^{-1}B = z \mathcal{G}_{s}(z)
            \qquad
            \forall z \in \mathbb{T}
        \end{equation*} 
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Dense Constraint Matrix}
    
    \begin{block}{}
        Form G using constraint matrices and prediction matrix:
        \begin{equation*}
        G \coloneqq \bar{D} \Gamma + \bar{E}
        \end{equation*}
    \end{block}
    
    \begin{block}{}
        This is Toeplitz with the matrix symbol
        \begin{equation*}
            \mathcal{P}_{G}(z) \coloneqq \begin{bmatrix}
            D \mathcal{P}_{\Gamma}(z)\\
            E \\
            \end{bmatrix}
            \qquad
            \forall z \in \mathbb{T}
        \end{equation*}
    \end{block}
\end{frame}


\begin{frame}
    \frametitle{Condensed Hessian Matrix}
    
    \begin{block}{Topelitx Structure}
        The Hessian matrix: $ H_{cQ} = \Gamma^{*} Q \Gamma + R $
        is also a Toeplitz matrix with the matrix symbol
        \begin{equation*}
            \mathcal{P}_{H_{cQ}}(z) \coloneqq \ctrans{\mathcal{P}_{\Gamma}(z)} Q \mathcal{P}_{\Gamma}(z) + R
        \end{equation*}    
    \end{block}


    \begin{block}{Spectral Bound}
        Bound the eigenvalues of the matrix (and the condition number) using the eigenvalues of the symbol
        
        \begin{equation*}
        \lambda_{min}( \mathcal{P}_{H_{cQ}} ) \leq  \lambda(H_{cQ}) \leq \lambda_{max}( \mathcal{P}_{H_{cQ}})
        \end{equation*}
    \end{block}
    
\end{frame}


\begin{frame}
    \frametitle{Condensed Hessian Matrix}
    
    \centering
    \includegraphics[width=\framewidth]{HessianSpectrum.png}
    
\end{frame}

\begin{frame}
    \frametitle{Dual Hessian Matrix}
    
    \begin{block}{Toeplitz Structure}
        The matrix $H_{d}$ is similar to the matrix $H_{d1} \coloneqq H_{c}^{-1} \trans{G} G$.
        
        Matrix $H_{d1}$ is Toeplitz with symbol:
        \begin{equation*}
            \mathcal{P}_{H_{d1}}(z) \coloneqq ( \mathcal{P}_{H_{cQ}}(z))^{-1} \ctrans{\mathcal{P}_{G}(z)} \mathcal{P}_{G}(z)
        \end{equation*}
    \end{block}


    \begin{block}{Spectral Bound}
        \begin{equation*}
            \lambda_{max}(H_{d}) \leq \lambda_{max}(\mathcal{P}_{H_{d1}}) \leq \Hinfnorm{\mathcal{P}_{H_{d1}}}
        \end{equation*}
     \end{block}
\end{frame}

\begin{frame}
    \frametitle{Dual Hessian Matrix}
    
    \centering
    \includegraphics[width=0.75\framewidth]{DualHessianMaxEigs.png}
\end{frame}

\subsection{Cost Function Scaling}
 
\begin{frame}
    \frametitle{Cost Function Scaling}
    \begin{block}{}
        Examine how the problem's computational complexity changes when the cost function is scaled using
         $\hat{Q} \coloneqq \alpha_1 Q$, $\hat{R} \coloneqq \alpha_2 R$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Condition Number Bound}
    
    \begin{block}{}
        \begin{equation*}
            \kappa(\hat{H}_c) \geq 1 + 2 \frac{\sqrt{ \alpha_1^2 n_1 + 2\alpha_1 \alpha_2 n_2 + \alpha_2^2 n_3}}{\alpha_1 \Htwonorm{\mathcal{G}_{Q}}^2 + \alpha_2 \Fronorm{\sqrtm{R}}^{2}},
        \end{equation*}
        with
        \begin{align*}
            n_1 &\coloneqq m I_{6} - \Htwonorm{\mathcal{G}_{Q}}^4, \\
            n_2 &\coloneqq m \Htwonorm{\mathcal{G}_{QR}}^2 - \Htwonorm{\mathcal{G}_{Q}}^2 \Fronorm{R^{\sfrac{1}{2}}}^{2}, \\
            n_3 &\coloneqq m \Fronorm{R}^{2} - \Fronorm{\sqrtm{R}}^{4}
        \end{align*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{}
    \centering
    \includegraphics[width=0.55\framewidth]{ConditionScaling.png}
\end{frame}

\begin{frame}
    \frametitle{FGM Scaling}
    
    \centering
    
    \includegraphics[width=0.6\textwidth]{FGMScaling.png}
\end{frame}

\begin{frame}
    \frametitle{DGP Scaling}

    \centering
    \includegraphics[width=0.55\textwidth]{DGP_Scaling1.png}
    \includegraphics[width=0.55\textwidth]{DGP_Scaling2.png}
\end{frame}

 
\subsection{Preconditioning}

\begin{frame}
    \frametitle{Preconditioning}
    
    \begin{block}{Extending Prior Analysis}
        The prior results are also valid when symmetric (left-right) preconditioners are used to form
        $$ H_{pre} = L^{-1} H_{c} \trans{(L^{-1})}$$
    \end{block}

    \begin{block}{Preconditioner Design}
        Toeplitz methods can be used to design the preconditioner $L$, where $L$ is the lower-triangular Cholesky-decomposition of
        \begin{equation*}
            M \coloneqq \trans{B} P B + \trans{S} B + \trans{B} S + R.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{}
    \centering
    \includegraphics[width=0.6\framewidth]{PreconditionerScaling.png}
\end{frame}


\section{Analyzing FGM in Fixed-Point Arithmetic}

\subsection{Why is this needed?}

\begin{frame}
    \frametitle{Motivation}
    
    \begin{block}{Stability}
        A necessary condition for the stability of FGM is that all the eigenvalues of the fixed-point Hessian matrix $\hat{H}$ are inside $(0,1)$.
    \end{block}
    
    \begin{block}{}
        The conversion into fixed-point does not preserve the spectrum of the Hessian.
    \end{block}

    \includegraphics[width=0.5\framewidth]{spectrum_maxEig.eps}
    \includegraphics[width=0.5\framewidth]{spectrum_minEig.eps}
\end{frame}

\subsection{Round-off Error Models}

\begin{frame}
    \frametitle{Generic Round-off Error Model}
    
    \begin{block}{}
        Assume every element of the matrix $H$ is subjected to the maximum round-off error possible, then the matrix after worst-case round-off error can be expressed as $\hat{H} = H_c + E_{g}$ with
        \begin{equation*}
        E_{g} \coloneqq \begin{bmatrix}
        \pm\epsilon_{f} & \pm\epsilon_{f} & \dotsc \\
        \pm\epsilon_{f} & \pm\epsilon_{f} & \dotsc \\
        \vdots          & \vdots          & \ddots
        \end{bmatrix}.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Parametric Round-off Error Model}
    
    \begin{block}{General Idea}
        Exploit the fact that $H_{c}$ will become banded in fixed-point notation since eventually the numbers in each block will be smaller than the lowest representable value.
    \end{block}

    \begin{block}{Method}
        Decompose the round-off error into 2 components:
        $$E_{p} \coloneqq E_{G} + E_{T}$$
        
        $E_{G}$ is the generic round-off from the banded portion.
        
        $E_{T}$ are the diagonal components that are rounded to $0$.
    \end{block}
        
\end{frame}

\subsection{Computing Fractional Length Requirement}

\begin{frame}
    \frametitle{Rounding Stability Margin}
    
    \begin{block}{Definition}
        Let $\hat{H} = H + E$ with $\Twonorm{E} = \beta$. The rounding stability margin is the smallest value of $\beta$ that causes the eigenvalues of $\hat{H}$ to leave the interval $(0,1)$.
    \end{block}

    \begin{block}{Computation}
        The rounding stability margin can be found using the matrix pseudospectrum:
        \begin{equation*}
            \eta(H) = \min\left\{ \invnb{\Twonorm{\inv{-H}}} , \invnb{\Twonorm{\inv{I - H}}} \right\},
        \end{equation*}
        or
        \begin{equation*}
            \eta(H) = \min\left\{ \invnb{\Hinfnorm*{ \inv{-\mathcal{P}_{H}}}}, \invnb{\Hinfnorm*{\inv{I_{m} - \mathcal{P}_{H}}}} \right\}.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Length Using Generic Model}

    \begin{block}{}
        Find closed-form expression for the length
        \begin{equation*}
        f = \begin{cases}
        \ceil*{ -\log_{2} \left( \frac{\eta}{mN}\right) } - 1 & \text{if using round to nearest}, \\
        \ceil*{ -\log_{2} \left( \frac{\eta}{mN}\right) } & \text{otherwise}.
        \end{cases}
        \end{equation*}
    \end{block}    
\end{frame}

\begin{frame}
    \frametitle{Length Using Generic Model}
    
    \centering
    \includegraphics[width=0.7\framewidth]{generic_FractionalHorizon.eps}
\end{frame}

\begin{frame}
    \frametitle{Length Using Parametric Model}

    \begin{block}{}
        Compute the needed fractional bits by finding $f$ which satisfies the inequality:
        \begin{equation*}
            \abs{\epsilon_{f}}m(2k-1) + 2\Hinfnorm{\mathcal{P}_{\bar{H}}(k, \cdot)} < \eta,
        \end{equation*}
        where
    \begin{align*}
    \mathcal{P}_{\bar{H}}(n, z) &\coloneqq z \mathcal{G}_{P}(z) - \trans{B} P \mathcal{P}_{n}(z) B \quad \forall z \in \mathbb{T},\\
    \mathcal{G}_{P} &\coloneqq \left\{
    \begin{array}{l}
    x^{+} = Ax + Bu\\
    y = \trans{B} P x
    \end{array}
    \right.,\\
    \mathcal{P}_{n}(z) &\coloneqq \sum_{i=0}^{n-1} A^{i} z^{-i} \quad \forall z \in \mathbb{T}.
    \end{align*}
    \end{block}
\end{frame}

\subsection{Effect of Bit Length}

\begin{frame}
    \frametitle{Scaling}
    \centering
    \includegraphics[width=\framewidth]{ScalingBits.png}
\end{frame}

\begin{frame}
\frametitle{Scaling}
\centering
\includegraphics[width=0.75\framewidth]{scaling_performance.eps}
\end{frame}

\begin{frame}
    \frametitle{Hardware Cost}
    \centering
    \includegraphics[width=0.75\framewidth]{ProtoIP_Table.png}
\end{frame}
\end{document}

