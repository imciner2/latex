%!TEX root = ../LitSurvey.tex

\section{Algorithm Selection for FPGA Implementation}

\subsection{Convergence Speed}

One of the most important questions when implementing real-time MPC is: will the optimization algorithm provide a solution within the desired time frame? At worst, this time frame should be the desired sample time of the system, but ideally it should converge to a solution quickly after obtaining a current state measurement (to minimize the computational delay). FPGA implementations are useful for determining the computational time required because once a circuit is designed, the latency of the computation is fixed, and therefore the computational time required is known at design-time.

\subsubsection{Interior-Point Methods}

Interior-point methods are a well-known polynomial-time algorithm for solving many different types of constrained optimization problems. This polynomial-time property guarantees that the algorithm is able to converge in a finite number of iterations, however the theoretical upper bound for the iteration count is very large, on the order of 8,000-10,000 for most problems \cite[\S 11.5]{Boyd2004}. In practice though, it has been observed that the number of iterations required is on the order of 10s, not 1000s, for MPC problems \cite{Wang2008}. Other experiments conducted in \cite{Lau2009} show that the number of iterations required for convergence is also not correlated with the number of decision variables or number of constraints in a problem.

When implementing interior-point in real-time, some designers choose to run the algorithm for a fixed number of iterations instead of checking a terminiation criteria. Making the algorithm run to a fixed iteration on FPGA-based designs is advantageous because it allows for the computation latency to be calculated at design-time, and will also guarantee that a solution is available when needed. A sampling of some iteration limits used in the literature is provided in table \ref{tab:fpga:alg:comp:ip}, and an examination of how the solver parameters (e.g.\ completion time, errors, control result) change as the number of interior-point iterations varies can be found in \cite{Hartley2013}.

\begin{table}[h!]
    \caption{Maximum iterations used for the interior-point solver}
    \label{tab:fpga:alg:comp:ip}
    \centering
    \begin{tabular}{cl}
        Design & Iterations \\
        \hline
        \cite{Wills2011} & 8 \\
        \cite{Jerez2011} & 14 \\
        \cite{Jerez2012} & 15 \\
        \cite{Hartley2014} & 18 \\
    \end{tabular}
\end{table}


\subsubsection{Active-Set Methods}

Active-set methods are designed to seek out the active inequality set at the optimal point (e.g.\ the inequalities which become equalities at the optimal point). To do this, the algorithm will add or remove constraints from the KKT matrix at each iteration, so each iteration of the algorithm is searching for the optimum with a different constraint set. For MPC problems, the number of constraints are finite, meaning there are a finite number of combinations of constraints. This, along with some other minor assumptions, help to show that the active-set method can converge in a finite-number of iterations for a convex QP \cite[\S 16.5]{Nocedal2006}.

The issue with the computational complexity of active-set methods is that the number of possible active set combinations grows exponentially with the number of constraints. For example, a problem with 52 inequality constraints (similar to what was solved using interior-point in \cite{Ling2008}), would have upwards of 4.5$\times 10^{15}$ possible active sets. In reality, the active-set methods do not search every combination of constraints, and instead smartly update the active set at each iteration to drastically reduce the number of iterations required. The scalability of an active-set method was examined in \cite{Lau2009}, where it was shown that the number of iterations required for convergence grows roughly linearly with the number of decision variables.

The active-set method used in \cite{Yang2012} is designed to perform one constraint addition/removal per iteration. This update scheme (known as a rank-one update) is very efficient when the active-set is not changing very quickly and the problem is warm-started using a previous active set. However, it has been shown in \cite{Herceg2015} that when a reference-change occurs in an MPC problem, active-set methods based on the rank-one update will be very inefficient, taking a larger number of iterations when the change occurs. Instead, they suggest using algorithms that can handle multiple updates in one iteration, such as the Dual Gradient Projection Method (DGP method). For the example in \cite{Herceg2015} where MPC is applied to a diesel engine, the DGP method performs upwards of 10 constraint changes in one iteration after the reference change, while the basic methods will only perform one change per iteration.

\subsubsection{First-Order Methods}

First-order methods have seen an increase in attention in the real-time MPC literature recently, with some results showing that FPGA implementations of them can reach sampling rates of ${>}1$ MHz \cite{Jerez2014}. This increase was due to results by Richter et.\ al.\ in \cite{Richter2011, Richter2012} which showed that for Nesterov's Fast Gradient Method, a lower limit on the number of iterations required for convergence to a specified tolerance can be computed at design-time. The limit proposed in \cite[Theorem 2]{Richter2012} is not necessarily a tight bound, but it has been shown to produce reasonable estimates when used in practice (e.g.\ on the order of 10s of iterations) \cite{Richter2012_thesis}.

\subsection{Operations Required}


\begin{table}[h!]
    \centering
    \caption{Per-iteration computational complexity of various algorithms. $N$ is the horizon length, $n$ is the number of states, and $m$ is the number of inputs.}
    \label{tab:alg:comp}
    \begin{tabular}{lcc}
        Algorithm & Complexity &\\
        \hline
        IP (general solver) & $O(N^3 (n+m)^3)$ & \cite{Rao1998}\\
        IP (specialized Cholesky) & $O(N (n+m)^3)$ & \cite{Rao1998}\\
        IP (MINRES) & $O(N^2(n+m)^3)$ & \cite{Jerez2012} \\
        FGM (Box Constraints) & $O((Nm)^2 + Nmn)$ & \cite{Richter2012}
    \end{tabular}
\end{table}

\subsubsection{Interior-Point Methods}

Early work on implementing real-time MPC used the interior-point methods presented in \cite{Wright1997, Rao1998, Wang2008}. These methods were composed of the outer-loop of Newton's steps to solve the non-linear KKT system, with the step direction being solved for using a direct method (e.g.\ Cholesky factorization). For an interior-point method with a general inner solver (e.g.\ no specialization to MPC), the operations required at each iteration is $O(N^3 (n+m)^3)$ (where $N$ is the horizon length, $n$ is the number of states, and $m$ is the number of inputs). However, using structural data from the MPC problem to specialize the inner Cholesky factorization, described in \cite{Wang2008}, can lead to $O(N (n+m)^3)$ operations per step. This allows the algorithm to have linear scaling with the horizon length.

Other work has examined the implementation of interior-point methods with an iterative inner solver (such as the MINRES solver). In this work, it was shown that the operations required in each iteration is $O(N^2(n+m)^3)$. In this case, the iterative solver scales quadratically with the horizon length versus the linear scaling seen with the direct solver.

While the majority of the operations of the interior-point method are located in the linear system solver, there is a fixed cost associated with setting up the linear system and then applying the search direction. This cost was examined for the primal-dual interior-point framework in \cite{Lau2009}, and it was shown that the fixed cost of this algorithm was higher than the fixed cost of an active-set method. Additionally, the fixed cost of the algorithm is dominant when the problem size (number of variables and number of constraints) is small, but as the problem size increases the linear solver operations become dominant.

\subsubsection{First-Order Methods}

First-order methods are simpler than the interior-point and active-set methods in the sense that they do not generally require the solution of a linear system as part of the algorithm. For unconstrained optimization, the main computational parts of the method are matrix-vector multiplications to compute a new search direction and the step-size, for instance the conjugate gradient method can be implemented using a single matrix-vector multiplication per iteration, along with additional scalar multiplications.

When first-order methods are modified to include inequality constraints in the computation, the matrix-vector multiplication may no longer be the dominant computation, a projection operation may be. Previous work in real-time MPC has explored using first-order methods with problems that have box constraints \cite{Richter2012, Jerez2014}, since projecting onto box constraints is just a saturation operation. For example, the computational complexity for fast gradient method applied to the dense form with box constraints is $O((Nm)^2 + Nmn)$ \cite{Richter2012}. Once the constraint set becomes more complex, the projection operation may require the solution of a constrained QP itself, increasing the computational effort required in each iteration.

\subsection{Overall Comparison}

A numerical comparison between the interior-point method and active-set method was conducted by Bartlett et.\ al. in \cite{Bartlett2000} and a more detailed comparison using an FPGA implementation of each algorithm was conducted by Lau et.\ al.\ in \cite{Lau2009}. In the results, they determined that the interior-point methods have a higher fixed cost in each iteration (which is dominated by the creation of the linear system), which is dominant for small problem sizes. As the problem size increases, the fixed cost is dwarfed by the linear system solver. Additionally, it was observed that the number of interior-point iterations needed for convergence remained relatively constant while the number of iterations for the active-set methods grew as the problem size increased. Concluding from this, the active-set method appears to be better for small problem sizes (where the interior-point method is dominated by the fixed cost), but as the problem size increases the interior-point method becomes the better method.

In terms of the memory usage of the algorithms, the interior-point method requires a constant memory footprint, while the active-set method has a changing memory footprint (at least in the survey's implementation). This is due to the fact that the active set is constantly changing in size between iterations, while for interior-point methods the system is a constant size. This may not be problematic though, since it is highly implementation dependent and there could be other ways of implementing the active-set method's linear system that have a constant memory footprint.

In terms of computational complexity, the Fast Gradient Method has a better theoretical lower-bound than the interior-point methods, which is then better than the active-set method. In practice though, the interior-point method exhibits a constant iteration count for convergence that is orders of magnitude better than its theoretical bound. In this respect, it may be difficult to choose between interior-point methods and the FGM.
