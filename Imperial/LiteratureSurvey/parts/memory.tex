%!TEX root = ../LitSurvey.tex
\section{Memory Architectures}

\subsection{Memory Type}

For FPGA implementations of algorithms, there are three main levels of memory available for storage of coefficients and matrices:
\begin{itemize}
    \item Distributed RAM - Lowest level of locality (right at the computation). Composed of the flip-flops inside the LUTs
    \item Block RAM (BRAM) - Dedicated memory banks located inside the FPGA at various places.
    \item Offchip RAM - Memory chips located separate from the FPGA chip
\end{itemize}
Each level of memory has its advantages and disadvantages. For example, the Distributed RAM has very low latency since it is right next to the computational logic (it is located inside the LUTs), but it has very limited storage capacity and also can block LUTs from being used in computations depending on the chip-level implementation of the flip-flops. The Block RAM is still located on-chip, so it can be accessed via the routing fabric, and also the individual BRAMS can be accessed in parallel, so there is no bus contention to affect throughput. The BRAMs do have a limited memory capacity though, but it is larger than the Distributed RAM capacity and doesn't use up the LUT resources for storage. The final level, offchip RAM, has the largest storage capacity, but it also has a larger latency since it must be accessed through a memory controller, and goes off-chip. Additionally, accesses to the RAM must be queued, so if the entire matrix is stored in an offchip RAM, accessing multiple coefficients becomes a sequential operation.

The choice of which RAM level to use depends heavily on the algorithm implementation, and also the problem size. For example, the Dual Gradient Projection algorithm from \cite{Rubagotti2016} utilizes Distributed RAM for storage of the coefficient matrix. This allows the matrix to be located near the computational elements to reduce latency, but it does limit the problem size. An example of the tradeoff between using the Distributed RAM and the BRAM can be found in \cite{Wills2012}, where the architecture was synthesized using both memory architectures. In that work, the resource usage approximately doubled when the Distributed RAM was used instead of the internal BRAM.

\subsection{Compressed Diagonal Storage}

Structure in the matrix can be exploited to reduce the storage requirements. The main structural advantage that has been exploited exists in the sparse QP formulation (which is used for the interior-point method from \cite{Jerez2012}), where the matrix being created is banded with a fixed structure (as shown in Figure \ref{fig:mem:cds}, left plot). This matrix is square-symmetric, with dimension $T \times T$ where $T=N(2n + m) + 2n$. Additionally, it has a half-bandwidth (e.g. distance from diagonal to band edge) of $M=2n+m$.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{images/CDS_Jerez2012.png}
    \caption{Matrix structure of the linear system being solved for the Interior-Point method in \cite{Jerez2012} (graphic from \cite{Jerez2012}). The left figure shows the original matrix structure, while the right matrix shows the structure when stored using symmetric Compressed Diagonal Storage. Variable elements (for the interior-point iteration) are black, constants are dark grey, ones are light grey, and zeros are white.}
    \label{fig:mem:cds}
\end{figure}

This matrix could be stored using a sparse matrix format (storing the indices with the coefficient), and then be used with sparse matrix operations. That method however increases the complexity of the matrix operations because the matrix elements now must be matched with the vector elements before the computation runs \cite{Boland2010}. Instead, an alternative storage method, called Compressed Diagonal Storage (CDS), can be used.

In symmetric-CDS, the symmetric $T \times T$ matrix is instead stored as a $T \times M$ matrix, where the columns of the CDS matrix correspond to a band of the matrix (so column 1 is the diagonal, 2 is the 1st band, etc.). This storage method is shown in the right plot in Figure \ref{fig:mem:cds}. This changes the memory required to store the coefficient matrix from $T^3$ to $TM$ \cite{Jerez2012, Boland2010}.\newline

\noindent\textbf{Exploiting MPC Structure}

Additional gains of up to 75\% can be achieved by further exploiting the structure of the MPC matrix inside the symmetric-CDS formulation \cite{Jerez2012}. Specifically, the following items can be exploited to reduce the storage requirement
\begin{itemize}
    \item Similar rows - The MPC matrix in symmetric CDS form has rows that contain zeros and ones in the same location. This can be exploited by storing a single row in memory, then designing an address decoder to remap the addresses to that row.
    \item Constant Blocks - The MPC matrix columns contain repeated blocks of size $2n+m$. Instead only store one block and implement address generation logic to read the block when needed.
\end{itemize}
A comparison of the memory requirements when the storage is implemented using the original dense method, the normal symmetric-CDS, and the MPC-specific CDS method can be seen in Figure \ref{fig:mem:comp}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{images/Memory_Jerez2012.png}
    \caption{Memory requirements for the coefficient matrix of a sample MPC problem (graphic from \cite{Jerez2012}). The original matrix (dense storage) are the circles, with the improved memory capacity of the MPC-specific implementation given by the triangles.}
    \label{fig:mem:comp}
\end{figure}
