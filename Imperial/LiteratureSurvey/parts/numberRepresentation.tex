\section{Number Representation in MPC}

In this section, an overview of the relevant work into determining the appropriate representation of the numbers inside the calculations is presented. First, Section \ref{sec:num:overview} provides an overview of three different number representations in embedded systems. Then Section \ref{sec:num:arbFloat} summarizes work that reduces numerical errors in an arbitrary-precision floating-point implementation of MPC. Section \ref{sec:num:fix} summarizes work on analyzing the implementation of interior-point and fast-gradient algorithms in fixed-point representation, and finally Section \ref{sec:num:lns} summarizes work applying the logarithmic number system to MPC.

\subsection{Number Systems}
\label{sec:num:overview}


\subsubsection{Floating-Point}

Floating-point representation is probably the best known number representation for computers due to its wide adoption by desktop architectures. The storage behind a floating-point number is split into three parts: a sign bit, a mantissa $m$ and an exponent $e$, which represent the number $y$ (in base $\beta$) through the equation $y = \pm m \times \beta^{e-t}$. The most common floating-point numbers are defined by the IEEE 754 standard, which outlines two main sizes in base 2: single precision and double precision. Single precision numbers are 32-bits total, with $m=23$, $e=8$, and double precision numbers are 64-bits total with $m=52$ and $e=11$ \cite{Higham2002}. The dynamic range and the accuracy of the floating-point number are coupled through the exponent term. A number that needs a larger exponent for representation will have less decimal places than a number with a smaller exponent. This leads to a non-uniform spacing between the represented numbers; for examples of this see \cite[\S 2.1]{Higham2002}

Other representations can be created by hardware designers if desired, and for custom implementations that do not have access to a floating-point computation unit already in hardware, it may be desired to change the mantissa width since the hardware required for computations scales quadratically with the mantissa \cite{Constantinides2009}.

In floating-point arithmetic, the operations of multiplication and division are the simple operations, since they can operate directly with the mantissa and exponent pairs. The operations of addition and subtraction have increased complexity though, since in order to perform them the two numbers must have the same exponent. This leads to a normalization step in the operation before actually doing the operation.

\subsubsection{Fixed-Point}

The fixed-point number system is a representation that utilizes an implicit binary point in the storage of the fractional number inside a 2's compliment integer. This breaks the number into two distinct parts: the integer part and the fractional part. When doing computations with fixed-point numbers, the binary points need to line-up, which can be accomplished through simple shift operations. Once the binary points are aligned, the operations are the same as on 2's compliment integers.

Another benefit to fixed-point numbers is that the dynamic range and precision are decoupled. The dynamic range of the number is affected by the number of integer bits and the precision is affected by the number of fractional bits. This means that when designing the representation, the two can be examined separately. To determine the number of integer bits required, the designer needs to find the largest possible number that could occur. To choose the number of fractional bits, the designer needs to examine the allowable round-off error in the computation.

\subsubsection{Logarithmic Number System}

The logarithmic number system is an implementation of the fixed-point representation, where the number stored in memory is actually the logarithm of the desired number (e.g.\ $x$ is stored in memory representing $y$ through $x = log(\abs{y})$ and a sign bit). It was first proposed in \cite{Swartzlander1975} to improve the computation of multiplication, division and exponents in a fixed-point number system. The idea is that the multiplication and division operations become addition and subtraction in the log-domain, which is a simpler operation to perform than fixed-point multiplication or division. Additionally, the power operation becomes a multiplication in the log-domain, and squaring becomes a simple shift operation. The downside to the logarithmic number system is that the addition and subtraction operations become more difficult.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delta-domain formulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Floating-Point Implementation}
\label{sec:num:arbFloat}

The MPC formulation presented in Section \ref{sec:mpc} uses a discrete-time model of the dynamical system to predict the state evolution over the prediction horizon. For systems which have continuous-time dynamics, the continuous-time model must be discretized in order to be used in the MPC formulation. The most common discretization used for dynamical systems is the \textit{shift}-operator, which is the discretization method implemented inside MATLAB's c2d function. In the \textit{shift}-operator approach, the continuous-time system is mapped to the discrete-time system through the equations
\begin{subequations}
    \begin{align}
        A_s &= e^{hA_c} \label{eq:num:delta:shiftA}\\
        B_s &= \int_{0}^{h} e^{\tau A_c} d\tau B \label{eq:num:delta:shiftB}
    \end{align}
\end{subequations}
Replacing the exponential in \eqref{eq:num:delta:shiftA} with its series expansion leads to
\begin{equation}
    A_s = I + A_c h + \frac{(A_c h)^2}{2!} + \frac{(A_c h)^3}{3!} + \dots
\end{equation}
In this formulation, it is obvious that as the sampling time gets smaller (e.g.\ $h \to 0$), the terms after $I$ will become smaller. This means that for small sample times, the diagonal elements of the matrix $A_s$ will be 1 plus a very small number. In floating-point arithmetic, this will lead to a loss of information about the state transition, specifically about the dynamics of the plant, because the dynamic range of the diagonal coefficients is reduced in comparison to the other coefficients. It has been reported that these effects can be seen for sample times on the order of 10-20 times less than the system's dominant time constant \cite{Kerrigan2012}.

To prevent the loss of the information about the system's dynamics in the prediction when using floating-point arithmetic, Longo et.\ al.\ proposed in \cite{Longo2013} the use of the \textit{delta}-domain approach to system discretization instead of the \textit{shift}-operator approach. The \textit{delta}-domain is similar to a first order difference discretization \cite{Middleton1986}, where the derivative $\dot{x}$ is instead replaced by
\begin{subequations}
    \begin{equation}
        \frac{x_{k+1} - x_k}{h} = A_{\delta} x_k + B_{\delta} u_k \label{eq:num:delta:deltaX}
    \end{equation}
    with
    \begin{equation}
        A_{\delta} = \Omega A_c, \quad B_{\delta} = \Omega B_c \label{eq:num:delta:ABdef}
    \end{equation}
    and 
    \begin{equation}
        \Omega = \frac{1}{h} \int_{0}^{h} e^{A_c \tau} d\tau \label{eq:num:delta:OmegaDef}
    \end{equation}
\end{subequations}

In \cite{Kerrigan2012, Longo2013}, this method was implemented in the QP by redefining the decision vector \eqref{eq:mpc:sparse:varVec} to be
\begin{equation}
    \theta = \begin{bmatrix}
    u_0 & \delta_0 & x_1 & u_1 & \delta_1 & x_2 & \cdots & u_{N-1} & \delta_{N-1} & x_N
    \end{bmatrix}^{T}
\end{equation}
and then rearranging the terms in \eqref{eq:num:delta:deltaX} to get a prediction system of
\begin{subequations}
    \label{eq:num:delta:delCompUp}
    \begin{align}
        \delta_k &= A_{\delta} x_k + B_{\delta} u_k \label{eq:num:delta:delUp}\\
        x_{k+1} &= x_k + h \delta_k \label{eq:num:delta:xUp}
    \end{align}
\end{subequations}
In this formulation, the $\delta$ variables are also included in the optimization problem as decision variables. Their inclusion means that the implementation of the QP solver must be carefully examined, since some solvers may run pre-processing that will remove them to decrease the problem size. The prediction formulation given in \eqref{eq:num:delta:delCompUp} will prevent the loss of information by separating out the smaller valued state-update coefficients from the delay coefficient, which will allow all coefficients in $A_\delta$ to use the full dynamic range for the representation of the system dynamics.

The \textit{delta}-domain approach was implemented in \cite{Longo2013, Longo2014, Kerrigan2012} on an example system, and was shown to have superior closed-loop performance when comparing the system trajectories to those from the \textit{shift}-operator approach when both were implemented with 5-bits of precision. The trajectory from the 5-bit \textit{delta}-domain implementation almost perfectly overlaps the trajectory from the double-precision floating-point (52-bit) implementation. In \cite{Longo2013}, it was shown that this method leads to fewer interior-point iterations at each sample instant. Additionally, inside each of those iterations the computational delay of the \textit{delta}-domain  formulation is equivalent to that of the \textit{shift}-operator formulation, and the same number of hardware resources (e.g.\ adders, multipliers, etc.) are required \cite{Longo2013}.


\subsection{Fixed-Point Implementation}
\label{sec:num:fix}

\subsubsection{Interior-Point Methods}
\label{sec:num:fix:intPoint}

Interior-point methods, as described in section \ref{sec:alg:ipm}, require the solution of the KKT system at each iteration in order to determine the next step towards the optimal point. The problem is, the KKT system being solved changes at every step and in general has undesirable numerical properties, such as a condition number that increases in size as the iterations progress \cite{Greif2014}. The gradual ill-conditioning of the KKT system means that in general the maximum value of variables inside linear system solvers cannot be bounded. This leads to a problem in fixed-point arithmetic, since there can be no guarantee of overflow not occuring \cite{Jerez2012_Num}.

Implementations of interior-point methods that use an iterative linear system solver usually use one based on the Lanzcos algorithm (e.g.\ Conjugate Gradient, MINRES, GMRES, etc.). To allow for the implementation of these algorithms in the fixed-point representation, Kerrigan et.\ al. proposed in \cite{Kerrigan2012} an online preconditioner for the linear system that is designed primarily to allow explicit bounding of the variables inside a Lanzcos iteration. For a matrix $A \in \mathcal{R}^{NxN}$, this preconditioner is a positive diagonal matrix  $M \in \mathcal{R}^{NxN}$ with elements
\begin{equation}
    M_{kk} = \sum_{j=1}^{N} \abs{A_{kj}}
    \label{eq:num:fix:precond}
\end{equation}
that is applied symmetrically to the linear system $Ax = b$, so the Lanzcos iteration then acts on the system
\begin{equation}
    M^{-\frac{1}{2}} A M^{-\frac{1}{2}} x = M^{-\frac{1}{2}} b
\end{equation}

It was shown in \cite{Kerrigan2012,Jerez2012_Num} that when the preconditioner \eqref{eq:num:fix:precond} is used with a symmetric matrix $A$, all variables inside the Lanzcos iteration are bounded inside one of 4 intervals: $(-1,1)$, $(-2, 2)$, $(\epsilon, 1)$ or $(1, \sfrac{1}{\epsilon})$, where $\epsilon$ is a small positive number (e.g.\ $\epsilon \ll 1$) related to the termination criteria. These interval bounds for the variables allow the integer potion of the fixed-point numbers to be fixed at design time to one bit, two bits, and $\lceil -\log_2(\epsilon) \rceil$ bits, and one bit respectively. This work did not explore the effect of the number of fractional digits on the solver, but results from \cite{Greenbaum1997} show that for these algorithms the number of fractional bits in the number will affect the convergence rate of the algorithm (the more fractional bits provided, the faster it will converge).

This preconditioner was implemented in the FPGA solver presented in \cite{Hartley2014}. A comparison of the preconditioned fixed-point versus the 52-bit floating point was done in \cite{Jerez2012_Num}. It was found that a 32-bit fixed-point implementation using the preconditioner had smaller error than a single precision floating-point implementation, and that the control quality of the fixed-point implementation was similar to that of a double precision floating-point implementation.

While the preconditioner was originally proposed to bound the iterates in the Lanzcos iteration, it was shown experimentally that it also has a positive effect on the convergence of the solver. When it was specifically used with a MINRES solver in \cite{Jerez2012_Num}, it allowed the fixed-point solver to successfully solve some systems where the unprecondtioned problem is unsolvable on a double precision floating-point solver.

\subsubsection{First-Order Methods}
\label{sec:num:fix:grad}

This section will outline the results obtained from analyzing thee different gradient methods and their application to MPC: Nesterov's Fast Gradient Method, the Dual Gradient Projection Method, and the Alternating-Direction Method of Multipliers.

In work conducted by Jerez et.\ al.\ in \cite{Jerez2013}, Nesterov's Fast Gradient Method (FGM) was examined. They showed that under fixed-point arithmetic (e.g.\ in the presence of round-off errors), if the step-size of the method is in the interval $[0,1]$, then the error recurrence is Schur stable. This means that the errors remain bounded at each iteration, leaving the algorithm with a persistent worst-case error.

They then derived a tight upper bound on the worst-case accumulation of errors up to a specific iteration using the number of fractional bits provided in the data type. Finally, they also derived upper bounds on all the intermediate variables in the FGM using only data known at design-time, which then allow the number of integer bits for each variable to be set \cite{Jerez2013}.

Patrinos et.\ al.\ analyze the Dual Gradient Projection algorithm under fixed-point arithmetic in \cite{Patrinos2013}. Their analysis derives convergence rates for the primal sequence, and shows that the algorithm will converge in a finite number of iterations. Using that rate analysis, they derive lower-bounds on the number of fractional bits needed to guarantee convergence of the primal sequence to a specified tolerance level. Finally, they derive a lower-bound on the number of integer bits needed to guarantee that there will be no overflow in a fixed-point implementation of the algorithm.

Jerez et.\ al.\ analyzed the implementation of an ADMM-based QP solver on an FPGA in \cite{Jerez2014}. In that work, they derived criteria on the number of fractional-bits required to guarantee stability of the ADMM algorithm in fixed-point implementation. No bounds on the integer portion was derived in that work, instead it was suggested that simulations be carried out at design time to bound the variables. In subsequent work by Dang et.\ al.\ in \cite{Dang2015}, upper-bounds on the magnitude of variables inside the ADMM iterations were derived. These bounds allowed the authors to determine an appropriate number of integer bits without running simulations.

\subsection{Logarithmic Number System}
\label{sec:num:lns}

Some previous work has examined the implementation of real-time MPC using the logarithmic number system. In the work by Vouzis et.\ al.\ \cite{Vouzis2009}, the Newton's method is used to solve the condensed linear MPC problem. They profiled the algorithm and determined that the main computational bottlenecks for the algorithm were the inversion of the matrices and the calculation of the Hessian. The main operations involved in these steps are inversion, squaring, and cubing. To speed up these operations, they chose to use the logarithmic number system since those operations become shifting and addition operations. The results presented in \cite{Vouzis2009} when benchmarking their implementation show improvement in the time spent computing the Hessian for the algorithm, but the inversion process still utilizes the most time as the horizon length increases.

Additional research conducted by Vouzis et.\ al.\ in \cite{Vouzis2007} examined using a Monte Carlo approach to the numerical calculations to reduce the likelihood of cancellation errors and round-off errors becoming more pronounced. In this method, they utilized a logarithmic number system for the computations, and added in small noise sources to the LNS multiplication operations. The idea behind the approach is to perform the same computations multiple times and each time slightly perturbing the problem data. This will reduce the likelihood of a catastrophic error occuring in each computation, so the errors can then be spotted. This method was found to degrade the performance of the MPC algorithm because each iteration had to be run multiple times, additionally there is an increase in the algorithmic error when compared to a normal floating-point implementation due to the noise introduced in the computation.