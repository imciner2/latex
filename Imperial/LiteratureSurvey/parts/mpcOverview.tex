\section{Model Predictive Control Overview}
\label{sec:mpc}

Model Predictive Control (MPC) is a method of implementing online optimal control of a system using a physics model to predict the system's response in the future to control inputs. The general idea is to optimize over the control inputs that will be applied to the plant over a specified time frame (termed the control horizon), while trying to minimize a function of the future plant trajectory (and control inputs) over another time frame (the prediction horizon). Once the optimal control problem is solved, the first control input in the computed sequence is applied to the system. This is repeated at each sampling instant, which means the optimization problem must be solved quickly and efficiently. MPC is readily applied when there are constraints on the system that must be met (such as state boundary constraints, or actuator limits), since the MPC formulations can include the constraints in the optimal control problem.

\subsection{Problem Formulation}

The general MPC problem for a generic cost function and a nonlinear discrete-time plant is given by \eqref{eq:mpc:genMPC}, where
$\bar{x} = \begin{bmatrix}
x_1 & x_2 & x_3 & \cdots & x_N
\end{bmatrix}^T$ and
$\bar{u} = \begin{bmatrix}
u_0 & u_1 & u_2 & \cdots & u_{N-1}
\end{bmatrix}^T$.
\begin{align}
    \label{eq:mpc:genMPC}
    \underset{\bar{u}, \bar{x}}{\text{min}} \quad & g(\bar{x}, \bar{u})\\
    \text{s.t.} \quad & x_{k+1} = f(x_k, u_k)  & k=0, \dots N-1 \nonumber \\
    & h_1(u_k) \leq 0 & k=0, \dots N-1 \nonumber \\
    & h_2(x_k) \leq 0 & k=1, \dots N \nonumber
\end{align}

This general MPC problem can be computationally intractable though, since the cost, inequality, and state functions may be nonlinear or non-convex. A simplified version of the MPC problem that is frequently used in practice is the constrained LQR version. In this version, the cost function is replaced by the LQR cost function, and the nonlinear discrete-time model is replaced by a linear discrete-time model, creating the optimization problem of the form \eqref{eq:mpc:linMPC}, where $P,Q \succeq 0, R \succ 0$ are cost matrices associated with the terminal state, stage cost, and input cost respectively \cite{Maciejowski2002,Borrelli2017}.
\begin{subequations}
\label{eq:mpc:linMPC}
\begin{align}
    \underset{\bar{u}, \bar{x}}{\text{min}} \quad & \norm{x_N}^2_P + \sum_{k=0}^{N-1} \norm{x_k}^2_Q + \norm{u_k}^2_R \label{eq:mpc:lin:cost}\\
    \text{s.t.} \quad & x_{k+1} = A x_k + B u_k & k=0, \dots N-1 \label{eq:mpc:lin:dyn} \\
    & F u_k \leq 0  & k=0, \dots N-1 \label{eq:mpc:lin:inputCon} \\
    & J x_k \leq 0  & k=1, \dots N \label{eq:mpc:lin:stateCon}
\end{align}
\end{subequations}

The optimization problem given in \eqref{eq:mpc:linMPC} is not the only possible formulation, others exist for minimum fuel problems or minimum time problems. This formulation though is used in most cases because it is a convex optimization problem that can be solved using many different algorithms for Quadratic Programming (QP).

\subsection{Explicit MPC}

One possible way of implementing MPC in a real-time system is through the use of a method called Explicit MPC \cite{Bemporad2002, Bemporad2002_expAuto}. In Explicit MPC, the optimization problem in \eqref{eq:mpc:linMPC} is solved at design-time for various initial states. This creates a linear control law, that is parameterized based upon the initial state the system is at. At run-time, the system state is used to find the applicable linear control law for the current region of the state-space, then the control law is applied to the system. In this formulation the only computations that occur online are to determine the region of the state-space the system is in, find the control law in a lookup table, then perform a linear-state feedback computation. This formulation does present challenges with memory usage though, since for large state-spaces or complex constraints/dynamics the number of control laws that need to be stored grows exponentially.

\subsection{Implicit MPC}

An alternative application of MPC is called Implicit MPC (or online MPC), where the optimization problem in \eqref{eq:mpc:linMPC} is solved at each sampling time given the new system state information. This method is more computationally demanding since it requires the solution of the QP at every sampling time, but it overcomes the memory usage problems from Explicit MPC. Since the optimization problem is being solved in real-time, computational complexity of the optimization solver becomes an important factor. To address this, there are three variations of the linear MPC optimization problem that can be passed to the optimization solver.

\subsubsection{Condensed Formulation}

For problems that do not have constraints on the state-space of the plant, it may be beneficial to remove the states from the decision vector through a process called ``condensing''. In condensing, the state variables are eliminated through recursively applying the system dynamics \eqref{eq:mpc:lin:dyn} to get a prediction matrix of the form
\begin{equation*}
    \label{eq:mpc:cond:pred}
    \bar{x} = 
    \underbrace{
    \begin{bmatrix}
    B & 0 & 0 & & 0\\
    AB & B & 0 & & 0\\
    A^2b & AB & B & & 0 \\
    \vdots & & & \ddots & \vdots\\
    A^{N-1}B & A^{N-2}B & A^{N-3}B & \cdots & B
    \end{bmatrix}}_{=\Gamma}  \bar{u}
    + \underbrace{
    \begin{bmatrix}
    A \\
    A^2 \\
    A^3 \\
    \vdots \\
    A^{N}
    \end{bmatrix}}_{=\Phi} x_0
\end{equation*}
Using this prediction matrix, and
\begin{equation*}
\bar{R} = (I_{N-1} \otimes R), \qquad
\bar{Q} =
\begin{bmatrix}
I_{N-1} \otimes Q & 0\\
0 & P
\end{bmatrix}
\end{equation*} the optimization problem in \eqref{eq:mpc:linMPC} becomes the one in \eqref{eq:mpc:con:MPC}, where the only optimization variables are the control inputs \cite{Maciejowski2002}.

\begin{subequations}
    \label{eq:mpc:con:MPC}
    \begin{align}
    \underset{\bar{u}}{\text{min}} \quad & \bar{u}^T \underbrace{(\Gamma^T \bar{Q} \Gamma + \bar{R})}_{=H} \bar{u} + 2 x_0^T \underbrace{(\Gamma^T \bar{Q} \Phi)}_{=G} \bar{u} \label{eq:mpc:con:cost}\\
    \text{s.t.} \quad & J u_k \leq 0  & k=0, \dots N-1 \label{eq:mpc:con:inputCon}
    \end{align}
\end{subequations}


\subsubsection{Sparse Formulation}

An alternative formulation is to leave the optimization problem in the form of \eqref{eq:mpc:linMPC}, and solve the problem by stacking the system dynamics equations into a large prediction matrix of the form given in \eqref{eq:mpc:sparse:pred}, with the variable vector \eqref{eq:mpc:sparse:varVec} \cite[\S 11.3.1]{Borrelli2017}.
\begin{equation}
    \label{eq:mpc:sparse:varVec}
    \theta = \begin{bmatrix}
    x_1 & u_0 & x_2 & u_1 & \cdots & x_N & u_{N-1}
    \end{bmatrix}^{T}
\end{equation}

\begin{equation}
\label{eq:mpc:sparse:pred}
0 = 
\underbrace{
    \begin{bmatrix}
    I & -B &  0 &  0 &  0 &\cdots & 0 & 0 & 0 & 0\\
    -A &  0 &  I & -B &  0 & & 0 & 0 & 0 & 0\\
    0 &  0 & -A &  0 &  I & & 0 & 0 & 0 & 0\\
    0 &  0 &  0 &  0 & -A & & 0 & 0 & 0 & 0\\
    \vdots & & & & & \ddots & & & & \vdots\\
    0 &  0 &  0  & 0 & 0 & \cdots &  I & -B & 0 &  0\\
    0 &  0 &  0  & 0 & 0 & \cdots & -A &  0 & I & -B
    \end{bmatrix}}_{=G}  \theta
- \underbrace{
    \begin{bmatrix}
    A \\
    0 \\
    0 \\
    0 \\
    \vdots \\
    0 \\
    0
    \end{bmatrix}}_{=E} x_0
\end{equation}

This sparse form can be used when the problem has constraints on the state variables, since it does not require condensing the stage constraints to use the input vector. Additionally, it provides good computational scaling when implemented in most solvers (the size of the matrix and its storage requirements scale linearly with horizon length). This is because the prediction matrix has a banded structure, so when it is placed into the KKT system for interior-point methods the resulting KKT matrix is a banded matrix, with a bandwidth independent of the horizon length. The constant bandwidth of the KKT system allows for the solver implementations to have a fixed datapath size versus the horizon length \cite{Jerez2012}. The downside to this formulation is that it keeps the states as optimization variables, so for systems with a large state space the number of optimization variables can be large.

\subsubsection{Variable-Sparsity Formulations}

An alternative formulation that has been developed are those that allow for variable sparsity while condensing the problem to only control inputs as optimization variables. This type of formulation can be useful if the system requires a long horizon but doesn't need to constrain the state space. One method of introducing sparsity into the condensed formulation is the ``condensed-sparse'' formulation proposed by Jerez et.\ al.\ in their work \cite{Jerez2011_CSF, Jerez2012_CSF}. In their method, a linear change of variables is applied to the state-space. In this case, the change of variables takes the form of a linear control law $u_k = K x_k + z_k$. It is then applied to the prediction system to create the discrete-time dynamics 
\begin{equation*}
    x_{k+1} = \underbrace{(A+ BK)}_{A_K} x_k + B z_k
\end{equation*}
The prediction matrix $\Gamma$ is then created using $A_k$ instead of $A$.

By choosing $K$ such that the resulting $A_K$ is nilpotent (e.g.\ $(A_K)^{i} {=} 0 \quad \forall i \geq r$), then the condensed matrix $\Gamma$ will have have a bandwidth of $r$ if the horizon is long enough (e.g.\ $N > r + 1$). The parameter $r$ is the controllability index of the pair $(A,B)$, and in practice $r = \lceil \frac{n}{m} \rceil$ in most cases. The controller that can be used to get a nilpotent $A_K$ is the deadbeat gain controller when used in a static state feedback context. In practice, this control gain doesn't have to actually be applied to the system. A new mapping of the optimization matrices is provided in \cite{Jerez2011_CSF} that places $K$ into the optimization problem alongside the matrices to map $\bar{u}$ into the new state space.

It was shown in \cite{Jerez2011_CSF} that using the condensed-sparse formulation is always as good as the standard condensed formulation when examining the computational complexity and memory requirements, and outperforms it when the horizon is large. Furthermore, if the number of states is larger than the number of inputs, the condensed-sparse formulation can outperform the fully-sparse formulation.

Another way of implementing a variable-sparsity MPC algorithm was proposed by Axehill in \cite{Axehill2015}. In this method, the prediction horizon is split into subintervals. The condensing operation is them performed on a subinterval basis instead of on the entire problem. This means that choosing the size of the subintervals induces a level of sparsity into the Hessian for the condensed form that can vary between the full sparsity (the sub interval is 1 stage long) to the fully condensed (the sub interval is the entire horizon).