\section{Quadratic Programming Algorithms}

In embedded linear MPC there are three main classes of algorithms that are used: Interior-Point Methods, Active-Set Methods, and First-Order Methods. Many different FPGA implementations have been created using these methods, and Figure \ref{fig:alg:taxon} gives a taxonomy chart listing some of these.

\begin{figure}[t]
    \centering
    {\small
        \input{images/algTaxon.pgf}}
    \caption{Taxonomy of the various optimization algorithm implementations discussed in this paper. The references refer to FPGA implementations of the algorithm.}
    \label{fig:alg:taxon}
\end{figure}

\subsection{Interior-Point Methods}
\label{sec:alg:ipm}

Interior-point methods are a very popular method of solving optimization problems with both equality and inequality constraints of the form shown in \eqref{eq:alg:ipm}, due to their polynomial complexity guarantees. The basic premise behind interior-point methods is the iterative solution of the non-linear KKT system through Newton iterations updating the guess of the optimal point.

\begin{align}
\label{eq:alg:ipm}
\underset{\theta}{min}  \quad & \frac{1}{2} \theta^{T} H \theta \\
\text{s.t.} \quad & F \theta = f \nonumber \\
&  G \theta \leq g \nonumber
\end{align}

The handling of the inequality constraints in the interior-point methods differs from variant to variant. One of the most well known methods of handling the constraints is a barrier-penalty function. In this method, the inequality constraints are replaced by a penalty function added to the cost function, which penalizes violation of the constraint. A common penalty function is a logarithmic-barrier. The logarithmic-barrier function presents a ideal penalty, where the additional cost is $0$ if the constraint is not violated and grows quickly to inifinity when it is violated. The interior-point iterations then seek to find a solution to the unconstrained problem as the penalty on the barrier term approaches infinity \cite[\S 19.6]{Nocedal2006}.

Another variant of the interior-point method turn the inequality constraints into equality constraints through the addition of slack variables \cite{Wright1997}. In this variant, the solution of both the primal variables and the dual variables is done, giving it the name Primal-Dual Interior-Point (PDIP) Method.

A brief outline of an algorithm used for the PDIP variant is given in Algorithm \ref{alg:ipm}. The presented method solves a reduced KKT system at each iteration to compute the Newton step for the primal variables and the dual variables for the equality constraints, then uses those results to compute the step for the inequality duals and the slack variables. Then a step-size is computed before being applied to the search direction get the next guess for the optimal point.

\begin{algorithm}
    \caption{Primal-dual interior-point method for solving \eqref{eq:alg:ipm} \cite{Jerez2012}.}
    \label{alg:ipm}
    
    \algrenewcommand\algorithmicrequire{
        \textbf{Input:}
    }
    
    \begin{algorithmic}[1]
        \Require $\theta_0, \nu_0, \lambda_0, s_0$ with $[\lambda_0, s_0] > 0$
        
        \For {$k=0$ to $I_{max}-1$}
        \State Solve $\begin{bmatrix}
        H + G^T W_k G & F^T \\
        F & 0
        \end{bmatrix} \begin{bmatrix}
        \Delta \theta_k \\ \Delta \nu_k
        \end{bmatrix} = 
        \begin{bmatrix}
        r_k^{\theta} \\ r_k^{\nu}
        \end{bmatrix}$
        \Comment Solve the linearized KKT system
        
        \State Compute $\Delta \lambda_k$
        \State Compute $\Delta s_k$
        
        \State $\alpha_k \gets \left\{ \begin{array}{cl}
            \underset{ (0,1] }{\text{min}} \quad & \alpha \\
            \text{s.t.} \quad & \lambda_k + \alpha \Delta \lambda_k > 0 \\
            & s_k + \alpha \Delta s_k > 0
        \end{array} \right\}$
        \Comment Compute step size
        
        \State $\begin{bmatrix}
        \theta_{k+1} \\ \nu_{k+1} \\ \lambda_{k+1} \\ s_{k+1}
        \end{bmatrix} = \begin{bmatrix}
        \theta_{k} \\ \nu_{k} \\ \lambda_{k} \\ s_{k}
        \end{bmatrix} + \alpha
        \begin{bmatrix}
        \Delta \theta_{k} \\ \Delta \nu_{k} \\ \Delta \lambda_{k} \\ \Delta s_{k}
        \end{bmatrix}$
        \Comment Compute next point
        
        \EndFor
    \end{algorithmic}
\end{algorithm}

Initializing an interior-point algorithm also depends on the variant used. The barrier-function algorithms require a feasible initial guess, so usually an additional linear optimization problem is solved initially to generate a feasible point. The PDIP variant on the other-hand doesn't require a feasible initial point, just that the dual and slack variables satisfy a positivity condition.

\subsection{Active-Set Methods}
\label{sec:alg:asm}

Another method that is designed to handle inequality constrained optimization problems of the form given in \eqref{eq:alg:asm} are the Active-Set methods.

\begin{align}
\label{eq:alg:asm}
\underset{z}{min}  \quad & \frac{1}{2} z^{T} Q z + c^T z \\
\text{s.t.} \quad & J z \leq g \nonumber
\end{align}

The idea behind the active-set methods is to find the inequality constraints that are active at the optimal point by solving a sequence of subproblems. Each of these subproblems has a subset of the inequality constraints turned into equality constraints. This is based on the idea that if a constraint is active at the optimal point, it will have equality with 0 \cite{Nocedal2006}.

The method used to update the active set between iterations differs between variants of the algorithm. The simplest variant is given in Algorithm \ref{alg:asm}, which will choose a single constraint each time to either add to or remove from the active set. More complicated variants are able to perform multiple additions/removals from the active set at each iteration. For an overview of some variants of the active-set method, see \cite{Herceg2015} and the references therein.

\begin{algorithm}
    \caption{Active-set method for solving \eqref{eq:alg:asm} \cite{Lau2009}.}
    \label{alg:asm}
    
    \algrenewcommand\algorithmicrequire{
        \textbf{Input:}
    }
    
    \begin{algorithmic}[1]
        \Require $z_0$ feasible 
        \Comment Feasible initial point
        
        \State $\mathcal{W}_0 = \emptyset$
        \Comment Start with no active constraints
        
        \For {$k=0,1,2$\dots}
        \State Solve $\begin{bmatrix}
            Q & J^T_{\mathcal{W}} \\
            J_{\mathcal{W}} & 0
        \end{bmatrix} \begin{bmatrix}
            \Delta z_k \\ \lambda
        \end{bmatrix} = 
        \begin{bmatrix}
            -c -Q z_k \\ 0
        \end{bmatrix}$
        \Comment Solve the KKT system
        
        \If {$\Delta z_k = 0$}
            \If {all entries of $\lambda$ are non negative}
                \State Terminate with $z^* = z_k$
            \Else
            \Comment A constraint in the active set is not active
                \State Remove the constraint from $\mathcal{W}_k$ that has the smallest entry of $\lambda$
                \State $z_{k+1} = z_k$
            \EndIf
        \Else
            \State $\mathcal{D}_k = \left\{ i \notin \mathcal{W}_k : J_i \Delta z_k > 0, \frac{g_i - J_i z_k}{J_i \Delta z_k} < 1 \right\}$
            \If { $\mathcal{D}_k = \emptyset$ }
                \State $z_{k+1} = z_k + \Delta z_k$ and $ \mathcal{W}_{k+1} = \mathcal{W}_k$
            \Else
            \Comment Update the active set with a new violated constraint
                \State $\alpha = \underset{i \in \mathcal{D}_k}{\text{min}} \frac{g_i - J_i z_k}{J_i \Delta z_k} $ and $z_{k+1} = z_k + \alpha \Delta z$
                \State $\mathcal{W}_{k+1} \gets \mathcal{W}_k$ + one element from $\mathcal{D}_k$
            \EndIf
        \EndIf
        \EndFor
    \end{algorithmic}
\end{algorithm}

\subsection{First-Order Methods}
\label{sec:alg:fom}

First-order optimization methods utilize information about the derivative of the cost function to guide their search for the optimal point. The derivative informs the creation of a search direction for the method (the direction in which the next change to the optimal point vector occurs), and then a step-size is computed that informs how far along the search direction to go. A survey of some first-order methods is presented in \cite{Kouzoupis2015}.

These methods are very popular for unconstrained optimization, since the search is carried out using only information from the cost function. There have been many adaptations of the algorithms to handle inequality constraints, with some notable ones being Nesterov's Fast Gradient Method (FGM), and the Method of Multipliers (and its variant, the Alternating Direction Method of Multipliers (ADMM)).

The Fast Gradient Method (or Accelerated Gradient Method) was developed by Nesterov in the 1980s \cite{Nesterov1983}, and couples a gradient descent algorithm with an acceleration step. This algorithm has also been modified to handle inequality constraints through the inclusion of a projection step, where the next optimal point is projected onto the constraint set. This projection step can range in complexity from a simple saturation function if the constraints are a box, to a convex program in its own right if the constraint set is more complex. One possible algorithmic adaptation of FGM is given by Algorithm \ref{alg:fgm} and used in \cite{Jerez2014} to solve \eqref{eq:alg:fgm}. This version utilizes a constant step-size, based on the eigenvalues of the Hessian matrix, and is easily parallizable providing the projection step is simple (such as with box constraints).

\begin{align}
    \label{eq:alg:fgm}
    \underset{z}{min}  \quad & \frac{1}{2} z^{T} H_F z + z^T \Phi x \\
    \text{s.t.} \quad & z \in \mathcal{K} \nonumber
\end{align}


\begin{algorithm}
    \caption{Fast Gradient Method for solving \eqref{eq:alg:fgm} in parallel \cite{Jerez2014}.}
    \label{alg:fgm}
    
    \algrenewcommand\algorithmicrequire{
        \textbf{Input:}
    }
    
    \begin{algorithmic}[1]
        \Require $L = \lambda_{max}(H_F) > 0$, $\mu = \lambda_{min}(H_F) > 0$
        \Comment Hessian eigenvalues
        \Require $z_0 \in \mathcal{K}$, $y_0 = z_0$ \Comment Feasible initial point
        
        \State $\beta = \frac{\sqrt{L} - \sqrt{\mu}}{\sqrt{L} + \sqrt{\mu}}$
        \Comment Step-size
        
        \For {$i=0$ to $I_{max}-1$}
        \State $t_i = (I - (1/L)H_{F}) y_i - (1/L) \Phi x$
        \Comment Unconstrained step
        
        \State $z_{i+1} = \pi_{\mathcal{K}}(t_i)$
        \Comment Projection onto feasible region
        
        \State $y_{i+1} = (1+ \beta)z_{i+1} - \beta z_i$
        \Comment Acceleration step
        \EndFor
    \end{algorithmic}
\end{algorithm}