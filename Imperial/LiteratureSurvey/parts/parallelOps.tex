%!TEX root = ../LitSurvey.tex
\section{Parallelization Opportunities}

Opportunities for parallelization of real-time MPC implementations occur at two distinct levels: the algorithmic level and the computational level. Parallelizations at the algorithmic level take advantage of the structure of the problem and the algorithm to efficiently parallelize computations, while those at the computational level exploit the implementation of basic operations to efficiently implement the algorithm.

\subsection{Algorithmic Level}

Many algorithms have characteristics that can be exploited to parallize computations and decrease the computation time of the algorithm.

\subsubsection{Variable-Separable Gradient Methods}
\label{sec:par:alg:fgm}

An example of algorithm level parallism can be found in gradient methods, such as the Fast Gradient Method implementation in \cite{Jerez2014}. In many gradient methods for unconstrained optimization, the only data-dependence in computations arises in the matrix-vector multiplication unit. This dependence is between iterations (the next iteration needs the complete variable vector). When inequality constraints are added to the methods, the projection step can introduce data dependencies inside the iteration.

For box-constrained problems though, the projection doesn't introduce data dependency inside the iteration. This allows for parallelism inside the iteration by implementing the computations for each variable separately.

An example of this can be found in the implementation of the Fast Gradient Method and ADMM by Jerez et.\ al.\ in \cite{Jerez2014}. Their unit, shown in Figure \ref{fig:par:alg:fgm}, implements multiple row-units (the shaded region) that communicate through buffers between each iteration. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{images/par_fgmJerez2014.png}
    \caption{Architecture used for the Fast Gradient Method in \cite{Jerez2014} (graphic from \cite{Jerez2014}).}
    \label{fig:par:alg:fgm}
\end{figure}

In this implementation, each module contains a dot-product unit to compute the appropriate component of the matrix-vector computation. This component is then fed into a projection module, which for the box constraints represents a saturation function. This result goes through some final computations before being passed into the buffer for communication to the other modules for the next iteration.

\subsection{Matrix-Vector Multiplication}

On the computational level, the most parallelizable portion in the algorithms for MPC are the matrix operations. These algorithms at their lowest level are composed of numerical linear algebra routines such as linear equation solvers, Givens rotations, etc. which at their lowest level are composed of matrix-vector products. The place where parallelization is most apparent in these routine is at the matrix-vector product level \cite{Constantinides2009}. A survey of all methods that parallelize the matrix-vector multiplications is outside the scope of this work, but a survey of the different methods implemented in several MPC architectures will be given. For notation, the matrix is assumed to be $A \in \mathcal{R}^{m \times n}$, the vector $x \in \mathcal{R}^{n}$, and the computation is $y = Ax$.\newline

\noindent\textbf{Column-Sweep Parallelism}\newline
In \cite{Rubagotti2016}, Rubagotti et.\ al.\ implemented row-level parallelism (processing an entire column at a time) for the matrix-vector multiplications. A schematic of the module is shown in Figure \ref{fig:par:mvm:rug}. This implementation utilized a multiply-accumulate (MAC) module for each row in the $A$ matrix (creating $m$ total of them). The $x$ vector was then iterated through, sending the current component to every MAC module simultaneously. This method (as implemented with control logic) requires $n+5$ clock cycles to compute the product, and is busy with the computation for a single matrix-vector product the entire time. The matrix data for this computation was stored in distributed RAM blocks on the chip to minimize latency in the datapath.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{images/MVM_GPD.png}
    \caption{Matrix-vector-multiplication unit from \cite{Rubagotti2016} (graphic from \cite{Rubagotti2016}).}
    \label{fig:par:mvm:rug}
\end{figure}

\noindent\textbf{Row-Sweep Parallelism}\newline
An alternative method of implementing the matrix-vector multiplication operation is in a row-sweep fashion (where an entire row is processed at a time). This method is also known as an inner product circuit, since it will compute the inner product between the row of $A$ and the vector $x$, and is shown in Figure \ref{fig:par:mvm:colmajor}. It was implemented by Wills et.\ al.\ in their work \cite{Wills2011}. In this implementation, there are $n$ multiplication units, followed by an adder tree of depth $\lceil log_2 n\rceil$.  It is heavily pipelined, with registers placed after each multiplier and between each level of the adder tree. The inner product operation takes $\lceil log_2 n\rceil$ clock cycles to complete in this unit, but a new row of $A$ can be fed into the unit at each clock cycle. This means that after $\lceil log_2 n\rceil$ clock cycles, the first component of the resultant vector is available at the circuit's output, and every subsequent clock cycle produces another component. Overall, this makes the computational delay of the matrix-vector multiplication
\begin{equation}
    \label{eq:par:lat:mvm:wills}
    C = m + \lceil log_2 n\rceil
\end{equation}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.2]{images/MVM_ColMajor.png}
    \caption{Matrix-vector multiplication unit from \cite{Wills2011} (graphic from \cite{Wills2011}).}
    \label{fig:par:mvm:colmajor}
\end{figure}

\noindent\textbf{Parallel Column-Sweep}\newline
Peyrl et.\ al.\ in \cite{Peyrl2014} implemented a second level of parallelism on top of the column-sweep implementation from \cite{Rubagotti2016}. In their implementation, a single MVM unit is composed of $m$ MAC modules, but there are also $p$  MVM units in parallel. The MVM units operate in a column-scalar manner, taking a single column of $A$ in each clock cycle along with the appropriate component of $x$. For example, if $p{=}2$ then unit 1 would receive an odd column each cycle (e.g. 1, 3, 5, etc.) and unit 2 would receive an even column each cycle (e.g. 2, 4, 6, etc.). Once all columns have been processed, the results from the MVM units are passed into adder trees to perform a sum across all the MACs from a matrix row. There are a total of $m$ adder trees, with $p$ inputs each.

This architecture is parameterizable through $p$, the number of columns processed in parallel. If $p{=}1$, then this is essentially the unit used by Rubagotti et.\ al.\ in \cite{Rubagotti2016}, whereas $p{=}n$ represents a full parallelization of the MVM, where every column is processed at the same time (this would be equivalent to $m$ inner product circuits in parallel). Varying the parameter $p$ also changes the depth of the adder trees, increasing $p$ requires more inputs to the tree, increasing its depth. These trees are pipelined to limit their effect on the critical path of the design, with the number of layers per stage, $d$, left to the designer (in \cite{Peyrl2014}, 2 layers were placed in each pipeline stage). The overall clock cycle latency to compute a single matrix-vector multiplication for this design is then parameterized through $p$ and $d$, and is given by \eqref{eq:par:lat:mvm:peyrl}.
\begin{equation}
\label{eq:par:lat:mvm:peyrl}
C = \overbrace{\left\lceil \frac{n}{p} \right\rceil}^{\text{MVM Units}}
+ \overbrace{\lceil log_{2^d} p \rceil}^{\text{Adder Tree}}
\end{equation}

\noindent\textbf{Summary}\newline
Overall, there are two distinct methods to parallelize the computation of the matrix-vector multiplication: row-sweep and column-sweep. The choice of which method to use depends on several factors, including parallelizations present in the algorithm, other computations needed for the algorithm, and the hardware resources available on the FPGA.

The row-sweep circuit is equivalent to an inner product circuit, which means if the algorithm being implemented requires the computation of inner products outside of the matrix-vector multiplication, or the computation of a vector 2-norm, the circuit hardware could be reused in the algorithm with appropriate resource scheduling. This is the approach taken by Jerez et.\ al. in \cite{Jerez2012}, since the MINRES algorithm requires the computation of a 2-norm \cite{Boland2008}. Alternatively, the column-sweep circuit from \cite{Rubagotti2016} could be efficiently mapped to the multiply-accumulate modules that compose the DSP blocks of many new FPGAs. These modules are designed to allow fast computations, and don't use the logic fabric to implement the computation circuitry.

Alternatively, if the algorithm being implemented has inherent parallelizations, then those can be used to inform the choice of multiplication circuits. This can be seen in the implementation of the Fast Gradient method by Jerez et.\ al.\ in \cite{Jerez2014}, as discussed in Section \ref{sec:par:alg:fgm}. In that work, the authors implemented the row-sweep method because that allowed for the remaining row operations to be paralellized effectively.

\subsection{Conclusions}

Overall, the amount of parallelization that should be used boils down to a tradeoff between the computational time and the area utilized for the algorithm on the device. In general, the more parallelization that is implemented, the more area and resources will be required. In \cite{Jerez2014}, the number of resources required for the computational circuit (which was essentially the matrix-vector multiplier) grew exponentially with the level of parallelization. For a square matrix of size 40, 42 multipliers were required for a single copy of the circuit. That number grew to 1344 multipliers for 32 separate copies of the circuit (which used all multipliers present on the chip).


