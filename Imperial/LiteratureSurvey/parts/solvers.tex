%!TEX root = ../LitSurvey.tex
\section{QP Solver Implementation Mediums}

\subsection{CPU}

One popular avenue for implementing MPC on embedded systems is using CPUs/embedded processors. These implementations have been helped along through the creation of various code generation tools that allow the designer to specify properties of the problem being solved, and then have the source code for the optimization solver automatically generated. One of the most widely recognized code generation tools is CVXGEN \cite{Mattingely2010, Mattingley2009}. CVXGEN is based on the CVX tool suite for its problem syntax, and is built at a high-level where the designer inputs problem data and information, and then the tool generates a custom interior-point method solver in C code.

Another tool widely used for optimal control is the ACADO toolset \cite{Houska2011}. This toolset produces library-free (e.g.\ minimal dependency) code tailored to optimal control problems. It can be used not just for convex problems, but also other non-linear problems through methods such as SQP, multiple-shooting, etc. Other tools that generate code for optimization solvers include FORCES \cite{Zanelli2017}, VIATOC \cite{Kalmari2015}, qpOASES \cite{Kufoalor2015}, IP2GO \cite{Krank2017}, and $\mu$AO-MPC \cite{Zometa2013}.

Additionally, recent work has focused on exploiting the architectural properties of modern embedded CPUs to speed-up the computations in MPC \cite{Frison2014,Frison2015,Frison2017}. This work seeks to optimize the low-level linear algebra routines used in the solvers, such as the matrix-vector and matrix-matrix multiplications for the small dense matrices that are found in the MPC solvers.

\subsection{GPU}

Other works have examined the applicability of using Graphics Processing Units (GPUs) to solve the MPC problem \cite{Gade-Nielsen2012,Gade-Nielsen2014,Phung2017}. These works have targeted the GPU because of its large number of computational units that can execute in parallel. This parallelization opportunity has led to speedup in the computation time of the algorithm, and the ability to use a longer horizon length than relation to the CPU implementations.

\subsection{FPGA}

The main focus of this work is the discussion of the implementations of MPC on Field-Programmable Gate Arrays (FPGAs). These implementations have been explored for nearly 20 years, with some of the early work provided in \cite{Ling2006,Ling2008}. Since then, many different implementations have been created using a plethora of different QP solver algorithms (see Figure \ref{fig:alg:taxon} for an overview of the implementations discussed in this work, and Table \ref{tab:fpga:imp} for an overview of various implementations). Recently there has also been work on extending the code generation paradigm from CPUs to FPGAs, with SPLIT \cite{Shukla2017} and $\mu$AO-MPC \cite{Lucia2018}.

\begin{table}[h!]
    \footnotesize
    \makebox[\textwidth][c]{
    \begin{threeparttable}[b]
        \caption{Implementation properties for some FPGA implementations of MPC solvers}
        \label{tab:fpga:imp}
        \begin{tabular}{cc| >{\centering}p{1cm}c| >{\centering}p{1.5cm} >{\centering}p{1.5cm} >{\centering}p{2.5cm} >{\centering}p{1.5cm} | >{\centering}p{1.5cm} >{\centering}p{2.0cm} p{0.001cm}}
            \hline
            Year & Source & QP\newline Form & Algorithm\tnote{4} & Number Format & Design Entry & Clock\newline Frequency & Matrix Storage & QP\newline Size\tnote{1} & Solver\newline Time & \\
            \hline \hline
            2006 & \cite{Ling2006} & D & IP/CHOL & float32 & Handel-C & 20 MHz & BRAM & 3/0/60 & 23.7 ms\tnote{5} & \\
            2008 & \cite{Ling2008} & D & IP/CHOL & float32 & Handel-C & 25 MHz & - & 3/0/52 & 9.1 ms & \\
            2011 & \cite{Basterretxea2011} & D & IP/CHOL & fixed & AccelDSP & 20 MHz & BRAM & 3/0/6 & 120 $\mu$s & \\
            2011 & \cite{Wills2011} & D & IP/CG & float16.6 & VHDL & 70 MHz & - & 12/0/24 & $<$200 $\mu$s & \\
            2012 & \cite{Yang2012} & D & ASM & float/fixed & Verilog & 100 MHz & BRAM & 3/0/6 & 20 $\mu$s & \\
            2012 & \cite{Wills2012} & D & ASM & float7 & VHDL & 70 MHz & BRAM & 12/0/24 & $<$30 $\mu$s & \\
            2014 & \cite{Peyrl2014} & D & FGM & fixed27.25 & VHDL & 120 MHz & - & 15/0/30 & $<$0.49 $\mu$s & \\
            2014 & \cite{Liu2014} & S & IP/CHOL & float32 & - & 200 MHz & BRAM & 300/-/600 & $<$4 ms & \\
            2014 & \cite{Hartley2014} & S & IP/MINRES & float32 & VHDL & 250 MHz & BRAM & 377/-/408 & $<$ 12 ms & \\
            2014 & \cite{Jerez2014} & D & FGM & fixed & VHDL & 400/230\tnote{2} ~MHz & - & 40/0/80  & 0.53/0.91\tnote{6} $\mu$s & \\
            2014 & \cite{Jerez2014} & S & ADMM & fixed & VHDL & 400/230\tnote{2} ~MHz & - & 216/-/- & 4.9/8.52\tnote{6} $\mu$s & \\
            2015 & \cite{Dang2015} & S & ADMM & fixed32.17 & - & - & - & 120/80/250 & 215 ms\tnote{5} & \\
            2016 & \cite{Rubagotti2016} & D & DGP & fixed32.16 & Simulink & 100 MHz & DRAM\tnote{3} & 20/0/208 & 239 $\mu$s & \\
            2017 & \cite{Zhang2017} & S & ADMM & float32 & VHDL & 340 MHz & BRAM & 204/-/- & 46.1 $\mu$s & \\
%            20 & \cite{} & & & & & & & & & \\           
        \end{tabular}
        \begin{tablenotes}
            \item - Indicates data that was not reported
            \item[1] Decision Variables/Equality Constraints/Inequality Constraints
            \item[2] 400 MHz on the Xilinx Virtex 6, 230 MHz on the Xilinx Spartan 6
            \item[3] Distributed RAM
            \item[4] ASM - Active-Set Method, IP - Interior-Point, FGM - Fast Gradient Method, ADMM - Alternating Direction Method of Multipliers, CHOL - Cholesky solver, CG - Conjugate Gradient solver, MINRES - Minimum Residual solver
            \item[5] Fully sequential implementation
            \item[6] Best case time reported
        \end{tablenotes}
    \end{threeparttable}
    }
\end{table}
