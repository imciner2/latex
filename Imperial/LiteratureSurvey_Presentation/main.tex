\documentclass[presentation]{beamer}
%\documentclass[handout]{beamer}

\usecolortheme{Imperial}
 
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{epstopdf}

\usepackage{algpseudocode}
\usepackage{algorithm}

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{tikz}

% complying UK date format, i.e. 1 January 2001
\usepackage{datetime}
\let\dateUKenglish\relax
\newdateformat{dateUKenglish}{\THEDAY~\monthname[\THEMONTH] \THEYEAR}

% Imperial College Logo, not to be changed!
\institute{\includegraphics[height=0.7cm]{Imperial_1_Pantone_solid.eps}
           \includegraphics[height=0.7cm]{hipeds.png}}

% Add the slide number to the footer
\addtobeamertemplate{footline}{}{%
\usebeamerfont{footline}%
\usebeamercolor{footline}%
\hfill\raisebox{5pt}[0pt][0pt]{\insertframenumber\quad}}

% -----------------------------------------------------------------------------

%Information to be included in the title page:
\title{FPGA Architectures for Linear Model Predictive Control}
\subtitle{A Survey of Prior Work}
\author{Ian McInerney}
\date{March 19, 2018}

\begin{document}
 
\frame{\titlepage}

\begin{frame}
	\frametitle{Outline}
    \tableofcontents
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{What is Model Predictive Control (MPC)}

\subsection{Overview}

\begin{frame}
	\frametitle{MPC - Overview}

    \begin{block}{Definition}
        A method of implementing optimal control of a system using a physics model to predict the system's response in the future to control inputs.
    \end{block}

    \begin{itemize}
        \item Choose the next control input based on solution to an optimization problem\\
        \item Can include constraints on input, state-space \\
        \item Many different variants: Minimum-Time, Minimum-Fuel, LQR, etc.
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{MPC - Problem}
    
    \begin{block}{Generic Problem}
        \begin{align*}
        \underset{\bar{u}, \bar{x}}{\text{min}} \quad & g(\bar{x}, \bar{u}, x_0)\\
        \text{s.t.} \quad & x_{k+1} = f(x_k, u_k)  & k=0, \dots N-1 \\
        & h(u_k, x_k, x_0) \leq 0 & k=0, \dots N 
        \end{align*}
    \end{block}
    \begin{align*}
        \bar{x} &= \begin{bmatrix}
        x_1 & x_2 & x_3 & \cdots & x_N
        \end{bmatrix}^T \\
        \bar{u} &= \begin{bmatrix}
        u_0 & u_1 & u_2 & \cdots & u_{N-1}
        \end{bmatrix}^T
    \end{align*}
\end{frame}


\begin{frame}
    \frametitle{MPC - Problem}

    \begin{block}{LQR Problem}
        \begin{align*}
        \underset{\bar{u}, \bar{x}}{\text{min}} \quad & \norm{x_N}^2_P + \sum_{k=0}^{N-1} \norm{x_k}^2_Q + \norm{u_k}^2_R \\
        \text{s.t.} \quad & x_{k+1} = A x_k + B u_k & k=0, \dots N-1 \\
        & F u_k \leq 0  & k=0, \dots N-1 \\
        & J x_k \leq 0  & k=1, \dots N 
        \end{align*}
    \end{block}

    \begin{itemize}
        \item If $P,Q, R \succeq 0$, this is a convex Quadratic Program (QP)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{MPC - Solution Methods}
    
    \begin{block}{Explicit MPC}
        \begin{itemize}
            \item Separate state-space into regions
            \item Precompute control law for each region at design-time
            \item Run-time selection of control law based on region
            \newline \newline
            \item Advantages:
            \begin{itemize}
                \item Fast \& low-complexity run-time implementation
            \end{itemize}
            \item Disadvantages:
            \begin{itemize}
                \item Memory required grows exponentially
            \end{itemize}
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}
\frametitle{MPC - Solution Methods}

\begin{block}{Implicit MPC}
    \begin{itemize}
        \item Solve optimization problem at each sampling instant
        \newline \newline
        \item Advantages:
        \begin{itemize}
            \item Lower memory footprint
        \end{itemize}
        \item Disadvantages:
        \begin{itemize}
            \item Computationally intensive
        \end{itemize}
    \end{itemize}
\end{block}

\end{frame}

\subsection{Real-Time Implementation}

\begin{frame}
    \frametitle{Real-Time Implementation}
    
    \begin{block}{CPU}
        \begin{itemize}
            \item Fixed data-path sizes \& representations
            \item Inherently sequential operations
            \newline
            \item Architecture agnostic code generation for solvers
        \end{itemize}
    \end{block}

    \begin{block}{GPU}
    \begin{itemize}
        \item High level of parallelism for supported operations
        \item Usually only supports double/single precision floating-point
        \item Very power \& thermal intensive
        \newline
        \item Limited Research from MPC community
    \end{itemize}
\end{block}


\end{frame}

\begin{frame}
    \frametitle{Real-Time Implementation}

    \begin{block}{FPGA}
        \begin{itemize}
            \item Reconfigurable computational fabric
            \item Custom datapath \& representations
            \item Low power
            \newline
            \item Intensive research in the MPC community
            \begin{itemize}
                \item Parallelizations
                \item Number representations
                \item Memory Allocation/Usage
            \end{itemize}
            \item Results showed sampling times of 1 MHz are possible
        \end{itemize}
        
    \end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Quadratic Programming Algorithms}

\begin{frame}
\frametitle{Algorithm Taxonomy}
\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/algTaxon.png}
\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Interior-Point Methods}

    \begin{block}{Overview}
        \begin{itemize}
            \item Solve constrained QPs by solving the non-linear KKT system.
            \item Perform Newton's iteration to solve the KKT system
            \item Handle inequality constraints
        \end{itemize}
    \end{block}
%
%    \begin{block}{Constraint Handling}
%        \begin{itemize}
%            \item Barrier-Penalty Method
%            \item Primal-Dual Method
%        \end{itemize}
%    \end{block}

    \begin{block}{Computation Steps}
        \begin{enumerate}
            \item[1.] Prepare linear system
            \item[2.] Solve linear system
            \item[3.] Updating of variables
        \end{enumerate}
    \end{block}

\end{frame}

%\begin{frame}
%    \frametitle{IPM - Computations}
%
%    \begin{minipage}{0.53\linewidth}
%    
%        \algrenewcommand\algorithmicrequire{
%            \textbf{Input:}
%        }
%        \footnotesize
%        
%        \textbf{Steps in 1 PDIP iteration:}
%        
%        \begin{algorithmic}[1]
%            
%            \State $\begin{bmatrix}
%            H + G^T W_k G & F^T \\
%            F & 0
%            \end{bmatrix} \begin{bmatrix}
%            \Delta \theta_k \\ \Delta \nu_k
%            \end{bmatrix} = 
%            \begin{bmatrix}
%            r_k^{\theta} \\ r_k^{\nu}
%            \end{bmatrix}$
%     %       \Comment Solve the linearized KKT system
%            
%            \State Compute $\Delta \lambda_k$, $\Delta s_k$
%            
%            \State $\alpha_k \gets \left\{ \begin{array}{cl}
%            \underset{ (0,1] }{\text{min}} \quad & \alpha \\
%            \text{s.t.} \quad & \lambda_k + \alpha \Delta \lambda_k > 0 \\
%            & s_k + \alpha \Delta s_k > 0
%            \end{array} \right\}$
%%            \Comment Compute step size
%            
%            \State $\begin{bmatrix}
%            \theta_{k+1} \\ \nu_{k+1} \\ \lambda_{k+1} \\ s_{k+1}
%            \end{bmatrix} = \begin{bmatrix}
%            \theta_{k} \\ \nu_{k} \\ \lambda_{k} \\ s_{k}
%            \end{bmatrix} + \alpha
%            \begin{bmatrix}
%            \Delta \theta_{k} \\ \Delta \nu_{k} \\ \Delta \lambda_{k} \\ \Delta s_{k}
%            \end{bmatrix}$
%            
%        \end{algorithmic}
%    \end{minipage}
%    \begin{minipage}{0.45\linewidth}
%        \begin{block}{}
%        \begin{itemize}
%            \item 3 main phases
%            \begin{enumerate}
%                \item[1.] Prepare linear system
%                \item[2.] Solve linear system
%                \item[3.] Updating of variables
%            \end{enumerate}
%        \end{itemize}
%        \end{block}
%    \end{minipage}
%\end{frame}

\begin{frame}
    \frametitle{IPM - Scaling}
    
    \begin{block}{Computational Scaling}
        \centering
        \begin{tabular}{cc}
            General Solver & $O(N^3 (n+m)^3)$ \\
            MINRES & $O(N^2(n+m)^3)$ \\
            Specialized Cholesky & $O(N (n+m)^3)$ \\
        \end{tabular}
        
    \end{block}

    \begin{block}{Iteration Counts}
        \begin{itemize}
            \item Theoretical bound is not tight (Upwards of 10,000)
            \item Experiments show convergence in 10-20 iterations independent of problem size
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{IPM - Number Representation}
    
    \begin{block}{Floating-Point}
        \begin{itemize}
            \item Most MPC implementations utilize floating-point IPM
        \end{itemize}
    \end{block}

    \begin{block}{Fixed-Point}
        \begin{itemize}
            \item No generic bounds on the variable size in IP iterations
            \item MINRES Linear Solver
            \begin{itemize}
                \item Use a preconditioner to bound the variables
                \item Stable under round-off errors
            \end{itemize}
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Active-Set Methods}
    
    \begin{block}{General Idea}
        Find the set of constraints that are active at the optimal point by iteratively trying different sets until all constraints are satisfied.
    \end{block}

    \begin{block}{Implementation}
        \begin{itemize}
            \item Finite convergence - Number of possible iterations grows exponentially with constraints
            \item Utilize smart update rules for adding/removing from the active-set
            \item Experiments show number of iterations grows $O(N)$ with problem size and number of constraints
        \end{itemize}
    \end{block}
    
\end{frame}

\begin{frame}
    \frametitle{First-Order Methods}
    
    \begin{block}{Overview}
        Only use derivative information to find the optimal point
    \end{block}

    \begin{block}{Computation Steps}
        \begin{itemize}
            \item[1.] Direction
            \item[2.] Projection
            \item[3.] Acceleration
        \end{itemize}
    \end{block}

%    \begin{block}{Sample Algorithms}
%        \begin{itemize}
%            \item Steepest Descent
%            \item Conjugate Gradient
%            \item Fast Gradient Method
%            \item Alternating Direction Method of Multipliers
%            \item Projected Gradient Methods
%            \item Subgradient Methods
%        \end{itemize}
%    \end{block}
\end{frame}

%\begin{frame}
%    \frametitle{Fast Gradient Method}
%    
%    
%    
%    \begin{minipage}{0.7\linewidth}
%    \algrenewcommand\algorithmicrequire{
%        \textbf{Input:}
%        }
%    
%    \begin{algorithmic}[1]
%        \Require $L = \lambda_{max}(H_F)$, $\mu = \lambda_{min}(H_F)$
%        \Require $z_0 \in \mathcal{K}$, $y_0 = z_0$
%        
%        \State $\beta = \frac{\sqrt{L} - \sqrt{\mu}}{\sqrt{L} + \sqrt{\mu}}$
%        
%        \For {$i=0$ to $I_{max}-1$}
%        \State $t_i = (I - (1/L)H_{F}) y_i - (1/L) \Phi x$
%        
%        \State $z_{i+1} = \pi_{\mathcal{K}}(t_i)$
%        
%        \State $y_{i+1} = (1+ \beta)z_{i+1} - \beta z_i$
%        \EndFor
%    \end{algorithmic}
%    \end{minipage}
%    \begin{minipage}{0.28\linewidth}
%        \begin{block}{}
%            3 steps:
%            \begin{itemize}
%                \item[1.] Direction
%                \item[2.] Projection
%                \item[3.] Acceleration
%            \end{itemize}
%        \end{block}
%    \end{minipage}
%    
%\end{frame}

\begin{frame}
    \frametitle{Fast Gradient Method}
    
    \begin{block}{Complexity}
        Design-time bound on number of iterations required to reach a specified tolerance
    \end{block}

    \begin{block}{Number Representation}
        Fixed-Point Representation
        \begin{itemize}
            \item Proved stable under presence of round-off errors
            \item Design-time formulas to determine number of bits given a specified error bound
        \end{itemize}
    \end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Formulations of MPC}

\begin{frame}
    \frametitle{MPC Problem Formulations}
    
    3 possible MPC problem formulations
    \begin{itemize}
        \item Condensed - Remove the states from the problem
        \item Sparse - All variables are in the problem
        \item Variable-Sparsity - In between the two others
    \end{itemize}
\end{frame}

%\begin{frame}
%    \frametitle{Condensed MPC}
%    
%    Reformulate prediction matrix to remove the states from the optimization variables
%    \begin{equation*}
%    \label{eq:mpc:cond:pred}
%    \bar{x} = 
%    \underbrace{
%        \begin{bmatrix}
%        B & 0 & 0 & & 0\\
%        AB & B & 0 & & 0\\
%        A^2b & AB & B & & 0 \\
%        \vdots & & & \ddots & \vdots\\
%        A^{N-1}B & A^{N-2}B & A^{N-3}B & \cdots & B
%        \end{bmatrix}}_{=\Gamma}  \bar{u}
%    + \underbrace{
%        \begin{bmatrix}
%        A \\
%        A^2 \\
%        A^3 \\
%        \vdots \\
%        A^{N}
%        \end{bmatrix}}_{=\Phi} x_0
%    \end{equation*}
%
%\end{frame}

\begin{frame}
    \frametitle{Condensed MPC}
    
    \begin{align*}
    \underset{\bar{u}}{\text{min}} \quad & \bar{u}^T \underbrace{(\Gamma^T \bar{Q} \Gamma + \bar{R})}_{=H} \bar{u} + 2 x_0^T \underbrace{(\Gamma^T \bar{Q} \Phi)}_{=G} \bar{u} \\
    \text{s.t.} \quad & J u_k \leq 0  & k=0, \dots N-1
    \end{align*}
    
    \begin{block}{Advantages}
        \begin{itemize}
            \item Remove optimization variables ($Nm$ instead of $N(n+m)$)
            \item No equality constraints
        \end{itemize}
    \end{block}


    \begin{block}{Disadvantages}
        \begin{itemize}
            \item Dense Hessian matrix
            \item Storage space for Hessian grows order $N^2$
            \item MVM data-path grows order $N$
        \end{itemize}
    \end{block}
\end{frame}

%\begin{frame}
%	\frametitle{Non-Condensed (Sparse) MPC}
%    Leave prediction as equality constraint
%    \begin{equation*}
%        \theta = \begin{bmatrix}
%        x_1 & u_0 & x_2 & u_1 & \cdots & x_N & u_{N-1}
%        \end{bmatrix}^{T}
%    \end{equation*}
%    \begin{equation*}
%    \label{eq:mpc:sparse:pred}
%    0 = 
%    \underbrace{
%        \begin{bmatrix}
%        I & -B &  0 &  0 &  0 &\cdots & 0 & 0 & 0 & 0\\
%        -A &  0 &  I & -B &  0 & & 0 & 0 & 0 & 0\\
%        0 &  0 & -A &  0 &  I & & 0 & 0 & 0 & 0\\
%        0 &  0 &  0 &  0 & -A & & 0 & 0 & 0 & 0\\
%        \vdots & & & & & \ddots & & & & \vdots\\
%        0 &  0 &  0  & 0 & 0 & \cdots &  I & -B & 0 &  0\\
%        0 &  0 &  0  & 0 & 0 & \cdots & -A &  0 & I & -B
%        \end{bmatrix}}_{=G}  \theta
%    - \underbrace{
%        \begin{bmatrix}
%        A \\
%        0 \\
%        0 \\
%        0 \\
%        \vdots \\
%        0 \\
%        0
%        \end{bmatrix}}_{=E} x_0
%    \end{equation*}
%\end{frame}

\begin{frame}
    \frametitle{Non-Condensed (Sparse) MPC}
    \begin{block}{Advantages}
        \begin{itemize}
            \item Banded KKT matrix
            \item Storage space required grows order $N$
            \item MVM data-path remains constant
        \end{itemize}
    \end{block}

    \begin{block}{Disadvantages}
        \begin{itemize}
            \item More optimization variables
            \item Optimization problem includes equality constraints
        \end{itemize}
    \end{block}
\end{frame}


\begin{frame}
    \frametitle{Variable-Sparsity MPC}
    
    \begin{block}{}
        Induce a banded matrix in the condensed formulation
    \end{block}

    \begin{columns}
        \begin{column}{0.48\textwidth}
        \begin{block}{Deadbeat Controller}
            \begin{itemize}
                \item Use a linear controller that makes $A$ nilpotent
                \item $H$ is then banded, with bandwidth of the controllability index
            \end{itemize}
        \end{block}
        \end{column}
        \begin{column}{0.48\textwidth}
            \begin{block}{Variable Condensing}
                \begin{itemize}
                    \item Split the horizon into subintervals
                    \item Condense inside the subintervals, not between
                    \item Can vary between fully sparse and fully condensed
                \end{itemize}
            \end{block}
        \end{column}
    \end{columns}


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parallelization Opportunities}

\begin{frame}
    \frametitle{Parallelization Opportunities}
    
    Opportunities for parallelization exist at 2 levels:
    
    \begin{block}{Algorithmic Level}
        Exploit data-dependencies in the algorithm steps to parallize the iterations/iteration steps
    \end{block}


    \begin{block}{Computational Level}
        Parallelize the low-level arithmetic computations of the algorithm
        \begin{itemize}
            \item Matrix-Vector Multiplication
            \item Cholesky Factorization
        \end{itemize}
    \end{block}
\end{frame}
 
\begin{frame}
    \frametitle{Parallelizations - Algorithmic Level}
    
    \begin{block}{Overview}
        Algorithms that have minimal data dependence for computations inside an iteration have algorithmic level parallelizations that can be exploited
    \end{block}

    \begin{figure}
        \centering
        \includegraphics[width=0.8\linewidth]{images/par_fgmJerez2014.png}
        \caption{FGM implementation from Jerez et.\ al.\ 2014}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Parallelizations - Algorithmic Level}
    
    \begin{block}{Interior-Point Methods}
        Usually parallelize the linear system solution
        
        Leave the update \& system setup as sequential
    \end{block}
    
    \begin{block}{First-Order Methods}
        Parallelization highly dependent upon the constraint set present
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Parallelizations - Computational Level}
    
    \begin{block}{}
        The basic computational element for all the methods is a matrix-vector multiplication
    \end{block}

    \begin{minipage}{0.48\linewidth}
            \centering
            \includegraphics[width=\textwidth]{images/MVM_GPD.png}
            
            Column-Sweep Implementation\footnotemark
    \end{minipage}%
    \begin{minipage}{0.48\linewidth}
            \centering
            \includegraphics[height=12em]{images/MVM_ColMajor.png}
            
            Row-Sweep Implementation\footnotemark
    \end{minipage}
    
    \footnotetext[1]{Rubagotti et.\ al.\ 2016}
    \footnotetext[2]{Wills et.\ al.\ 2011}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Memory Architectures}

%\begin{frame}
%    \frametitle{FPGA Memory Regions}
%    
%    3 main FPGA memory regions:
%    \begin{itemize}
%        \item Distributed RAM
%        \item Block RAM
%        \item Off-chip Memory
%    \end{itemize}
%\end{frame}

\begin{frame}
    \frametitle{FPGA Memory Regions}

    \begin{block}{Distributed RAM}
        \begin{itemize}
            \item Flip-flops inside the LUTs
            \item Very small amount of storage available
        \end{itemize}
    \end{block}

    \begin{block}{Block RAM}
        \begin{itemize}
            \item Dedicated memory regions inside the FPGA
            \item Medium level of storage available (a few Mb)
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{FPGA Memory Region - Selection}
    
    \begin{block}{Distributed RAM}
        \begin{itemize}
            \item Usually used for the computations products
            \item Sometimes used for high-locality of the matrix coefficients
        \end{itemize}
    \end{block}

    \begin{block}{Block RAM}
        \begin{itemize}
            \item Heavily used in MPC for storing the matrices
        \end{itemize}
    \end{block}

    Results show a doubling of resource usage when switching matrices from BRAM to DRAM
\end{frame}

\begin{frame}
    \frametitle{Matrix Storage}
    
    \begin{block}{Structure Explotation}
        Sparse formulation has a banded structure that can be exploited to reduce the memory footprint from $O(N^2)$ to $O(N(2n+m))$
    \end{block}

    \begin{figure}
        \includegraphics[width=0.7\textwidth]{images/CDS_Jerez2012.png}
    \end{figure}

\footnotetext[1]{Jerez et.\ al. 2012}
\end{frame}

\begin{frame}
    \frametitle{Matrix Storage}
    
    \begin{block}{Linear MPC Structure Exploitation}
        A further savings of 75\% can be attained by also exploiting the structure of the LTI MPC problem
        \begin{itemize}
            \item Similar rows - Many rows of the matrix will be repeated
            \item Similar blocks - Many of the blocks will be repeated in the matrix
        \end{itemize}
    \end{block}
\end{frame}

\section{Conclusion}

\begin{frame}
    \frametitle{Conclusion}

    \begin{block}{Key Points}
        \begin{itemize}
            \item Algorithm choice drives implementation
            \item Effects of implementation on algorithm must be considered
            \item Mainly parallelize low-level computations
            \item Structure exploitation saves memory
        \end{itemize}
    \end{block}

    \begin{block}{Future Directions}
        \begin{itemize}
            \item How can the FPGA design be streamlined?
            \item How do these results extend to the nonlinear MPC problem?
        \end{itemize}
        
    \end{block}
    
\end{frame}
 
\end{document}

