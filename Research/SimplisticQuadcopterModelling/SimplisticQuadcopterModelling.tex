% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: August 25, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
% These packages are all incorporated in the memoir class to one degree or another...

% Include custom commands
\usepackage{Unit-Declaration}
\usepackage{Sci-Formatting}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{}\rhead{}
\lfoot{EE 475}\cfoot{Simplistic Quadrotor Modelling}\rfoot{\thepage}

\title{Simplistic Quadrotor Modelling}
\author{EE 475}


%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%\graphicspath{{Images/}}

%%% END Article customizations

%%% The "real" document content comes below...

\begin{document}
	
\maketitle

\section{Overview}

The model you are deriving is for a quadrotor, specifically for the CrazyFlie 2.0 (A small 4-propeller flying vehicle). Each propeller has its own motor, and with good controllers, a quadrotor can hover without flipping over or spinning uncontrollably. Designing those controllers is difficult though. As you saw earlier it is very difficult to hand-tune a PID controller to stabilize the pitch and roll axis of the quadrotor. There is another option when tuning the PID controllers, and that is to derive the contants using a physics model of the system.

Your task is to derive a simple physics model for the quadrotor's pitch, roll and yaw axis.


\section{Physical Description}

The basic design of the quadrotor is shown in figure \ref{fig:quadrotor}.

A few important things to note:
\begin{itemize}[noitemsep]
	\item On this quadrotor, the propellers are located off-axis, so each propeller affects both pitch and roll
	\item The coordinate space has positive z in the downward direction and positive x facing the front
	\item Pitch is the angle of rotation about the x axis, and is symbolized by $\theta$
	\item Roll is the angle of rotation about the y axis, and is symbolized by $\phi$
	\item Yaw is the angle of rotation about the z axis, and is symbolized by $\psi$
	\item Propellers diagonally opposite from eachother spin the same direction, propellers directly next to eachother spin opposite directions
\end{itemize}

\begin{figure}[h!]
	\center
	\includegraphics[scale=1.0]{QuadrotorOrientation.png}
	\caption{Axis diagram, rotor labeling, and propeller angular velocity directions of the quadrotor}
	\label{fig:quadrotor}
\end{figure}

\newpage

\section{Physical Constants}

\begin{table}[h!]
	\center
	\begin{tabular}{|c|c|p{10cm}|}
		\hline Constant & Units & Description\\
		\hline $k_t$ & $\frac{N}{\sfrac{rad^2}{sec^2}}$ & The propeller thrust force constant\\
		\hline $k_d$ & $\frac{N}{\sfrac{rad^2}{sec^2}}$ & The propeller drag force constant\\
		\hline $k_v$ & $\frac{\sfrac{rad}{sec}}{V}$ & The motor voltage constant\\
		\hline $v_b$ & $V$ & The battery voltage\\
		\hline $J_{xx}$ & $kgm^2$ & Rotational inertia about the x-axis\\
		\hline $J_{yy}$ & $kgm^2$ & Rotational inertia about the y-axis\\
		\hline $J_{zz}$ & $kgm^2$ & Rotational inertia about the z-axis\\
		\hline $l$ & $m$ & Distance from center of mass to propeller hub\\
		\hline
	\end{tabular}
	\caption{Physical contants used in this model}
	\label{tab:physicalConstants}
\end{table}

\section{Requirements}

The following are the requirements for your completed model:
\begin{itemize}[noitemsep]
	\item Derive 3 different models:
	
	\begin{tabular}{c l}
		1: & Pitch\\
		2: & Roll\\
		3: & Yaw
	\end{tabular}
	
	\item Each model should only have 1 input
	\item Model input should be normalized motor command (percent of battery voltage to apply to motor)
	\item Each model should have 2 outputs:
	
	\begin{tabular}{c l}
		1: & Angular Velocity\\
		2: & Angular Position
	\end{tabular}
	\item Model output should be in radians per second and radians respectively

\end{itemize}

\section{Modelling Advice}

\subsection{System Inputs}

For most systems the default idea is to view each motor/actuator as a system input. In this case that would lead to 4 inputs for the system. This is a problem for PID design though, because PIDs cannot natively control a system with multiple inputs. You will find that for this system you can create a \textit{meta-actuator} that represents all 4 motors as one input to the system. This \textit{meta-actuator} is simply a linear composition of the 4 motors, and will allow you to model the system as having only 1 input.

This actuator can be derived from the orientation of each propeller and their affect on the axis being controlled. To generate a positive pitch on the quadcopter, motors 1 \& 2 must slow down and motors 3 \& 4 must speed up. To generate a positive roll on the quadcopter, motors 2 \& 3 must slow down and motors 1 \& 4 must speed up. Each of these actions is decoupled from the yaw orientation, because in each case a pair of opposite spinning propellers is changing speeds (so their generated torques will cancel to 0). To generate a positive yaw, motors 2 \& 4 must slow down and motors 1 \& 3 must speed up.

This relationship can be written in matrix form (given in equation \ref{eg:mixingMatrix}), called a mixing matrix. The inputs to the matrix are the desired pitch, roll, yaw, and Z command with the outputs being the relative motor speeds to generate those commands.

\begin{equation}
	\left[\begin{matrix}
	m_1\\ m_2\\ m_3\\ m_4
	\end{matrix}\right]
	 = 
	\left[\begin{matrix}
	-1 &  1 & 1 & 1\\
	-1 & -1 & -1 & 1\\
	 1 & -1 & 1 & 1\\
	 1 &  1 & -1 & 1
	\end{matrix}\right]
	\left[\begin{matrix}
	u_{\theta}\\ u_{\phi} \\ u_{\psi}\\ u_{F_z}
	\end{matrix}\right]
	\label{eg:mixingMatrix}
\end{equation}

The mixing matrix given above is full rank (all columns are linearly independent). That means that it is a unique mapping from the inputs to the outputs, and its inverse can be used to map the outputs back to the inputs. Based upon this we can argue that the inputs act independently from eachother (so an input in pitch will not affect roll or yaw).

The only wrinkle in this argument is the $u_{F_z}$ input. In the above argument we claim that pitch and roll will not affect the $u_{F_z}$. This is true in the body coordinates of the quadcopter, but is not true in inertial coordinates of the Earth. In reality, a change in pitch or roll will alter the amount of force directed in the Earth z-axis and cause a change in the quadcopter's z-position in the Earth reference frame (and also a change in the quadcopter's x and y position in the Earth frame). For this model, the mapping between the two reference frames is accomplished using simple trigonometric functions.

\subsection{Propeller Thrust Force Generation}

Propellers generate thrust in proportion to the square of their rotational velocity, ie. $F_p = k_t \omega^2$. However, this squared term would make this a non-linear system, which makes subsequent analysis and control design more difficult. There is a process called Linearization which takes a non-linear system and creates a linear system (this is a topic discussed in EE 476). This derived linear system can then be used for analysis and control design.

For this model, you will design a linear system from the start. To help you with this, you should make the assumption that the generated thrust is related to the propeller rotational velocity through equation \ref{(eq:linearPropellerThrust)}.

\begin{equation}
	F_p = \frac{1}{2} k_t \omega
	\label{(eq:linearPropellerThrust)}
\end{equation}


\subsection{Propeller Drag Force Generation}

As propellers spin, they generate a force in their plane of rotation. On a quadrotor, that force will generate a torque about the center of mass. The direction of this torque is in the direction opposite the propellers angular velocity.

Similar to the thrust force, the drag force has a non-linear relationship with angular velocity ($T_d = - \frac{1}{2} k_d \omega^2$). For the purposes of this model though, use the relation given in equation \ref{(eq:linearPropellerDrag)}.

\begin{equation}
	T_d = - \frac{1}{2} k_d \omega
	\label{(eq:linearPropellerDrag)}
\end{equation}

\end{document}
