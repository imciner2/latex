#!/bin/bash
# Clear out all the temp Latex files

# A list of all the extensions to delete
LATEX_EXTENSIONS="*.aux *.log *.synctex *.synctex.gz *.bbl *.blg *.dvi *.ps *.tps *.out *.lof *.lot *.tex.* *.snm *.toc *.nav"

# Delete the files
echo "Deleting the following temporary Latex files:"
for filename in $LATEX_EXTENSIONS
do
	find . -name $filename -type f
	find . -name $filename -type f -delete
done
