% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: October 27, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}
% These packages are all incorporated in the memoir class to one degree or another...

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{datetime}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{Proof}\rhead{}
\lfoot{Draft: \today\ \currenttime}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\bibliographystyle{IEEEtran}

\newtheorem{assumption}{Assumption}[section]
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\graphicspath{{./images/}}

%%% END Article customizations


\begin{document}

\section{Proof}

A key component of a useful trilateration scheme is the ability for the algorithm to either account for errors in the node position or measured distance, or somehow minimize them. These errors can be introduced in many ways, for example some situations where an error in node position is introduced are:
\begin{itemize}[nolistsep, noitemsep]
	\item Error in position system (ie. GPS error)
	\item Constant movement of the measurement node (ie.  quadcopter in flight)
\end{itemize}

\begin{assumption}
	The measured node location, $P'$, and the actual node location, $P$, are not the ends of a chord of the circle centered at the point $X$.
	\label{assump:noChord}
\end{assumption}

This assumption is required to be able to map a change in the point $P$ to a change in the distance $PX$. When the error is exactly a chord of the circle, then the distance $PX$ will not change with the added error. A case violating assumption \ref{assump:noChord} is shown in figure \ref{fig:chordAssumption}. Making this assumption is not limiting to the problems this method can be applied to, because in actual implementation the error $\Delta P$ will be a probabilistic error, so as the location is sampled with time, the observations will have slightly different $\Delta P$ values, so the assumption will not be consistently violated over time.

\begin{figure}[h!]
	\center
	\includegraphics[scale=.30]{chordExample.eps}
	\caption{Example of a situation violating assumption \ref{assump:noChord}.}
	\label{fig:chordAssumption}
\end{figure}



\begin{lemma}
	Let a circle with radius $r$ be centered at $\hat{P}$, and a point on the circle be $X$. If the center of the circle is perturbed by a perturbation $\Delta P$ satisfying assumption \ref{assump:noChord}, while keeping the circle passing through $X$, then the new radius $\hat{r}$ will be different from the original radius $r$.
	\label{lemma:circleRadius}
\end{lemma}

Lemma \ref{lemma:circleRadius} basically says that any error in the node position measurement can be interpreted as an error in the measured distance. This result may seem trivial, but the proof is provided below for completeness.

\begin{proof}
	Assume the situation shown in figure \ref{fig:circleRadiusLemma1} with the following definitions: $\Delta P :=$ Error in position, $P :=$ Real position, $\hat{P} :=$ Measured position, $\hat{r} :=$ Radius with position error, $r :=$ Radius with no position error.
	\begin{figure}[h!]
		\center
		\includegraphics[scale=0.30]{circleRadiusLemma1.eps}
		\caption{Geometric overview for proving lemma \ref{lemma:circleRadius}.}
		\label{fig:circleRadiusLemma1}
	\end{figure}
	
	\noindent First, find the $\gamma$ where assumption \ref{assump:noChord} is violated, namely $r = \hat{r}$. From law of cosines:
	$$
	\hat{r}^2 = r^2 + \norm{\Delta P}^2 - 2 r \norm{\Delta P} \cos{\gamma}
	$$
	Since $r = \hat{r}$, this becomes:
	$$
	2 r \cos{\gamma} = \norm{\Delta P} 
	$$
	So the angle at which the assumption is violated is:
	$$
	\cos{ \hat{\gamma} } = \frac{\norm{\Delta P}}{2 r} 
	$$
	The remainder of the proof is broken into three distinct cases:
	\begin{itemize}
		\item[(a)] $\gamma = 0^{\circ}, \gamma = 180^{\circ}$. This is the case when the position error is in the direction of the radius. The result here is trivial, any non-zero error produces a change in the radius.
		
		\item[(b)] $0 < \gamma < \hat{\gamma}$. This leads to: $\cos{\gamma} > \cos{ \hat{\gamma} }$ and $\norm{\Delta P} \neq 0$.\\
		From law of cosines:
		\begin{align*}
		\frac{r^2 -\hat{r}^2 + \norm{\Delta P}^2}{2 r \norm{\Delta P}} &=  \cos{\gamma}\\
		\frac{r^2 -\hat{r}^2 + \norm{\Delta P}^2}{2 r \norm{\Delta P}} &>  \cos{ \hat{\gamma} }\\
		\frac{r^2 -\hat{r}^2 + \norm{\Delta P}^2}{2 r \norm{\Delta P}} &>  \frac{\norm{\Delta P}}{2 r}\\
		r^2 - \hat{r}^2 &> 0\\
		r &> \hat{r}
		\end{align*}
		In this case, $r$ is strictly greater than $\hat{r}$.
		
		\item[(c)] $180 > \gamma > \hat{\gamma} $. This leads to: $\cos{\gamma} < \cos{ \hat{\gamma} }$  and $\norm{\Delta P} \neq 0$.\\
		This case follows the same format as (b), except the inequality is in the opposite direction. The final result is then:
		$$
		r < \hat{r}
		$$
		In this case, $r$ is strictly less than $\hat{r}$.
	\end{itemize}
	\qedhere
\end{proof}

\begin{theorem}
	The optimization problem proposed in equation \ref{eq:optimProblemGeneric} can handle errors in the measured distances.
	\label{thm:rangeErrorRobustness}
\end{theorem}
\begin{corollary}
	Under assumption \ref{assump:noChord}, the optimization problem proposed in equation \ref{eq:optimProblemGeneric} can handle errors in both the node position and the measured distances.
	\label{cor:positionRangeRobustness}
\end{corollary}
\begin{proof}
	The error present in the measured distance is handled in the optimization problem through the constraint. The $a$ variables can be thought of as a residual variable for the distance measurements (difference between the measured distance and the computed distance). The optimization problem is then a problem of minimizing the distance residuals.
	
	The corollary trivially follows from the theorem when lemma \ref{lemma:circleRadius} is applied and the errors meet assumption \ref{assump:noChord}.
\end{proof}

\end{document}
