% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: September October 14, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}
% These packages are all incorporated in the memoir class to one degree or another...

% Include custom commands
\usepackage{Unit-Declaration}
\usepackage{Sci-Formatting}

\usepackage{datetime}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{PhD Research Proposal}\rhead{Ian McInerney}
%\lfoot{Draft: \today\ \currenttime}\cfoot{\thepage}\rfoot{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

% Remove space around lists
%\setlist{nolistsep}

\bibliographystyle{IEEEtran}

%\graphicspath{{Images/}}

%%% END Article customizations

%%% The "real" document content comes below...

\begin{document}

%\begin{centering}
%	\section*{PhD Research Proposal}
%\end{centering}

\subsection*{Proposed Project:}
\textbf{Embedded Model Predictive Control Using Trajectory-Based Optimization Methods}\newline

%The concept of model predictive control has been around for decades, and is based upon human behavior. When controlling something manually, we naturally think about what will happen in the future and base our actions on that. For example, when driving a car you look farther down the road and estimate where the car will be in the future when determining how to move the steering wheel. We subconsciously do this all the time, regularly updating our actions based upon updated predictions.

Model Predictive Control (MPC) is a branch of optimal control that uses current data about a system to predict the system's eventual path. MPC utilizes numerical optimization methods to design a controller input at each time step that meets certain criteria. The current solver architectures in use do not update their result when new data is obtained, they instead start the decision process anew with every datapoint. \textbf{I propose implementing continually-revising solvers in model predictive control, so that the computerized process doesn't restart everytime but instead updates its previous result.} Additionally, I will explore the implementation of these solvers on embedded hardware with real-time constraints, and demonstrate their usage by implementing an MPC satellite attitude controller using them.
\begin{figure}[h!]
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[scale=0.6]{IP_Algorithm.png}
	\end{subfigure}
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[scale=0.6]{TBO_Algorithm.png}
	\end{subfigure}
	\caption{Comparison of solver architectures when run on 2 sequential problems. The 1st problem is defined by the shaded region, while the 2nd is an extension of the 1st region to the red lines. The current solvers restart completely on the 2nd run, whereas the proposed solver just updates its previous result.}
	\label{fig:solverComparison}
\end{figure}
\vspace{-0.4cm}

While this project will focus on applying the developed algorithms to Model Predictive Control, the principle behind the solvers developed in this project can also be applied to more problems in other areas such as:
\begin{itemize}[noitemsep,nolistsep]
	\item Adaptive signal processing
	\item Online resource allocation
	\item Optimal order execution in the financial industry
\end{itemize}

\subsection*{Model Predictive Control Overview}

When doing computerized Model Predictive Control, the problem is posed in an optimization framework where the controller seeks to minimize attributes of the predicted system response such as:
\begin{itemize}[noitemsep,nolistsep]
	\item The amount of energy expended controlling the system
	\item The difference between the system response and a desired response
	\item The time it takes for the system to respond
\end{itemize}
These attributes are calculated over a future timerange, called a horizon, which is usually finite. This optimization problem is solved at every new datapoint. So if new data is received 500 times per second, this optimization is done 500 times per second.

In the past, optimization solvers have had running-times on the order of minutes. This running-time is not a fatal roadblock for normal uses of optimization. Applications such as fitting a numerical model to collected experimental data or to scheduling workers in a shift-based schedule are done either once or infrequently, so they can tolerate delays in computations. On the other hand, \textbf{MPC starts the solver when new data is received, and the solver must finish before the next datapoint is collected so the new control can be applied to the system.} This is referred to as a hard real-time constraint. Recent research has led to solvers that are more efficient and faster than previous ones, allowing for MPC to be applied to systems with faster time constraints (such as systems requiring milli-second update rates) \cite{Jerez14}. While these advances have increased the utility of MPC, they still utilize algorithms that restart at every step.


\subsection*{Numerical Optimization Solvers}

Optimization solvers in the past have been designed with the common theme that they will run once. This design choice has led to solvers that start execution at a pre-set point and then work their way towards an optimum solution. This execution model can introduce inefficiencies when comparing computations across time points in MPC though. Algorithms such as Interior-point methods use no knowledge of previous optimal values when computing a new value (known as hot-starting). Research into the addition of hot-starting to the interior-point methods has been done with some success \cite{Shahzad2011}, \textbf{but those modifications carry with them a strong caveat that improper hot-starting can make the algorithm more inefficient than the original.}

My proposal is that instead of examining how to add hot-starting behavior to the existing iterative solvers, a new set of solvers based upon continually revising the previous solution of the optimization problem be explored. These solvers would be constantly running in the background (rather than starting and stopping), and therefore utilize prior knowledge of the optimal solution more effectively in their computation.

\subsection*{Proposed Method}

The proposed solvers can make use of trajectory-based optimization techniques, where a dynamical system is created that reaches equilibrium at the optimal solution. The solver's dynamical system will be constantly evolved in the background (using a numerical ODE solver), with the optimal solution given by the solver's states at equilibrium. The controller would then update the dynamical system with new parameters at the next time point, effectively creating a disturbance from equilibrium for the system. The dynamical system then seeks the new equilibrium point (which is the new optimal solution), starting from its last position.

Some research has been conducted into the use of trajectory-based methods as a supplement to normal iterative methods, such as using them as a replacement for the Newton's method inside an Interior-Point method in power-flow applications \cite{Milano2009,XiaohuiZhao2012}. There has also been some research in the derivation of continuous methods for linear programming \cite{Liao2010}. However there are very few research results on the application of a continuously-revising solver to a problem with hard real-time constraints, such as the MPC problem.

This proposed approach presents several advantages over current iterative solvers, including:
\begin{itemize}[noitemsep,nolistsep]
	\item Ability to update current solution when new data is available
	\item Leverage existing systems theory in their analysis and improvement
	\item Computationally simpler implementation using numerical Ordinary Differential Equation (ODE) methods
	\item Possible extension to distributed architectures (see \cite{Jing2014} for an example)
\end{itemize}

%This way the solver is using previous information about the optimal solution in its computations. Additionally, trajectory-based solvers can leverage existing theory in both the analysis of their operation (such as stability analysis), and in the improvement of their properties (such as convergence rate and stability region).

\subsection*{Proposed Implementation and Application}

\textbf{An additional aspect of the proposed research is to examine the implementation and application of this method on real-time systems with strict timing constraints.} Digital implementation of these solvers can be done using standard numerical ODE methods, such as the Runge-Kutta method solvers.

Implementation of the continuous solver using a Runge-Kutta method presents computational advantages. In traditional solvers, an inner part of the algorithm was solving a set of linear equations. These linear equation solvers introduce more computational complexity through their internal iteration scheme or problem preconditioning required to get convergence guarantees. An explicit Runge-Kutta method implementation would consist of repeated multiply-accumulate operations, with some added complexity in step-size computation. This implementation would effectively reduce the solver to a single-layer iterative method.

While the Runge-Kutta is the normal starting point for numerical ODE methods, there are many other methods in existance. \textbf{Part of this work will be to examine the tradeoffs inherent in the usage of different ODE solution methods with this solver}, and determine the most appropriate methods to use in specific cases.

To examine the applicability of this proposed solver, actual implementation on a testing platform will be conducted (similar to that done in \cite{Hartley2014}). This work will examine the application of the solver to the problem of real-time satellite attitude control under constraints. This application is based on two premises:
\begin{itemize}[noitemsep,nolistsep]
	\item Certain sensors must remain pointed in a given direction during attitude movements (such as radio antennas remaining pointed at Earth)
	\item Other sensors must not be pointed in a given direction during attitude movements (such as telescopes with sensitive optics not being aimed at the sun)
\end{itemize}
The goal is to transition the satellite from its current attitude to a final attitude using the least amount of fuel while respecting the constraints.

%\subsection*{Additional Benefits}

%\textbf{Investigation of the continuous solvers can offer additional benefits in two main categories: Application to other areas utilizing real-time optimization of changing data, and Analog computation.}

%Analog computation is a growing field due to the rise in popularity of neural computing and the availability of hardware for reconfigurable analog computation. The analog realm is naturally continuous, and modeled using ODEs. Therefore, optimization solvers in the analog realm would make use of ODE-based methods to solve optimization problems. Overall theoretical results from this project would be transferable to analog solvers of the same type.



%Another byproduct of this solver architecture is a possible extension to a distributed optimization solver. The addition of network topology information into the optimization problem derivation allows for the derived solver to be spread across multiple nodes . This will allow for per-actuator computation, where the MPC problem is solved at each actuator with input from its neighbors (provided actuators are distributed), or for distribution across multiple pieces of hardware if the problem size is large.

%In order to determine the optimal controller, a numerical optimization problem is solved at every time point. It is the solution time of this optimization problem that determines the rate a controller can operate at. Customarily, the optimization solvers haven't been designed to run on low-resource systems or to meet strict execution times. Recently, research has been conducted on improving the solver speed for MPC applications by utilizing dedicated hardware components for the solver \cite{Jerez14}.

%\newpage

\subsection*{References}

{\footnotesize
\renewcommand{\section}[2]{}
\bibliography{references.bib}
}
\end{document}
