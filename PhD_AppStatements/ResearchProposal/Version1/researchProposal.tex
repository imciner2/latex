% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: September 25, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
% These packages are all incorporated in the memoir class to one degree or another...

% Include custom commands
\usepackage{Unit-Declaration}
\usepackage{Sci-Formatting}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{}\rhead{}
\lfoot{Ian McInerney}\cfoot{PhD Research Proposal}\rfoot{\thepage}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\bibliographystyle{IEEEtran}

%\graphicspath{{Images/}}

%%% END Article customizations

%%% The "real" document content comes below...

\begin{document}

\begin{centering}
	\section*{PhD Research Proposal}
\end{centering}

\noindent Proposed project:\newline
\textbf{Embedded Model Predictive Control Using Trajectory-Based Optimization Methods}

The idea of Model Predictive Control (MPC) has been around for decades, but has customarily only been used in industrial environments where the process timescale is on the order of hours or minutes. The idea behind MPC is that the controller inputs given to the system should be decided upon using a prediction about what the system will do in the future. The controller uses information from the prediction (such as the predicted state trajectory and control inputs) to determine the controller's cost. The objective of MPC is to find a set of control inputs which will minimize the controller's costs over the prediction horizon.

In order to determine the optimal controller, a numerical optimization problem is solved at every time point. It is the solution time of this optimization problem that determines the rate a controller can operate at. Customarily, the optimization solvers haven't been designed to run on low-resource systems or to meet strict execution times. Recently, research has been conducted on improving the solver speed for MPC applications by utilizing dedicated hardware components for the solver \cite{Jerez14}.

These solvers all share a common trait: they begin execution at the time point, and then terminate once the optimal solution is found, repeating this cycle for every time point. This execution model can introduce inefficiencies when comparing computations across time points though. Algorithms such as Interior-point methods use no knowledge of previous optimal values when computing a new value (known as hot-starting). Research into the addition of hot-starting to the interior-point methods has been done with some success \cite{Shahzad2011}, but carries with it a strong caveat that improper hot-starting can make the algorithm more inefficient than the original.

I propose that instead of examining how to add hot-starting behavior to the existing iterative solvers, a new set of solvers based upon the continuous solution of the optimization problem be explored. These solvers would be constantly running in the background (rather than starting and stopping), and therefore utilize prior knowledge of the optimal solution more effectively in their computation.

The design of these solvers can make use of trajectory-based optimization techniques, where a dynamical system is created that reaches equilibrium at the optimal solution, to run. The solver's dynamical system will be constantly evolved in the background (via a Runge-Kutta method for example), with the optimal solution given by the solver's states at equilibrium. The controller would then update the dynamical system with new parameters at the next time point, effectively creating a disturbance from equilibrium for the system. The dynamical system then seeks the new equilibrium state, starting from its last position. This way the solver is using previous information about the optimal solution in its computations. Additionally, trajectory-based solvers can leverage existing theory in both the analysis of their operation (such as stability analysis), and in the improvement of their properties (such as convergence rate and stability region).

Implementation of these solvers using a Runge-Kutta method also has computational advantages. In traditional solvers, an inner part of the algorithm was solving a set of linear equations. These linear equation solvers introduce more computational complexity through their internal iteration scheme or problem preconditioning required to get convergence guarantees. An explicit Runge-Kutta method implementation would consist of repeated multiply-accumulate operations, with some added complexity in step-size computation. This implementation would effectively reduce the solver to a single-layer iterative method.

Another byproduct of this solver architecture is a possible extension to a distributed optimization solver. The addition of network topology information into the optimization problem derivation allows for the derived solver to be spread across multiple nodes (see \cite{Jing2014} for an example). This will allow for per-actuator computation, where the MPC problem is solved at each actuator with input from its neighbors (provided actuators are distributed), or for distribution across multiple pieces of hardware if the problem size is large.

Some research has been conducted into the use of trajectory-based methods as a supplement to normal iterative methods, such as using them as a replacement for the Newton's method inside an Interior-Point method in power-flow applications \cite{Milano2009,XiaohuiZhao2012} or linear programming \cite{Liao2010}. However there are very few results showing research has been done into the application of a completely continuous solver as proposed above.\\

{\footnotesize
\renewcommand{\section}[2]{}
\bibliography{references.bib}
}
\end{document}
