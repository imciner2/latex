% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: September October 14, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=0.75in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
%\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage{url}
\usepackage{hyperref}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}
% These packages are all incorporated in the memoir class to one degree or another...

% Include custom commands
\usepackage{Unit-Declaration}
\usepackage{Sci-Formatting}

\usepackage{datetime}
\usepackage[backend=bibtex,style=authortitle]{biblatex}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{PhD Research Proposal}\rhead{Ian McInerney}
%\lfoot{Draft: \today\ \currenttime}\cfoot{\thepage}\rfoot{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

% Remove space around lists
%\setlist{nolistsep}

%\bibliographystyle{IEEEtran}
\addbibresource{references.bib}

% Setup the hyperlinks
\hypersetup{
	colorlinks=true,
	linkcolor=blue,
	urlcolor=blue,
	pdftitle={PhD Personal Statement - McInerney},
	bookmarks=true,
}

%\graphicspath{{Images/}}

%%% END Article customizations

%%% The "real" document content comes below...

\begin{document}

%\begin{centering}
%	\section*{PhD Research Proposal}
%\end{centering}

\chead{PhD Personal Statement}

I am applying to study for a Doctorate of Philosophy at the High Performance Embedded and Distributed Systems center to focus specifically in the derivation and implementation of high-speed algorithms for control systems, specifically for Model Predictive Control. Working on a PhD with the HiPEDS center would allow me to further develop my ability to produce theoretical concepts that have practical applications. I feel that my diverse skillset and previous industry experience make me an excellent candidate for the research project I am proposing.

\subsection*{Why Imperial}

I am drawn to Imperial College and this program because of the unique opportunity it presents. During my research into the many PhD programs available, I kept coming back to look at the HiPEDS program and found myself comparing many programs against it. I am drawn specifically to the intermixing of theoretical results and practical applications, something that I had a hard time locating in other prospective programs.

Additionally, many of the papers and presentations I found relating to the implementation of Model Predictive Control were either authored by researchers at Imperial College, or involved a collaboration with those researchers. This program would allow me to work with and learn from the leading experts in the field.

%After completing my education, I plan to seek employment doing advanced research and development in either an industrial setting or at a national laboratory. Working on a PhD with the HiPEDS center would allow me keep that practical grounding that is required of researchers in industrial environments.

\subsection*{Proposed Research Project}

Model Predictive Control (MPC) is a branch of optimal control that uses current data about a system to predict the system's eventual path. MPC utilizes numerical optimization methods to design a controller input at each time step that meets certain criteria. The current solver architectures in use do not update their result when new data is obtained, they instead start the decision process anew with every datapoint. \textbf{I propose implementing continually-revising solvers in model predictive control, so that the computerized process doesn't restart everytime but instead updates its previous result.} These solvers will make use of a technique called trajectory-based optimization, where a dynamical system is created whose equilibrium points are the optimal solution. I will explore the implementation of these solvers on embedded hardware with real-time constraints, and demonstrate their usage by implementing an MPC satellite attitude controller using them \footnote{A more detailed project proposal can be found here: \url{http://home.engineering.iastate.edu/~imciner2/ResearchProposal_Imperial.pdf}}.
\begin{figure}[h!]
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[scale=0.6]{IP_Algorithm.png}
	\end{subfigure}
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[scale=0.6]{TBO_Algorithm.png}
	\end{subfigure}
	\caption{Comparison of solver architectures when run on 2 sequential problems. The 1st problem is defined by the shaded region, while the 2nd is an extension of the 1st region to the red lines. The current solvers restart completely on the 2nd run, whereas the proposed solver just updates its previous result.}
	\label{fig:solverComparison}
\end{figure}
\vspace{-0.4cm}

\subsection*{Education and Research History}

%During this last year and a half, I developed an interest in Model Predictive Control (MPC). While my current university offers no controls coursework related to MPC, I was attended the MPC workshop offered at the American Controls Conference in 2016 to get an introduction. This workshop showed me that while MPC is a powerful tool, its capabilities are limited by computational bottlenecks. Because of this, I have decided to work on computational tools for Model Predictive Control.

I have used my Masters work to gain a fundamental understanding of controls, optimization, and embedded systems design. For my coursework component, I have taken courses relating to the following sub-areas:
\begin{itemize}[nolistsep, noitemsep]
	\item Control Systems - Linear Systems, Optimal Control, Digital Control, Non-linear Systems
	\item Optimization - Convex Functions, Continuous \& Non-linear Functions, Discrete/Graphs
	\item Embedded Systems - Design \& Implementation, FPGA Computing
	\item Numerical Analysis
\end{itemize}
These courses have given me a strong theoretical background, which has been complemented nicely by my practical experience from my research and industrial internships.

I have worked on two research projects that relate closely to the proposed project:
\begin{enumerate}[nolistsep, noitemsep]
	\item Implementation of a linear system solver on reconfigurable analog hardware
	\item Development of trajectory-based optimization methods for trilateration of an object
\end{enumerate}
The first project used a solver that was designed by a previous student as a continuous-time algorithm to find the least-norm solution to a system of equations \footcite{Jing2014}. My main research push was to explore what analog technologies were available to implement the algorithm on. I spent a summer researching the state-of-the-art in Field Programmable Analog Arrays (FPAA), and then spent a semester using an FPAA from Anadigm Technologies for practical experiments of the solution system. I determined that while the FPAA technology was interesting, it lagged behind the Field Programmable Gate Arrays (FPGA) technology in its scalability, commercial availability, and ease of use.

For a subsequent course I took on Reconfigurable/FPGA Computing, I again used the trajectory-based linear system solver as a class project, and this time explored its implementation on an FPGA system using a simple Euler discretization scheme. This project was basically a mini research project, requiring a literature review, implementation of the system, analysis of the results, and creation of a research report\footnote{Project documents: \url{http://home.engineering.iastate.edu/~imciner2/583Project/}}. This project served as the starting point for the idea of my proposed project, and has provided me with insight into some of the complexities the project may have and possible directions it may go.

The second project I have worked on involves using trajectory-based optimization methods to solve the trilateration problem (locating an object using only distance measurements). This project was just started this fall, and will be used as the research project for my Masters thesis \footnote{Project description: \url{http://home.engineering.iastate.edu/~imciner2/TriLatProblemStatement.pdf}}.

\subsection*{Industrial Experience}

My industrial experience relates mainly to embedded systems design and implementation. The first internship I had was with Caterpillar Inc., where I was assigned to the sensing components team. In this internship I developed the hardware and software for a sensing system to determine oil quality, and then applied that hardware to detect the kind of oil in an engine\footnote{Led to US patent application 14/547640.}. On my second internship with Caterpillar, I mainly focused on embedded systems programming, successfully writing code to perform functional tests on a new display system. This past summer, I was a Summer Research Program Intern with the Massachusetts Institute of Technology's Lincoln Laboratory. My main project that summer was to design a small motor controller for a micro-UAV. This project involved me taking existing theoretical control results and implementing them in custom hardware and software.

I feel that a strong balance between theory and practice is needed in research, because without the practical knowledge it is harder for the ideas to be transferred to real world applications. That is why as part of my research project, I plan to focus on implementation of the results and to explore their design options.

\end{document}
