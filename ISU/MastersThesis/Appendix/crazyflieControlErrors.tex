% !TeX root = ../thesis.tex

\chapter{CRAZYFLIE FIRMWARE CONTROL LOOP INCONSISTENCIES}
\label{app:inconsistencies}

During the modeling process, two discrepancies were noted when comparing measured datasets (such as the dataset given in figure \ref{fig:inconsistencies:dataset}) from the system in \cite{Noronha2016}:
\begin{itemize}[nolistsep, noitemsep]
	\item The internal quadcopter roll corresponded with the camera system pitch (and the quadcopter pitch with the camera system roll)
	\item The internal quadcopter roll was negated compared to the camera system pitch
\end{itemize}
A thorough examination of the Crazyflie firmware revealed 5 inconsistencies compared to the expected operation of a quadroto control loop\footnote{This discussion focuses on the control loop found in the 2016.09 release of the Crazyflie firmware, available at \url{https://github.com/bitcraze/crazyflie-firmware/releases/tag/2016.09}}, and an additional two inconsistencies in the operation of the ground station control software from \cite{Noronha2016}.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.45]{ExistingSystem_Discrepancy}
	\caption{Comparison of the captured angles using the system developed in \cite{Noronha2016}.}
	\label{fig:inconsistencies:dataset}
\end{figure}

The inconsistencies are listed below, and are highlighted in red and numbered in figure \ref{fig:inconsistencies:incorrectStructure}. Numbers 1-5 are located in the Crazyflie firmware, while number 6 and 7 are located in the control software from \cite{Noronha2016}.

\noindent\textbf{Inconsistencies:}
\begin{enumerate}[noitemsep, nolistsep]
	\item Body axes used for filter computation is not aligned with the aerospace coordinate system
	\item The vector accelerometer inputs to the Mahoney filter are feeding in the negative of the gravitational vector
	\item The computed pitch angle is negated
	\item The yaw rate PID output is negated before entering the mixer
	\item The $q$ body-rate measurement is negated before being used in the $q$ rate PID computation
	\item The measured yaw angle is negated before transmission to the Crazyflie
	\item The $x$ positon PID controls the roll angle, and the $y$ positon PID controls the pitch angle
\end{enumerate}

The Crazyflie is able to be flown using the software and firmware containing these issues, however the accurate modeling of the firmware and physics system is very difficult with them. After examining the issues, 1 and 2 appear to be the reason for the existence of 3-5 in the firmware and 6 in the software. The last one, 7,  is caused by an incorrect definition of $0^\circ$ yaw when defining the flight system.

\begin{sidewaysfigure}[p!]
	\centering
	\includegraphics[width=\textheight]{CompleteControlStructure_Crazyflie}\
	\caption{Mathematical structure of the default firmware's control loop with inconsistencies labeled}
	\label{fig:inconsistencies:incorrectStructure}
	\rotateFigurePageForLabel{fig:inconsistencies:incorrectStructure}
\end{sidewaysfigure}

What follows is a brief description of each inconsistency and the change made to the firmware to work around it.


\noindent\textbf{Inconsistency 1: Incorrect Body Axes}

Most aerospace systems utilize an inertial axes system called the aerospace axes. These axes are a right-handed coordinate system with the positive $x$ axis pointing towards the front, and the positive $z$ axis pointing down towards the earth \cite{Rolfe1986}. This means that estimators designed for aerospace applications are designed to determine the orientation of the craft with respect to the aerospace axes. Since the Mahoney filter estimator used on the Crazyflie's is designed for estimating the attitude of a UAV, it assumes that the body axes system will align with the inertial axes if no rotation has occured \cite{Euston2008}.

On the Crazyflie, these frames are further complicated by the fact that the sensor is in its own frame with the positive $x$ axis pointing left and positive $z$ pointing up. In order to do the angle estimation, the sensor measurements need to be converted into the body frame. The way this is done in the main Crazyflie firmware release is to swap the $x$ and $y$ readings, then negate the $x$ reading, while leaving $z$ alone. eg.
$$x^b = -y^s \qquad y^b = x^s \qquad z^b = z^s \qquad\qquad \text{(Incorrect conversion)}$$
This actually moves the sensor readings into a coordinate system where the positive $x$ axis is to the front and the positive $z$ axis is up. This axes system is the aerospace system rotated with a roll of $180^\circ$ (or equivalently a yaw of $180^\circ$ followed by a pitch of $180^\circ$).

The correct method of converting the sensor frame into the body frame would be to negate all the sensor readings, then swap the $x$ and $y$ readings. eg.
$$x^b = -y^s \qquad y^b = -x^s \qquad z^b = -z^s \qquad\qquad \text{(Correct conversion)}$$


\noindent\textbf{Inconsistency 2: Incorrect Gravitational Vector}

As discussed in section \ref{sec:model:firmware:attEst}, the Crazyflie utilizes a Mahoney filter for its attitude estimation by default. This filter assumes that the inertial axes is the aerospace system, and that all sensor readings are in that coordinate system.

As input, the Mahoney filter takes in the sensed gravitational vector $g^b$ and the sensed angular rates about the body axes. Note, this means that in the aerospace system, $g^b = \left[ \begin{smallmatrix}0 & 0 & 1 \end{smallmatrix} \right]$ when the quadcopter is not rotated from the inertial frame. However an accelerometer actually senses an acceleration vector of $a = \left[ \begin{smallmatrix}0 & 0 & -1 \end{smallmatrix} \right]$ when the sensor axes are perfectly aligned with the inertial axes \cite{Pedley2013}. Therefore it is necessary to negate all the accelerometer readings before providing them to the Mahoney filter.

In the 2016.09 release version, the raw accelerometer values in the body axes were passed directly into the Mahoney filter, instead of negating the raw accelerometer values.
\begin{align*}
	g^b &= a^b \qquad\qquad \text{(Incorrect)}\\
	g^b &= -a^b \qquad\qquad \text{(Correct)}
\end{align*}


\noindent\textbf{Inconsistency 3: Negated Pitch Angle}

The attitude estimated by the Mahoney filter is natively in a quaternion representation, while the control loops require the attitude in its Euler angle form. This means that the two must be converted. The code does the conversion for pitch using the following formula:
$$\sin^{-1}( 2(q_1 q_3 - q_0 q_2) )\qquad\qquad \text{(Incorrect conversion)}$$
This formula is missing a negative sign though. It should instead be:
$$\sin^{-1}( -2(q_1 q_3 - q_0 q_2) )\qquad\qquad \text{(Correct conversion \cite{Rolfe1986})}$$


\noindent\textbf{Inconsistency 4: Negated Yaw Mixer Command}

After the $r$ rate PID output, there is a negative sign on the $\mu_r$ command before it enters the mixer. This sign is reversing the control input, so a positive PID constant and positive error will actually produce a negative yaw command. In normal operation, a positive error and a positive PID constant should produce a positive yaw command. This negative sign should not be present between the PIDs and the mixer.

This negative sign is necessary because the shift from the sensor axes to the body axes frame does not negate the gyroscope readings on the $z$ axes (the measurement producing the $r$ rate measurement). This lack of negation means the controller should be negated, which this sign change is doing.


\noindent\textbf{Inconsistency 5: Negated $q$ Rate Measurement}

In the control loop, the $q$ rate measurement from the gyroscope is negated before being fed into the PID for computing the error. 

This negative sign is necessary because the shift from the sensor axes to the body axes frame does not negate the gyroscope readings on the $y$ axes (the measurement producing the $q$ rate measurement). This lack of negation could be handled three ways: negating the mixer command (like inconsistency 4 did), negate the error input, or negate the PID constants. In this case, the input received from the Pitch angular controller is already negated (due to inconsistency 3), so the error negation is what was done. To finish negating the error, the sensed value was negated.


\noindent\textbf{Inconsistency 6: Negated Yaw Angle}

The control software developed in \cite{Noronha2016} utilized the camera system for the yaw angle instead of the internal yaw computation on the Crazyflie. Before the yaw measurement was sent to the PID for controlling the yaw angle, it was negated.

Since the camera is used as the sensor, the controlled yaw angle is relative to an aerospace axes system on the Crazyflie, whereas the Crazyflie had an axes system rotated $180^\circ$ from that. This means that the two readings would differ in sign, making a negative sign necessary in order not to destabilize the $r$ rate loop (since the rate loop already had a negated measurement due to inconsistency 1 and the output was negated in inconsistency 4, the PID required the reference input to be negated so that all the negations would cancel).


\noindent\textbf{Inconsistency 7: Swapped Position PID Angle Outputs}

In the control software, the PIDs for the $x$ and $y$ axes were computed using the camera system measurements (so they were in the camera system reference frame), while the internal PIDs for the roll and pitch were computed using the internal measurements (so they are in the Crazyflie reference frame). Normally the $x$ PID will control the pitch angle and the $y$ PID will control the roll angle. In this case however, they were reversed. The $x$ PID controlled the roll angle and the $y$ PID controller the pitch angle.

This is due to a rotation in the orientation of the Crazyflie's when physically flown. When aligned inside the camera system, the front of the Crazyflie's was facing the negative $y$ camera axes. This made a positive roll command move the quadcopter in positive $x$ and a positive pitch command move the quadcopter in positive $y$.