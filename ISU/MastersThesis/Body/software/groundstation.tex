% !TeX root = ../../thesis.tex

\section{Groundstation Client}
\label{sec:software:groundstation}

The groundstation client developed in this work is based upon the initial work in \cite{Noronha2016}, which is in turn based on the \textit{libcflie} developed by Jan Winkler \cite{winklerRepo}

\subsection{Overall Structure}

The overall structure of this client software is a multi-threaded application with 4 main thread types: User input, User output, VRPN processing, and CrazyRadio.

The user input thread is responsible for monitoring the keyboard for typed commands, interpreting the commands received, and then forwarding them onto the appropriate Crazyflie for the actuation. This thread just passes the commands from the user into a command queue in the Crazyflie for the final parsing and actuation, no actual setpoints or modifications to the controller happen in this thread.

The user output thread is responsible for updating the terminal display with pertinent information about each Crazyflie's flight status. This information includes the current status (Grounded, Takeoff, Hover, Landing, etc), the current setpoints, the current position, and information about the Crazyflie's attitude (both from the camera system and from the quadrotor itself). This display can also be modified to include other information from the Crazyflie, such as sensor readings, computation values, etc.

The VRPN processing thread is responsible for constantly calling the VRPN \textit{mainloop} functions for the main connection and for each trackable. From these function the VRPN library will process the packets received from the server by calling trackable callbacks embedded in the Crazyflie C++ class. These callbacks do not do any computation work, they simply update data structures in the Crazyflie class (unlike the callbacks in \cite{Noronha2016} which actually performed the control function as well).

The final thread type is the CrazyRadio thread. This thread is the workhorse of the software, where all of the control decisions are made and all of the communications take place. More information about this thread can be found in section \ref{sec:software:client:crazyradio}.

\subsection{Startup Routine}

When the client software first starts, it runs as a single-threaded application. This application will first prepare various subsystems of the client, such as the computation network, computation logger, VRPN callbacks, and then move onto initializing each individual Crazyflie one at a time. Initialization of the Crazyflie includes setting of the controller parameters, setting of the computation parameters, reading the logging and parameters tables, and starting the desired logging blocks.

During each stage of the startup, output is displayed to the user so they can verify the operation and data being sent (to ensure the software is sending the appropriate controller values, configuring the network properly, etc.). Sample output from the startup routine can be seen in figure \ref{fig:software:client:startup}.

Once the startup routine is over, the program pauses so the user can inspect the startup and the Crazyflies before running the actual control software. Once the user continues the program, all the other threads are spawned.

\begin{figure}[p]
	\centering
	\includegraphics[scale=0.5]{Software_ClientStartup}
	\caption{Terminal display during client startup}
	\label{fig:software:client:startup}
\end{figure}

\subsection{CrazyRadio Interfaces}
\label{sec:software:client:crazyradio}

The CrazyRadio class contains all of the functions necessary to interface with a CrazyRadio and send packets to a Crazyflie. Additionally, this class contains the main loop for a thread that handles all interfacing with the radio, and controlling the Crazyflies associated with the radio.

During the startup routine, the Crazyflies are associated with a CrazyRadio. That process passes the Crazyflie object to the CrazyRadio so the radio then can access the Crazyflie. Once startup is complete and the radio thread has been spawned, the thread operates in an infinite loop performing the following actions for each associated Crazyflie:
\begin{enumerate}[noitemsep, nolistsep]
	\item Set radio channel and datarate for the Crazyflie
	\item Call the Crazyflie's \textit{cycle()} function
	\item Call the Crazyflie's \textit{logger()} function
	\item Move to next Crazyflie associated with the radio
\end{enumerate}

\subsection{Crazyflie Interfaces}

The main workhorse of the software is the Crazyflie class. This class contains all of the functions required for packetizing the data to send to the radio, parsing the user commands into setpoints, and handling the logging of data.

\subsubsection{Main cycle function}

The main function of the class is a \textit{cycle()} function. This function follows the flow given in figure \ref{fig:software:crazyflieCycle}, where first it handles the cases where the Crazyflie is taking off or landing, then sends built-up packets from the network and controller update. Once those are sent, it parses the commands from the user into setpoint changes. Then it will send the new setpoints if there are any (the setpoints are not repeated, they are only sent once), and then it will send position data if there is any new data. If there is no new position data, the loop sends a \textit{dummy} packet. This packet contains no data, but gives a chance for the Crazyflie to respond with any accumulated packets in its transmit queue (so the Crazyflie can send back at least one packet per cycle).

Along with the \textit{cycle()} function there are many different support functions with roles such as:
\begin{itemize}[noitemsep, nolistsep]
	\item Get/Set position data
	\item Get/Set setpoints
	\item Set controller parameters
	\item Configure on-board computation system
	\item Modify the local coordinate system
\end{itemize}

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{Software_CrazyflieCycle}
	\caption{Flow of the Crazyflie \textit{cycle()} function}
	\label{fig:software:crazyflieCycle}
\end{figure}

\subsubsection{Takeoff Routine}

This software is also responsible for managing the takeoff routine of the Crazyflies. In this system, the $z$ axis controllers are switched off during the takeoff routine, so from the initiation of takeoff to the end of the final step there is no control of the thrust input. The takeoff routine is then defined by different steps of base thrust values, which are switched as different conditions occur. There are two possible conditions for switching the thrust:
\newpage
\begin{itemize}[noitemsep, nolistsep]
	\item Position - Change if the $z$ position reaches a threshold value
	\item Time - Change after a certain number of seconds elapsed since the last change
\end{itemize}
For example, the normal takeoff routine used is shown in table \ref{tab:groundstation:takeoff}.
\begin{table}[h!]
	\begin{center}
	\caption{Sample takeoff routine}
	\label{tab:groundstation:takeoff}
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Base Thrust} & \textbf{Condition for next step} \\
		\hline
		19000 & 1 second elapsed\\
		54000 & $z$ position $< -0.3$m\\
		50000 & Final thrust value\\
		\hline
	\end{tabular}
	\end{center}
\end{table}

\subsubsection{Local Coordinate System}

The Crazyflie class contains the ability for the Crazyflie to be translated into a local coordinate system instead of the global camera coordinate system. This process is transparent to the Crazyflie itself, since the translation is handled in the ground station client software, so the Crazyflie will continue executing the controllers as normal.

When the Crazyflie is first switched into the local coordinate system, both the setpoints and the position are translated, so the Crazyflie will maintain the same position. Any subsequent setpoint commands will be relative to the local coordinate system then, and all received camera system data will be translated into the local coordinate frame upon reception. When the Crazyflie is switched out of the local system back to the global system, the setpoint is again translated, so there is no large change in position during the coordinate system change.

\subsubsection{Callback Functions}

The Crazyflie class contains two callbacks: a VRPN packet callback, and a CRTP packet callback. The VRPN packet callback is associated with the VRPN tracker used in the VRPN thread, so this function is called whenever a new position is received.

The CRTP packet callback is called whenever the radio receives a packet from a Crazyflie. This callback function examines the packet's port information and routes it appropriately. Currently only 3 ports are utilized in the callback, given in table \ref{tab:software:ports}.

\begin{table}[h!]
	\caption{CRTP ports utilized in the \textit{libcflie} callback}
	\label{tab:software:ports}
	\centering
\begin{tabular}{|c|l|}
	\hline
	\textbf{Port} & \textbf{Routing}\\
	\hline
	Logging & Pass packet into the logging subfunction to update internal variables\\
	Console & Write console data to file\\
	Computation & Pass packet into the network router\\
	\hline
\end{tabular}
\end{table}

\subsubsection{CRTP Communications}

The Crazyflie/CrazyRadio class has two modes for transmitting packets over the radio link:
\begin{itemize}[noitemsep, nolistsep]
	\item Send and Receive
	\item Send only
\end{itemize}

The names for these two modes are slightly misleading, since technically the Crazyflie is always sending packets back to the ground if a packet is transmitted to it. Instead, these modes refer to whether the software should expect a response packet to the exact command sent. This response packet is sent back with the same port and channel information as the sent packet.

If a packet is sent requesting a response (ie. the send and receive mode), then the radio will continue to send packets to the Crazyflie until it receives a response with the desired port and channel. While the radio is processing a Send and Receive mode packet, the radio is blocking all other transmissions on it. This is the equivalent of a TCP protocol on the radio stream.

The other mode is the send only mode. In this mode the packet will only be sent to the Crazyflie once, and only one response will be received. This mode is designed for faster transmission of data and is the equivalent of a UDP protocol on the radio stream.

In the software, all position and setpoint commands use the send only mode, along with all computation packets. The controller update packets utilize the send and receive mode though, so those commands should not be sent during flight (otherwise the radio link may not send position updates as required).

A block diagram of the CRTP packet's journey through the software is in figure \ref{fig:software:client:packetProcess}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\textwidth]{Software_LifeOfAPacket}
	\caption{Progression of a CRTP packet through the software}
	\label{fig:software:client:packetProcess}
\end{figure}

\subsection{Computation Network Emulation}
\label{sec:software:groundStation:network}

The last subsystem of the ground station client is the emulation of a distributed network for the computational system on the Crazyflies. This part is designed to route the computational data packets from one agent to its neighbors, over an arbitrary network topology. This system can handle both bidirectional and directional communication links, since the way the neighbors are specified is through defining the edges as a to/from relation.

At system startup, the network graph is initialized by defining the number of total agents in the network, and then entering each edge individually into the network. This network object is then passed into the Crazyflie object, where it is used inside the CRTP packet callback. When a computation packet is received in the CRTP callback, the packetReceived function of the network is called. This function will go through the neighbors of the agent, and add the received packet to their sending queue. Then the packet is sent to a logger instance, so all the computational traffic can be logged for offline analysis.

During the \textit{cycle()} function in the Crazyflie object, the Crazyflie calls the network system with its agent number and receives the next packet waiting to be transmitted. This system is designed around a First-In First-Out (FIFO) buffer, so the oldest packet in the queue is the next one sent.