% !TeX root = ../../thesis.tex

\newpage
\section{Crazyflie Firmware}

In this work, three major features were added to the Crazyflie firmware:
\begin{itemize}[noitemsep, nolistsep]
	\item On-board position control
	\item The ability to update controller parameters without reflashing the firmware
	\item The ability to perform distributed computation with multiple Crazyflies
\end{itemize}

\subsection{On-Board Position Control}

One of the changes made to the Crazyflie firmware was to move the position controller on-board. In the original system from \cite{Noronha2016}, the position controller was on the ground station computer with the radio link sending the pitch/roll attitude commands and the yaw rate commands. In this work, the commander subsystem of the Crazyflie was modified to receive two new datatypes:
\begin{itemize}[noitemsep, nolistsep]
	\item Position Data - 4 floating point numbers representing the current position of the Crazyflie in the flight volume ($x$, $y$, $z$ and yaw)
	\item Position Setpoints - 4 floating point numbers representing the desired position of the Crazyflie in the flight volume ($x$, $y$, $z$ and yaw)
\end{itemize}
Additionally, inside the setpoints packet there were extra fields to include the desired base thrust value and also a flag to reset the controllers (to allow for clearing of integrators).

This system also included a watchdog on the position data packet, so if a new data packet was not received within 2 seconds of the previous one, the controllers will be disabled and the Crazyflie will fall to the ground. This was done so that if the Crazyflie flew out of range of the camera system, it would automatically stop.

The actual position controllers use the same PID controller functions as the on-board attitude and rate controllers, but have their own variables for storing the gains and setpoints. These controllers were added into the firmware, creating the nested loop shown in figure \ref{fig:control:PIDarch}.

\subsection{Controller Update}

Another major change in the Crazyflie firmware was the addition of a CRTP packet port to facilitate the modification of the controllers on-line. This port contains three different channels, as given in table \ref{tab:software:updateChannels}.

\begin{table}[h!]
	\centering
	\caption{Channels contained on the controller update CRTP port}
	\label{tab:software:updateChannels}
\begin{tabular}{|c|c|}
	\hline
	\textbf{Channel \#} & \textbf{Operation} \\
	\hline
	0 & Query controller type\\
	1 & Call controller update function\\
	2 & Modify the thrust controller\\
	\hline
\end{tabular}
\end{table}

For channel 0, the Crazyflie will respond with an integer value providing the currently active controller. This allows for the ground station to determine if there is a PID controller running, a state feedback controller running, or if the controller is bypassed. This allows the ground station to make decisions about setting the controller (such as not allowing PID updates if the controller is not a PID type).

For channel 1, the Crazyflie will pass the update packet to a controller specific parsing function. This is usually used for setting controller gains, which will have different formats depending upon the controller.

Channel 2 allows for the ground station to enable and disable the thrust controller. When the thrust controller is disabled, the mixer's $u_T$ input is being fed directly from the base thrust command in the setpoint packet, with no modification. When the controller is enabled, the $u_T$ input has the thrust controller output added onto it. This command is usually used during the takeoff routine to turn on/off the height controller.

Each controller is responsible for implementing the controller specific parsing function as it wants, and if desired can even contain a sub-op code if the controller wants to be able to have multiple kinds of update commands specific to it.

\subsection{Computation System}
\label{sec:software:firmware:computation}

The last major change to the Crazyflie is the addition of the ability for the Crazyflie to operate as an agent in a network with distributed computation (computation run on the agents). To accomplish this, a new CRTP port was added that has 3 channels, given in table \ref{tab:software:computationChannels}.

\begin{table}[h!]
	\centering
	\caption{Channels contained on the computation system CRTP port}
	\label{tab:software:computationChannels}
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Channel \#} & \textbf{Operation} \\
		\hline
		0 & Query computation type\\
		1 & Call computation data received function\\
		2 & Call the computation configure function\\
		\hline
	\end{tabular}
\end{table}

Channel 0 responds with an integer value representing the kind of computation installed on the Crazyflie.

Channel 1 is the main channel used in the computation block, since it is the channel that the node data will be sent and received on. Data sent over this channel will be forwarded by the ground station network forwarder in section \ref{sec:software:groundStation:network} to all the neighboring quadrotors. The Crazyflie will receive the data from the forwarder over this channel, and then save that data into its memory for the computation to use in the future.

Channel 2 allows for the ground station to update parameters of the computation. This channel makes use of an additional operation code in the first byte of data. The possible operation codes are given in table \ref{tab:software:computationOpcodes}.

\begin{table}[h!]
	\centering
	\caption{Opcodes for the computation update packets}
	\label{tab:software:computationOpcodes}
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Opcode} & \textbf{Operation} \\
		\hline
		0 & Enable/Disable the computation\\
		1 & Call computation update function\\
		2 & Modify the rate of the computation\\
		\hline
	\end{tabular}
\end{table}

Operation code 0 allows for the computation to be disabled and enabled. Operation code 1 allows for specifics of the installed computation to be modified. For instance, the computation discussed in chapter \ref{sec:app:localization} has multiple parameters such as the step size, node number, and other parameters that are set through this operation code. While operation code 2 allows for the rate at which the computation runs to be modified. By default, the computations run at 100Hz, but they can be modified to run up to 1000Hz (if the computation can execute that quickly).

The actual computation process runs in its own task on the Crazyflie. This allows for the computation to be done independently of the control loop and not interfere with its operation. The computation task does have hooks in the control loop though, allowing the computation task to read the current quadrotor state (such as position, velocity, pose, etc), and also read and modify the setpoints of the controller. This provides maximum flexibility for the computational framework since now the computation could be used to implement calculations to do distributed control of the group of Crazyflies.