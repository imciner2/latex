% !TeX root = ../../thesis.tex
\section{PID Controller}
\label{sec:control:pid}

The first controllers examined on the Crazyflie quadrotor were Proportional-Integral-Derivative (PID) controllers. PID controllers are one of the most prevalent controllers in the hobbyist quadrotor market due to the ease of their implementation and also the ability for their control response to be tuned with little knowledge of the underlying physics. The Crazyflie in this work uses 9 different PID controllers in a nested-loop configuration (as shown in figure \ref{fig:control:PIDarch}).

\subsection{Theory}

The PID controller is one of the most ubiquitous controllers in use today, and its theory is covered extensively in most texts for an undergraduate control systems course (see e.g. \cite{Nise2004}, \cite{Ogata2010}). What follows is a short summary of the relevant theory.

The overall PID controller takes the $s$-domain form shown in \eqref{eq:control:pidCont}. This equation shows the input-output relation for the controller where the input signal is the error signal (reference signal minus the current system state) and the output signal is the actuator input \cite{Nise2004}. It consists of three distinct components: a proportional term, an integral term and a derivative term.
\begin{equation}
\label{eq:control:pidCont}
\frac{U(s)}{E(s)} = k_p + \frac{k_i}{s} + k_d s
\end{equation}

Each component plays a different part in controlling the system. The proportional component makes the controller react to the instantaneous error, so if the system has large error then the controller produces a large output. The integral term examines the error over time, which will reduce the steady-state error of the system. The derivative term examines the rate of change of the error (the speed at which the error changes), which will allow for the controller to dampen high-speed oscillations in the response.

Each term contains a coefficient which specifies the amount the term affects the controller output. The process of finding the right coefficients is referred to as tuning the controller. This process can be difficult for complex or unstable systems. In this case, the Crazyflie comes with its internal control loops already tuned with stabilizing coefficients, so the tuning focused on the outer loops.

In order to implement the PID controller shown in \eqref{eq:control:pidCont} on the Crazyflie, it needs to be discretized. In this case, the PID utilizes the right-sided rectangle rule (or backward rule) for its integral calculation and a backward difference method for its derivative. Using the appropriate transforms from \cite{Phillips1995} and \cite{Franklin1998}, the complete discretized forumla for the PID in the $z$-domain can be seen in \eqref{eq:control:pidDiscrete}. Note that this has a fourth parameter in it, $T_s$, which is the sampling rate of the controller.
\begin{equation}
	\label{eq:control:pidDiscrete}
	\frac{U(z)}{E(z)} = k_p + k_i \left( \frac{T_s z}{z - 1} \right) + k_d \left(\frac{z - 1}{T_sz}\right)
\end{equation}

\subsection{Implementation}

The discrete PID controller from \eqref{eq:control:pidDiscrete} is implemented in the Crazyflie using the C programming language, and is the default controller shipped with the Crazyflie. A block diagram of the controller implementation can be seen in figure \ref{fig:control:pidDiagram}. It utilizes floating-point computation and contains three important features:
\begin{itemize}[nolistsep, noitemsep]
	\item Integral term saturation
	\item Resettable integral term
	\item No derivative term immediately after reset
\end{itemize}
\begin{figure}[h!]
	\includegraphics[trim={100 125 150 125},clip,scale=0.75]{Controller_PID_BlockDiagram}
	\caption{Block diagram for the PID implemented on the Crazyflie}
	\label{fig:control:pidDiagram}
\end{figure}

The above PID control block was replicated to create 9 different controllers on the Crazyflie firmware. These controllers are arranged in a nested loop architecture. The outer most loop contains 3 controllers for controlling the $x$, $y$, $z$ position of the quadrotor in the flight area. From this loop, the $z$ controller output is fed directly into the mixing matrix as the $u_t$ input, with the scaling thrust compensation from \eqref{eq:model:thrustRemap} applied. The output of the $x$ and $y$ controllers are fed into the reference inputs to the pitch and roll controllers respectively.

The middle loop contains 3 controllers responsible for controlling the angular position of the quadrotor. As mentioned before the reference input for pitch and roll came from controllers on the outer loop, while the reference for the yaw controller is a user-provided value. Additionally, the sensor input for the controllers come from different places. The roll and pitch controllers receive their Euler angle sensor input from the on-board Mahoney filter (which uses the on-board sensors for finding the orientation), and the yaw controller receives its Euler angle from the external camera system.

The inner most loop contains the controllers responsible for controlling the angular rate of the quadrotor. The output of each of the three middle loops provides the reference inputs to the inner most loop controllers. The sensor inputs for these three controllers come from the on-board gyroscope sensor. These controllers provide their output directly to three mixing matrix channels (no thrust compensation is used on these three channels).

A diagram showing the interconnection of the nested loop PID structure can be seen in figure \ref{fig:control:PIDarch}.
\begin{figure}[h!]
	\centering
	\includegraphics[trim={0, 150, 50, 10},clip,scale=0.5]{ControlDiagram_PID}
	\caption{Nested loop PID architecture}
	\label{fig:control:PIDarch}
\end{figure}

The Crazyflie comes with PID controllers already designed for the inner two loops, which were used as a baseline point for tuning stabilizing controllers on the outer loop. The final PID values used on the Crazyflie are given in table \ref{tab:control:pidParam}.

\begin{table}[h!]
	\centering
	\caption{PID controller parameters}
	\label{tab:control:pidParam}
	\begin{tabular}{|c||c|c|c||c|c|c||c|c|c|}
		\hline
		& \textbf{$p$ Rate} & \textbf{$q$ Rate} & \textbf{$r$ Rate} & \textbf{$\phi$ Angle} & \textbf{$\theta$ angle} & \textbf{$\psi$ Angle} & \textbf{Y} & \textbf{X} & \textbf{Z} \\
		\hline
		$T_s$ & 0.002s & 0.002s & 0.002s & 0.002s & 0.002s & 0.01s & 0.01s & 0.01s & 0.01s \\
		\hline
		$K_p$ & 250.0 & 250.0 & 70.0 & 6.0 & 6.0 & 6.0 & 20.0 &-20.0 & -10000 \\
		$K_i$ & 500.0 & 500.0 & 16.7 & 1.0 & 1.0 & 0.0 & 1.0 & -1.0 & -2000 \\
		$K_d$ & 2.5 & 2.5 & 0.0 & 0.0 & 0.0 & 0.35 & 22 & -22 & -15000\\
		\hline
		$i_{limit}$ & 33.3 & 33.3 & 166.7 & 20.0 & 20.0 & 360.0 & 40 & 40 & 10000\\
		\hline
	\end{tabular}
\end{table}

\addtocontents{toc}{\protect\newpage}
\subsection{Pseudo-Nonlinear Extension}
\label{sec:control:pseudononlinear}

The PID structure defined in the previous subsection  works well for when the quadrotor is hovering at an angle of $0^\circ$ yaw. However, since the $x$ and $y$ position sensors and setpoints are in the inertial frame of reference, if the quadrotor yaws any, the two frames will no longer be in-sync and the mapping of $x$ motion to pitch angle is no longer completely valid (for instance, under a $90^\circ$ yaw the roll angle would then affect the position on the inertial $x$ axes).

To allow for the quadrotor to fly while at a non-zero yaw angle, a remapping of the $x$ and $y$ axes must be done. This mapping is referred to as a pseudo-nonlinear extension to the controllers in \cite{Rich2012}. The mapping consists of simply rotating the errors in the inertial frame to be inside the body frame, which can be accomplished through the rotation matrix for a yaw rotation. This matrix is applied to the error signal before it is given to the controllers, so the PID controllers are actually operating on the body frame $x$ and $y$ axes. The mapping is:
\begin{equation}
	\begin{bmatrix}
		\hat{x}_b\\
		\hat{y}_b\\
		\hat{z}_b
	\end{bmatrix} = 
	\begin{bmatrix}
		\cos{(\psi)} & -\sin{(\psi)} & 0\\
		\sin{(\psi)} & \cos{(\psi)} & 0\\
		0 & 0 & 1
	\end{bmatrix}
	\begin{bmatrix}
		x_e\\
		y_e\\
		z_e
	\end{bmatrix}
\end{equation}
With the $x_e$, $y_e$ and $z_e$ being the inertial frame errors, and $\hat{x}_b$, $\hat{y}_b$ and $\hat{z}_b$ being the errors moved into the body frame under the currently sensed yaw, $\psi$.

\subsection{Response}

To examine the response of the Crazyflie controllers, step inputs were given to the three major axes ($x$, $y$, and $z$) on both the physical quadrotor system and the non-linear physics model. The results comparing the step response of the three axes can be seen in figures \ref{fig:control:pid:xStep}, \ref{fig:control:pid:yStep} and \ref{fig:control:pid:zStep}.

On the $x$ axes, multiple step input changes were commanded of the quadrotor. The response can be seen in figure \ref{fig:control:pid:xStep}. In this response, the simulation corresponds very closely with the actual quadrotor response in the position step response. One thing to note though is the pitch angle of the quadrotor (as measured by the on-board Mahoney filter) has an offset when the quadrotor is just hovering. This is most-likely caused by the physical center of mass being offset from the origin of the body frame, whereas the model assumes the center of mass is located at the origin of the body frame. The physical response of the pitch angle to a setpoint change (caused by the step on the position) still matches the expected simulation during the transient period though.

On the $y$ axes, step input changes were commanded of the quadrotor. The response can be seen in figure \ref{fig:control:pid:yStep}. In this response, the simulation result corresponds very closely with the actual quadrotor response. Similar to the pitch angle, the roll angle has a slight offset when the quadrotor is just hovering, and it is probably due to the same reason. The physical response of the roll angle does still seem to match the simulated response during the transient period.

The response of the quadrotor to a step on the $z$ axes can be seen in figure \ref{fig:control:pid:zStep}. This response shows a slight inconsistency between the simulated and experimental responses. The actual response appears to be slightly slower than the simulated response, and has a slightly higher overshoot. This discrepancy is most likely due to a modeling error in the mass of the quadrotor. The model uses the idealized mass determined in section \ref{sec:param}, while the actual quadrotor is flying with a trackable frame attached, adding mass to the overall system.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{Controller_PID_xAxes}
	\caption{Response of the PID controller to a step input in the $x$ direction.}
	\label{fig:control:pid:xStep}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{Controller_PID_yAxes}
	\caption{Response of the PID controller to a step input in the $y$ direction.}
	\label{fig:control:pid:yStep}
\end{figure}
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{Controller_PID_zAxes}
	\caption{Response of the actual PID controller (green line) and simulated PID controller (blue line) to a step input (red line) in the $z$ direction.}
	\label{fig:control:pid:zStep}
\end{figure}