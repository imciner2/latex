% !TeX root = ../../thesis.tex
\section{Linear State-Feedback Controller}
\label{sec:control:statespace}

The next controller type implemented on the Crazyflie platform was a linear state-feedback controller.

\subsection{Linearized Model}
\label{sec:control:stateSpace:linearModel}

In order to design the state-feedback controllers, a linear model of the Crazyflie dynamics was created. This model uses the physical dynamics derived in \cite{Rich2012}, and the parameters found in section \ref{sec:param}.

For the model, the state ordering used is:
\begin{equation}
\Lambda = \begin{bmatrix}
u & v & w & p & q & r & x & y & z & \psi & \theta & \phi
\end{bmatrix}^{T}
\label{eq:statespace:stateVector}
\end{equation}
This state vector does not include any rotor speed states. Those states were removed from the system for controller design (so the command output is directly proportional to the motor speed with no transient). This was done because there is no way of directly measuring the rotor speeds on the Crazyflie, so an estimator would have been needed. Additionally, since the rotor state transient is much faster than the quadrotor response (by an order of magnitude or more), the controller response would not be greatly affected by having those states excluded. So by removing those states, a full state estimator was not needed.

The input ordering used on the model is:
\begin{equation}
U = \begin{bmatrix}
u_t & u_A & u_E & u_r
\end{bmatrix}^{T}
\label{eq:statespace:inputVector}
\end{equation}

This forms the linear system
\begin{equation*}
\dot{\Lambda} = A_s \Lambda + B_s U
\end{equation*}
Where $A_s$ and $B_s$ are given in \eqref{eq:control:linearModel}.

{\footnotesize
\begin{equation}
\label{eq:control:linearModel}
\hspace{-2em} A_s = \begin{bmatrix}
  0 &   0 &   0 &   0 &   0 &   0 &  0 &  0 &  0 &     0 & -9.81 &  0\\
  0 &   0 &   0 &   0 &   0 &   0 &  0 &  0 &  0 &  9.81 &     0 &  0\\
  0 &   0 &   0 &   0 &   0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
  0 &   0 &   0 &   0 &   0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
  0 &   0 &   0 &   0 &   0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
  0 &   0 &   0 &   0 &   0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
1.0 &   0 &   0 &   0 &   0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
  0 & 1.0 &   0 &   0 &   0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
  0 &   0 & 1.0 &   0 &   0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
  0 &   0 &   0 & 1.0 &   0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
  0 &   0 &   0 &   0 & 1.0 &   0 &  0 &  0 &  0 &     0 &     0 &  0\\
  0 &   0 &   0 &   0 &   0 & 1.0 &  0 &  0 &  0 &     0 &     0 &  0\\
\end{bmatrix} \quad
B_s = \begin{bmatrix}
      0 &      0 &      0 &      0\\
      0 &      0 &      0 &      0\\
-0.0003 &      0 &      0 &      0\\
      0 & 0.0092 &      0 &      0\\
      0 &      0 & 0.0091 &      0\\
      0 &      0 &      0 & 0.0029\\
      0 &      0 &      0 &      0\\
      0 &      0 &      0 &      0\\
      0 &      0 &      0 &      0\\
      0 &      0 &      0 &      0\\
      0 &      0 &      0 &      0\\
      0 &      0 &      0 &      0\\
\end{bmatrix}
\end{equation}}

\subsection{Controller Implementation}

The state space controller implemented on the Crazyflie was designed to be a generic linear static-gain state-feedback (SGSF) controller, where the gain matrix can be modified in-flight by the ground station (similar to the PID update architecture). To make this controller generic, the 12 quadrotor states were supplemented with 12 additional integrator states, making 24 total controller states (so the controller $K$ matrix is $4 \times 24$). The 12 additional states are simply integrator states on each of the quadrotor states' error terms, allowing for the controller to use any integral term desired without having to modify the Crazyflie's firmware. An overview of the structure can be seen in figure \ref{fig:control:statespace:contStructure}.

For the Crazyflie system used here, all the physical states (except for linear velocity) are available directly from sensors. The choice was made to simplify implementation by using direct sensor measurements, and then determine the linear velocity by a numerical derivative. The integrator states were added in software thru a numerical integration using the right-sided rectangle rule (or backward rule). Additionally, the pseudo non-linear extension from section \ref{sec:control:pseudononlinear} was implemented on the linear position and linear velocity errors, and their integrals.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth, trim={50 100 50 100}, clip]{ControlDiagram_SGSF}
	\caption{State space controller structure}
	\label{fig:control:statespace:contStructure}
\end{figure}

\subsection{Controller Design}

The controller design for the Crazyflie was conducted in 2 parts: 1) state-feedback using the 12 physical states, 2) addition of control using integrator states.

When doing the design for the SGSF controller without integration, the linearized model was used. Since the model was linearized about the hover condition using the state vector given in \eqref{eq:statespace:stateVector} and input ordering in \eqref{eq:statespace:inputVector}, this introduces a saw-tooth structure to the resulting $K$ matrix, namely (where $x$ is a non-zero value):
\begin{equation*}
K = \begin{bmatrix}
0 & 0 & x & 0 & 0 & 0 & 0 & 0 & x & 0 & 0 & 0\\
0 & x & 0 & x & 0 & 0 & 0 & x & 0 & x & 0 & 0\\
x & 0 & 0 & 0 & x & 0 & x & 0 & 0 & 0 & x & 0\\
0 & 0 & 0 & 0 & 0 & x & 0 & 0 & 0 & 0 & 0 & x
\end{bmatrix}
\end{equation*}

This $K$ matrix can be designed a number of ways, with the most common for quadrotors being using the Linear Quadratic Regulator technique (LQR) (see eg. \cite{Rich2012, Zivkovic2017, Kim2016, Luis2016, Everett2015}).

\subsubsection{LQR Design Methodology}

This section contains a brief introduction to the theory of LQR controllers, more in-depth discussions can be found in the control literature (eg. \cite{Nise2004, Ogata2010} and other textbooks).

The idea behind LQR control is to design a controller to regulate the physical system to a desired state vector (usually the origin) while minimizing a quadratic cost function given by:
\begin{equation}
	J = \int x^{T} Q x + x^{T} S u + u^{T} R u
	\label{eq:statespace:lqrCostFunction}
\end{equation}
In this cost function, there are 3 weighting matrices:
\begin{itemize}[noitemsep, nolistsep]
	\item[$Q$] - Penalizes deviation of the states from the origin (note: $Q \succeq 0$)
	\item[$R$] - Penalizes large control inputs (note: $R \succ 0$)
	\item[$S$] - Cross term (penalizes state deviation in relation to control effort)
\end{itemize}

When the infinite-horizon optimal control problem using the cost function in \eqref{eq:statespace:lqrCostFunction} is solved, the result is a constant gain matrix, denoted as $K$.

\subsubsection{Crazyflie Controller}

In this work, the initial state-space controller was designed using an LQR methodology. This controller did not behave well in practice though, with large oscillations on the $x$ and $y$ response, and an inability to hold its position. What followed was iterative selection of $Q$ and $R$ matrices in an attempt to get better physical response. This desgin process proved to be tedious and did not result in good stabilizing controllers.

Instead, the structure of the $K$ matrix was exploited, and hand-tuning of the control gains was done. The hand-tuning was possible because of the saw-tooth structure of the $K$ matrix. This meant that each input corresponded to only a select number of states.

For instance, the $u_{A}$ input was affected only by 4 states, $v$, $p$, $y$ and $\phi$. This combination of states can be thought of as a PD controller on the $\phi$ Euler angle and a PD controller on the $y$ position. So hand-tuning the controller could be done using the rules for hand-tuning a PD controller. This led to the $K_s$ matrix shown below.

\begin{equation*}
K_s = \begin{bmatrix}
    0 &    0 & -8345 &    0 &    0 &    0 &     0 &    0 & -18000 &    0 &    0 &     0\\
    0 & 1500 &     0 & 2400 &    0 &    0 &     0 & 5119 &      0 & 8173 &    0 &     0\\
-1500 &    0 &     0 &    0 & 2340 &    0 & -5000 &    0 &      0 &    0 & 6825 &     0\\
    0 &    0 &     0 &    0 &    0 & 5952 &     0 &    0 &      0 &    0 &    0 & 40000
\end{bmatrix}
\end{equation*}

The next step was to add integration on the outermost loop, namely integration on the $x$, $y$, $z$, and $\phi$ states. These constants were chosen by a hand-tuning process as well and ended up being $1000$, $-1000$, $5000$ and $-500$ respectively.

Flight tests with only $K_s$ and integration on the outermost loop still had oscillations appearing on the Euler angles ($\phi$ and $\theta$), which in turn caused oscillations on the $x$ and $y$ axis. Addition of integral action onto the $\phi$ and $\theta$ states improved the performance by reducing the oscillations on the Euler angles. The addition of these integral terms is similar to what was done with the PID controller in section \ref{sec:control:pid}. Additionally, this phenomenon was briefly noted in \cite{Luis2016}, and their results also show improved performance with the additional integral terms. The $K_i$ matrix containing the integral gains is:

\begin{equation*}
K_i = \begin{bmatrix}
0 & 0 & 0 & 0 & 0 & 0 &    0 &     0 & 5000 &    0 &     0 & 0\\
0 & 0 & 0 & 0 & 0 & 0 &    0 & -1000 &    0 & 1000 &     0 & 0\\
0 & 0 & 0 & 0 & 0 & 0 & 1000 &     0 &    0 &    0 & -1000 & 0\\
0 & 0 & 0 & 0 & 0 & 0 &    0 &     0 &    0 &    0 &     0 & -500
\end{bmatrix}
\end{equation*}

While the addition of the $\phi$ and $\theta$ improved performance, their values cannot be found using traditional control design methods on the linearized model (such as the LQR method) and must be found using hand-tuning.

One of the ways that the integral terms can be added into the control design scheme is through augmenting the physical system with new integrator states for the error from hover (assumed zero state for simplicity), ie:
\begin{equation*}
A = \begin{bmatrix}
A_s                             &   0 \\
-I_{\footnotesize 12 \times 12} & 0
\end{bmatrix} \qquad
B = \begin{bmatrix}
B_s\\
0
\end{bmatrix}
\end{equation*}
where $I$ is the identity matrix.
%\begin{equation*}
%A_i = \begin{bmatrix}
%-1 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0\\
% 0 & -1 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0\\
% 0 &  0 & -1 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0\\
% 0 &  0 &  0 & -1 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0\\
% 0 &  0 &  0 &  0 & -1 &  0 &  0 &  0 &  0 &  0 &  0 &  0\\
% 0 &  0 &  0 &  0 &  0 & -1 &  0 &  0 &  0 &  0 &  0 &  0\\
% 0 &  0 &  0 &  0 &  0 &  0 & -1 &  0 &  0 &  0 &  0 &  0\\
% 0 &  0 &  0 &  0 &  0 &  0 &  0 & -1 &  0 &  0 &  0 &  0\\
% 0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 & -1 &  0 &  0 &  0\\
% 0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 & -1 &  0 &  0\\
% 0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 & -1 &  0\\
% 0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 & -1
%\end{bmatrix}
%\end{equation*}

Since the parameters derived in chapter \ref{sec:param} did not include the cross-terms, the linearized model becomes pure integrators linking states together (as seen in \ref{sec:control:stateSpace:linearModel}). This means that the controllability matrix has rank 16, which is rank deficient making the system uncontrollable technically. This is not a problem in practice though for 2 reasons:
\begin{enumerate}[noitemsep,nolistsep]
	\item The 8 ``uncontrollable'' states are integrating stable states, so their value will be bounded, making them stable
	\item The non-linear dynamics are not pure integration, so the non-linear system will be controllable with the new integration states
\end{enumerate}

\subsection{System Response}

To examine the response of the SGSF controller when implemented on the Crazyflie, step inputs were given on the $x$, $y$ and $z$ axes, and the quadrotor's response was recorded. The response can be seen in figures \ref{fig:control:comparison:xStep}, \ref{fig:control:comparison:yStep}, and \ref{fig:control:comparison:zStep} respectively. These show a comparison between the SGSF controller and the PID controller from section \ref{sec:control:pid}.

Examining the response graphs, the SGSF controller responds to the step-input faster, but has more oscillations around the setpoint than the PID does. With the $z$ axes response, the SGSF controller responds faster, with slightly more overshoot, but recovers to the setpoint quicker than the PID.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{Controller_Comparison_xAxes}
	\caption{Response of the implemented SGSF (red line) and PID (blue line) controllers to a step input (green line) in the $x$ direction.}
	\label{fig:control:comparison:xStep}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{Controller_Comparison_yAxes}
    \caption{Response of the implemented SGSF (red line) and PID (blue line) controllers to a step input (green line) in the $y$ direction.}
    \label{fig:control:comparison:yStep}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Controller_Comparison_zAxes}
    \caption{Response of the implemented SGSF (red line) and PID (blue line) controllers to a step input (green line) in the $z$ direction.}
    \label{fig:control:comparison:zStep}
\end{figure}
