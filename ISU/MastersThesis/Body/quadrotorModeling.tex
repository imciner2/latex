% !TeX root = ../thesis.tex
\chapter{QUADROTOR MODELING PRELIMIARIES}
\label{sec:quadModeling}

In order to understand some of the aspects of this work, it is important to understand some of the basic fundamentals of the physics of quadrotors. This chapter does not provide a very in-depth description of the physics model of the quadrotor. For an in-depth discussion, the reader is directed to \cite{Rich2012} and the references therein.

\section{Representation of Axis Rotations}

As an aerial vehicle, a quadrotor has 6 degrees of freedom - movement in $\mathbb{R}^3$ ($x$, $y$ and $z$) and movement in the angular space (roll, pitch and yaw). Representing a system with just translational movement in 3D space is straightforward, since the only change will be in the origin location. This means that a simple addition/subtraction can be done to move the quadrotor's body frame into aligment with the inertial frame.

However, since the quadrotor can move in the angular space as well, the mapping of the body frame into the inertial frame must be done using a rotation matrix. Rotation matrices are special matrices belonging to the Special Orthogonal Group 3 ($\mathbf{SO}(3)$). Matrices in the $\mathbf{SO}(3)$ group have the property of being orthogonal, ie $A^T = A^{-1}$, and also are related to the rotation of a coordinate system in $\mathbb{R}^3$, namely a matrix in $\mathbf{SO}(3)$ can be used to represent any arbitrary rotation in $\mathbb{R}^3$. The matrix representing the rotation is referred to as a rotation matrix.

\subsection{Euler Angles}

Rotation in $\mathbb{R}^3$ can be thought of as three separate rotations, applied sequentially to the coordinate system. These three rotations are commonly referred to as Euler angles. Each of the three rotations will bring a specific plane of the body frame into alignment with the inertial frame. Since the rotations are applied sequentially, the order in which the rotation matrices are formed is important, in fact there are 12 different sequences of rotation in Euler angles.

For this work, the sequence Yaw -> Pitch -> Roll, ($\psi \to \theta \to \phi$) is used. This rotation is applied in 4 steps:
\begin{enumerate}[noitemsep, nolistsep]
	\item Translation - The translation is applied to move the inertial frame origin onto the body frame origin. This produces the intermediate frame
	$E' = [\begin{smallmatrix}
	e'_x & e'_y & e'_z
	\end{smallmatrix}]^T$
	\item Yaw rotation - The yaw rotation is applied to rotate $E'$ around $e'_z$ by $\psi$ forming another intermediate frame
	$E'' = [\begin{smallmatrix}
	e''_x & e''_y & e''_z
	\end{smallmatrix}]^T$
	\item Pitch rotation - The pitch rotation is applied to rotate $E''$ around $e''_y$ by $\theta$ forming another intermediate frame
	$E''' = [\begin{smallmatrix}
	e'''_x & e'''_y & e'''_z
	\end{smallmatrix}]^T$
	\item Roll rotation - The roll rotation is applied to rotate $E'''$ around $e'''_x$ by $\phi$ forming the final body frame
\end{enumerate}

Each rotation can be expressed as a vector-matrix product, ie.
\begin{equation*}
	E'' = E' \begin{bmatrix}
	\cos{(\psi)} & -\sin{(\psi)} & 0 \\
	\sin{(\psi)} & \cos{(\psi)} & 0 \\
	0 & 0 & 1
	\end{bmatrix} = E' R_1
\end{equation*}

\begin{equation*}
	E''' = E'' \begin{bmatrix}
	\cos{(\theta)} & 0 & \sin{(\theta)} \\
	0 & 1 & 0 \\
	-\sin{(\theta)} & 0 & \cos{(\theta)}
	\end{bmatrix} = E'' R_2
\end{equation*}

\begin{equation*}
	B = E''' \begin{bmatrix}
	1 & 0 & 0 \\
	0 & \cos{(\phi)} & -\sin{(\phi)} \\
	0 & \sin{(\phi)} & \cos{(\phi)}
	\end{bmatrix} = E''' R_3
\end{equation*}
Which can then be further expressed as the product of those rotations: $B = E' R_1 R_2 R_3$. The matrix product $L_{EB} = R_1 R_2 R_3$ is then the rotation matrix to move the inertial frame into the body frame, ie $B = E' L_{EB}$. To move from the body frame to the inertial frame, simply do $L_{BE} = L_{EB}^{-1}$, which since $L_{EB}$ is in $\mathbf{SO}(3)$, $L_{BE} = L_{EB}^{T}$. Meaning to go from body frame to inertial frame do: $E' = L_{EB} B$.

\subsection{Quaternions}

Euler angles are not the only way of representing rotations though, a method called quaternions can also be used. The quaternions are a set of generalized complex numbers with certain conditions, but the details of quaternions are not needed to understand this work so they are omitted (for more on the quaternions, see \cite{Triola2009}). In the quaternion system, there are 4 numbers used to represent the rotation, ie $q = [\begin{smallmatrix}
q_0 & q_1 & q_2 & q_3
\end{smallmatrix}]^T$. These numbers have the additional constraint that $\norm{q}^2 = 1$, ie. $q_0^2 + q_1^2 + q_2^2 + q_3^2 = 1$.

The quaternions can then be used to represent rotations through the rotation matrix
\begin{equation*}
	B = \begin{bmatrix}
	q_0^2 + q_1^2 - q_2^2 - q_3^2 & 2(q_1q_2 + q_0q_3) & 2(q_1q_3 - q_0q_2)\\
	2(q_1q_2 - q_0q_3) & q_0^2 - q_1^2 + q_2^2 - q_3^2 & 2(q_2q_3 + q_0q_1)\\
	2(q_0q_2 + q_1q_3) & 2(q_2q_3 - q_0q_1) & q_0^2 - q_1^2 - q_2^2 + q_3^2
	\end{bmatrix} E'
\end{equation*}

Additionally, the quaternion representation of rotations can be converted into the Euler angle representation (following the sequence discussed above) using:
\begin{align*}
	\theta &= \sin^{-1}{(-2(q_1q_3 - q_0q_2))}\\
	\phi &= \tan^{-1}{\left( \frac{2(q_0q_1 + q_2q_3)}{q_0^2 - q_1^2 - q_2^2 + q_3^2} \right)}\\
	\psi &= \tan^{-1}{\left( \frac{2(q_0q_3 + q_1q_2)}{q_0^2 + q_1^2 - q_2^2 - q_3^2} \right)}
\end{align*}

\subsection{Euler Rates}

The rate of change of the Euler angles is not the same as the body angle rate of change. Instead, a rotation matrix must be applied to the body rates to find the Euler rates. In the sequence used in this work, the $\phi$ rate is equal to the angular velocity around $b_x$, meaning $p = \dot{\phi}$. The remaining rates must have the above rotation matrices applied to them in sequence, ie
\begin{equation}
	\begin{bmatrix}
	p\\
	q\\
	r
	\end{bmatrix} = 
	R_3 R_2
	\begin{bmatrix}
	0\\
	0\\
	\dot{\psi}
	\end{bmatrix} +
	R_3
	\begin{bmatrix}
	0\\
	\dot{\theta}\\
	0
	\end{bmatrix} +
	\begin{bmatrix}
	\dot{\phi} \\
	0 \\
	0
	\end{bmatrix}
\end{equation}

Grouping the matrices into a single transformation produces
\begin{equation}
	\begin{bmatrix}
	p\\
	q\\
	r
	\end{bmatrix} = 
	\begin{bmatrix}
	1 & 0 & -\sin{(\theta)} \\
	0 & \cos{(\phi)} & \sin{(\phi)}\cos{(\theta)} \\
	0 & -\sin{(\phi)} & \cos{(\phi)}\cos{(\theta)}
	\end{bmatrix}
	\begin{bmatrix}
	\dot{\phi}\\
	\dot{\theta}\\
	\dot{\psi}
	\end{bmatrix} = A_{BE}
	\begin{bmatrix}
	\dot{\phi}\\
	\dot{\theta}\\
	\dot{\psi}
	\end{bmatrix}
\end{equation}

Taking the inverse of $A_{BE}$ will then convert the body rates into the euler rates, ie:
\begin{equation}
	\begin{bmatrix}
	\dot{\phi}\\
	\dot{\theta}\\
	\dot{\psi}
	\end{bmatrix} = 
	\begin{bmatrix}
	1 & \sin{(\phi)}\tan{(\theta)} & \cos{(\phi)}\tan{(\theta)} \\
	0 & \cos{(\phi)} & -\sin{(\phi)} \\
	0 & \sin{(\phi)} / \cos{(\theta)} & \cos{(\phi)} / \cos{(\theta)}
	\end{bmatrix}
	\begin{bmatrix}
	p\\
	q\\
	r
	\end{bmatrix} = A_{EB}
	\begin{bmatrix}
	p\\
	q\\
	r
	\end{bmatrix}
\end{equation}

\section{Rigid Body Dynamics}

The rigid body dynamics of the quadrotor are very complex, and consist of 12 different states:

{\setcounter{MaxMatrixCols}{20}
	\begin{equation}
	\Lambda = \begin{bmatrix}
	u & v & w & p & q & r & x & y & z & \phi & \theta & \psi
	\end{bmatrix}^T
	\end{equation}
}

For the non-linear rigid-body dynamics, only six of these states are needed, and can be grouped into the following two subgroups:

\noindent Translational velocities in the body frame:
\begin{equation*}
	^Bv_o = \begin{bmatrix} u & v & w \end{bmatrix} \\
\end{equation*}

\noindent Angular velocities in the body frame:
\begin{equation*}
	^B\Omega_o = \begin{bmatrix} p & q & r \end{bmatrix}
\end{equation*}


Then, the non-linear dynamics can be represented by:
\begin{equation}
	\begin{bmatrix}
	{^B\dot{v}_o} \\
	{^B\dot{\Omega}}
	\end{bmatrix} = 
	\begin{bmatrix}
	mI & -m [^Br_{oc}] \\
	0 & J
	\end{bmatrix}^{-1}
	\begin{bmatrix}
	{^BF} - {^B\Omega} \times m(^Bv_o + {^B\Omega} \times {^Br_{oc}}) \\
	{^BQ} - {^B\Omega} \times J {^B\Omega} - {^Br_{oc}} \times {^BF}
	\end{bmatrix}
\end{equation}
Where $I$ is the identity matrix, $m$ is the mass, $J$ is the moment of inertia matrix (about the body axes), $^Br_{oc}$ is the vector from the body origin to the center of mass, $^BQ$ are the input torques in the body frame, and $^BF$ are the input forces in the body frame

By then assuming that the center of mass is located at the origin of the body axes (ie. $^B r_{oc} = 0$) and the moment of inertia $J$ is diagonal, the state space equation is

\begin{equation}
	\begin{bmatrix}
	^B\dot{v}_o \\
	^B\dot{\Omega} \\
	^E \dot{r}_0 \\
	\dot{\Theta}
	\end{bmatrix} = 
	\begin{bmatrix}
	\frac{1}{m} {^BF} - {^B\Omega} \times {^Bv_o} \\
	J^{-1} {^BQ} - J^{-1} {^B\Omega} \times J {^B\Omega} \\
	L_{EB} {^Bv_o} \\
	A_{EB} {^B\Omega}
	\end{bmatrix}
\end{equation}
Where $\Theta$ are the Euler angles and $r_o$ is the earth frame position.

\section{Powertrain}

The powertrain of the quadrotor consists of three distinct pieces:
\begin{enumerate}[noitemsep, nolistsep]
	\item Motor driver (Electronic Speed Controller)
	\item Motor
	\item Rotor
\end{enumerate}
Where the input to the powertrain system is the command to send to the motor driver/ESC and the output of the powertrain are forces and torques on the body frame of the quadrotor.

Customarily, the first two pieces are lumped together into one piece, ie. the ESC is lumped together with the motor, and then the third is modeled separately for some constants but is included for others.

\subsection{Rotor Thrust and Drag Torque}

The rotor produces thrust and drag when it spins. The thrust can be modeled as a quadratic relation versus rotor speed, ie
\begin{equation*}
	\abs{T} = K_T \omega^2
\end{equation*}
Where the rotor speed is $\omega$, the thrust produced is $T$ and the thrust constant is $K_T$.

The drag torque of the rotor can be modeled as a quadratic relation versus rotor speed, ie
\begin{equation*}
	Q = -K_d \omega^2 \Gamma_i
\end{equation*}
Where $Q$ is the torque produced, $K_d$ is the rotor drag torque constant, and $\Gamma_i$ is the unit vector for rotor $i$ giving its direction of rotation.

\subsection{Motor Velocity Dynamics}

The angular velocity of the motor shaft (which is also the angular velocity of the rotor), is a dynamical system with the differential equation
\begin{equation*}
	J_r \dot{\omega} = \frac{1}{R_m K_Q}u_p V_b - \frac{1}{R_m K_Q K_V} \omega - \frac{1}{K_Q} i_f - K_d \omega^2
\end{equation*}
Where $J_r$ is the moment of inertia of the rotor and motor system, $R_m$ is the resistance of the motor windings, $K_Q$ is the motor torque constant, $K_V$ is the motor back-emf constant, $i_f$ is the motor no-load current, $V_b$ is the battery voltage, and $K_d$ is the rotor drag torque constant from above.

If the motor transient is not required (such as for calculating steady-state velocities), the above system can be placed at equilbrium
\begin{equation*}
0 = \frac{1}{R_m K_Q}u_p V_b - \frac{1}{R_m K_Q K_V} \omega - \frac{1}{K_Q} i_f - K_d \omega^2
\end{equation*}
Then, for the rotor steady-state velocity given all other parameters, solve the above quadratic equation to find:
\begin{equation*}
	\omega = \frac{-1 + \sqrt{1 - 4 R_m K_V K_Q K_d (K_V R_m i_f - K_V u_p V_b)}}{2 R_m K_V K_Q K_d}
\end{equation*}