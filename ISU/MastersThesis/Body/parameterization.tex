% !TeX root = ../thesis.tex
\chapter{PARAMETERIZATION OF THE CRAZYFLIE QUADROTOR}
\label{sec:param}

\section{Mass}

The mass of the quadrotor was measured using an Ohaus Model CS200 scale. This scale has a maximum capacity of 200g, with an accuracy of 0.1g \cite{OhausCS200}. The mass of the Crazyflie was measured in 3 configurations (all without trackables/trackable support frame): no battery, standard 240mAh battery, 380mAh battery. The masses are given in tables \ref{tab:params_nobatt}, \ref{tab:params_240}, \ref{tab:params_380} respectively.

\section{Motors}

The Crazyflie uses 4 brushed DC motors to spin the rotors, which have the parameters given in table \ref{tab:param:motors}. These motors are designed for 4.2V operation with a maximum current of 1A. The datasheet specification lists the motor back-emf constant, $K_v$, as 14,000 RPM (or 1466.1 $\frac{\text{rad}}{\text{Vs}}$) \cite{bitcrazeMotors}. The other parameter needed is the motor torque constant. For brushed DC motors, this constant is usually equal to the motor back-emf constant \cite{precisionMicroDrivesBlog}.

The motor resistance was measured by using a 4-port test system attached to the motor leads. The 4-port test system allows for the tester to remove the resistance of the test leads, which is important when measuring small resistance values.

The other important parameter is the motor's no-load current. This is the current that is required to turn the motor and overcome the internal inertia of the rotor. This parameter is measured by removing all loading from the motor (so no rotor is attached), then varying the input voltage and recording the measured current. Eventually the current will plateau at a constant value as the voltage is changing. This value is the no-load current, $i_f$.

\begin{table}
	\centering
	\caption{Motor parameters for the Crazyflie brushed DC motors}
	\label{tab:param:motors}
\begin{tabular}{|c|c|}
	\hline
	\textbf{Motor Parameter} & \textbf{Measured Value}\\
	\hline
	$K_v$ & 1466.1 $\frac{\text{rad}}{\text{Vs}}$\\
	\hline
	$K_Q$ & 1466.1 $\frac{\text{Nm}}{\text{A}}$\\
	\hline
	$ R_m $ & $1.1\Omega$\\
	\hline
	$i_f$ & $0.0182$A\\
	\hline
\end{tabular}
\end{table}

\input{Body/parameterization/momentofinertia}

\input{Body/parameterization/rotor}


\section{Additional Parameters}

Since the model being used in this work is the one developed in \cite{Rich2012}, there are several other parameters required from the Crazyflie, namely the $K_H$ and $\delta T$ parameters along with direction unit-vectors for the rotor forces and drag constants.

The unit vectors for the rotor forces are simple to derive, since the rotor produces thrust in the negative $z$ direction only, the unit vector is 
$\Gamma_T = [
\begin{smallmatrix}
0, 0, -1
\end{smallmatrix}]^T
$.
The unit vectors for the rotor drag constants are similar, just dependent upon the direction of spin for the rotor (since the drag will be in the direction opposite the spin). This means that rotors 1 and 2 have 
$\Gamma_\Omega = [
\begin{smallmatrix}
0, 0, 1
\end{smallmatrix}]^T
$
and rotors 3 and 4 have
$\Gamma_\Omega = [
\begin{smallmatrix}
0, 0, -1
\end{smallmatrix}]^T
$.

The $K_H$ and $\delta T$ terms are harder to compute though, and as discussed in \cite{Rich2012}, must be done iteratively using the simulation. The $K_H$ term is hand-tuned until the simulation matches with the actual data for the $x$ and $y$ axes step response, while the $\delta T$ parameter can be computed through comparing the quadratic approximation of the rotor force with more complex equations relating the rotor force to the rotor speed. For this work, both of those terms are neglected. This can be done because the simulations run with $K_H = 0$ and $\delta T = 0$ compare very well to the actual flight data captured using the PID controllers from section \ref{sec:control:pid}.

\section{Overall Parameters}
The tables inside table \ref{tab:params} contain all the parameters estimated for the Crazyflie quadrotor. They are broken apart into 4 subtables:
\begin{itemize}[noitemsep]
	\item Table \ref{tab:params_general} contains parameters related to the Crazyflie quadrotor in general.
	\item Table \ref{tab:params_nobatt} contains physical parameters of the Crazyflie with no battery.
	\item Table \ref{tab:params_240} contains physical parameters of the Crazyflie with the default 240mAh battery.
	\item Table \ref{tab:params_380} contains physical parameters of the Crazyflie with a larger 380mAh battery.
\end{itemize}

\input{Body/parameterization/parameters}
