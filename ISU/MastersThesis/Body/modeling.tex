% !TeX root = ../thesis.tex
\addtocontents{toc}{\protect\newpage}
\chapter{MODELING OF THE CRAZYFLIE ON-BOARD CONTROLLER}
\label{sec:crazyflieModeling}

\section{Overall Flow}

In any controller there are two main tasks: sensing and control. These tasks are also sequential tasks usually, since the control to be computed requires knowledge of the current system state. In the Crazyflie these two tasks are present, and contain smaller parts as well. The overall structure of the controller flow used with the Crazyflie's in this work can be seen in figure \ref{fig:model:correctStructure}. Note that this controller flow is not the same as in the default Crazyflie firmware. The default control flow contains several discrepancies in it that make modeling the quadrotor difficult, so those discrepancies were identified and fixed. More discussion on the discrepancies can be found in appendix \ref{app:inconsistencies}.

\begin{sidewaysfigure}[p!]
	\centering
	\includegraphics[width=\textwidth]{CompleteControlStructure_Correct}\
	\caption{Mathematical structure of the firmware's control loop}
	\label{fig:model:correctStructure}
	\rotateFigurePageForLabel{fig:model:correctStructure}
\end{sidewaysfigure}

\section{Sensing Subsystem}

The first stop in the controller flow is the sensing subsystem. This subsystem's purpose is to estimate the current attitude and angular rates of the quadrotor. To do this, the Crazyflie has two sensors: an accelerometer and a gyroscope. The accelerometer allows the Crazyflie to measure the gravitational vector (in the quadcopter body frame) and the gyroscope allows the Crazyflie to measure the angular rates about the principal body axes.

On the Crazyflie, those two sensors are contained in a single chip, the Invensense MPU-9250 \cite{invensense9250}. The Crazyflie mounting of this chip has the sensor axes system not aligned with the aerospace axes system the model and controllers use. The chip instead uses a right-handed coordinate system where the $x$ axis is to the left, and $z$ is up. This means the first step after reading the sensor is to move the sensor readings into the body frame from the sensor frame.

\subsection{Attitude Estimation}
\label{sec:model:firmware:attEst}

Once the sensor readings are aligned with the body frame, the actual attitude estimation occurs. On the Crazyflie there are three options for estimating the attitude: an Extended Kalman Filter \cite{Mueller2015}, a Madgwick Filter \cite{Madgwick2011}, and a Mahoney filter \cite{Euston2008}. In the firmware, the Mahoney filter is the one that is active by default.

The Mahoney filter is a recursive filter that operates in the following manner:
\begin{enumerate}[noitemsep]
	\item Take the sensed gravitational vector and angular rates as input
	\item Compute the estimated gravitational vector using the previous pose estimate
	\item Compute the error between the graviational vectors
	\item Update the pose estimate using the error and the angular rates
\end{enumerate}
The filter keeps track of the pose of the UAV using the quaternion representation for rotation.

The filter in equation form is:
\begin{subequations}
	\begin{align}
		e &= \bar{g} \times \hat{g} \label{eq:model:attest:error} \\
		\delta &= K_p e + K_i \int e \label{eq:model:attest:pi} \\
		\dot{\hat{q}} &= \frac{1}{2}\hat{q} \otimes \mathbf{p}(\bar{\Omega} + \delta) \label{eq:model:attest:filterUpdate}
	\end{align}
\end{subequations}
Where $\hat{g}$ is the estimated normalized gravitational vector, $\bar{g}$ is the measured gravitational vector, $\bar{\Omega}$ is the measured angular rates, and $\hat{q}$ is the estimated attitude in quaternions. Note that the operations $\otimes$ and $\mathbf{p}()$ are the quaternion multiplication and pure quaternion operation respectively.

Breaking down the filter operations in small chunks, there are three distinct phases. In the first phase, \eqref{eq:model:attest:error} is used to figure out the angular error between the estimated gravitational vector and the measured gravitational vector. In the second phase, that angular error is fed into the PI controller \eqref{eq:model:attest:pi}, which allows for a tuned filter response. This PI controller will drive the estimated vector to the measured vector, and remove the steady state error between them. The third step is the filter update phase. In this phase the measured angular velocity is combined with the PI output and used to update the quaternion estimate in \eqref{eq:model:attest:filterUpdate}. A block diagram of this filter can be seen in \ref{fig:model:firmware:mahoneyFilter}.

\begin{figure}[h!]
	\centering
	\includegraphics[trim={0 200 0 200},clip,scale=0.6]{Model_Mahoney_Filter}
	\caption{Block diagram of the Mahoney filter}
	\label{fig:model:firmware:mahoneyFilter}
\end{figure}

Usually, UAVs use an accelerometer to measure the graviational field direction. These accelerometers actually measure a vector in the opposite direction of gravity \cite{Pedley2013}, so the measured gravitational vector is actually $\bar{g} = - a$ where $a$ is the accelerometer reading. The original Crazyflie firmware actually used the raw accelerometer vector in the filter computation, instead of the gravitational vector. This work uses firmware modified to use the gravitational vector instead. More description of the filter input discrepancy can be found in appendix \ref{app:inconsistencies}.

\section{Controller Subsystem}

The controller subsystem on the Crazyflie's is designed to be a modular component. In the firmware, the controller functions get passed all the states and all the setpoints, so the controller function can use any available information about the quadrotor in its computation. Several controllers have been implemented on the Crazyflie, including nested-loop PIDs (which are the default controllers), the large-angle nonlinear from \cite{Mellinger2012} (implemented with slight modifications by \cite{Preiss2017}), and many others. This framework also makes it simple to implement custom controllers such as a generic state feedback, or other nonlinear controllers.

More discussion on the nested-loop PID controllers, including their structure and control response can be found in section \ref{sec:control:pid}.

\subsection{Thrust Compensation}
\label{sec:model:firmware:thrustComp}

Quadrotors are usually battery powered devices, with a finite amount of power available per charge to fly with. As the quadrotor flys, it depletes the charge available in the battery causing a lower voltage output. This voltage drop means the software motor command does not correspond with a uniform speed command over the flight of the quadrotor. When doing linear control design, the nominal battery voltage is normally used (which is 3.7V for the Crazyflie). This poses a problem though, since at the beginning of a flight the battery may be charged up to 4.1V, and then at the end the battery may be down to 3.2V. This represents a drastic change in the produced speed of the motors over the flight.

To overcome this, the default firmware includes a thrust compensation function for the brushed motors. The implementation code for the function is:
\begin{singlespace}
	\footnotesize
	\begin{lstlisting}[language=c,frame=single]
	float thrust = ((float)ithrust / 65536.0f) * 60;
	float volts = -0.0006239 * thrust * thrust + 0.088 * thrust;
	float supply_voltage = pmGetBatteryVoltage();
	float percentage = volts / supply_voltage;
	percentage = percentage > 1.0 ? 1.0 : percentage;
	ratio = percentage * UINT16_MAX;
	\end{lstlisting}
\end{singlespace}

This code performs a remapping of the thrust commanded (\textit{ithrust}) to the actual motor command (\textit{ratio}). A plot of this algorithm can be seen in figure \ref{fig:model:thrust:Existing}. Note in this plot that the actual command output never reaches 1 even though 1 is inputted, so over the range 3.2 to 4.1 volts the motors will never run at full speed. This means that the firmware will be artificially limiting the thrust commands possible, making it so the Crazyflie can only lift 37 grams (including itself) with the battery at 3.7V.

Instead of the default thrust compensation, the Crazyflie was modified to use a scale-factor based method. This comprised two equations:
\begin{subequations}
	\begin{align}
		s &= 1 + \frac{3.7 - V_b}{3.7}\\
		T_a &= s * T_c
	\end{align}
	\label{eq:model:thrustRemap}
\end{subequations}
Where $V_b$ is the current battery voltage, $T_c$ is the software commanded thrust and $T_a$ is the thrust actually written to the motors. A plot of the implemented thrust compensation can be seen in figure \ref{fig:model:thrust:Proposed}. Basically, this method would produce a thrust scaling such that at 3.7V if software commanded maximum thrust, then the actual command would be maximum thrust. This allows the Crazyflie to lift 49 grams (including itself) with the battery at 3.7V.

\begin{figure}[p]
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[scale=0.35]{Model_Thrust_Existing}
		\caption{Existing thrust remapping}
		\label{fig:model:thrust:Existing}
	\end{subfigure}	
	\begin{subfigure}{\textwidth}
		\centering
		\includegraphics[scale=0.35]{Model_Thrust_Proposed}
		\caption{Proposed thrust remapping}
		\label{fig:model:thrust:Proposed}
	\end{subfigure}
	\caption{Thrust compensation methods for overcoming battery drop}
\end{figure}

\subsection{Input Mixing}

The final step in the controller module is to mix the controller outputs ($u_T$, $u_A$, $u_E$ and $u_R$) into the motor inputs. This is accomplished using a simple linear mixing system called a mixing matrix. This matrix takes the controller outputs and performs a weighted sum of them to compute the motor outputs. The weights on the controller inputs are determined by the direction a motor must change to create a positive force. For example, if a motor must increase speed for the pitch angle to increase, then the weight on $u_E$ will be positive.

There are two common configurations for the motors on a quadrotor: in a cross configuration, where the motors are located on the principal body axes, and in an X configuration, where the motors are located on a line rotated $45^\circ$ from the body axes. The configuration of the motors affects the mixing matrix structure, for instance a cross configuration would introduce zeros into the matrix since only two motors can affect pitch and two can affect roll. In the cross configuration though, every motor affects every rotation.

The Crazyflie 2.0 uses the cross configuration, meaning it uses the mixing matrix given in \eqref{eq:model:mixer} for its motor mapping. Note in this mixer that the $u_A$ and $u_R$ inputs are scaled by $\sfrac{1}{2}$, this is just a quirk of the firmware though and is not needed.

\begin{equation}
	\label{eq:model:mixer}
	\begin{bmatrix}
		m_1\\
		m_2\\
		m_3\\
		m_4
	\end{bmatrix} = 
	\begin{bmatrix}
		1 & -\frac{1}{2} &  \frac{1}{2} &  1\\
		1 & -\frac{1}{2} & -\frac{1}{2} & -1\\
		1 &  \frac{1}{2} & -\frac{1}{2} &  1\\
		1 &  \frac{1}{2} &  \frac{1}{2} & -1
	\end{bmatrix}
	\begin{bmatrix}
		u_T\\
		u_A\\
		u_E\\
		u_R
	\end{bmatrix}
\end{equation}