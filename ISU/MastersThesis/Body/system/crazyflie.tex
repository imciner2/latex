% !TeX root = ../../thesis.tex

\section{Crazyflie Quadrotor Platform}
\label{sec:sys:crazy}

\subsection{Overview}

The Crazyflie is a small quadrotor, termed a nano-quadrotor, that measures 9.5cm from rotor to rotor diagonally, and takes up a horizontal area of 12cm by 12cm when flying. It has one main circuit board that contains all the electronics and motor control hardware, and then 4 legs that attach to the circuit board. These legs provide holders for the motors, and also landing gear to support the quadrotor. A Crazyflie can be seen in figure \ref{fig:sys:crazyflie}.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.07]{System_Crazyflie}
	\caption{Crazyflie quadrotor with Loco Positioning deck and 380mAh battery}
	\label{fig:sys:crazyflie}
\end{figure}

The propulsion system is composed of 4 brushed DC motors, each spinning a propeller measuring 4.6cm in diameter to provide the thrust required for flight and maneuvers.

\subsection{Electronics}

The Crazyflie electronics system is composed of two microprocessors: an ST Micro STM32F405 running at 168MHz, and a Nordic Semiconductor nRF51822 running at 16MHz. The STM32F405 is the main processor, performing all of the control and sensing functions for the Crazyflie. The nRF51822 is a secondary processor responsible for performing the radio communications and also power management of the Crazyflie. An overall diagram of the electronics system can be seen in figure \ref{fig:sys:crazyflieElectronics}. The Crazyflie uses an Invensense MPU-9250 sensor for its attitude estimation. The MPU-9250 contains 3 different sensors: a 3-axes accelerometer, a 3-axes gyroscope, and a 3-axes magnetometer.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.75]{System_CrazyflieElectronicsArchitecture}
	\caption{Architecture of the Crazyflie electronics \cite{crazyflieSystemDiagram}}
	\label{fig:sys:crazyflieElectronics}
\end{figure}

The Crazyflie receives its control inputs over a 2.4GHz Bluetooth link. This link is handled by the nRF51822 chip on the Crazyflie, and interfaces to a computer through a CrazyRadio dongle (which is a 2.4GHz to USB bridge developed by Bitcraze).

\subsection{Firmware}

The Crazyflie operates using firmware\footnote{The description of the firmware in this section is a general overview. Details of specific changes made to the firmware will be discussed in later sections.} 
developed with the C programming language, and FreeRTOS (an open-source real-time operating system \cite{freertos})\footnote{In this work, only the firmware running on the STM32F405 chip was modified and analyzed, the firmware running on the nRF51822 was not examined.}. In the firmware, each main part is contained within its own task. This means that each part has its own function running in an infinite loop, and then the FreeRTOS system periodically interrupts the task to allow for the others to execute. 


\subsection{Trackable Mounting}
\label{sec:sys:crazy:trackableMounting}

In order for the Optitrack camera system to locate each Crazyflie, they must be outfitted with at least three IR reflective markers to create the rigid body. Additionally, the Tracking Tools software requires that each rigid body have a distinct trackable constellation to guarantee reliable tracking. If the constellation is not unique between two bodies, the software may confuse the bodies and provide inaccurate data.

The existing system used pre-made IR trackable orbs that can be purchased directly from OptiTrack. Three of these orbs were mounted at varying positions on the Crazyflie to create the constellation, as seen in figure \ref{fig:existSystem:trackables}. The mounting system utilized bolts attached to the Crazyflie with Velcro as the primary method of attaching the orbs. The issue is, when the Crazyflie would perform aggressive maneuvers (such as a fast takeoff), the velcro adhesive would fail and the orb would fall off. This meant the constellation was no longer trackable, and no position data was available.

To overcome this issue, a frame for the trackables was used. This frame is based off of a design created in the MIT CSAIL lab for holding their constellation during flight \cite{Landry2015, mitCadRepo}. A CAD rendering of the frame used can be seen in figure \ref{fig:sys:frameCad}.
\begin{figure}[h!]
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.2]{System_TrackableFrameRendering}
		\caption{CAD rendering of frame}
		\label{fig:sys:frameCad}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.07]{Hardware_CrazyflieWithTrackables}
		\caption{Crazyflie with trackables attached}
		\label{fig:sys:crazyflieWithTrackables}
	\end{subfigure}
	
	\caption{Trackable mounting system for the Crazyflie quadrotor}
	\label{fig:sys:trackableMounts}
\end{figure}

Additionally, the use of the trackable orbs on the Crazyflie posed issues with increasing the mass and moving the center of mass away from the origin of the principal body axes. This is because each trackable orb weighed approximately 1 gram, and the support hardware for each orb was another 1-2 grams. This meant that approximately 10 grams of mass was added to the Crazyflie by the orbs, mounting hardware, and other mass used to re-center the center of mass. This additional mass caused a reduction in flight-time and thrust available for performing maneuvers.

Instead, the trackable orbs were replaced by custom trackables fashioned out of nylon standoffs and IR reflective tape. One standoff and its mounting hardware had a mass of 0.5 grams. These standoffs could be placed at 6 different locations on the Crazyflie: four on the frame and two on the main board. An arrangement of these standoff-based trackables on a Crazyflie can be seen in figure \ref{fig:sys:crazyflieWithTrackables}. Overall, the Crazyflie quadrotor with a 380mAh battery, and the new trackables had a mass of 36 grams.
