% !TeX root = ../../thesis.tex
\section{Distributed Computation}

\subsection{Ideal System}

The ideal system for performing distributed computation on the Crazyflies would have the following features:
\begin{itemize}[noitemsep,nolistsep]
	\item Mesh-network structure (agent-to-agent communication)
	\item Ability for each agent to run different algorithms
	\item Expandable to large number of agents
	\item Interaction between algorithm and control loop
\end{itemize}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth, trim={0 0 350 0}, clip]{Computation_IdealNetwork}
	\caption{Ideal distributed computation system}
	\label{fig:sys:comp:ideal}
\end{figure}

\subsection{Realized System}

In reality, the ideal system is not realizable on the current Crazyflie hardware due to limitations in its communications system. Instead, the systen shown in figure \ref{fig:sys:comp:realized} was implemented.

The Crazyflie communications system is currently designed to talk directly with a computer using the CrazyRadio dongle and the nRF51 chip. When this system was designed, the computer was thought of as the originator for all important communications, and the Crazyflie would only provide brief status updates. This means that the communications stack was implemented such that the Crazyflie cannot initiate communications, it can only return data as a response to a data packet from the computer.

This means that the Crazyflie's cannot natively have a mesh network capability, all communications must go through a host computer. This also limits the number of agents on a network to a small number, directly related to the number of CrazyRadio dongles available on the host computer. For example, testing with the computation example in chapter \ref{sec:app:localization} (with 100 computations per second) shows that when more than two Crazyflie's are placed on a single radio when the algorithm runs, then the data update rate for the control loop can slow down and cause unstable flight.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{Computation_RealizedNetwork}
	\caption{Realized distributed computation system}
	\label{fig:sys:comp:realized}
\end{figure}

The system is broken up into two distinct components: Ground Station Network Emulation and the Agent.

\subsubsection{Ground Station Network Emulation}

To overcome the communications limitation, this system utilizes a simulated network topology, run as a packet router on the ground station computer. This router can be loaded with an arbitrary network structure, which can be updated at run-time. The router treats all communications links as directed links, so both undirected and directed communications topologies can be implemented on this system.

Since this network coexists with the existing communications infrastructure of the Crazyflie, it relies on constant packet transmissions to the Crazyflie from the ground station (so the Crazyflie can reply with the algorithm output). When the ground station receives an algorithm output packet, it is sent to the network router. The router examines the from address, and looks up all the agents that are to receive the packet. The packet is then placed into the FIFO for each receiving agent, where it is then sent to the agent at the next opportunity.

\subsubsection{Agent}

The agent component is the driving piece behind the computational network. Each agent is responsible for executing its algorithm's computations at the desired interval, and then transmitting the result over the network when desired. This means that the computation on the agent will occur at the desired rate, independent of data reception.

A key component of the agent's algorithm structure is the tie-in between the control loop and the algorithm computation. Inside each control loop iteration there is a call to the algorithm component. This call does two things:
\begin{enumerate}[noitemsep, nolistsep]
	\item Pass the current state vector to the algorithm
	\item Update the control setpoints with values from the algorithm
\end{enumerate}
This interconnection allows for the algorithm to use current state data (such as position, velocity, etc) in its computations, and then also to modify the setpoint of the control loops based on the computations (such as the current linear position).