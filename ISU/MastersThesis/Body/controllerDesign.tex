% !TeX root = ../thesis.tex
\chapter{MODEL VERIFICATION AND CONTROLLER DESIGN}
\label{sec:control}

With the quadrotor parameterization conducted in chapter \ref{sec:param} and the physics model of the quadrotor described in \cite{Rich2012}, the quadrotor system can be simulated in Simulink to view the response of the physics to different controllers, and also to design more complex model-based controllers. The first controllers examined were nested loop PIDs. This control scheme is common on quadrotors, and comes in the standard Crazyflie firmware. This controller scheme was also used to stabilize the quadrotor so that the model and the actual response could be compared to gauge the accuracy of the model.

Next, state feedback controllers were designed using the model. Initially an LQR design methodology was used to find the feedback gains, however flight tests showed that the LQR had large steady-state offsets on $x$ and $y$ along with oscillations on the Euler angles. To reduce the steady-state offsets and the oscillations, integrator states were added to the controller on the $x$, $y$, $z$, $\phi$, $\theta$ and $\psi$ states.

\input{Body/control/pid}

\clearpage

\input{Body/control/statespace}

\clearpage

\section{Observations}

Overall, the controllers developed in this work are capable of stabilizing the position of the Crazyflie. The proposed state space controller was difficult to obtain through normal methods (such as LQR design methodology), so instead the structure of the $K$ matrix was exploited to hand-tune the gains.

One of the reasons the state space controller was difficult to design is due to the small size of the Crazyflie. The small size makes the Crazyflie more susceptible to disturbances, such as brief gusts of air (e.g. from an HVAC system), than a larger quadrotor with more mass would be. Additionally, the LQR controller was difficult to design because the initial weights were not obvious/known. This meant that a lot of time was spent trying out different weights to see their behavior.

An additional part of the Crazyflie that has a drastic effect on the controller performance is the center of mass location. The model used in this work assumes a center of mass located at the origin of the body coordinate system. In actuality, the center of mass of the Crazyflie is offset due to the battery and trackable frame. Before the trackable frame was redesigned to add counterweights, the controllers would have difficulty stabilizing during takeoff. Normally the Crazyflie would shoot to one side, then slowly recover to the desired setpoint. This also introduced asymmetry in the angular movements, with movement in certain directions faster than others. With the redesigned trackable frame, the center of gravity was moved closer to the origin, allowing for smoother takeoffs. However, lingering effects still remain.

In order to overcome the center of mass offset, experimental measurement of its location should be conducted. This can then be fed into the model, and a new linearized system can be developed. This system will not be as nice to work with as the one in this work, since it will contain cross terms between the axes. Additionally, the addition of the center of mass will remove the saw-tooth pattern in the SGSF controller matrix and make $K$ be fully populated.

An additional model parameter to revisit would be the $K_H$ term. In chapter \ref{sec:param}, it was believed based upon the experimental PID comparison against the simulation that the model was in good agreement with $K_H = 0$. Future work should revisit that assumption, and use the SGSF controller to determine if a better value for $K_H$ exists.

Finally, some more advanced controllers should be explored, such as the \textbf{SO}(3) controller proposed by \cite{Mellinger2012}. This controller has been shown to work nicely on the system in \cite{Preiss2017}, so it should provide a good starting point for experiments in advanced control.