% !TeX root = ../../thesis.tex

\section{Moment of Inertia}

Determining the moment of inertia for the Crazyflie quadrotor is difficult due to its small size and mass. Prior work has focused on either using accurate CAD models with the proper densities of parts to compute the values (eg. \cite{Landry2015} for the Crazyflie 2.0 or \cite{Subramanian2015} for the Crazyflie 1.0) or mounting the Crazyflie as a pendulum and measuring the period of its swing (eg. \cite{Forster2015}). Since the Crazyflie's used in this work utilize a larger battery than standard, the moment of inertia measurements in previous works would not be valid, so new measurements were needed. 

Previous experiments in the lab at Iowa State had measured the moment of inertia of a quadrotor using a known torque applied to a rotating test stand. Based upon the measured angular position and applied torque, the moment of inertia could then be computed \cite{Rich2012}. This method worked well for that quadrotor since its inertias were on the order of $10^{-3}$ and the test stand had inertia on the order of $10^{-3}$ \cite{ECP220}. However, since the Crazyflie's inertia has been previously estimated to be on the order of $10^{-5}$, that test stand will not allow for accurate measurement of the moment of inertia. Instead, this work uses a bifilar pendulum to measure the moment of inertia about each of the three main rotational axes on the Crazyflie. The bifilar pendulum has been used in other works to measure the moment of inertia of objects ranging from tennis rackets \cite{Spurr2014}, to model aircraft \cite{Kim2017, Jardin2007}.

\subsection{Bifilar Pendulum Theory}

A normal pendulum consists of an object attached to a single string which then is secured to a beam. To start rotation, the object is pulled vertically towards the beam while keeping the string taut, and then released. The motion of the object is then in an arc traced in a single vertical plane, as shown in figure \ref{fig:param:normalPendulum}.

In a bifilar pendulum, the object is attached to two strings that are then run parallel to each other up to the beam where they are attached. To start rotation, the object is rotated horizontally about a point centered between the two strings. This then causes the object to begin rotating in the horizontal plane, as shown in figure \ref{fig:param:bifilarPendulum}.

\begin{figure}[h!]
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.3]{BifilarPendulum_NormalPendulum}
		\caption{Normal pendulum}
		\label{fig:param:normalPendulum}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.5]{BifilarPendulum_Bifilar}
		\caption{Bifilar pendulum}
		\label{fig:param:bifilarPendulum}
	\end{subfigure}
	
	\caption{Illustration of angular path for the normal pendulum and bifilar pendulum}
\end{figure}

When the object is rotated in the horizontal plane, the fact that the bifilar pendulum's wires cannot change length means that the object is actually pulled up slightly from its equilibrium position. This slight movement is enough to create gravitational potential energy in the system in excess of the equilibrium energy. When it is released, this potential energy is subsequently converted into kinetic energy for the pendulum's movement, and so the rotation begins. In the real-world, the pendulum will experience damping effects due to the aerodynamic drag of the object and the viscous damping of the pendulum. This causes the rotations to gradually decay.

To develop a physics model for this system, it is common to use the Lagrangian dynamics approach to examine the energy transfer in the system \cite{Jardin2007}. Through this approach, the nonlinear mathematical model given in \eqref{eq:param:bifilarNonlinear} can be found with the variables listed below.
{\footnotesize
\begin{align*}
	\theta &= \text{Angular position relative to equilibrium}\\[-1em]
	I &= \text{Moment of inertia of the object} \\[-1em]
	K &= \text{Aerodynamic drag constant} \\[-1em]
	C &= \text{Viscous damping constant} \\[-1em]
	D &= \text{Distance between the wires} \\[-1em]
	m &= \text{Mass of the object} \\[-1em]
	h &= \text{Height of wires (from center of mass to attachment point)}
\end{align*}}
\begin{equation}
	\label{eq:param:bifilarNonlinear}
	\ddot{\theta}
	 + \left[ \frac{K}{I} \dot{\theta} \abs{\dot{\theta}} + \frac{C}{I} \dot{\theta} \right]
	 + \left( \frac{m g D^2}{4 I h} \right)
	 \frac{\sin{\theta}}{\sqrt{1 - (1/2)(D/h)^2(1 - \cos{\theta})}}
	 = 0
\end{equation}

Additionally, the nonlinear mathematical model can be linearized about the equilibrium point $\theta = 0$, giving \eqref{eq:param:bifilarLinear} where $A$ is the average amplitude of the oscillations.
\begin{equation}
	\ddot{\theta} + \left[ \frac{8 A K}{3 \pi I} + \frac{C}{I} \right]\dot{\theta} + \left( \frac{m g D^2}{4 I h} \right)\theta = 0
	\label{eq:param:bifilarLinear}
\end{equation}
Exploiting the fact that this is a second-order system, the moment of inertia can be found in relation to the other given variables and the natural frequency of the oscillation, since
$$
\omega_n^2 = \frac{m g D^2}{4 I h}
$$
Solving for $I$ then produces \eqref{eq:param:bilinarMomentOfIntertia} \cite{Jardin2009}.
\begin{equation}
	I = \frac{m g D^2}{4 h \omega_n^2}
	\label{eq:param:bilinarMomentOfIntertia}
\end{equation}

\subsection{Measurement Procedure}

When using the bifilar pendulum technique to measure the moment of inertia, the setup of the experiment is critical to the accuracy of the results. When setting up the pendulum itself two things must be kept in mind, otherwise errors in the measurements will occur:
\begin{itemize}[nolistsep, noitemsep]
	\item The wires must be parallel to each other
	\item The point of rotation (located halfway between the wires) should be located at the center of mass of the object
\end{itemize}

There are two parameters in the calculation that are user-controllable and can be used to improve the accuracy of the measurements. It has been determined analytically in \cite{Jardin2009} that $h$ should be as large as possible. This helps because when $h$ is large, the frequency of oscillation is decreased making its measurement more accurate. Additionally, they discuss how there is an optimum value for $D$ based upon the desired error variance, damping coefficients, and moment of inertia. This value is difficult to calculate a priori though, so in this work the wire spacing was determined by the ease of physically attaching the strings to the Crazyflie.

\begin{figure}[h!]
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[scale=0.13]{BifilarPendulum_RollPitch.jpg}
		\caption{$I_{xx}$ mounting}
		\label{fig:param:bifilarIxxMounting}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[scale=0.15]{BifilarPendulum_Yaw.jpg}
		\caption{$I_{zz}$ mounting}
		\label{fig:param:bifilarIzzMounting}
	\end{subfigure}
	
	\caption{Mounting orientations for the bifilar pendulum experiments.}
	\label{fig:param:bifilarMounting}
\end{figure}

The physical mounting of the Crazyflie for measuring the $I_{xx}$ and $I_{yy}$ can be seen in figure \ref{fig:param:bifilarIxxMounting}. Fishing line was attached to the motors on the Crazyflie, making $D = 6.35$cm. This fishing line was then connected to the ceiling of the lab so that the height was as large as possible at $h = 222.25$cm. For mounting the Crazyflie to measure the $I_{zz}$ value, the fishing line was attached to the plastic motor support legs. In this position though, the Crazyflie's center of mass is higher up than the motor supports, causing it to want to flip upside down. To overcome this, the Crazyflie was just mounted upside down, since this will have no effect on the measured moment of inertia but make the mounting more stable.

The Crazyflie under test was instrumented with small pieces of IR reflective tape at three positions, allowing it to be tracked using the Optitrack camera system in the lab. The angular position was then measured using the camera system. While normally a small initial angular displacement should be used to avoid nonlinear affects, the size of the Crazyflie made measuring small angles and rotating it to small angles difficult. So a larger angular displacement was used initially, meaning the full non-linear model is needed for the final parameter estimation. The oscillations were then recorded for 2 minutes and the data was exported so that it could be post-processed in MATLAB.

\subsection{Results}

To do the parameter estimation for the moment of inertia, the nonlinear dynamics equation in \eqref{eq:param:bifilarNonlinear} was implemented in a Simulink block diagram. The parameters $K$, $C$, and $I$ were made tunnable by a script. Then a simulation was run starting at the maximum angular deflection and running for as long as the dataset. The simulated trajectory was plotted against the recorded data and a manual process of tunning the parameters was used to hone in on the parameters. A sample plot showing the simulated trajectory versus the measured trajectory can be seen in figure \ref{fig:param:bifilarResults}. The final moments of inertia can be seen in tables \ref{tab:params_nobatt}, \ref{tab:params_240}, and \ref{tab:params_380} for no-battery, the 240mAh battery and the 380mAh battery respectively.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{BifilarPendulum_Results_Crazyflie_380mAh_Z}
	\caption{Plot showing the bifilar pendulum's measured angular position versus the simulated angular position}
	\label{fig:param:bifilarResults}
\end{figure}

Comparing the results presented here against those in \cite{Landry2015, Forster2015} for the Crazyflie 2.0 shows that the computed moments of inertia are similar. Additionally, based upon the results in \cite{Forster2015}, the assumption that the non-diagonal terms in the moment of inertia matrix are zero is valid, since their results show the non-diagonal terms to be at least an order of magnitude smaller than the diagonal components.