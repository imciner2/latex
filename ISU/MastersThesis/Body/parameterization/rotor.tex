% !TeX root = ../../thesis.tex

\section{Rotor Parameters}

In the model of the quadrotor that is being used, there are three main parameters associated with the rotor subsystem: the thrust constant, the in-plane drag constant, and the equivalent moment of inertia of the rotor and motor shaft. While each of these three parameters could be analytically found using various dimensions and assumptions about the design of the rotor (see the discussion in section 4.2 of \cite{Rich2012}), those calculations require parameters which are difficult to measure and not widely available. Instead, methods to measure the parameters directly were used, and the appropriate experimental setup was created.

\subsection{Measurement Setup}

\subsubsection{Measured Quantities}

In order to calculate the three parameters for the rotor system, the following physical quantities must be simultaneously measured:
\begin{enumerate}[noitemsep, nolistsep]
	\item Thrust produced
	\item Rotor angular speed
	\item Motor current draw
	\item Commanded duty cycle
\end{enumerate}
The method of measuring each of those quantities is now discussed

\newpage
\noindent \textbf{Thrust}

In order to measure the thrust produced by the rotors, the Crazyflie was secured to a Vernier Dual-Range Force Sensor \cite{vernierForce}. This sensor is able to measure the force applied to it on either a 10 or 50 Newton scale with an accuracy of 0.01N and 0.05N respectively. For these experiments, it was set on the 10N scale. This sensor was connected to a Vernier LabPro interface which was attached to a computer running the Vernier LoggerPro software \cite{vernierLoggerPro}.

\noindent \textbf{Rotor angular speed}

Since the rotors of the Crazyflie are very small and it uses brushed motors, the method of measuring rotor speed given in \cite{Rich2012} is not possible. Instead a new method was devised that used a photointerrupter to count the number of times a rotor blade passed through the beam in 1 second. More details on this device can be found in the next section.

\noindent \textbf{Motor current draw}

To measure the current draw of the motors, the display on the power supply was used. This power supply was capable of supplying up to 5A at the desired voltages, and could measure in the milliamp range.

\noindent \textbf{Commanded duty cycle}

The commanded duty cycle was measured as the value sent directly to the \textit{setMotorRatio()} function in the Crazyflie firmware. Additionally, when performing these test the thrust compensation feature of the firmware was disabled since this ffunction will change the duty cycle actually sent to the motor from what is desired. A special firmware version was developed that bypassed the controller structure and allowed for the motors to be set directly from the ground station software.

\subsubsection{Physical Test Apparatus}
\label{sec:param:rotor:testApparatus}

The physical test apparatus used in these experiments consists of two parts: mechanical and electrical\footnote{The CAD files, electrical schematics, and source code files can be found at \url{https://github.com/imciner2/Crazyflie_Tools}}.

\begin{figure}[hp]
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.25]{ThrustStand_ForceSensorMount}
		\caption{CAD rendering of the force sensor mount}
		\label{fig:param:cadBase}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.25]{ThrustStand_PhotointerrupterBoard}
		\caption{Circuit board for the photointerrupter}
		\label{fig:param:photoBoard}
	\end{subfigure}\newline\newline
	
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.085]{ThrustStand_FrontView.jpg}
		\caption{Front view}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.085]{ThrustStand_SideView.jpg}
		\caption{Side view}
	\end{subfigure}
	\caption{Setup used to measure the rotor parameters.}
	\label{fig:param:rotorSetup}
\end{figure}

The main portion of the mechanical part consists of a 3D printed base in the shape of an X. The Crazyflie's feet attach into slots at the end of each leg of the X, and the center of the base contains a mounting hole and slot to attach to the force sensor and prevent rotation. There are also two supports on the base to hold the four photointerrupter boards so that the propeller passes through the beam in the center of the opening. A CAD rendering of the base can be seen in figure \ref{fig:param:cadBase}. Additionally, the force sensor was wrapped in aluminum window screen to shield it from electrical noise. It was found that the Crazyflie's Bluetooth radio easily interferes with the force sensor, creating measurement bias and more high frequency noise.

Each photointerrupter is mounted on a custom designed circuit board. That circuit board contains the passive electrical components necessary for each photointerrupter, and easy wire attachments for the power and signal wires. Additionally, the circuit board acts as the main support to hold the photointerrupter vertical on the base. A rendering of the board can be seen in figure \ref{fig:param:photoBoard}.

The photointerrupter used is the Sharp GP1A57HRJ00F \cite{SharpPhotointerrupter}. It consists of an IR transmitter and IR receiver, with a gap of 1cm between them. It outputs a 5V logical signal indicating if the IR beam is sensed at the receiver (5V if the gap is clear, 0V if the gap is blocked). The four photointerrupters are wired back to a single ST Microelectronics Nucleo F401RE development board \cite{Nucleo}. This development board contains an ST Microelectronics STM32F401RE ARM core processor and integrated JTAG programmer.

There are two different software programs that were developed for the STM32 processor, one to measure the rotor speed and the other to measure the motor transient. To measure the motor speed, the software counts the number of times the photointerrupter beam is broken in one second. The motor speed is then half of that number (since the Crazyflie's have two blades on the rotors). The measured rotor speeds are reported over the serial port every two seconds for recording.

To measure the motor transient, the software counts the amount of time between beam breaks. This time when multiplied by two gives the rotor's instantaneous angular period. The raw period values are sent over the serial port to a recording computer, where they are post-processed in MATLAB to compute the instaneous angular velocity.

\subsubsection{Procedure}
\label{sec:param:rotorProcedure}

In order to get the best parameter fit possible for the thrust constant and the in-plane drag constant, data was collected at 21 duty cycles between 15000 and 65000, and at five different supply voltages (3.2V, 3.5V, 3.7V, 4.0V, and 4.2V).

The procedure for gathering the data is as follows:
\begin{enumerate}[noitemsep, nolistsep]
	\item Set the power supply to the desired voltage
	\item Set the motors to the desired duty cycle using the software
	\item Collect 1 minute of force data in LoggerPro then average the data
	\item Record:
	\subitem Measured angular speed of each rotor
	\subitem The average force data
	\subitem Power supply current
\end{enumerate}

To measure the equivalent moment of inertia, the rise-time of the motor-rotor system was measured using the following procedure
\begin{enumerate}[noitemsep, nolistsep]
	\item Command the motor to a speed so that it is just barely spinning
	\item Provide a step change in the speed to just below full-speed
	\item Record the instantaneous speed as the step occurs
\end{enumerate}

\subsection{Thrust Constant Calculation}

The rotor thrust constant $K_t$ relates the rotor angular speed to the thrust produced by the rotor through
$$ \abs{T} = K_t \omega^2$$
In these experiments, the thrust measured is the combined thrust of all four rotors. This means that at each datapoint the relation is
$$ \abs{T} = K_t (\omega_1^2 + \omega_2^2 + \omega_3^2 + \omega_4^2) $$
Since 105 datapoints were collected, an overdetermined system of equations is created of the form
\begin{equation*}
	\begin{bmatrix}
	\abs{T_1}\\
	\abs{T_2}\\
	\vdots\\
	\abs{T_{105}}
	\end{bmatrix} = 
	K_t
	\begin{bmatrix}
	\omega_{1,1}^2 + \omega_{2,1}^2 + \omega_{3,1}^2 + \omega_{4,1}^2\\
	\omega_{1,2}^2 + \omega_{2,2}^2 + \omega_{3,2}^2 + \omega_{4,2}^2\\
	\vdots\\
	\omega_{1,105}^2 + \omega_{2,105}^2 + \omega_{3,105}^2 + \omega_{4,105}^2\\
	\end{bmatrix}
\end{equation*}
This system can then be solved for $K_t$ using least-squares regression producing
$$ K_t = 1.7449 \times 10^{-8} $$
The resulting fit line can be seen in figure \ref{fig:param:propellerForce}.

\subsection{Rotor Drag Constant Calculation}

The rotor drag constant relates the drag torque generated by the rotor's spinning motion to the current rotor speed through
$$ T_d = k_d \omega^2 $$

Calculating this parameter is not as simple as the thrust constant from the previous section, since measuring the actual torque produced by the rotor is not a simple task. Instead, the drag torque can be estimated by observing the steady-state velocity of the rotor at a given command (as described in \cite{Rich2012}), since the steady-state velocity $\omega$ is given by
\begin{equation}
	0 = \frac{1}{R_m K_Q}u_p V_b - \frac{1}{R_m K_Q K_v}\omega - \frac{1}{K_Q}i_f - K_d \omega^2
	\label{eq:param:rotorSteadyStateSpeed}
\end{equation}

Finding $K_d$ using this relation now depends on the following physical parameters: $R_m$, $K_Q$, $K_v$ and $i_f$. (all of which are known). Additionally, three different variables must be measured at every data point for this equation:
\begin{itemize}[noitemsep, nolistsep]
	\item $u_p$ - The command given to the motor (in the range $[0,1]$)
	\item $V_b$ - The voltage being supplied to the motor
	\item $\omega$ - The angular velocity of the rotor
\end{itemize}

In this work, those variables were measured using the setup described in \ref{sec:param:rotor:testApparatus}, while using the procedure given in section \ref{sec:param:rotorProcedure}. Each rotor speed command created 4 instances of \eqref{eq:param:rotorSteadyStateSpeed}, meaning there are 420 equations. Now the unknown parameter $K_d$ could be solved using the following linear relation

\begin{equation*}
	\begin{bmatrix}
		\frac{1}{R_m K_Q}u_{p(1,1)} V_{b(1,1)} - \frac{1}{R_m K_Q K_v}\omega_{1,1} - \frac{1}{K_Q}i_f\\
		\frac{1}{R_m K_Q}u_{p(2,1)} V_{b(2,1)} - \frac{1}{R_m K_Q K_v}\omega_{2,1} - \frac{1}{K_Q}i_f\\
		\frac{1}{R_m K_Q}u_{p(3,1)} V_{b(3,1)} - \frac{1}{R_m K_Q K_v}\omega_{3,1} - \frac{1}{K_Q}i_f\\
		\frac{1}{R_m K_Q}u_{p(4,1)} V_{b(4,1)} - \frac{1}{R_m K_Q K_v}\omega_{4,1} - \frac{1}{K_Q}i_f\\
		\frac{1}{R_m K_Q}u_{p(1,2)} V_{b(1,2)} - \frac{1}{R_m K_Q K_v}\omega_{1,2} - \frac{1}{K_Q}i_f\\
%		\frac{1}{R_m K_Q}u_{p(2,2)} V_{b(2,2)} - \frac{1}{R_m K_Q K_v}\omega_{2,2} - \frac{1}{K_Q}i_f\\
		\vdots\\
		\frac{1}{R_m K_Q}u_{p(3,105)} V_{b(3,105)} - \frac{1}{R_m K_Q K_v}\omega_{3,105} - \frac{1}{K_Q}i_f\\
		\frac{1}{R_m K_Q}u_{p(4,105)} V_{b(4,105)} - \frac{1}{R_m K_Q K_v}\omega_{4,105} - \frac{1}{K_Q}i_f\\
	\end{bmatrix} = 
	K_d
	\begin{bmatrix}
		\omega_{1,1}^2\\
		\omega_{2,1}^2\\
		\omega_{3,1}^2\\
		\omega_{4,1}^2\\
		\omega_{1,2}^2\\
%		\omega_{2,2}^2\\
		\vdots\\
		\omega_{3,105}^2\\
		\omega_{4,105}^2\\
	\end{bmatrix}
\end{equation*}
By then solving this over-determined system as a least-squares problem, the following value was found, which created the fit line seen in figure \ref{fig:param:propellerDragTorque}.
$$
K_d = 1.6881 \times 10^{-10}
$$



\begin{figure}
	\begin{subfigure}{0.9\textwidth}
		\centering
		\includegraphics[scale=0.65]{CrazyfliePropeller_RotorForce}
		\caption{Rotor force curve}
		\label{fig:param:propellerForce}
	\end{subfigure}
	
	\begin{subfigure}{0.9\textwidth}
		\centering
		\includegraphics[scale=0.65]{CrazyfliePropeller_DragTorque}
		\caption{Rotor drag curve}
		\label{fig:param:propellerDragTorque}
	\end{subfigure}
	\caption{Computed rotor parameter versus the measured dataset}
\end{figure}

\subsection{Equivalent Moment of Inertia Calculation}

To compute the equivalent moment of inertia for the motor-rotor system, the first-order transfer function model of the rotor speed generation is used
$$ \omega(s) = \frac{K_v}{R_m K_v K_q J_r s + 1} U(s)$$
A measurement of the time constant of the physical system was then made by providing a step input and using the second of the software programs discussed in \ref{sec:param:rotor:testApparatus} to record the response. The resulting step response can be seen in figure \ref{fig:param:propellerStepResponse}. The time constant is then the time between the step starting and when it crosses $67\%$ of the final value, in this case $\tau = 0.0703 s$. By then using the fact that $\tau = R_m K_v K_q J_r$ and all parameters other than $J_r$ are known, the final value of $J_r$ can be found, producing
$$ J_r = 2.9747 \times 10^{-8} $$

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.65]{CrazyfliePropeller_StepResponse}
	\caption{Step response of the motor-propeller system when commanded duty cycle is changed}
	\label{fig:param:propellerStepResponse}
\end{figure}