% !TeX root = ../thesis.tex
\chapter{INTRODUCTION}
\label{sec:intro}

This thesis presents a multi-agent research platform based on the Crazyflie 2.0 quadrotor from Bitcraze. This research platform is capable of supporting multiple quadrotors flying at once, and also the ability to perform distributed computations using the quadrotor's CPU.

\section{Literature review}

\subsection{Quadrotor Modeling}

Quadrotors have gained a lot of interest in the research community over the past 5 years, and many papers and dissertations have been published describing the physics models of quadrotors.

In \cite{Rich2012}, a full featured non-linear physics model is developed. This physics model includes factors such as: the loss of rotor thrust due to airflow over the propellers (in all directions), a center of mass that is not coincident with the origin of the body frame, and many other things. This work also explored the application of the nested loop PID structure on a Gaui quadrotor, along with other more advanced controllers.

Some literature has also been published related to the modeling of the Crazyflie specifically. For instance, in \cite{GopabhatMadhusudhan2016} a model for the Crazyflie 1.0 is developed, and also parameters are given for the quadrotor. The Crazyflie 2.0 has also been studied in the existing literature. A model of the Crazyflie using differential flatness is developed in \cite{Landry2015}. That work also identified some physical parameters such as rotor constants, moments of inertias and masses. A more rigorous study of the Crazyflie platform was conducted in \cite{Forster2015}, where moments of inertias are measured, along with mappings of the input command to thrust/torque and aerodynamic drag coefficients for flight through the air.

\subsection{Multi-Agent Research Platforms}

In recent years, there has been a large push to develop systems that allow for experimental verification of mutli-agent swarm strategies and controller architectures. Many universities have developed systems to prototype algorithms in a mutli-agent environment, including the Massachusetts Institute of Technology's RAVEN testbed \cite{How2008}, the University of Pennsylvania's GRASP lab \cite{Michael2010, Kushleyev2013}, ETH Zurich's Flying Machine Arena \cite{Lupashin2011, Lupashin2014}, the University of Southern California's Crazyswarm \cite{Preiss2017}, and the University of Bologna \cite{Furci2015}.

The MIT RAVEN testbed \cite{How2008} supports both ground-based agents and UAV agents, and also allows for new agents to be added or removed depending on the test being performed. This system utilizes a Vicon motion capture system to gather position data of all the agents. This position data is then fed into a master controller to determine the mission plan (such as desired trajectories, desired agent positions, etc.). Those plans are then sent to the agent systems for implementation. In the case of the UAV systems, the actual flying systems are standard RC quadrotors (the Dragonflyer V Ti Pro) receiving their input (for thrust/roll/pitch/yaw commands ) over a Pule Position Modulated (PPM) signal. All outer-loop control (such as position) is done off-agent on dedicated processors, with the resulting commands transmitted over the RC link.

The University of Pennsylvania GRASP testbed \cite{Michael2010, Kushleyev2013} is designed solely for aerial robotics research. Its main large flight vehicle is the Hummingbird quadrotor from Ascending Technologies, with another custom developed quadrotor used for cases where smaller agents are required (such as large swarms). The position of the agents is sensed using a Vicon motion capture system, which is then connected to the agents using the Robot Operating System (ROS). Each quadrotor is connected to the system using Zigbee transceivers, which send and receive commands and can transmit log data back to the ground. The small quadrotors (described in \cite{Kushleyev2013}), take only angular position setpoints as command input and then perform on-board control of angular position and rate. All other trajectory generation and position control is done by a ground-based computer.

ETH Zurich's Flying Machine Arena \cite{Lupashin2011, Lupashin2014} is a very large indoor flying space equipped with a Vicon motion capture system with a sensed volume measuring 10 meters on each side. This space primarily uses the Ascending Technologies Hummingbird quadrotor as its flight vehicle, however other experimental systems can be tested in it (such as the distributed flight array \cite{Oung2010} or an omnidirectional cube \cite{Brescianini2016}). The system is designed similar to the RAVEN and GRASP testbeds discussed earlier, where the agents all communicate with a central network that consists of ground-based control computers and the agents. Those control computers monitor all the agents' states, and communicate with the agents over a non-ack'd communicatons link (so reception of a packet is not guaranteed). These control channels may be standard ethernet, or another industrial wireless channel (such as Zigbee). In this system, the agents are able to provide data to the ground control computers, as well as receive commands from them.

The University of Bologna has developed a multi-agent testbed \cite{Furci2015} using the Crazyflie 1.0 quadrotor from Bitcraze. This quadrotor is a small, inexpensive quadrotor that contains an on-board ARM Cortex M3 processor for its primary data processing and control. In this system, the quadrotors communicate with a ground station computer over a Bluetooth radio link developed by Bitcraze. Each Crazyflie gets its own dedicated ground radio (called the CrazyRadio) on the ground computer. The quadrotor only contains two control loops, the angular position and rate loops. The other control loops are contained within MATLAB and Simulink algorithms running on the ground station. The ground station receives position information for each quadrotor from an OptiTrack motion capture system.

The University of Southern California has developed a multi-agent test system, called the Crazyswarm \cite{Preiss2017}, based around the Crazyflie 2.0 from Bitcraze. This is the successor to the Crazyflie 1.0 which was used in the University of Bologna's testbed. It still communicates over a Bluetooth radio link with a ground station computer, but the Crazyswarm system modified the firmware and communications protocol to allow for multiple Crazyflies to run on a single radio. This allows them to run 39 Crazyflies on just 3 radios. In order to accomplish this, they implemented broadcast packets that are not ack'd by the quadrotors. This allows them to transmit the position of up to 2 Crazyflies in every packet. When flying, the swarm is not able to log data back to the ground station, since doing so will cause latency issues and delays in receiving the position information from the ground station. They have implemented the majority of the controls processing on-board the Crazyflie, allowing for internal control of its position and internal trajectory generation.

The Crazyswarm uses a Vicon camera system to determine the location of the Crazyflie's when flying. They do not use the proprietary tracking software that Vicon sells with the camera system though, and instead have implemented a tracking system based upon the Iterative Closest Point algorithm \cite{Besl1992}. This custom algorithm allows for every Crazyflie in their swarm to have the same arrangement of camera system markers, since the physical size of the Crazyflie makes creating a lot of distinct marker arrangements very difficult.

\input{Body/introduction/existingInfrastructure}

\section{Contributions}

\subsection{Goal}

The goal of this work is to extend the system from \cite{Noronha2016} to include the following:
\begin{itemize}[noitemsep, nolistsep]
	\item Parameterization of the Crazyflie system to allow for modeling and model-based control design
	\item Migration of all controllers onto the Crazyflie firmware
	\item Methods to modify on-board controllers while the quadcopter is running
	\item Multi-threaded ground station application
	\item Framework for performing distributed computation using the Crazyflies
\end{itemize}

This system will then be demonstrated on an application that requires distributed computation capability.

\section{Novelty}

This work presents several novel contributions. First, the parameterization done in this work is more fine-grained than that presented in previous works (such as \cite{GopabhatMadhusudhan2016, Landry2015, Forster2015}. The work done here includes not only the physical subsystem modeling, but also the modeling of the existing firmware structure.

Second, this work presents a system that is capable of supporting multiple Crazyflies flying at one time while performing distributed computations. While some larger systems such as GRASP and the Flying Machine Arena could support the distributed computations those systems use larger and more expensive quadrotors. The comparable system would be the CrazySwarm. This system does not contain the ability for distributed computation though, and focuses mainly on emulating swarm behavior.

Third, this work presents a novel algorithm for performing object localization using a multi-agent system with only distance measurements to the target. This method was developed in collaboration with Dr. Xu Ma, a former PhD student in our research group at Iowa State. This system is based on the solution of a non-convex Quadratically Constrained Quadratic Program (QCQP) using optimization dynamics methods. The Crazyflie system is then used to test this method in real life.


\section{Organization}

This work is organized into 9 chapters. Chapter \ref{sec:intro} presents the literature review for quadrotor modeling and multi-agent testbeds. In addition it presents the existing system, and outlines the contributions of this work. Chapter \ref{sec:sys} presents an overview of the overall system, the Crazyflie hardware platform, and the distributed computation framework that was developed. Chapter \ref{sec:quadModeling} presents an introduction to attitude representation and the physics of quadrotors. Chapter \ref{sec:software} presents the new software developed for this work. Chapter \ref{sec:crazyflieModeling} presents a discussion on the control flow of the on-board controller for the Crazyflie. Chapter \ref{sec:param} presents the parameterization methods and final parameters for the Crazyflie 2.0. Chapter \ref{sec:control} presents a description of the on-board PID controllers, a comparison between a non-linear simulation and actual flight data, and the design of a linear static-gain state-feedback controller. Chapter \ref{sec:app:localization} presents the object localization problem and the proposed algorithm based upon optimization dynamics. This chapter also presents the test setup and test results from applying the algorithm. Finally chapter \ref{sec:conclusion} summarizes the work and provides future directions.

At the end there are also two appendices discussing in more detail some items from this work. Appendix \ref{app:inconsistencies} discusses the inconsistencies found in the default control loop of the Crazyflie and the control loop from \cite{Noronha2016}. Appendix \ref{app:localizationCode} presents the source code of the localization algorithm from chapter \ref{sec:app:localization}.