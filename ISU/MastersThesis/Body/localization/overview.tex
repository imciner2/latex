% !TeX root = ../../thesis.tex

\section{Problem Overview}

One possible use for a multi-agent system is to gather information on a target object and then estimate its location. There are two main ways this can be accomplished: Triangulation and Trilateration. Triangulation is where the agents sense the relative angle of a received signal, to create a bearing to the object. Then with multiple bearings the agents can determine where the bearings intersect and estimate the position of the target at the intersection point.

Trilateration uses only distance measurements from the agents to the target as its information. This creates a sphere around each agent where the target could be located. By then finding the intersection point of these spheres, the target can be located. As precise as sensors are, all distance measurements will contain noise and inaccuracies, so the intersection point may be either a region or the spheres may never intersect. To deal with this, the trilateration problem is formatted as an optimization problem where the target location is chosen that minimizes the error between the measured and estimated distances.

\subsection{Original Problem}

The method that comes most naturally from the optimization problem is called Range-Based Least Squares (R-LS) is used, which takes the form of the optimization problem in \eqref{eq:localization:R-LSproblem}.
\begin{equation}
\label{eq:localization:R-LSproblem}
\begin{aligned}
& \underset{\hat{p}}{\text{minimize}}
& & \sum_{i=1}^{N} \left(r_i {-} \norm{\hat{p} - p_i} \right)^2
\end{aligned}
\end{equation}
Where $\hat{p}$ is the estimated target location, and $p_i$, $r_i$ are the $i$th agent's location and distance the target respectively. This method seeks to find the estimated position that makes the measured distance as close to the actual distance as possible (in the 2-norm sense). This method is also the Maximum-likelihood estimator for the trilateration problem when the distance measurements have additive zero-mean guassian noise \cite{Cheung2004}.

Rewriting \eqref{eq:localization:R-LSproblem} as a constrained optimization problem produces \eqref{eq:localization:generalProblem}. Where $a_i$ has the physical meaning of being the distance between the estimated target location and the measured target location.
\begin{equation}
	\begin{aligned}
		& \underset{\hat{x_t}, \hat{y_t}, \hat{z_t}, a_i}{\text{minimize}}
		& & \sum_{i=1}^{N} a_i^2 \\
		& \text{subject to}
		& &
		\norm{\hat{p}_i - p_i}^2 = (a_i {-} r_i)^2 \qquad \forall i{=}1, 2, \dots, N	
	\end{aligned}
	\label{eq:localization:generalProblem}
\end{equation}
In this form it can be seen that the R-LS method produces a non-convex optimization problem, specifically an equality constrained Quadratically-Constrained Quadratic Program.

Since \eqref{eq:localization:generalProblem} takes the form of a QCQP, it can be rewritten as a semidefinite program with the following variable definitions:
\begin{equation*}
g_i = \norm{\hat{p} - p_i}
,\quad
X = \left[ \begin{matrix}
\hat{p}\\
1
\end{matrix} \right]
\left[ \begin{matrix}
\hat{p}^T & 1
\end{matrix} \right]
,\quad
G = \left[ \begin{matrix}
\hat{g}\\
1
\end{matrix} \right]
\left[ \begin{matrix}
\hat{g}^T & 1
\end{matrix} \right]
,\quad
C_i = \left[\begin{matrix}
I & -p_i\\
-p_i^T & \norm{p_i}^2
\end{matrix} \right]
\end{equation*}
The semidefinite program version of \eqref{eq:localization:generalProblem} is then given in \eqref{eq:localization:semidefiniteExact}.
\begin{equation}
\label{eq:localization:semidefiniteExact}
\begin{aligned}
& \underset{X, G}{\text{minimize}}
& & \sum_{i=1}^{N} G_{ii} - 2 r_i G_{m+1, i} + r_i^2 \\
& \text{subject to}
& &
G_{ii} = \textbf{Tr}(C_i X) \qquad \forall i{=}1, 2, \dots, N\\
& & & G \succeq 0, X \succeq 0\\
& & & G_{m+1,m+1} = X_{4,4} = 1\\
& & & \textbf{Rank}(X) = \textbf{Rank}(G) = 1
\end{aligned}
\end{equation}

Note that this problem has a rank 1 constraint on the matrices $X$ and $G$, which is a non-convex constraint. Many methods (eg. \cite{Cheung2004, Biswas2004, Biswas2006, Beck2008}) further relax the problem to create a convex problem by simply removing that constraint (which is referred to as a semidefinite relaxation), which produces the Semidefinite Relaxation (SDR) method.

Theoretical results developed in \cite{Beck2008} show that the rank of the matrix $G$ will always be 1 for the SDR method, but there exist cases where the rank of matrix $X$ will be greater than 1 (e.g. example 1 in \cite{Beck2008}). In the cases where $X$ has rank greater than 1, the solution is no longer exact and is instead an approximate solution, which can be found by doing a rank-one approximation of the matrix $X$.

\subsection{Squared Approximation}

An alternative method of solving \eqref{eq:localization:R-LSproblem} is to instead square the ranges, creating what is known as the Squared-Range Least Squares (SR-LS) method, shown in \eqref{eq:localization:SR-LSproblem}.
\begin{equation}
\label{eq:localization:SR-LSproblem}
\begin{aligned}
& \underset{\hat{p}}{\text{minimize}}
& & \sum_{i=1}^{N} \left(r_i^2 {-} \norm{\hat{p} - p_i}^2 \right)^2 \\
\end{aligned}
\end{equation}

This problem is also non-convex, but prior results have shown that the form of \eqref{eq:localization:SR-LSproblem} is the same as a generalized trust region subproblem (GTRS). The GTRS problems allow for conditions of optimality to be derived, which led to the creation of a numerical algorithm in \cite{Beck2008} that can find the global minimizer of the problem. Note that this problem formulation is no longer a Maximum-likelihood estimator for the target object's position, so with the ability to solve the problem comes a loss of estimation properties.

\section{Proposed Method}

The method we propose is based on the R-LS method in \eqref{eq:localization:R-LSproblem}, but done over a multi-agent networked system where each agent has an estimated position of the target. In order to use this method, the following three assumptions are made:
\begin{assumption}
The number of agents should be greater than the dimension of the space being searched.
\end{assumption}
\begin{assumption}
\label{assump:rangeSpace}
The agents are distributed in the search space such that they do not form a lower dimensional space (e.g. for a 3D search space the agents are not co-planar).
\end{assumption}
\begin{assumption}
\label{asump:network}
The communications network is a simple, connected graph.
\end{assumption}

Since each agent will have their own estimate of the target's position, three more constraints must be introduced to force the estimates to converge to a single value. These constraints are based on the network's Laplacian matrix $L$, and when added to \eqref{eq:localization:R-LSproblem} create \eqref{eq:localization:distributedProblem}.

\begin{equation}
	\begin{aligned}
	& \underset{\hat{x_t}, \hat{y_t}, \hat{z_t}, a_i}{\text{minimize}}
	& & \sum_{i=1}^{N} a_i^2 \\
	& \text{subject to}
	& &
	L\hat{x} = 0\\
	& & & 
	L\hat{y} = 0\\
	& & & 
	L\hat{z} = 0\\
	& & &
	\norm{\hat{p}_i - p_i}^2 = (a_i {-} r_i)^2 \qquad \forall i{=}1, 2, \dots, N
	\end{aligned}
	\label{eq:localization:distributedProblem}
\end{equation}