% !TeX root = ../../thesis.tex

\section{Experimental Results}

To assess the performance of the algorithm, two different tests were run: static tests and flight tests.

\subsection{Static Test}

In the static test, the agents were statically placed on chairs/objects around the target node. A sample setup can be seen in figure \ref{fig:localization:staticTestSetup}. In that setup, there are 4 agents placed around the target node in the center. The agents are placed such that they do not all lie in a single plane (so that assumption \ref{assump:rangeSpace} is satisfied).

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.1]{Computation_StaticTest_Setup}
	\caption{Setup used for a static test of the algorithm}
	\label{fig:localization:staticTestSetup}
\end{figure}

The network used in these tests was a ring configuration with the graph Laplacian given in \eqref{eq:localization:ringLaplacian}, and a step size for the Forward-Euler method of $h{=}0.01$.
\begin{equation}
	L = \begin{bmatrix}
		2 & -1 & 0 & -1 \\
		-1 & 2 & -1 & 0 \\
		0 & -1 & 2 & -1 \\
		-1 & 0 & -1 & 2
	\end{bmatrix}
	\label{eq:localization:ringLaplacian}
\end{equation}

The localization algorithm was then started and allowed to run for approximately 75 seconds. By that time the agents had converged to an estimated position for the target node. Figure \ref{fig:localization:staticDistances} shows the distances measured by the agents compared with the actual distance as computed from the camera system. Figure \ref{fig:localization:staticPositions} then shows the estimated positions and figure \ref{fig:localization:staticError} shows the error in the estimated positions.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Computation_StaticTest_1_AgentDistance}
	\caption{Measured versus actual distance for the static test}
	\label{fig:localization:staticDistances}
\end{figure}

From these figures, it can be seen that the agents were able to localize the node with an error on the order of 10cm, and were able to converge to a consensus about the estimated position in less than 10 seconds. Note however that the estimate in figure \ref{fig:localization:staticDistances} is offset from the true measurement. This is caused by the fact that the sensor in use is a biased sensor. When the algorithm is simulated using the actual data from the test but with the sensor bias removed, the resulting estimate is closer to the actual location. The results of this test can be seen in figure \ref{fig:localization:staticDistancesUnbiased}.

\begin{sidewaysfigure}[p!]
	\centering
	\includegraphics[width=\textwidth]{Computation_StaticTest_1_Positions}
	\caption{Experimental position estimate of the target node from a static test with 4 agents.}
	\label{fig:localization:staticPositions}
	\rotateFigurePageForLabel{fig:localization:staticPositions}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
	\centering
	\includegraphics[width=\textwidth]{Computation_StaticTest_1_ErrorPlots}
	\caption{Experimental error in position estimates from a static test with 4 agents.}
	\label{fig:localization:staticError}
	\rotateFigurePageForLabel{fig:localization:staticError}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
	\centering
	\includegraphics[width=\textwidth]{Computation_StaticTest_1_UnbiasedComparison}
	\caption{Results from a simulation showing estimated location if sensor bias were removed (blue line) for the static test}
	\label{fig:localization:staticDistancesUnbiased}
	\rotateFigurePageForLabel{fig:localization:staticDistancesUnbiased}
\end{sidewaysfigure}

\subsection{Flight Test}

After the static test was conducted, flight tests were conducted. These tests involved actually having the agents takeoff, and hover at a predetermined point. While hovering the agents are localizing the target node using the distance measurements. The results of one such experiment with 4 agents is reported here. As seen in figure \ref{fig:localization:flightDistances}, the errors in the distance measurements were slightly worse than the static test measurements. Additionally, during this test run, agent 0 was moved up on the $z$ axes partway through the computation (between 20 and 30 seconds in). Based upon figure \ref{fig:localization:flightDistances}, that change also affected agent 1's measured distance.

\begin{figure}[t!]
	\centering
	\includegraphics[width=\textwidth]{Computation_FlightTest_1_AgentDistance}
	\caption{Measured versus actual distance for the flight test}
	\label{fig:localization:flightDistances}
\end{figure}

The results of the localization can be seen in figures \ref{fig:localization:flightPositions} and \ref{fig:localization:flightError}. The errors in the computed position (shown in figure \ref{fig:localization:flightError}) are on the order of 10-12cm, and the step on the $z$ axes for agent 0 seems to have reduced the error by half (from a 25cm error to a 12cm error). The data collected in this test was then fed into the simulated algorithm with the sensor bias removed (similar to the static test). This resulted in the estimate of the location converging closer to the actual location, as seen in figure \ref{fig:localization:flightTestUnbiased}.

\begin{sidewaysfigure}[p!]
	\centering
	\includegraphics[width=\textwidth]{Computation_FlightTest_1_PositionPlots}
	\caption{Experimental position estimate of the target node for a flight test with 4 agents.}
	\label{fig:localization:flightPositions}
	\rotateFigurePageForLabel{fig:localization:flightPositions}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
	\centering
	\includegraphics[width=\textwidth]{Computation_FlightTest_1_ErrorPlots}
	\caption{Experimental error in position estimates for a flight test with 4 agents.}
	\label{fig:localization:flightError}
	\rotateFigurePageForLabel{fig:localization:flightError}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
	\centering
	\includegraphics[width=\textwidth]{Computation_FlightTest_1_UnbiasedComparison}
	\caption{Results from a simulation showing estimated location if sensor bias were removed (blue line) for the flight test}
	\label{fig:localization:flightTestUnbiased}
	\rotateFigurePageForLabel{fig:localization:flightTestUnbiased}
\end{sidewaysfigure}

\section{Observations}

Based upon all the experimental results presented in this section, it can be seen that the proposed algorithm can localize a target object under real-world conditions. When comparing the two different tests (static versus flight), the static test appears to have a smaller error on the three axes when using the biased sensors, and also has smoother state trajectories. This is caused by the fact that there is less noise in the distributed system, since the positions of the agents are fixed.

When running the flight test, the estimated position is a noisier trajectory, due to the increased noise on the position of the agent. The agent positions were noisier in the flight test because the positional control loop on the Crazyflie's had a degraded communications channel with the camera system. When all of the Crazyflies were passing data on the network, the ground station control loop was slowed down, so the position packets to the Crazyflies were slightly delayed, causing slight oscillations on the Crazyflie position.

As can be seen in the static test (figure \ref{fig:localization:staticDistancesUnbiased}), the majority of the bias on the estimated position in this test system is caused by the bias in the sensor measurements. This shows that this algorithm needs an unbiased sensor, or a sensor with a method for removing its bias. While the current experimental setup is sufficient for these tests, future work should be done to develop a distance measurement system with less measurement bias.

Another avenue to explore to remove the bias in the estimated positon is to examine how the bias changes when more agents are added to the computation. Adding more agents spread across the search space could theoretically reduce the effect of the measurement bias by moving the centroid of the error region closer to the actual location. This experimental system is currently limited by the amount of actual flight space available, so adding more agents will be difficult. Expanding the flight area, and developing better communications strategies (such as the Wifi network) will allow for this to be tested.

Overall, the algorithm presented is an effective way to localize a target object using a swarm of agents with distributed computational capability. As these experiments showed though, this algorithm is extremely sensitive to measurement bias with the low number of agents tested here.