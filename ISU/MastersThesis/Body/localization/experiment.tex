% !TeX root = ../../thesis.tex

\section{Experimental Setup}

An important step in developing an algorithm is experimental implementation and verification of its performance. For this algorithm, that means developing a method to measure the distance between the agent and a target node. There are two main methods for measuring the distance between objects: RF time of flight and acoustic time of flight. Both methods broadcast a signal, then use the propagation time of the signal in the medium (usually air) to determine the distance between the emitter and the sensor.

Bitcraze, the manufacturer of the Crazyflie, has developed a system that relies on RF time of flight for determining the distance between a Crazyflie and a target node. This system was developed as a localization system for the Crazyflie, where multiple nodes would be located throughout the flight area and then the Crazyflie would use the distance to each node to estimate its location in the flight area \cite{bitcrazeLocopos}. In this work, the system is used in reverse. The firmware of the Crazyflie was modified to provide the raw distance data in meters to the computational algorithm.

\subsection{Distance Sensor}

The Crazyflie's distance measurement system is based upon an Ultra-Wideband (UWB) RF transceiver called the Decawave DWM1000. There is one transceiver located on a Crazyflie deck, and another on a standalone board, called the targent node. The Crazyflie with the deck can be seen in figure \ref{fig:sys:crazyflieWithTrackables}, and the target node can be seen in figure \ref{fig:localization:targetNode}. Trackables were also attached to the target node to allow for the reading of its position using the camera system, so the algorithm result could be compared against the actual location.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.1]{Computation_TrilaterationTargetNode}
	\caption{Target node used for the trilateration experiments}
	\label{fig:localization:targetNode}
\end{figure}

Bitcraze has also released an experimental feature on the localization system allowing for the tags and anchors to operate in a Time Division Multiple Access (TDMA) manner. This allows for multiple Crazyflie decks to measure the distance to a single anchor, by dividing the transmit times into 4ms slots. Each deck is then assigned a specific slot for it to perform its ranging. This is the mode used in these experiments, so that the four Crazyflies can all measure the distance to the target node. Additionally, the firmware was modified to allow the TDMA slot to be selected by software instead of being hardcoded at build time. This allowed for every Crazyflie to receive the same firmware, but then assign a timeslot based upon the agent number it was assigned by the control software.

The advertised accuracy of the localization system using these distance measurements is on the order of 10cm \cite{bitcrazeLocopos}. Experimental data of just the distance measurement system was collected by moving the Crazyflie with a localization deck around the target node in the camera system volume. The actual location of both objects was recorded, so the actual distance could be calculated in the analysis phase. The results of this testing can be seen in figure \ref{fig:localization:distanceError}. These results show that the error being experienced by a node in this test setup is closer to 30cm, but that the distance measure still tracks the changes in distance. This implies that the sensor has non-zero mean noise (which is similar to the results reported in \cite{Jakob2016}).
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.3]{Computation_RangeTesting_NodeDistance}
	\caption{Experimental results showing ranging error of localization system}
	\label{fig:localization:distanceError}
\end{figure}

\subsection{Algorithm Implementation}

The system given in \eqref{eq:distributedSystem} is a continuous-time dynamical system, while the experimental setup constructed is a digital (sampled-data) system. This means that the system is not natively compatible, and instead must be sampled. In this work, the system was sampled and implemented using the Forward-Euler Method. This method of solving a continuous time ODE (given by $\dot{x} = f(x)$) takes the form
\begin{equation}
	x_{n+1} = x_n + hf( x_n )
\end{equation}
Where $x_{n+1}$ and $x_n$ are the state values at sample $n{+}1$ and $n$ respectively, and $h$ is the time interval between the two samples.

This discrete-time algorithm was then implemented in the computational framework developed for the Crazyflies in section \ref{sec:software:firmware:computation}. In this algorithm, only 6 variables need to be shared between a node and its neighbors: $\hat{x}_i$, $\hat{y}_i$, $\hat{z}_i$, $\alpha_i$, $\beta_i$, and $\gamma_i$. These six variables were placed into a CRTP packet (along with the measured distance, for logging purposes), which was then transmitted to the ground station after every computation. The groundstation was configured to act as a network forwarder, so it would forward the received CRTP packet to all the neighboring agents based upon a predetermined network structure.

Each agent would execute the above discrete-time state update at a rate of 100Hz. This would occur whether or not new data was received, so if no new data was received from a neighboring agent the previously received data would be used for the next update. There was a timeout feature implemented, so that if no new data is received from an agent in 1 second, that agent's data is removed from the computation.