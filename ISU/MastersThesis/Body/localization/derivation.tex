% !TeX root = ../../thesis.tex

\section{Algorithm Derivation}

To derive the algorithm, we start with the minimization problem given in \eqref{eq:localization:distributedProblem} and introduce three quadratic penalty terms. These terms will penalize differences in the estimated position of the target across the agents in the network, forcing faster convergence to one solution. The modified problem can be seen in \eqref{eq:complete}.
\begin{subequations}
\label{eq:complete}
	\begin{align}
	& \underset{\hat{x_t}, \hat{y_t}, \hat{z_t}, a_i}{\text{minimize}}
	& &
	\sum_{i=1}^{N} a_i^2 {+} \left(\norm{\hat{p}_i - p_i}^2 {-} (a_i {-} r_i)^2\right)^2 
	{+} k_1 \hat{x}^T L \hat{x} {+} k_2 \hat{y}^T L \hat{y} {+} k_3 \hat{z}^T L \hat{z} \label{eq:complete_cost}\\
	& \text{subject to}
	& &
	L\hat{x} = 0 \label{eq:complete_xConsensus}\\
	& & &
	L\hat{y} = 0 \label{eq:complete_yConsensus}\\
	& & & 
	L\hat{z} = 0 \label{eq:complete_zConsensus}\\
	& & &
	\norm{\hat{p}_i - p_i}^2 = (a_i {-} r_i)^2 \quad \forall i{=}1, 2, \dots, N \label{eq:complete_distanceConstraint}
	\end{align}
\end{subequations}

Then we define the Lagrangian function for the optimization problem given in \eqref{eq:complete}, letting the dual variables be represented by $\mu$, $\alpha$, $\beta$, and $\gamma$. By then also expanding the norm function, the Lagrangian in \eqref{eq:localization:lagrangian} is derived. Note that this function is actually the fully augmented Lagrangian function of \eqref{eq:localization:distributedProblem}.
\begin{equation}
	\label{eq:localization:lagrangian}
	\begin{split}
	L(\hat{x}, \hat{y}, \hat{z}, a, \mu, \alpha, \beta, \gamma) = &k_1 \hat{x}^T L \hat{x} {+} k_2 \hat{y}^T L \hat{y} {+} k_3 \hat{z}^T L \hat{z} {+} \alpha^T L \hat{x} + \beta^T L \hat{y} + \gamma^T L \hat{z}\\
	& {+} \sum_{i=1}^{N} \bigg( a_i^2 {+} \left[(\hat{x}_i {-} x_i)^2 {+} (\hat{y}_i {-} y_i)^2 {+} (\hat{z}_i {-} z_i)^2 {-} (a_i {-} r_i)^2 \right]^2 \\
	& {+} \mu_i \left[(\hat{x}_i {-} x_i)^2 {+} (\hat{y}_i {-} y_i)^2 {+} (\hat{z}_i {-} z_i)^2 {-} (a_i {-} r_i)^2 \right] \bigg)\\
	\end{split}
\end{equation}
The original optimization problem can then be solved by finding the solution to the min-max optimization problem in \eqref{eq:localization:lagrangeDistributedProblem}.
\begin{equation}
	\begin{aligned}
	\underset{\mu,\alpha,\beta,\gamma}{\text{max}} & \hspace{1em} \underset{\hat{x},\hat{y},\hat{z},a_i}{\text{min}} &
	L(\hat{x}, \hat{y}, \hat{z}, a, \mu, \alpha, \beta, \gamma)
	\end{aligned}
	\label{eq:localization:lagrangeDistributedProblem}
\end{equation}

To solve this problem, we use the optimization dynamics approach. This approach is based around finding a dynamical system whose equilibrium points (the points where the derivatives of the states are zero) are the solution of the opimization problem. This comes from the Karush-Kuhn-Tucker (KKT) conditions that an optimal point must satisfy. Specifically, The first KKT condition states that
$$
\nabla_x L(x, \mu) = 0
$$
Examining this, it can be seen that this condition can also be interpreted as an equilibrium condition for a dynamical system, where the state variables are the primal variables of the optimization problem. This condition also provides an obvious choice for the dynamical system, namely choose $\nabla L$ as the dynamical system.

In order to get convergence of the dynamical system, it is necessary to negate the differential equations for the primal variables, as discussed in \cite{Ma2015, Ma2016}. The final system of differential equations is given in \eqref{eq:distributedSystem}.

\begin{subequations}
\label{eq:distributedSystem}
\begin{align}
\dot{\mu}_i &= (\hat{x}_i {-} x_i)^2 {+} (\hat{y}_i {-} y_i)^2 {+} (\hat{z}_i {-} z_i)^2 {-} (a_i {-} r_i)^2 \label{eq:dist_mu}\\
\dot{a}_i &= 2 \left(2 \dot{\mu}_i + \mu_i \right) (a_i - r_i) - 2 a_i \label{eq:dist_a} \\
\dot{\hat{x}}_i &= - 2 \left( 2 \dot{\mu}_i + \mu_i \right) (\hat{x}_i - x_i) - e_i^T L \alpha - 2 k_1 e_i^T L \hat{x} \label{eq:dist_x} \\
\dot{\hat{y}}_i &= - 2 \left( 2 \dot{\mu}_i + \mu_i \right) (\hat{y}_i - y_i) - e_i^T L \beta - 2 k_2 e_i^T L \hat{y} \label{eq:dist_y} \\
\dot{\hat{z}}_i &= - 2 \left( 2 \dot{\mu}_i + \mu_i \right) (\hat{z}_i - z_i) - e_i^T L \gamma - 2 k_3 e_i^T L \hat{z} \label{eq:dist_z} \\
\dot{\alpha}_i &= e_i^T L \hat{x} \label{eq:dist_alpha}\\
\dot{\beta}_i &= e_i^T L \hat{y} \label{eq:dist_beta} \\
\dot{\gamma}_i &= e_i^T L \hat{z} \label{eq:dist_gamma}
\end{align}
\end{subequations}

Prior work by Xu Ma in \cite{Ma2015} and \cite{Ma2016} has shown that a dynamical system derived from a QCQP problem will converge to the optimal point if the Hessian of the Lagrangian is positive definite at the optimal point. This allows for a convergence theory to be developed.

\begin{theorem}
\label{thm:equilibrium}
The optimal solution to the problem given in \eqref{eq:localization:lagrangeDistributedProblem} occurs at an equilibrium point of the dynamical system in \eqref{eq:distributedSystem}.
\end{theorem}

\begin{proof}
Any optimal solution for the problem in \eqref{eq:localization:lagrangeDistributedProblem} must satisfy the KKT conditions, the first of which is that the derivative of the Lagrangian with respect to the primal variables must be zero at the optimal point:
\begin{equation*}
\nabla_{x} L(x^{*}, \lambda^{*} ) {=} 0\\
\end{equation*}
It can be seen by inspection that equations \eqref{eq:dist_a} through \eqref{eq:dist_z} are equivalent to the derivatives of \eqref{eq:localization:lagrangian} with respect to the primal variables.

The equilibrium points of \eqref{eq:distributedSystem} occur when the derivative terms equal zero, eg. $\dot{\hat{x}} {=} 0$, $\dot{\hat{y}} {=} 0$, $\dot{\hat{z}} {=} 0$, etc. Therefore when \eqref{eq:distributedSystem} reaches equilibrium, it is equivalent to the Lagrangian in \eqref{eq:localization:lagrangian} having its primal derivatives equal to zero.

This implies that the optimal solution to \eqref{eq:localization:lagrangeDistributedProblem} occurs at an equilibrium point of \eqref{eq:distributedSystem}.

\end{proof}

\newpage
Note: The following lemma was derived with the help of Xu Ma.

\begin{lemma}
\label{lem:posDefHess}
The Hessian of the augmented Lagrangian \eqref{eq:localization:lagrangian} with respect to the primal variables is positive definite at the equilibrium point.
\end{lemma}

\begin{proof}
%	We now present a proof to show that the Hessian of the augmented Lagrangian with respect to the primal variable is positive definite at the equilibrium point. Recall the augmented Lagrangian from \eqref{eq:lagrangian} 
%	\begin{align*}
%	& L = \textstyle \sum_i a_i^2 + k_1\hat{x}^TL\hat{x} + k_2\hat{y}^TL\hat{y} + k_3\hat{z}^TL\hat{z} \\
%	& + \alpha^TL\hat{x} + \beta^TL\hat{y} + \gamma^TL\hat{z} \\
%	& + \textstyle \sum_i [ (\hat{x}_i-x_i)^2 + (\hat{y}_i-y_i)^2 + (\hat{z}_i-z_i)^2 - (a_i-r_i)^2 ]^2 \\
%	& + \textstyle \sum_i \mu_i  [ (\hat{x}_i-x_i)^2 + (\hat{y}_i-y_i)^2 + (\hat{z}_i-z_i)^2 - (a_i-r_i)^2 ]
%	\end{align*} 

Define the primal variable as $[\hat{x} \ \hat{y} \ \hat{z} \ a]^T$ and let $U = \text{diag}(\mu_1, \mu_2, \dots, \mu_N)$.

Suppose that the primal equilibrium is $(x^\star, y^\star, z^\star, a^\star)$, and then by algebraic computation, the Hessian of \eqref{eq:localization:lagrangian} evaluated at this equilibrium is given by

\begin{align*}
H^\star & = 2B + 8D \\
&= 2\begin{bmatrix} B_1 & 0 & 0 & 0 \\ 0 & B_2 & 0 & 0 \\ 0 & 0 & B_3 & 0 \\ 0 & 0 & 0 & B_4 \end{bmatrix} + 8\begin{bmatrix} D_{xx} & D_{xy} & D_{xz} & D_{xa} \\ D_{xy} & D_{yy} & D_{yz} & D_{ya} \\ D_{zx} & D_{yz} & D_{zz} & D_{za} \\ D_{xa} & D_{ya} & D_{za} & D_{aa} \end{bmatrix}
\end{align*}
where $B_1 = U+k_1L$, $B_2 = U+k_2L$, $B_3 = U+k_3L$, $B_4 = I-U$. Moreover, we have
\begin{align*}
D_{xx} & = \textbf{diag}\{(\hat{x}_1^\star-x_1)^2, (\hat{x}_2^\star-x_2)^2, \cdots, (\hat{x}_N^\star-x_N)^2\} \\
D_{yy} & = \textbf{diag}\{(\hat{y}_1^\star-y_1)^2, (\hat{y}_2^\star-y_2)^2, \cdots, (\hat{y}_N^\star-y_N)^2\} \\
D_{zz} & = \textbf{diag}\{(\hat{z}_1^\star-z_1)^2, (\hat{z}_2^\star-z_2)^2, \cdots, (\hat{z}_N^\star-z_N)^2\} \\
D_{aa} & = \textbf{diag}\{(a_1^\star-r_1)^2, (a_2^\star-r_2)^2, \cdots, (a_N^\star-r_N)^2\} \\
D_{xy} & = \textbf{diag}\{(\hat{x}_1^\star-x_1)(\hat{y}_1^\star-y_1), \cdots, (\hat{x}_N^\star-x_N)(\hat{y}_N^\star-y_N)\} \\
D_{xz} & = \textbf{diag}\{(\hat{x}_1^\star-x_1)(\hat{z}_1^\star-z_1), \cdots, (\hat{x}_N^\star-x_N)(\hat{z}_N^\star-z_N)\} \\
D_{yz} & = \textbf{diag}\{(\hat{y}_1^\star-y_1)(\hat{z}_1^\star-z_1), \cdots, (\hat{y}_N^\star-y_N)(\hat{z}_N^\star-z_N)\} \\
D_{xa} & = \textbf{diag}\{(\hat{x}_1^\star-x_1)(a_1^\star-r_1), \cdots, (\hat{x}_N^\star-x_N)(a_N^\star-r_N)\} \\
D_{za} & = \textbf{diag}\{(\hat{z}_1^\star-z_1)(a_1^\star-r_1), \cdots, (\hat{z}_N^\star-z_N)(a_N^\star-r_N)\} \\
D_{ya} & = \textbf{diag}\{(\hat{y}_1^\star-y_1)(a_1^\star-r_1), \cdots, (\hat{y}_N^\star-y_N)(a_N^\star-r_N)\}.
\end{align*}

Now it is not difficult to verify two points: 1) For those $k_1, k_2, k_3$ large enough, the positive semidefinite Laplacian $L$ will make the $B$ matrix positive semidefinite. 2) The matrix $D$ is also positive semidefinite since all of its principal minors are positive semidefinite. Therefore, the Hessian $H^\star$ is at least positive semidefinite.

Next we show that $H^\star$ is positive definite. Because of assumption \ref{asump:network} the graph is fully connected, so we know that $L$ has a one-dimensional null space given by $[1 \ 1 \cdots 1]^T$. As a result, the $B$ matrix has a three-dimensional null space given by
\begin{align*}
\textbf{span}\Big\{ & [1 \ 1 \cdots 1, 0 \ 0 \cdots 0, 0 \ 0 \cdots 0, 0 \ 0 \cdots 0]^T, \\
& [0 \ 0 \cdots 0, 1 \ 1 \cdots 1, 0 \ 0 \cdots 0, 0 \ 0 \cdots 0]^T, \\
& [0 \ 0 \cdots 0, 0 \ 0 \cdots 0, 1 \ 1 \cdots 1, 0 \ 0 \cdots 0]^T \Big\}.
\end{align*}

Checking these vectors one by one, we find that none of them is in the null space of $D$, which means that those two matrices $B$ and $D$ can compensate the null space of each other. Therefore, we can finally conclude that $H^\star$ is positive definite.
\end{proof}

\begin{theorem}
\label{thm:convergence}
There exist values for $k_1 {>} 0$, $k_2 {>} 0$, and $k_3 {>} 0$ such that if the system in \eqref{eq:distributedSystem} starts in a neighborhood around the optimal point $(x^{*}, \lambda^{*})$, it will converge to $(x^{*}, \lambda^{*})$.
\end{theorem}
\begin{proof}
By theorem 7 in \cite{Ma2016}, a general QCQP problem has convergent optimization dynamics if the Hessian of the associated Lagrangian is positive definite when evaluated at the equilibrium point.

From lemma \ref{lem:posDefHess}, the Hessian of the augmented Lagrangian in \eqref{eq:localization:lagrangian} at the equilibrium point is positive definite. Therefore by \cite{Ma2016}, the optimization dynamics in \eqref{eq:distributedSystem} will converge to a unique solution when started in a neighborhood around $(x^{*}, \lambda^{*})$.
\end{proof}

So, using theorem \ref{thm:convergence}, we know that the dynamical system in \eqref{eq:distributedSystem} will converge under mild conditions to a point, which according to theorem \ref{thm:equilibrium} is the optimal point for \eqref{eq:complete}.