% !TeX root = ../ISU_Defense.tex

\section{Distributed Computation}

\subsection{Overview}

\begin{frame}
	\frametitle{Goals}

	\begin{minipage}{0.5\textwidth}
		\begin{center}
			\includegraphics[trim={0 0 350 0},clip,scale=0.3]{Computation_IdealNetwork}
		\end{center}
	\end{minipage}
	\begin{minipage}{0.45\textwidth}
		\begin{itemize}
			\item Each agent contains an algorithm\newline (either identical or different)
			\item Algorithm can interface with the local control loop to get data/modify controller parameters
			\item Data is exchanged directly between agents over a network
		\end{itemize}
	\end{minipage}	
	
\end{frame}

\begin{frame}
	\frametitle{Crazyflie Limitations}
	
	\begin{minipage}{0.5\textwidth}
		\begin{center}
			\includegraphics[trim={0 0 350 0},clip,scale=0.3]{Computation_IdealNetwork}
		\end{center}
	\end{minipage}
	\begin{minipage}{0.45\textwidth}
		\begin{itemize}
			\item Existing radio link does not support point-to-point links
			\item Crazyflie must talk with a CrazyRadio on a computer
		\end{itemize}
	\end{minipage}	
	
\end{frame}

\begin{frame}
	\frametitle{Realized System}
	
	\begin{minipage}{\textwidth}
		\begin{itemize}
			\item Network replaced by a routing system on groundstation computer
		\end{itemize}
	\end{minipage}
		
	\begin{minipage}{\textwidth}
		\begin{center}
			\includegraphics[trim={0 0 0 0},clip,scale=0.3]{Computation_RealizedNetwork}
		\end{center}
	\end{minipage}
\end{frame}

\subsection{Technical Description}

\begin{frame}
	\frametitle{Firmware Modifications}
	\framesubtitle{Computation Task}
	
	\begin{itemize}
		\item Separate task/thread created in firmware to handle computation algorithm execution
		\begin{itemize}
			\item Ability to start/stop computation
			\item Variable computation rate
		\end{itemize}
		\item Hooks into the local control loop
		\begin{itemize}
			\item Algorithm has access to the agent's state estimates
			\item Algorithm can modify controller setpoints
		\end{itemize}
	\end{itemize}
	
\end{frame}


\begin{frame}
	\frametitle{Firmware Modifications}
	\framesubtitle{Communications System}
	\begin{itemize}
		\item No pre-known network configuration on Crazyflie
		\begin{itemize}
			\item As packets received, neighbor list is updated
			\item If no packets received from a neighbor in specified period, neighbor relation is removed
		\end{itemize}
		\item Data reception independent of computation
		\begin{itemize}
			\item Data will be received and saved at all times
		\end{itemize}
		\item Data transmitted at end of computation iteration
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Groundstation Network Emulation}
	\framesubtitle{Features}
	
	\begin{minipage}{0.55\textwidth}
		\begin{itemize}
			\item Arbitrary network graph support
			\begin{itemize}
				\item Supports directed and undirected graphs
				\item Edges can be modified on-the-fly
			\end{itemize}
			
			\item Data-agnostic routing
			\begin{itemize}
				\item Only knowledge of packet structure is in the logger system
			\end{itemize}
			
			\item FIFO packet storage for tranmission
		\end{itemize}
	\end{minipage}
	\begin{minipage}{0.4\textwidth}
		\includegraphics[trim={450 0 0 0},clip,width=\textwidth]{Computation_RealizedNetwork}
	\end{minipage}

\end{frame}

%\begin{frame}
%	\frametitle{Groundstation Network Emulation}
%	\framesubtitle{Sequence of Events}
%	
%	When a packet is received:
%	\begin{enumerate}
%		\item Radio directs packet to the router module
%		\item Router finds all neighbors of agent that sent packet
%		\item Packet is added to the Transmit FIFO for all neighbors
%		\item Packet is removed from FIFO for transmission during next radio loop
%	\end{enumerate}
%	
%\end{frame}

\subsection{Application: Trilateration}
\begin{frame}
	\frametitle{Problem Overview}

	\begin{minipage}{0.49\textwidth}
		\textbf{Problem:}\newline
		Estimate the position of a target node
		\vspace{1em}
		
		\textbf{Constraints:}\newline
		Distributed computation
		
		Only noisy distance measurements available

	\end{minipage}	
	\begin{minipage}{0.5\textwidth}
		\begin{center}
			\includegraphics[width=\textwidth]{OverlappingCircles}
		\end{center}
	\end{minipage}

\end{frame}

\begin{frame}
	\frametitle{Algorithm}
	
	Form an optimization problem:
	{\footnotesize
	\begin{align*}
	& \underset{\hat{x_t}, \hat{y_t}, \hat{z_t}, a_i}{\text{minimize}}
	& &
	\sum_{i=1}^{N} a_i^2 {+} \left(\norm{\hat{p}_i - p_i}^2 {-} (a_i {-} r_i)^2\right)^2 
	{+} k_1 \hat{x}^T L \hat{x} {+} k_2 \hat{y}^T L \hat{y} {+} k_3 \hat{z}^T L \hat{z}\\
	& \text{subject to}
	& &
	L\hat{x} = 0 \\
	& & &
	L\hat{y} = 0 \\
	& & & 
	L\hat{z} = 0 \\
	& & &
	\norm{\hat{p}_i - p_i}^2 = (a_i {-} r_i)^2 \quad \forall i{=}1, 2, \dots, N
	\end{align*}}

\end{frame}

\begin{frame}
	\frametitle{Algorithm}
	
	Solve using Optimization Dynamics\footnote{Derived in collaboration with Xu Ma}:
	{\footnotesize
	\begin{align*}
	\dot{\mu}_i &= (\hat{x}_i {-} x_i)^2 {+} (\hat{y}_i {-} y_i)^2 {+} (\hat{z}_i {-} z_i)^2 {-} (a_i {-} r_i)^2 \\
	\dot{a}_i &= 2 \left(2 \dot{\mu}_i + \mu_i \right) (a_i - r_i) - 2 a_i \\
	\dot{\hat{x}}_i &= - 2 \left( 2 \dot{\mu}_i + \mu_i \right) (\hat{x}_i - x_i) - e_i^T L \alpha - 2 k_1 e_i^T L \hat{x} \\
	\dot{\hat{y}}_i &= - 2 \left( 2 \dot{\mu}_i + \mu_i \right) (\hat{y}_i - y_i) - e_i^T L \beta - 2 k_2 e_i^T L \hat{y} \\
	\dot{\hat{z}}_i &= - 2 \left( 2 \dot{\mu}_i + \mu_i \right) (\hat{z}_i - z_i) - e_i^T L \gamma - 2 k_3 e_i^T L \hat{z} \\
	\dot{\alpha}_i &= e_i^T L \hat{x} \\
	\dot{\beta}_i &= e_i^T L \hat{y} \\
	\dot{\gamma}_i &= e_i^T L \hat{z} 
	\end{align*}}

	\begin{itemize}
		\item Derived dynamics are continuous, implementation is discrete
		\begin{itemize}
			\item Discretize using Forward Euler method
		\end{itemize}
		\item Node operation count increases linearly with number of neighbors
		\item Only $\hat{x}_i$, $\hat{y}_i$, $\hat{z}_i$, $\alpha_i$, $\beta_i$ and $\gamma_i$ get shared with neighbors
	\end{itemize}
	
\end{frame}

\begin{frame}
	\frametitle{Experimental Equipment}
	\begin{minipage}{0.5\textwidth}
		\begin{center}
			\includegraphics[scale=0.06]{Computation_TrilaterationTargetNode}
		
			Target Node
		\end{center}
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\begin{itemize}
			\item Distances from Bitcraze Loco Positioning System
			\begin{itemize}
				\item RF time-of-flight system
			\end{itemize}
			\item IR constellation on target node for truth measurement
		\end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Experimental Setup}
	\begin{center}
		\includegraphics[width=0.9\textwidth]{Computation_StaticTest_Setup}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Experimental Results}
	\framesubtitle{Position Estimates}
	\begin{center}
		\includegraphics[width=\textwidth]{Computation_FlightTest_1_PositionPlots}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Experimental Results}
	\framesubtitle{Position Estimate Error}
	\begin{center}
		\includegraphics[width=\textwidth]{Computation_FlightTest_1_ErrorPlots}
	\end{center}
\end{frame}

\subsection{Future Extensions}

\begin{frame}
	\frametitle{Summary}
	
	This section presented a distributed computational framework with:
	\begin{itemize}
		\item Arbitrary network support
		\item Hooks into local agent control loop
		\item The network routing handled by ground station software
	\end{itemize}
	\vspace{1em}
	
	This system was then applied to the problem of distributed object localization using a novel algorithm developed jointly with Xu Ma.
\end{frame}

\begin{frame}
	\frametitle{Distributed Computation Limitations \& Future Extensions}
	
	\textbf{Limitations:}
	\begin{itemize}
		\item Lack of point-to-point communication
		\item Scalability dependent on number of CrazyRadios
		\begin{itemize}
			\item Too many Crazyflies on one radio slows down datarate
		\end{itemize}
	\end{itemize}
	
	\pause
	\vspace{1em}
	\textbf{Future Extension:}
	\begin{itemize}
		\item Implementation of Wifi on Crazyflie using ESP8266 chipset
		\begin{itemize}
			\item Allows for faster communication
			\item Mesh capability to support point-to-point communication
		\end{itemize}
	\end{itemize}
\end{frame}