\documentclass{beamer}
\include{Beamer-Includes} % Include the standard packages used in every file I create

% Include custom commands
\include{Unit-Declaration}
\include{Sci-Formatting}

\usetheme{CambridgeUS}
\usecolortheme{dolphin}

%\usepackage{footbib}
\usepackage[backend=bibtex,style=authortitle]{biblatex}
\addbibresource{Presentation-Ref.bib}
%\footbibliography{Presentation-Ref.bib}

\usepackage{threeparttable}
\usepackage{tabularx}
\usepackage{ragged2e}  % for '\RaggedRight' macro (allows hyphenation)
\newcolumntype{Y}{>{\RaggedRight\arraybackslash}X}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{colortbl}

%\usepackage[pdftex]{graphicx}
%\usepackage{epstopdf}

%\epstopdfsetup{update}
\usepackage{graphicx}
\graphicspath{ {./Diagrams/}, {./Images/} }


\usetikzlibrary{dsp,chains,automata,positioning}

\begin{document}

\title{CprE 583 Final Project}
\subtitle{An FPGA-Based Implementation of the Dynamical Linear System Method for Solving Systems of Linear Equations}
\author{Ian McInerney, Paul Uhing}
\date{December 18, 2015}

%\bibliographystyle{ieeetr}
%\setbeamertemplate{bibliography item}{\insertbiblabel}

% The title slide
\begin{frame}
	\maketitle
\end{frame}

\begin{frame}
	\frametitle{Outline}
	\tableofcontents
\end{frame}

\section{Project Concept}

\begin{frame}
	\frametitle{Project Concept}
	\begin{itemize}
		\item[$\bullet$] Many engineering fields require the solution of linear systems of equations
		\item[$\bullet$] Many different linear system solvers exist:
\begin{table}
\centering
\begin{threeparttable}
\begin{tabular}{|c|c|c|c|c|}
\hline
Solver & Type & Requirements on A \tnote{1} \tnote{2}\\
\hline
LU Decomposition & Direct & Non-Singular\\
Cholesky Decomposition & Direct & Symmetric Positive-Definite\\
\hline
Simple Iteration & Iterative & $\rho (I-M^{-1}A) < 1$\\
Conjugate Gradient & Iterative & Symmetric Positive-Definite\\
Biconjugate Gradient & Iterative & Non-zero determinant\\
GMRES & Iterative & Non-Singular\\
MINRES-QLP & Iterative & Symmetric\\
\hline
\end{tabular}
\begin{tablenotes}
\item [1] $\rho(A)$ computes the spectral radius of matrix $A$
\item [2] $M$ is an invertible preconditioner matrix
\end{tablenotes}
\end{threeparttable}
\end{table}
	\end{itemize}
\end{frame}

\section{Algorithm}

\begin{frame}
	\frametitle{General Algorithm}
	\begin{itemize}
		\item[$\bullet$] This project will implement a new algorithm, the Dynamical Linear System (DLS) method\footcite{Jing2014}, on an FPGA
		\item[$\bullet$] This method solves the optimization problem:\\
	\begin{equation*}
		\begin{aligned}
		& \underset{x}{\text{minimize}} & \norm{x}^2_2 \\
		& \text{subject to} &Ax = b
		\end{aligned}
	\end{equation*}
		\item[$\bullet$] Solution is the state trajectory of:\\
	\begin{equation*}
		\begin{aligned}
		x[k+1] &= (1-2\gamma)x[k] - \gamma A'v[k]\\
		v[k+1] &= v[k] + \gamma (Ax[k] - b)
		\end{aligned}
	\end{equation*}
		\item[$\bullet$] The DLS method is an iterative solution usable on general A matrices
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Computational Algorithm}
	\begin{algorithm}[H]
		{\tiny
		\caption{DLS Algorithm for a $n$-variable system.}
		\input{DLS_Algorithm}}
	\end{algorithm}
\end{frame}

\section{Hardware Design}
\begin{frame}
	\frametitle{Overall Architecture}
	\begin{minipage}{0.45\textwidth}
		\begin{center}
			\begin{figure}[h!]
				\includegraphics[scale=0.4]{Architecture.png}
				\caption{Solver Architecture}
			\end{figure}
		\end{center}	
	\end{minipage}
	\begin{minipage}{0.45\textwidth}
		Three main components in the design
		\begin{itemize}[noitemsep]
			\item[1.] Dual-node Accumulators
			\item[2.] Primal-node Accumulators
			\item[3.] Vector-Vector multiplier (Fan-in module)
		\end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Dual-node Accumulators}
	\begin{center}
		\begin{figure}[h!]
			\includegraphics[scale=0.4]{DualNode.png}
			\caption{Node Block Diagram}
		\end{figure}
	\end{center}	
%	\setbeamertemplate{itemize items}[ball]
	\begin{itemize}
		\item[$\bullet$] Implement the computations:
			$$ v_i[k+1] = v_i[k] + d_i[k] - b_i $$
		\item[$\bullet$] $b_i$ value stored inside node
		\item[$\bullet$] Registers added along computation path to breakup critical path
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Primal-node Accumulators}
	\begin{center}
		\begin{figure}[h!]
			\includegraphics[scale=0.4]{PrimalNode.png}
			\caption{Node Block Diagram}
		\end{figure}
	\end{center}	
%	\setbeamertemplate{itemize items}[ball]
	\begin{itemize}
		\item[$\bullet$] Implement the computations:
			$$ x_i[k+1] = x_i[k] - (2\gamma*x_i[k] + u_i[k]) $$
		\item[$\bullet$] Registers added along computation path to breakup critical path
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Vector-Vector Multiplier (Fan-in Module)}
	\begin{center}
		\begin{figure}[h!]
			\includegraphics[scale=0.4]{Fanin.png}
			\caption{Fan-in Block Diagram}
		\end{figure}
	\end{center}	
%	\setbeamertemplate{itemize items}[ball]
	\begin{itemize}
		\item[$\bullet$] Implement the computations:
			\begin{align*}
			 d_i[k] &= a_{i,:} * x[k-1]\\
			 u_i[k] &= a_{:,i} * v[k-1]
			\end{align*}
		\item[$\bullet$] BlockRAM stores row/column of $A$ inside fan-in
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Fixed-Point Justification}
	\begin{itemize}[noitemsep]
		\item[$\bullet$] 32-bit word, 24-bit fractional fixed-point representation used
	\end{itemize}
	\begin{minipage}{0.45\textwidth}
		\begin{figure}
			\includegraphics[scale=0.35]{Analysis_Vfanin.png}
			\caption{Bit usage for dual fan-in}
		\end{figure}
	\end{minipage}
	\begin{minipage}{0.5\textwidth}
		\begin{figure}
			\includegraphics[scale=0.35]{Analysis_Vnode.png}
			\caption{Bit usage for dual accumulator}
		\end{figure}
	\end{minipage}
\end{frame}

\section{Analysis}

\begin{frame}
	\frametitle{Implementation Details}
	\begin{itemize}
		\item[$\bullet$] Implemented on a Zedboard (Xilinx XC7020-1CLG484)
		\item[$\bullet$] AXI wrapper used to communicate with ARM processor
		\begin{itemize}
			\item[$\bullet$] Attempted to make core AXI master, but was unsuccessful
			\item[$\bullet$] Slave registers used for data load/store instead\
		\end{itemize}
		\item[$\bullet$] Final core clock speed was 83.333336MHz
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Runtime Analysis}
	Three test cases (4000 iterations per case):\\
	\begin{table}
	\begin{tabular}{lll}
		\tiny
		$A = \begin{bmatrix}
		1 & 2 & 0 & 0\\
		0 & 2 & 0 & 1\\
		-1 & 0 & 0 & 1\\
		0 & 0 & -1 & -1
		\end{bmatrix}$\\
		\tiny
		$b = \begin{bmatrix}
		1 & 1 & 1 & 1\\
		\end{bmatrix}'$
	&
		\tiny
		\begin{tabular}{l|l}
		Procedure & Time (ms)\\
		\hline
		MATLAB & 31\\
		Hardware Load/Store & 0.03\\
		Hardware Solver & 0.624
		\end{tabular}
	&
		Speed up: 48x\\
	\hline
		\tiny
		$A = \begin{bmatrix}
		1 & 2 & 0 & 0\\
		0 & 2 & 0 & 1\\
		-1 & 0 & 0 & 1\\
		0 & 0 & -1 & -1
		\end{bmatrix}$\\
		\tiny
		$b = \begin{bmatrix}
		5 & 2 & -3 & -1\\
		\end{bmatrix}'$
	&
		\tiny
		\begin{tabular}{l|l}
		Procedure & Time (ms)\\
		\hline
		MATLAB & 30\\
		Hardware Load/Store & 0.03\\
		Hardware Solver & 0.624
		\end{tabular}
	&
		Speed up: 46x\\
	\hline
		\tiny
		$A = \begin{bmatrix}
		-0.831 & -2.002 & -0.034 & -0.714\\
		-0.979 & 0.964 & -0.798 & 1.351\\
		-1.156 & 0.520 & 1.018 & -0.224\\
		-0.533 & -0.020 & -0.133 & -0.598
		\end{bmatrix}$\\
		\tiny
		$b = \begin{bmatrix}
		-0.293 & -0.847 & -1.120 & 2.525\\
		\end{bmatrix}'$
	&
		\tiny
		\begin{tabular}{l|l}
		Procedure & Time (ms)\\
		\hline
		MATLAB & 30\\
		Hardware Load/Store & 0.03\\
		Hardware Solver & 0.641
		\end{tabular}
	&
		Speed up: 46x
	\end{tabular}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Area Analysis}
	\begin{itemize}
		\item[$\bullet$] Resource usage grows $O(n)$ until $n=16$
		\item[$\bullet$] Design exceeds LUTs available at $n=24$
	\end{itemize}
	\begin{figure}
		\includegraphics[scale=0.5]{AreaResults.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Project Team Contributions}
	Ian McInerney - 
		\begin{itemize}
			\item[$\bullet$] Perform fixed-point analysis
			\item[$\bullet$] Create co-processor
			\item[$\bullet$] Conduct co-processor area/resource utilization study
			\item[$\bullet$] Draft report
		\end{itemize}
	
	Paul Uhing - 
		\begin{itemize}
			\item[$\bullet$] Attempted to integrate with Axi Bus as a bus Master
			\item[$\bullet$] Developed AXI slave core for co-processor interface
			\item[$\bullet$] Wrote C code to test platform
			\item[$\bullet$] Draft report
		\end{itemize}
\end{frame}

% The project timeline
\begin{frame}
	\frametitle{Project Hurdles}
	\begin{itemize}
		\item[$\bullet$] Misprinted sign in original Elia paper
		\item[$\bullet$] MATLAB Fixed-Point analysis toolbox is terrible... It can't save data nicely
		\item[$\bullet$] Auto generated Xilinx AXI Master core not usable
		\item[$\bullet$] Paul's inability to count (0, 1, 2, ... 4)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Conclusions}
	The good...
	\begin{itemize}
		\item[$\bullet$] Algorithm is implementable in hardware
		\item[$\bullet$] Increased solution performance by 46 times compared to MATLAB execution time
	\end{itemize}
	The bad...
	\begin{itemize}
		\item[$\bullet$] Algorithm exceeds LUTs available with $n=24$ matrix size
		\item[$\bullet$] No magic number of iterations for convergence guarantee
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Future Work}
	\begin{itemize}
		\item[$\bullet$] Examine effect of matrix scaling on execution time
		\item[$\bullet$] Examine effect of vector-vector multiplication unit design on algorithm performance
		\item[$\bullet$] Compare against a C code implementation on embedded hardware
		\item[$\bullet$] Compare execution time against analog (continuous-time) implementation
	\end{itemize}
\end{frame}

% The rubric for grading
\begin{frame}
	\frametitle{Self Assessment}
	\footnotesize
	\begin{table}
		\begin{tabularx}{\textwidth}{|Y|Y|Y|Y|}
			\hline			
			& Unsatisfactory & Satisfactory & Exceptional\\
			\hline
			Co-processor design & Not working in hardware, works in sim\newline [15 points] & \cellcolor{blue!25}Works correctly in hardware\newline [25 points] & Multiple fan-in styles working\newline [45 points]\\
			\hline
			Axi Bus Integration & \cellcolor{blue!25}Co-processor integrated with simple Axi lite slave interface wrapper\newline [10 points] & \cellcolor{blue!25} Co-processor integrated with Axi lite master functionality\newline [25 points] &\\
			\hline
			Analysis of Design & Only fixed-point analyzed\newline [10 points] & \cellcolor{blue!25}Fixed-point and resource scaling analyzed\newline [25 points] & Fixed-point, resource scaling, and fan-in comparison done [40 points]\\
			\hline
			Presentation and Report & Incomplete report\newline [10] points] & \cellcolor{blue!25}Complete report\newline [25 points] &\\
			\hline
		\end{tabularx}
	\end{table}
\end{frame}

\end{document}