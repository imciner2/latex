% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: October 28, 2014
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[10pt,titlepage]{IEEEtran} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PACKAGES
\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{cite}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{graphicx}
\graphicspath{ {./Diagrams/}, {./Images/} }

\title{An FPGA-Based Implementation of the Dynamical Linear System Method for Solving Systems of Linear Equations}
%\date{December 12, 2014}
\author{Ian McInerney, Paul Uhing}


% Include custom commands
\include{Unit-Declaration}
\include{Sci-Formatting}

\usetikzlibrary{dsp,chains,automata,positioning}


\begin{document}

\maketitle


\section{Introduction}

Various techniques in the fields of control systems, optimization, and signal processing rely on linear systems of equations for their ultimate implementation. There are many different methods of solving these linear systems of equations ranging from direct methods such as LU Decomposition that solve for the exact solution in one step, to iterative methods that arrive at the solution after multiple steps. Each of these methods has their advantages and disadvantages for implementation and usage. This project explored the FPGA implementation of a recently developed algorithm for the iterative solution of linear systems of equations, called the Dynamical Linear System (DLS) method.

\section{Linear System Solvers}

There are two main types of solvers for linear systems that have been developed: direct and iterative.

\subsection{Direct Methods}

Direct solution methods are able to obtain a solution to the system of equations in one numerical time step. The most common solution style is the LU Decomposition method.

The LU decomposition method for solving systems of linear equations splits the given $A$ matrix into two matrices such that $A=LU$ with $L$ a lower triangular matrix and $U$ an upper triangular matrix. Once those two matrices are determined, the system $Ly=b$ and $Ux=y$ is solved. These two systems are generally easier to solve for because of the triangular nature of the $L$ and $U$ matrices \cite{Greenbaum1997}.

There are several downsides to this method of solution, namely that $A$ must be non-singular and the $L$ and $U$ matrices will not be sparse if $A$ was sparse. Additionally, the computational complexity of these solutions is large, requiring approximately $O(n^3)$ for a $n \times n$ $A$ matrix \cite{Greisen2013}.


\subsection{Iterative Methods}

At their core, the iterative methods for the solution of a system of linear equations are methods for gradually refining an initial solution guess into the final solution over a specified number of iterations. The simplest iteration method starts with an approximation $x_o$, and then gradually refine that approximation using the equation $$M(x_{i+1} - x_{i}) = b-Ax_{i}$$ This method requires another matrix, $M$, to be defined. That matrix is known as a pre-conditioner matrix and can change the rate at which the solution is found.

This iteration method has an approximate computational complexity of $O(n)$, but has very strict conditions that must be observed for a solution to be found \cite{Greisen2013}. These strict convergence requirements make the simple iterative solver impractical to use on its own in most systems \cite{Morris2005}.


More advanced iterative methods are generally founded upon the optimization problem in \ref{eq:generalOpt} which seeks to minimize the error of the solution vector.
\begin{equation}
\label{eq:generalOpt}
\begin{aligned}
& \text{minimize} & \norm{x}^2_2 \\
& \text{subject to} & x \in\underset{x}{\text{argmin}} \norm{Ax-b}_2
\end{aligned}
\end{equation}
There are many different advanced iterative methods such as the Conjugate Gradient method, Biconjugate Gradient method, Generalized Minimal Residual (GMRES) method and the Minimum Residual - QLP Decomposition (MINRES-QLP). Each of these methods has distinct requirements that the $A$ matrix must satisfy to guarantee convergence to an approximate solution. An overview of these solver types, and references to more detailed solver descriptions can be seen in Table \ref{tab:solverComparison}.
\begin{table*}[t]
\centering
\begin{threeparttable}
\caption{Comparison of linear system solvers.}
\label{tab:solverComparison}
\begin{tabular}{|c|c|c|c|c|}
\hline
\multicolumn{2}{|c|}{Solver} & Type & Requirements on A \tnote{1} \tnote{2}\\
\hline
LU Decomposition & \cite{Greenbaum1997} & Direct & Non-Singular\\
\hline
Simple Iteration & \cite{Greenbaum1997} & Iterative & $\rho (I-M^{-1}A) < 1$\\
Conjugate Gradient & \cite{Greenbaum1997} & Iterative & Symmetric Positive-Definite\\
Biconjugate Gradient & \cite{Olshanskii2014} & Iterative & Non-zero determinant\\
GMRES & \cite{Olshanskii2014} & Iterative & Non-Singular\\
MINRES-QLP & \cite{Choi2014} & Iterative & Symmetric\\
DLS\tnote{3} & \cite{Wang2014} & Iterative & General\\
\hline
\end{tabular}
\begin{tablenotes}
\item [1] $\rho(A)$ computes the spectral radius of matrix $A$
\item [2] $M$ is an invertible preconditioner matrix
\item [3] This solver is the focus of this project
\end{tablenotes}
\end{threeparttable}
\end{table*}

\subsection{Implementation of Solvers}

The implementation of the linear system solvers onto an FPGA is an important field of ongoing research, and is usually examined along with an application domain. For example, \cite{Greisen2013} examines the implementation of two distinct solvers in the field of real-time image processing:
\begin{enumerate}
\item A Direct-method Cholesky decomposition (which is a specific LU-decomposition with $U=L^{\text{T}}$) 
\item An iterative Biconjugate Gradient Stabilized solver (extension of the Biconjugate Gradient)
\end{enumerate}

Other researchers have examined the application of linear system solvers to the Model Predictive Control (MPC) problem. In \cite{Hartley2013}, the problem of aircraft control was examined using a MPC approach. This approach utilized a real-time optimization algorithm that had a linear system solver at its core, specifically the MINRES solver. This work examined the implementation details of a MINRES solver utilizing two forms of preconditioning: online (where the FPGA logic applies the preconditioner) and offline (where the preconditioner is applied before the matrices are given to the solver). Similar research was also conducted by \cite{Hartley2013}, with the application of real-time MPC to the control of spacecraft orbits.

Pure implementation work for the MINRES solver was first conducted in \cite{Boland2008}. This work examined the parallelisation of the algorithm, and the growth of the resource usage as the matrix size increases.

\section{Proposed Algorithm (DLS Method)}
\label{sec:alg}

\subsection{General Algorithm}

The algorithm we propose to use to solve linear systems of equations was developed by Elia and Wang in \cite{Wang2014} as a method of solving the optimization problem shown in \ref{eq:convexProblem} (which is simply a special case of the optimization problem in \ref{eq:generalOpt}).

\begin{equation}
\label{eq:convexProblem}
\begin{aligned}
& \underset{x}{\text{minimize}} & \norm{x}^2_2 \\
& \text{subject to} &Ax = b
\end{aligned}
\end{equation}

Their algorithm presents the solution to the optimization problem in \ref{eq:convexProblem} as the state trajectory of the dynamical linear system shown in \ref{eq:dls_cont}. Where $x$ are the primal problem solutions, and $v$ are the Lagrangian-dual problem solutions.

\begin{equation}
\label{eq:dls_cont}
\begin{aligned}
\dot{x} &= -2x - A'v\\
\dot{v} &= Ax - b
\end{aligned}
\end{equation}

Over time, the state trajectory of the system given in \ref{eq:dls_cont} will converge to either:
\begin{enumerate}
\item The unique solution vector for the initial equation $Ax=b$ (when A is non-singular)
\item The solution vector that minimizes its Euclidean norm (when A is singular)
\end{enumerate}
Note that only option 1 is unique, the other option will give a solution dependent upon the initial conditions of the system.

The advantage of this algorithm is that it is able to solve for a solution vector with general A matrices. This means there are no singularity, definiteness, or symmetric conditions required of the A matrix.

\subsection{Discrete-Time Algorithm}

The system presented in \ref{eq:dls_cont} is designed for continuous-time implementation (such as analog circuits). In order for this algorithm to converge on an FPGA, its discrete-time implementation must be used. Elia and Wang suggest using an Euler approximation to discretize the algorithm, which results in the discrete-time algorithm shown in \ref{eq:dls_discrete}, with $\gamma$ as the sample-time of the algorithm.

\begin{equation}
\label{eq:dls_discrete}
\begin{aligned}
x[k+1] &= (1-2\gamma)x[k] - \gamma A'v[k]\\
v[k+1] &= v[k] + \gamma (Ax[k] - b)
\end{aligned}
\end{equation}

This algorithm is guaranteed to converge to a solution provided that $\gamma$ meets the following requirement:
$$ 0 < \gamma \leq \min{ \left\{ 1, \frac{2}{\sigma_{max}^2} \right\} }$$
with $\sigma_{max}$ being the largest eigenvalue of $AA'$.


\section{Proposed Architecture}

\subsection{High Level}

The DLS method described in section \ref{sec:alg} can be decomposed into the system shown in \ref{eq:dls_discrete_decomposed}. At its heart, this system is composed of two sets of accumulators (one set for the primal solutions and the other for the dual solutions). Note also that $\gamma$ can be held constant during the solution, so the products $\gamma A$, $\gamma A'$ and $\gamma b$ will be constant across iterations.
\begin{equation}
\label{eq:dls_discrete_decomposed}
\begin{aligned}
x[k+1] &= x[k] - (2\gamma x[k] + \gamma A'v[k] )\\
v[k+1] &= v[k] + \gamma A x[k] - \gamma b
\end{aligned}
\end{equation}

A possible algorithm to implement the system from \ref{eq:dls_discrete_decomposed} is shown in algorithm \ref{alg:dls}. This algorithm contains only vector-matrix multiplication and addition/subtraction, unlike the MINRES algorithm which contains square root and division operations as well (see \cite{Boland2008}).

This algorithm is composed of three major steps:
\begin{enumerate}
\item Pre-multiply the $A$ and $b$ matrices by $\gamma$
\item Compute the dot-product between the previous node values and rows/columns of $A$
\item Perform the accumulation for each solution
\end{enumerate}

\begin{algorithm}
\caption{DLS Algorithm for a $n$-variable system.}
\label{alg:dls}
\input{DLS_Algorithm}
\end{algorithm}


\subsection{FPGA Implementation}

There are certain features of algorithm \ref{alg:dls} which can be exploited to create a parallel architecture, namely:
\begin{enumerate}
\item Computations for iteration $k$ only require solution data from iteration $k-1$ and the constants $\gamma A$, $\gamma A'$, $\gamma b$
\item Each primal accumulator is performing the same input processing and accumulator operation
\item Each dual accumulator is performing the same input processing and accumulator operation
\end{enumerate}

Based upon these observations, the accumulators can be decomposed into two distinct sets of accumulators all operating in parallel to find the solution at the next time point with no data dependencies between them. This leads to the overall parallel architecture shown in figure \ref{fig:dls_archi}.
\begin{figure}
\centering
\includegraphics[scale=0.5]{Architecture.png}
\caption{Architecture for a DLS solver.}
\label{fig:dls_archi}
\end{figure}
 This architecture has four main components: primal (X) accumulators, dual (V) accumulators, and accumulator fan-in calculations (vector dot-product).

\subsection{Fixed-Point Arithmetic}
\label{sec:fixedPoint}

This implementation utilized fixed-point arithmetic operations for all of its computations to reduce the complexity due to floating-point operations. It was determined that a fixed-point word length of 32 bits and a fraction length of 24 bits would be sufficient to represent the matrix data during the computations when the following constraints were placed on the design:
\begin{enumerate}
\item $\gamma=1$, to remove multiplication by small decimal numbers
\item Prescaled $A$ and $b$ matrices - scale $A$ such that $\lambda (AA') \le 1$ to guarantee convergence and reduce large numbers
\end{enumerate}
With those constraints, simulations showed that the 4 major data connections in this design would rarely have values below $2^{-16}$, and above $2^7$. Sample outputs from the simulations can be seen in figure \ref{fig:fixedPointAnalysis}.

\begin{figure*}
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[scale=0.33]{Analysis_Vfanin.png}
\subcaption{V-node fanin}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[scale=0.33]{Analysis_Xfanin.png}
\subcaption{X-node fanin}
\end{minipage}\\
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[scale=0.33]{Analysis_Vnode.png}
\subcaption{V-node accumulator}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[scale=0.33]{Analysis_Xnode.png}
\subcaption{X-node accumulator}
\end{minipage}
\caption{Bit usage for the major signals in the design.}
\label{fig:fixedPointAnalysis}
\end{figure*}

\subsection{Hardware Design}

The architecture for this algorithm implementation, as shown in figure \ref{fig:dls_archi}, is composed of three major components:
\begin{itemize}
\item Dual Nodes (Accumulators)
\item Primal Nodes (Accumulators)
\item Vector-Vector Multipliers (Fan-in modules)
\end{itemize}

\subsubsection{Dual Accumulators}

The dual node accumulators in this system are a dual-input, single-output data block with the architecture shown in figure \ref{fig:dualnode}. The accumulator node is designed to implement the accumulation operation described in equation \ref{eq:dualNode}.
\begin{equation}
v_i[k+1] = v_i[k] + d_i[k] - b_i
\label{eq:dualNode}
\end{equation}
This operation has two inputs, $b_i$ and $d_i$. The value for $b_i$ is loaded into the node during the setup phase, and is then stored in a register. The value for $d_i$ is the output of the vector-vector multiplication between a row of the $A$ matrix and the values of the previous iteration's primal nodes (this computation is performed in the fan-in module). The result of the accumulator operation is then stored in the accumulator register, after which it is placed on the $v_i$ output signal and used in the next iteration.

\subsubsection{Primal Accumulators}

The primal accumulators in this system are a dual-input, single-output data block with the architecture shown in figure \ref{fig:primalnode}. These accumulator nodes are designed to implement the accumulation operation described by equation \ref{eq:primalNode}.
\begin{equation}
x_i[k+1] = x_i[k] - (2\gamma*x_i[k] + u_i[k])
\label{eq:primalNode}
\end{equation}
This operation has two inputs, $u_i$ and $\gamma$. The value for $\gamma$ is a constant value when the solver runs, so all primal nodes use the same value. This value is then shifted left 1-bit to multiply it by two (this was another advantage of using fixed-point over floating-point). The $d_i$ input consists of the product of the vector-vector multiplication between a column of the $A$ matrix and the previous iterations' dual node values (this computation is performed in the fan-in module). The result of the accumulator operation is then stored in the accumulator register, where it is then output on the $x_i$ signal and used in the next iteration.

\subsubsection{Vector-Vector Multipliers (Fan-in)}

The vector-vector multipliers are designed to perform the dot-product between two vectors. These modules act as the fan-in computation for each node, taking the previous iteration's node values, scaling them, then combining them into one output. The exact vectors utilized in the fan-in module differe between the ones in front of a dual accumulator and the ones in front of a primal accumulator.

The module in front of the $i-th$ dual accumulator performs the operation shown in equation\ref{eq:dualFanin}, which is the dot-product between the $i-th$ row of $A$ and the previous iteration's primal accumulator outputs. The module in front of the $i-th$ primal accumulator performs the operation shown in equation \ref{eq:primalFanin}, which is the dot-product between the $i-th$ column of $A$ and the previous iteration's dual accumulator outputs.
\begin{align}
d_i[k] &= a_{i,:} * x[k-1]\label{eq:dualFanin}\\
u_i[k] &= a_{:,i} * v[k-1]\label{eq:primalFanin}
\end{align}

The vector-vector multiplier implemented in this design was a temporal-based design where there was a single multiplication unit followed by an accumulator, and the two vectors were shifted into the multiplication unit's inputs. This architecture is shown in figure \ref{fig:fanin}.

\begin{figure*}[t]
\centering
\begin{minipage}{0.3\textwidth}
\centering
\includegraphics[scale=0.33]{Fanin.png}
\subcaption{Vector-vector multiplier architecture}
\label{fig:fanin}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\centering
\includegraphics[scale=0.33]{DualNode.png}
\subcaption{Dual accumulator architecture}
\label{fig:dualnode}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\centering
\includegraphics[scale=0.33]{PrimalNode.png}
\subcaption{Primal accumulator architecture}
\label{fig:primalnode}
\end{minipage}
\caption{Architectures for relevant parts of the system.}
\end{figure*} 


\subsubsection{Matrix Storage}

Storage of the $A$ and $b$ matrices in this design is distributed throughout the vector-vector multiplier modules and the dual nodes. Each dual node contains a 32-bit register that the value of $b$ is latched into before computation begins.

Each vector-vector multiplier contains a 32-bit by $n$-slot BlockRAM module to hold the row/column of $A$ required for computation. A distributed storage system was chosen because of the data demand by the vector-vector multiplier, namely that at each instance every multiplier module requires a different coefficient from the $A$ matrix for computation. Implementing this in a single RAM would require either $2n$ output ports from the RAM, or remove the parallel computation nature of the vector-vector multiplication modules.


\section{Resource Utilization Scaling}

To investigate how this design scaled with matrix size, several matrix dimensions were chosen to act as representative samples. Those dimensions were then used to synthesize a solver design in Xilinx ISE, which then provided an estimate on the number of slice registers, slice LUTs, and DSP slices the design would use.

The overall percent utilization of each FPGA element is shown in figure \ref{fig:areaScaling}, and follows a general linear scaling trend. That is, the DSP slice usage, slice register and slice LUT usage are $O(n)$ until $n=16$. Between $n=16$ and $n=20$, the usage of slice LUTs dramatically increases, while DSP slice usage decreases. This point is probably where the synthesizer has run out of DSP slices available to route/implement the design on, so the LUTs must be configured to be the main computational elements. Between $n=20$ and $n=24$ the design becomes too large for the number of LUTs on the chosen chip, preventing its implementation.

\begin{figure*}
\centering
\includegraphics[scale=0.65]{AreaResults.png}
\caption{Resource usage on the Xilinx Zynq 7020-1CLG484 chipset, as reported by ISE synthesis tools.}
\label{fig:areaScaling}
\end{figure*}


\input{AxiInterface}
\section{Results}\label{results}
The hardware implementation of the DLS algorithm was tested against three separate systems. The first two cases had the same A matrix and is shown in Equation \ref{eq:Atest1}. This matrix is non-invertable and non-symmetric causing all of the algorithms in Table \ref{tab:solverComparison} to fail to converge to the solution. The third test was of a randomly gereated A and B matrix. Table \ref{table:results} shows the performance of our DLS FPGA implementation compared to a DLS MATLAB implementation under the same test cases. This is a valid comparison to our hardware because our algorithm has not been implemented on embedded devices previously, and has only been used in MATLAB simulations.

\begin{equation}\label{eq:Atest1}
A=\begin{bmatrix} 1 & 2 & 0 & 0 \\ 0 & 2 & 0 & 1 \\ -1 &  0 & 0 & 1 \\ 0 & 0 & -1 & 1
\end{bmatrix}
\end{equation}

\begin{table}[t]
\centering
\begin{tabular}{|c|c||c|c||c|}
\hline
Test & MATLAB & FPGA Load/Store & FPGA Solver & Speed up \\
\hline
1 & 31 ms & 0.03 ms & 0.624 ms & 48x \\
2 & 30 ms & 0.03 ms & 0.624 ms & 46x \\
3 & 30 ms & 0.03 ms & 0.624 ms & 46x \\
\hline
\end{tabular}
\caption{Solver timing results for sample systems (4000 iterations)}
\label{table:results}
\end{table}

\section{Conclusions}

Overall, the DLS algorithm for solving linear systems produces a speed-up of 46 times in comparison to MATLAB, showing that this algorithm is feasible to implement in digital hardware. This algorithm does have several downsides though, namely:
\begin{itemize}
\item Difficult to computer upper bound on iterations required to converge
\item Scaling difficulty to large matrices
\end{itemize}
On the other hand though, the algorithm does have several promising features, namely:
\begin{itemize}
\item Distributed nature, with minimal data passing between nodes
\item Requires only addition/subtraction and multiplication operations, no division or square-root operations needed
\end{itemize}

\section{Future Work}

This design provides a starting point for further investigation into this algorithm. Some possible areas of future work are:
\begin{itemize}
\item Analysis of quantization effects on algorithm correctness
\item Comparison against C-code implementation on an embedded platform
\item Analysis of the effect of the vector-vector multiplier design on the algorithm performance
\item Analysis of performance (execution time and clock speed) as matrix size increases
\end{itemize}

\bibliographystyle{plain}
\IEEEtriggercmd{\newpage}
%\IEEEtriggeratref{7}
\newpage
\bibliography{References}




\end{document}
