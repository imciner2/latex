% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: October 28, 2014
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[10pt,titlepage]{IEEEtran} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PACKAGES
\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{cite}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim

\title{An FPGA-Based Implementation of the Dynamical Linear System Method for Solving Systems of Linear Equations}
%\date{December 12, 2014}
\author{Ian McInerney, Paul Uhing}


% Include custom commands
\include{Unit-Declaration}
\include{Sci-Formatting}

\usetikzlibrary{dsp,chains,automata,positioning}


\begin{document}

\maketitle


\section{Introduction}

Various techniques in the fields of control systems, optimization, and signal processing rely on linear systems of equations for their ultimate implementation. There are many different methods of solving these linear systems of equations ranging from direct methods such as LU Decomposition that solve for the exact solution in one step, to iterative methods that arrive at the solution after multiple steps. Each of these methods has their advantages and disadvantages for implementation and usage. This project will explore the implementation of a recently developed algorithm for the iterative solution of linear systems of equations, called the Dynamical Linear System (DLS) method.

\section{Linear System Solvers}

There are two main types of solvers for linear systems that have been developed: direct and iterative.

\subsection{Direct Methods}

Direct solution methods are able to obtain a solution to the system of equations in one numerical time step. The most common solution style is the LU Decomposition method.

The LU decomposition method for solving systems of linear equations splits the given $A$ matrix into two matrices such that $A=LU$ with $L$ a lower triangular matrix and $U$ an upper triangular matrix. Once those two matrices are determined, the system $Ly=b$ and $Ux=y$ is solved. These two systems are generally easier to solve for because of the triangular nature of the $L$ and $U$ matrices \cite{Greenbaum1997}.

There are several downsides to this method of solution, namely that $A$ must be non-singular and the $L$ and $U$ matrices will not be sparse if $A$ was sparse. Additionally, the computational complexity of these solutions is large, requiring approximately $O(n^3)$ for a $n \times n$ $A$ matrix \cite{Greisen2013}.


\subsection{Iterative Methods}

At their core, the iterative methods for the solution of a system of linear equations are methods for gradually refining an initial solution guess into the final solution over a specified number of iterations. The simplest iteration method starts with an approximation $x_o$, and then gradually refine that approximation using the equation $$M(x_{i+1} - x_{i}) = b-Ax_{i}$$ This method requires another matrix, $M$, to be defined. That matrix is known as a pre-conditioner matrix and can change the rate at which the solution is found.

This iteration method has an approximate computational complexity of $O(n)$, but has very strict conditions that must be observed for a solution to be found \cite{Greisen2013}. These strict convergence requirements make the simple iterative solver impractical to use on its own in most systems \cite{Morris2005}.


More advanced iterative methods are generally founded upon the optimization problem in \ref{eq:generalOpt} which seeks to minimize the error of the solution vector.
\begin{equation}
\label{eq:generalOpt}
\begin{aligned}
& \text{minimize} & \norm{x}^2_2 \\
& \text{subject to} & x \in\underset{x}{\text{argmin}} \norm{Ax-b}_2
\end{aligned}
\end{equation}
There are many different advanced iterative methods such as the Conjugate Gradient method, Biconjugate Gradient method, Generalized Minimal Residual (GMRES) method and the Minimum Residual - QLP Decomposition (MINRES-QLP). Each of these methods has distinct requirements that the $A$ matrix must satisfy to guarantee convergence to an approximate solution. An overview of these solver types, and references to more detailed solver descriptions can be seen in Table \ref{tab:solverComparison}.
\begin{table*}[t]
\centering
\begin{threeparttable}
\caption{Comparison of linear system solvers.}
\label{tab:solverComparison}
\begin{tabular}{|c|c|c|c|c|}
\hline
\multicolumn{2}{|c|}{Solver} & Type & Requirements on A \tnote{1} \tnote{2}\\
\hline
LU Decomposition & \cite{Greenbaum1997} & Direct & Non-Singular\\
\hline
Simple Iteration & \cite{Greenbaum1997} & Iterative & $\rho (I-M^{-1}A) < 1$\\
Conjugate Gradient & \cite{Greenbaum1997} & Iterative & Symmetric Positive-Definite\\
Biconjugate Gradient & \cite{Olshanskii2014} & Iterative & Non-zero determinant\\
GMRES & \cite{Olshanskii2014} & Iterative & Non-Singular\\
MINRES-QLP & \cite{Choi2014} & Iterative & Symmetric\\
DLS\tnote{3} & \cite{Wang2014} & Iterative & General\\
\hline
\end{tabular}
\begin{tablenotes}
\item [1] $\rho(A)$ computes the spectral radius of matrix $A$
\item [2] $M$ is an invertible preconditioner matrix
\item [3] This solver is the focus of this project
\end{tablenotes}
\end{threeparttable}
\end{table*}

\subsection{Implementation of Solvers}

The implementation of the linear system solvers onto an FPGA is an important field of ongoing research, and is usually examined along with an application domain. For example, \cite{Greisen2013} examines the implementation of two distinct solvers in the field of real-time image processing:
\begin{enumerate}
\item A Direct-method Cholesky decomposition (which is a specific LU-decomposition with $U=L^{\text{T}}$) 
\item An iterative Biconjugate Gradient Stabilized solver (extension of the Biconjugate Gradient)
\end{enumerate}

Other researchers have examined the application of linear system solvers to the Model Predictive Control (MPC) problem. In \cite{Hartley2013}, the problem of aircraft control was examined using a MPC approach. This approach utilized a real-time optimization algorithm that had a linear system solver at its core, specifically the MINRES solver. This work examined the implementation details of a MINRES solver utilizing two forms of preconditioning: online (where the FPGA logic applies the preconditioner) and offline (where the preconditioner is applied before the matrices are given to the solver). Similar research was also conducted by \cite{Hartley2013}, with the application of real-time MPC to the control of spacecraft orbits.

Pure implementation work for the MINRES solver was first conducted in \cite{Boland2008}. This work examined the parallelisation of the algorithm, and the growth of the resource usage as the matrix size increases.

\section{Proposed Algorithm (DLS Method)}
\label{sec:alg}

\subsection{General Algorithm}

The algorithm we propose to use to solve linear systems of equations was developed by Elia and Wang in \cite{Wang2014} as a method of solving the optimization problem shown in \ref{eq:convexProblem} (which is simply a special case of the optimization problem in \ref{eq:generalOpt}).

\begin{equation}
\label{eq:convexProblem}
\begin{aligned}
& \underset{x}{\text{minimize}} & \norm{x}^2_2 \\
& \text{subject to} &Ax = b
\end{aligned}
\end{equation}

Their algorithm presents the solution to the optimization problem in \ref{eq:convexProblem} as the state trajectory of the dynamical linear system shown in \ref{eq:dls_cont}. Where $x$ are the primal problem solutions, and $v$ are the Lagrangian-dual problem solutions.

\begin{equation}
\label{eq:dls_cont}
\begin{aligned}
\dot{x} &= -2x - A'v\\
\dot{v} &= Ax - b
\end{aligned}
\end{equation}

Over time, the state trajectory of the system given in \ref{eq:dls_cont} will converge to either:
\begin{enumerate}
\item The unique solution vector for the initial equation $Ax=b$ (when A is non-singular)
\item The solution vector that minimizes its Euclidean norm (when A is singular)
\end{enumerate}
Note that only option 1 is unique, the other option will give a solution dependent upon the initial conditions of the system.

The advantage of this algorithm is that it is able to solve for a solution vector with general A matrices. This means there are no singularity, definiteness, or symmetric conditions required of the A matrix for a solution.

\subsection{Discrete-Time Algorithm}

The system presented in \ref{eq:dls_cont} is designed for continuous-time implementation (such as analog circuits). In order for this algorithm to converge on an FPGA, its discrete-time implementation must be used. Elia and Wang give the discrete-time algorithm shown in \ref{eq:dls_discrete}, with $\gamma$ as the sample-time of the algorithm.

\begin{equation}
\label{eq:dls_discrete}
\begin{aligned}
x[k+1] &= (1-2\gamma)x[k] - \gamma A'v[k]\\
v[k+1] &= v[k] - \gamma (Ax[k] - b)
\end{aligned}
\end{equation}

This algorithm is guaranteed to converge to a solution provided that $\gamma$ meets the following requirement:
$$ 0 < \gamma < \min{ \left\{ 1, \frac{2}{\sigma_{max}^2} \right\} }$$
with $\sigma_{max}$ being the largest eigenvalue of $AA'$.


\section{Proposed Architecture}

\subsection{High Level}

The DLS method described in section \ref{sec:alg} can be decomposed into the system shown in \ref{eq:dls_discrete_decomposed}. At its heart, this system is composed of two sets of accumulators (one set for the primal solutions and the other for the dual solutions). Note also that $\gamma$ can be held constant during the solution (by holding the step size constant), so the products $\gamma A$, $\gamma A'$ and $\gamma b$ will be constant across iterations.
\begin{equation}
\label{eq:dls_discrete_decomposed}
\begin{aligned}
x[k+1] &= x[k] - (2\gamma x[k] + \gamma A'v[k] )\\
v[k+1] &= v[k] - \gamma A x[k] - \gamma b)
\end{aligned}
\end{equation}

A possible algorithm to implement the system from \ref{eq:dls_discrete_decomposed} is shown in algorithm \ref{alg:dls}. This algorithm contains only vector-matrix multiplication and addition/subtraction, unlike the MINRES algorithm which contains square root and division operations as well (see \cite{Boland2008}).

This algorithm is composed of three major steps:
\begin{enumerate}
\item Pre-multiply the $A$ and $b$ matrices by $\gamma$
\item Compute the dot-product between the previous node values and rows/columns of $A$
\item Perform the accumulation for each solution
\end{enumerate}

\begin{algorithm}
\caption{DLS Algorithm for a $n$-variable system.}
\label{alg:dls}
\input{DLS_Algorithm}
\end{algorithm}


\subsection{FPGA Implementation}

There are certain features of algorithm \ref{alg:dls} which can be exploited to create a parallel architecture, namely:
\begin{enumerate}
\item Computations for iteration $k$ only require solution data from iteration $k-1$ and the constants $\gamma A$, $\gamma A'$, $\gamma b$
\item Each primal accumulator is performing the same input processing and accumulator operation
\item Each dual accumulator is performing the same input processing and accumulator operation
\end{enumerate}

Based upon these observations, the accumulators can be decomposed into two distinct sets of accumulators all operating in parallel to find the solution at the next time point with no data dependencies between them. This leads to the overall parallel architecture shown in figure \ref{fig:dls_archi}.
\begin{figure*}
\centering
\include{SystemDiagram}
\caption{Architecture for a $n=4$ DLS solver.}
\label{fig:dls_archi}
\end{figure*}
 This architecture has four main components: primal (X) accumulators, dual (V) accumulators, accumulator fan-in calculations, and memory banks for the storage of the $A$ and $b$ matrices.
 
This design will utilize fixed-point arithmetic for its computations. An initial study will be conducted as part of the project to determine the optimum fixed-point representation size to utilize for the design.
 
\begin{figure*}[t]
\centering
\begin{minipage}{0.3\textwidth}
\centering
\include{MemoryUnit}
\subcaption{Memory unit that performs the multiplication by gamma.}
\label{fig:memoryGamma}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\centering
\include{DualNode}
\subcaption{Dual accumulator architecture}
\label{fig:dualnode}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\centering
\include{PrimalNode}
\subcaption{Primal accumulator architecture}
\label{fig:primalnode}
\end{minipage}
\caption{Architectures for relevant parts of the system.}
\end{figure*} 

\subsubsection{Primal Accumulators}

The primal accumulators in this system are a single-input, single-output data block that takes in the computed $u_i$ value, performs the accumulation operation, then outputs the resulting $x_i$ value. A possible architecture to implement this is shown in figure \ref{fig:primalnode}.

\subsubsection{Dual Accumulators}

The dual accumulators in this system are a dual-input, single-output data block. They will take in the computed $d_i$ value and the $b_i$ value from the original equation. The accumulation operation is then performed, and the resulting $v_i$ value is output. A possible architecture to implement this is shown in figure \ref{fig:dualnode}.

% Put this here to make the table be on the last page (instead of on its own page)
%\begin{table*}[!t]
%\centering
%\caption{Co-processor instructions for interfacing with the DLS solution architecture}
%\label{tab:coproc_instruc}
%\begin{tabular}{|l|l|p{12cm}|}
%\hline
%Pnemonic & Format & Description\\
%\hline
%LDA & LDA \textit{row}, \textit{col}, \textit{data} & Store the number given by %\textit{data} into the $A$ matrix at location (\textit{row}, \textit{column})\\
%LDB & LDB \textit{index}, \textit{data} & Store the number given by \textit{data} into the $b$ matrix at location \textit{index}\\
%\hline
%RDX & RDX \textit{index} & Read the primal solution from the primal accumulator specified by \textit{index}\\
%RDV & RDV \textit{index} & Read the dual solution from the dual accumulator specified by \textit{index}\\
%\hline
%RUN & RUN \textit{iter} & Start the solution and run for \textit{iter} number of %iterations\\
%\hline
%\end{tabular}
%\end{table*}

\subsubsection{Fan-In Calculations}

The fan-in calculation performed for each accumulator is simply the dot-product between two vectors. For the dual accumulators, it is the dot product between the rows of the $A$ matrix and the primal solutions at the previous time step. For the primal accumulators, it is the dot product between the columns of the $A$ matrix and the dual solutions at the previous time step.

\subsubsection{Memory Banks}

The $A$ and $b$ matrices for the computations are stored inside special memory banks. These memory banks will exploit the fact that $\gamma$ and the matrices remain constant across iterations, so the memory banks will compute $\gamma A$, $\gamma A'$ and $\gamma b$ internal to the banks. This means that these scalings will occur at memory load time, so they will not affect the performance of the iterative algorithm. This is similar to the method done in \cite{Hartley2014} for their online matrix preconditioning.

\section{Test Methodology}

To allow for the testing of the DLS solution architecture, it will be implemented as a co-processor on the Axi bus with both a master and slave interface. This interface will allow the core to retrieve the input and write the solution to memory on the bus. The interface will allow the user to  where necessary input is in memory and where to place the solution and provide necessary information to compute the solution.

%In order to allow for the interface to the DLS solution architecture, five new co-processor instructions must be implemented. These five instructions are shown in table \ref{tab:coproc_instruc}.

The overall project progress will be as follows:
\begin{enumerate}
\item Create the individual entities (dual accumulator, primal accumulator, fan-in and memory) and test separately in simulation
\item Put the individual entities together and test the co-processor in simulation
\item Merge the co-processor with the Axi bus interface
\item Explore the solution time and co-processor area/resources for various solution sizes
\end{enumerate}

\bibliographystyle{plain}
\IEEEtriggercmd{\newpage}
%\IEEEtriggeratref{7}
\newpage
\bibliography{References}




\end{document}
