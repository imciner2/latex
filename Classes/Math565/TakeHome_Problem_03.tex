% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: April 27, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
% These packages are all incorporated in the memoir class to one degree or another...

% Include custom commands
\usepackage{Unit-Declaration}
\usepackage{Sci-Formatting}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{}\rhead{}
\lfoot{Math 565 - Spring 2016}\cfoot{Problem 3}\rfoot{\thepage}

\title{Math 565 - Take Home Exam Problem 3}
\author{Ian McInerney}


%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%\graphicspath{{Images/}}

%%% END Article customizations

%%% The "real" document content comes below...

\begin{document}

\section*{Problem 3}

The main task for this problem is to solve the optimization problem given in \ref{eq:3_original} using the built-in MATLAB functions:
\begin{equation}
	\begin{aligned}
	& \underset{x\in\mathbb{R}^n ,\lambda\in\mathbb{R}}{\text{minimize}} & x^T(\lambda e - Ux) \\
	& \text{subject to} & \lambda e - Ux \geq 0\\
	& & x \geq 0\\
	& & e^T x = 1
	\end{aligned}
	\label{eq:3_original}
\end{equation}
In order to use the MATLAB functions, the problem must be in terms of a single variable.
 To do this, define
$z = \left[ \begin{matrix} x^T & \lambda\end{matrix}\right]^T$. MATLAB also wants the problem to fit into the structure shown in \ref{eq:3_matlab}.
\begin{equation}
\begin{aligned}
& \underset{z\in\mathbb{R}^{n+1}}{\text{minimize}} & \frac{1}{2}z^T H z + f^T z \\
& \text{subject to} & A z \leq b\\
& & A_{eq} z = b_{eq}\\
\end{aligned}
\label{eq:3_matlab}
\end{equation}
To accomplish this, notice that the cost function is composed of a quadratic term in $x$ and a cross term between $x$ and $\lambda$.
 These terms can be composed into a single matrix
$	H = \left[
	\begin{matrix}
	2U & 1\\
	1 & 0
	\end{matrix}
	\right]
$. This matrix expresses the entire original cost function, so $f$ is a $1 \times (n+1)$ matrix of zeros.

The next parts to map are the constraints. The easiest one to map is the equality constraint which will be composed of a row vector $A_{eq}$ which is $1 \times (n+1)$, with $n$ 1s and a single 0 (so of the form
$ A_{eq} = \left[ 
\begin{smallmatrix}
1 & \cdots & 1 & 0
\end{smallmatrix} \right]
$), and a scaler $b_{eq} = 1$.

The inequality constraints must be reduced into a single constraint equation satisfying $A z \leq b$. In order to do this, $A$ should be a partitioned matrix that is $2n \times (n+1)$ and has the structure 
$	A = \left[
\begin{matrix}
U & -1\\
\textbf{diag}(-1) & 0
\end{matrix}
\right]
$ (where $\textbf{diag}(-1)$ is a $n \times n$ matrix with -1 on the diagonal and 0 everywhere else).
 This matrix picks up the negative signs on the submatrices to allow for the $\leq$ sign to be used instead of the $\geq$ sign that is in the original problem.
  The final matrix necessary is the $b$ matrix, which is a $2n \times 1$ of zeros.
  
Overall, the matrices used in this problem are:
\begin{center}
\begin{minipage}{0.2\textwidth}
	\begin{equation*}
		H = \left[
		\begin{matrix}
		2U & 1\\
		1 & 0
		\end{matrix}
		\right]
	\end{equation*}
\end{minipage}
\begin{minipage}{0.2\textwidth}
	\begin{equation*}
	f = \left[ \begin{matrix}
	0 & \cdots & 0
	\end{matrix} \right]
	\end{equation*}
\end{minipage}
\begin{minipage}{0.2\textwidth}
	$ H \in \mathbb{R}^{(n+1) \times (n+1)}	$
	
	$ f \in \mathbb{R}^{1 \times n}	$
\end{minipage}

\begin{minipage}{0.2\textwidth}
	\begin{equation*}
		A = \left[
		\begin{matrix}
		U & -1\\
		\textbf{diag}(-1) & 0
		\end{matrix}
		\right]
	\end{equation*}
\end{minipage}
\begin{minipage}{0.2\textwidth}
	\begin{equation*}
		b = \left[ \begin{matrix}
		0\\
		\vdots \\
		0
		\end{matrix}
		\right]
	\end{equation*}
\end{minipage}
\begin{minipage}{0.2\textwidth}
	$ A \in \mathbb{R}^{2n \times (n+1)} $
	
	$ b \in \mathbb{R}^{2n \times 1} $
\end{minipage}

\begin{minipage}{0.2\textwidth}
	\begin{equation*}
		A_{eq} = \left[ \begin{matrix}
		1 & \cdots & 1 & 0
		\end{matrix} \right]
	\end{equation*}	
\end{minipage}
\begin{minipage}{0.2\textwidth}
	\begin{equation*}
		b_{eq} = \left[ \begin{matrix}
		1\\
		\vdots \\
		1 \\
		\end{matrix}
		\right]
	\end{equation*}
\end{minipage}
\begin{minipage}{0.2\textwidth}
	$ A_{eq} \in \mathbb{R}^{1 \times (n+1)} $
	
	$ b_{eq} \in \mathbb{R}^{(n+1) \times 1} $
\end{minipage}
\end{center}
\end{document}
