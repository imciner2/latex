% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: October 27, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}
% These packages are all incorporated in the memoir class to one degree or another...

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\usepackage{graphicx}
\usepackage{listings}
\usepackage{subcaption}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{datetime}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{EE 578 Assignment 2.5}\rhead{Ian McInerney}
%\lfoot{Draft: \today\ \currenttime}\cfoot{\thepage}\rfoot{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\bibliographystyle{IEEEtran}

%%% END Article customizations


\begin{document}

\section*{Problem}
Given the logistic map: $ x_{n+1} = a x_n ( 1 - x_n)$, and $a \in [0, 4]$, create the invariant measure plots for several values of $a$ and compare them against the PF operator plots.

\section*{Solution}

To solve this problem, I created the following MATLAB script. This script will create 100 initial conditions uniformly distributed in $x_0 \in [0,1]$, then run the logistic map for 500 iterations. It will then find the average number of times the value for the logistic map  was in a specified bin (in this case there are 100 bins between $[0,1]$).

{\footnotesize
\begin{lstlisting}[language=matlab,frame=single]
%% Specify the bins
numBins = 100;
bins = 0:1/numBins:1;

completeBinProb = [];
for a=0:0.01:4.0
close all

%% Create the initial conditions
numInit = 100;

x0 = rand(100,1);


%% Run the logistic map update
T = 500;

x = [];
for i=0:1:numInit
xComp = x0;
for t = 1:1:T
xComp(t+1) = a*xComp(t) * (1 - xComp(t) );
end

x = [x; xComp];
end


%% Bin the result
binProb = zeros(numBins,1);
for i = 1:1:numBins
binProb(i) = (1/(T*numInit + numInit))*length(find( (bins(i) <= x) & (x < bins(i+1)) ));
end

completeBinProb = [completeBinProb binProb];

%% Create the histogram
figure;
binCenter = (bins(1:end-1) + bins(2:end))/2;
bar(binCenter, binProb);
title(['Invariant measure plot for the Logistic map with a=' num2str(a)]);
ylabel('Probability in bin');
xlabel('X value');
xlim([0,1]);
ylim([0,1]);
print(['Assignment_2-5_InvariantPlot_a' num2str(a, '%1.2f') '.png'], '-dpng');

end


%% Do a 2d color plot of the results of the histogram
figure;
binCenter = (bins(1:end-1) + bins(2:end))/2;
a = 0:0.01:4.0;
TwodProb = reshape(completeBinProb, length(binCenter), length(a));
pcolor( a, binCenter, TwodProb );
title('Invariant measure plot for the Logistic map with multiple a values');
ylabel('X value');
xlabel('a value');
xlim([0,4]);
ylim([0,1]);
colorbar;
\end{lstlisting}}

An interesting plot I decided to create was to plot the different invariant measure plots side-by-side in a 2d plot. This plot is shown in figure \ref{fig:invariantBifurcation}, and the bifurcation plot created for the midterm can be seen in figure \ref{fig:bifurcation}. Notice that these plots look very similar, but it becomes very difficult to show what is going on above $a=3.25$ because the probability of being in a bin becomes close to 0.

\begin{figure}[h!]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_invariantSweepinga.png}
		\caption{Invarant measure plot for $a \in [0,4]$.}
		\label{fig:invariantBifurcation}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_BifurcationPlot.png}
		\caption{Bifurcation plot for $a \in [0,4]$.}
		\label{fig:bifurcation}
	\end{subfigure}
\end{figure}

Comparing the Invariant measure plots for specific values of $a$ against the PF operator plots for those same values of $a$ results in similar plots. These can be seen in figures \ref{fig:invariantPF_Part1}, \ref{fig:invariantPF_Part2}, \ref{fig:invariantPF_Part3}, \ref{fig:invariantPF_Part4}, \ref{fig:invariantPF_Part5}. The invariant measure histograms are shown on the left, and the PF eigenfunction plots are shown on the right. These PF plots are the same ones generated during the midterm, so they have $\sigma = 0.25$ and 200 dictionary functions.

Note that the PF operator plots show peaks/troughs at roughly the same position as the invariant measure plots, but the peaks for the PF operator are wider due to the spread of the Gaussian RBF. These peaks could be sharpened if the number of dictionary functions are increased as the $\sigma$ value is lowered, but this will also increase the computational effort.

{\footnotesize
\begin{figure}[h!]
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a1_00.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a1_00.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a2_00.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a2_00.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a2_20.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a2_20.png}
	\end{subfigure}
	
	\caption{Comparison between invariant measure and PF operator, Part 1}
	\label{fig:invariantPF_Part1}
\end{figure}}


{\footnotesize
\begin{figure}[h!]
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a2_40.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a2_40.png}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a2_60.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a2_60.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a2_80.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a2_80.png}
	\end{subfigure}
	
	\caption{Comparison between invariant measure and PF operator, Part 2}
	\label{fig:invariantPF_Part2}
\end{figure}}


{\footnotesize
\begin{figure}[h!]
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a3_00.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a3_00.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a3_20.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a3_20.png}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a3_40.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a3_40.png}
	\end{subfigure}
	
	\caption{Comparison between invariant measure and PF operator, Part 3}
	\label{fig:invariantPF_Part3}
\end{figure}}

{\footnotesize
\begin{figure}[h!]
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a3_50.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a3_50.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a3_60.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a3_60.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a3_80.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a3_80.png}
	\end{subfigure}
	
	\caption{Comparison between invariant measure and PF operator, Part 4}
	\label{fig:invariantPF_Part4}
\end{figure}}

{\footnotesize
\begin{figure}[h!]
	\vspace{-1cm}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a3_90.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig1_a3_90.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Assignment_2-5_InvariantPlot_a4_00.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{plots/Midterm1_P2_PF_eig2_a4_00.png}
	\end{subfigure}
	
	\caption{Comparison between invariant measure and PF operator, Part 5}
	\label{fig:invariantPF_Part5}
\end{figure}}
	
\end{document}
