% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: October 27, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}
% These packages are all incorporated in the memoir class to one degree or another...

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\usepackage{graphicx}
\usepackage{listings}
\usepackage{subcaption}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{datetime}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{EE 578 Midterm 1}\rhead{Ian McInerney}
%\lfoot{Draft: \today\ \currenttime}\cfoot{\thepage}\rfoot{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\bibliographystyle{IEEEtran}

%%% END Article customizations


\begin{document}

\section*{Problem 1}

Given
\begin{align*}
	\dot{x}_1 &= x_1 - x_1^3 + x_2 \\
	\dot{x}_2 &= 2 x_1 - x_2
\end{align*}

\subsection*{Part a - Find the equilibrium points}

To do so, set the derivatives to zero, ie:
\begin{align*}
	0 &= x_1 - x_1^3 + x_2 \\
	0 &= 2 x_1 - x_2
\end{align*}
Notice that $(0,0)$ is a trivial solution to this set of equations. From the second equation it is known that $x_2 = 2 x_1$. Substituting that into the 1st equation gives $3 x_1 - x1^3 = 0$. This then has three possible solutions: $x_1 = 0$, $\pm \sqrt{3}$. Plugging those into the equation for $x_2$ gives the following three equilibrium points:
\begin{equation*}
	(0,0), \qquad (\sqrt{3}, 2 \sqrt{3}), \qquad (-\sqrt{3}, -2 \sqrt{3})
\end{equation*}

\subsection*{Part b - Evaluate stability of equilibrium points}

To evaluate the stability of the equilibrium points, linearize the system by forming the Jacobian.

The partial derivatives are:
\begin{align*}
	\frac{\partial f_1}{\partial x_1} &= 1 - 3 x_1^2 \\
	\frac{\partial f_1}{\partial x_2} &= 1\\
	\frac{\partial f_2}{\partial x_1} &= 2\\
	\frac{\partial f_2}{\partial x_2} &= -1
\end{align*}
This makes the Jacobian matrix
\begin{equation*}
	\left[\begin{matrix}
	1 - 3 x_1^2 & 1\\
	2 & -1
	\end{matrix}\right]
\end{equation*}

Evaluate it at the point $(0,0)$ to find
\begin{equation*}
	A = 
	\left[\begin{matrix}
	1 & 1\\
	2 & -1
	\end{matrix}\right]
\end{equation*}
This matrix has a characteristic polynomial of $\chi (\lambda) = \lambda^2 - 3$, which has roots at $\lambda = \pm \sqrt{3}$. This means that this equilibrium point has one eigenvalue in the right-hand plane. That means that there is one unstable direction (so the flow moves away from the equilibrium point). Making this point unstable.

Evaluate it at the point $(\sqrt{3}, 2\sqrt{3})$ to find
\begin{equation*}
	A = 
	\left[\begin{matrix}
	-8 & 1\\
	2 & -1
	\end{matrix}\right]
\end{equation*}
This matrix has a characteristic polynomial of $\chi (\lambda) = \lambda^2 + 9 \lambda + 6$, which has roots at $\lambda \approx -0.72, -8.27$. This means that this equilibrium point has both eigenvalues in the left-hand plane. That means both directions are stable (the flows move towards the equilibrium point).

When it is evaluated at the point $(-\sqrt{3}, -2\sqrt{3})$, $A$ is the same as above. This means that this point has the same linearization as the other point, and is also stable.

\begin{table}[h!]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		Equilibrium Point & Stability\\
		\hline
		$(0,0)$ & Unstable\\
		\hline
		$(\sqrt{3}, 2\sqrt{3})$ & Stable\\
		\hline 
		$(-\sqrt{3}, -2\sqrt{3})$ & Stable\\
		\hline
	\end{tabular}
\end{table}

\subsection*{Part c - Examine regions of attraction}

To examine the regions of attraction, use a quadratic Lyapunov function namely $V(x) = \frac{1}{2} ( x_1^2 + x_2^2 ) $. This function should be shifted to be centered at the equilibrium point.

For the point $(\sqrt{3}, 2\sqrt{3})$,
\begin{equation*}
	V(x) = \frac{1}{2} ( ( x_1 - \sqrt{3})^2 + (x_2 - 2\sqrt{3})^2 )
\end{equation*}

To examine the region of attraction, find the largest set that has no flows leaving it with the contour defined by $V(x)$. To do this find $ \dot{V}(x) = 0$ where 

\begin{align*}
	\dot{V}(x) &= \frac{\partial V}{\partial x1} f1 + \frac{\partial V}{\partial x_2} f2\\
	&= (x_1 - \sqrt{3}) (x_1 - x_1^3 + x_2) + (x_2 - 2\sqrt{3}) (2 x_1 - x_2)\\
	&= x_1^2 - x_1^4 - x_2^2 + 3 x_1 x_2 - 5 \sqrt{3} x_1 + \sqrt{3} x_2 + \sqrt{3} x_1^3
\end{align*}

That equation is a 4th order polynomial in $x_1$ and only a 2nd order polynomial in $x_2$. To make this analysis, solve it for $x_2$ in terms of $x_1$. This way a closed-form solution can be found:
\begin{equation*}
	x_2 = \frac{3 x_1 + \sqrt{3}}{2} \pm \frac{1}{2} \sqrt{ -4 x_1^4 + 4\sqrt{3} x_1^3 + 13 x_1^2 - 14\sqrt{3} x_1 + 3 }
\end{equation*}

This then produces the boundary for the region of convergence using this Lyapunov function.

The analysis for the other stable equilibrium point $(-\sqrt{3}, -2 \sqrt{3})$ is similar. Its Lyapunov function is 
\begin{equation*}
V(x) = \frac{1}{2} ( ( x_1 + \sqrt{3})^2 + (x_2 + 2\sqrt{3})^2 )
\end{equation*}
which makes 
\begin{align*}
\dot{V}(x) &= \frac{\partial V}{\partial x1} f1 + \frac{\partial V}{\partial x_2} f2\\
&= (x_1 + \sqrt{3}) (x_1 - x_1^3 + x_2) + (x_2 + 2\sqrt{3}) (2 x_1 - x_2)\\
&= x_1^2 - x_1^4 - x_2^2 + 3 x_1 x_2 + 5 \sqrt{3} x_1 - \sqrt{3} x_2 -\sqrt{3} x_1^3
\end{align*}

When solved for $x_2$ it produces
\begin{equation*}
x_2 = \frac{3 x_1 - \sqrt{3}}{2} \pm \frac{1}{2} \sqrt{ -4 x_1^4 - 4\sqrt{3} x_1^3 + 13 x_1^2 + 14\sqrt{3} x_1 + 3 }
\end{equation*}
as the boundary for the region of convergence.

\subsection*{Part d - Construct a Phase Portrait}

The phase portrait for this system was constructed using MATLAB and the quiver command to plot the velocity vectors.

\begin{center}
\begin{figure}[h!]
	\includegraphics[width=\textwidth]{P1_PhasePlot.png}
	\caption{Phase plot for the system in Problem 1. The circular marks are the calculated regions of convergence from part c for each stable equilibrium point. The stable equilibrium points are marked by asteriks and the unstable point is marked by a circle.}
\end{figure}
\end{center}

Based on this plot, the unstable manifold for the unstable equilibrium point can be clearly seen (it directs the flow directly to the stable equilibrium points). The stable flow to that unstable point is the dividing line between the two regions of attraction, but is somewhat difficult to discern. It is approximately the reflection of the unstable manifold over the $x_2$ axis.

\subsection*{Part e - Use transfer operator analysis}

To further analyze this system, the Koopman operator and PF operator were computed using provided MATLAB code. In this code, the ode23t routine was used to simulate the system with a timestep of 0.1 seconds, starting from 200 randomly generated initial conditions inside $[-5, 5] {\times} [-5, 5]$. The operator was then determined using 500 Radial Basis Functions with $\sigma = 0.75$. The trajectories used in the analysis can be seen in figure \ref{fig:koopmanTrajec}.

The analysis produced a linear system with the eigenvalues shown in \ref{fig:koopmanEigen}. Of more interest though are the dominant Koopman and PF eigenfunctions. Those are shown in figures \ref{fig:koopmanRegionsObvious} and \ref{fig:PFeqpointsObvious} respectively. In figure \ref{fig:koopmanRegionsObvious}, the regions of attraction for the two equilibrium points become very pronounced. The region in blue is the region that converges to the positive equilibrium point, and the region in red is the region that converges to the negative equilibrium point. The dividing line (in the light blue) is the stable manifold of the unstable equilibrium point. This stable manifold can be more clearly seen in another Koopman eigenfunction graph (shown in figure \ref{fig:koopmanManifoldObvious}). In the PF eigenfunction graph, the stable equilibrium points become very defined and obvious. This can be seen in figure \ref{fig:PFeqpointsObvious}.

The regions of convergence computed by the Koopman operator and the regions that I computed manually are very different. My regions are a very small subset of the overall region. This subset is the largest that I could compute using the quadratic Lyapunov function because it ends up with the unstable point on its boundary. If it grew anymore, the trajectories would no be guaranteed to stay inside. It is also forced to take an elliptical shape because of teh quadratic nature of the Lyapunov function. If another Lyapunov function was used (say one with more degrees of freedom), then the region would look different and possibly be larger. For this reason, the Koopman operator plot tells us more about the system than the hand calculated Lyapunov region of convergence.

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{Problem1_Koopman/Midterm1_P1_PartE_Trajectories.png}
	\caption{Trajectories used to compute the Koopman and PF operators}
	\label{fig:koopmanTrajec}
\end{figure}

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{Problem1_Koopman/Midterm1_P1_PartE_Eigenvalues.png}
	\caption{Eigenvalues for the Koopman operator.}
	\label{fig:koopmanEigen}
\end{figure}

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{Problem1_Koopman/Midterm1_P1_PartE_PFlargestEigenfunc.png}
	\caption{Eigenfunction for the largest PF operator eigenvalue.}
	\label{fig:PFeqpointsObvious}
\end{figure}

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{Problem1_Koopman/Midterm1_P1_PartE_KoopmanEigenfunc.png}
	\caption{Eigenfunction for a Koopman eigenvalue.}
	\label{fig:koopmanManifoldObvious}
\end{figure}

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{Problem1_Koopman/Midterm1_P1_PartE_Koopman_LargestEigenFunc.png}
	\caption{Eigenfunction for the largest Koopman operator eigenvalue.}
	\label{fig:koopmanRegionsObvious}
\end{figure}

\clearpage

\section*{Problem 2}

Given the logistic map: $ x_{n+1} = a x_n ( 1 - x_n)$, and $a \in [0, 4]$.

\subsection*{Part a - Compute the bifurcation plot}

For this part, the bifurcation plot of the logistic map function was numerically computed. To do this, the map was iterated using 4001 values for $a$ (so $a$ went from 0 to 4 in steps of 0.001). The unique values in the last 50 samples were then taken and used as the points for the convergence of the logistic map. The MATLAB code to do this is shown below. The bifurcation plot created can be seen in figure \ref{fig:bifurcationPlot}.

{\footnotesize
\begin{lstlisting}[language=matlab,frame=single]
%% The Logistic map function
f = @(x, a) a*x*(1-x);

x0 = 0.5;
T = 5000;

%% Iterate over many values of A to find the final values the map goes to
plottingVals = [];
for a = 0:0.001:4
    x = x0;
    for i=2:1:T
        x(i) = f(x(i-1), a);
    end

    eqVals = unique( x(end-50:end) );
    plottingVals = [plottingVals; a*ones(length(eqVals),1), eqVals'];
end

%% Make the Bifurcation plot
plot(plottingVals(:,1), plottingVals(:,2), 'b.', 'MarkerSize', 0.5);
xlabel('a');
ylabel('x');
title('Bifurcation plot for the Logistic map');
\end{lstlisting}}

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{Midterm1_P2_PartA.png}
	\caption{Bifurcation plot for the logistic map.}
	\label{fig:bifurcationPlot}
\end{figure}

\subsection*{Parts b and c}

These two parts were combined into one in my work since they both focus on the analysis of the logistic map using the Koopman operator theory. To analyze the system, the transfer function code provided was modified to work with a 1-dimensional system. It was then ran with 200 Radial Basis Functions with $\sigma = 0.25$.  The logistic map was run for 500 iterations starting at $x_0 = 0.5$.

The first results examined are the eigenvalues of the $K$ matrix, plotted in figure \ref{fig:logEigen}. Each of those figures shows the spectrum of $K$ for a certain $a$ value. From those plots, it can be seen that the spectrum of the logistic map is purely real when $a < 3$ which corresponds to the point on teh bifurcation plot where there is only one value for the function. Then as $a$ increases, the map begins to develop conjugate pairs of eigenvalues. This implies there is oscillation growing in the system, with more oscillation the larger $a$ gets. This corresponds to the portion of the bifurcation plot when the system gets periodic orbits and also chaotic behavior.

The next part studied were the largest eigenfunctions for the Koopman operator of the logistic map. A selection of the graphs of the eigenfunctions can be seen in figure \ref{fig:logKoopman}. In these functions, the distribution of the points in the trajectory of the solution can be seen. For the lower values of $a$ (below 3.0), there is a single limit of the map. These eigenfunctions show a peak near that limit, and taper off away from it. For $a {=} 3.2$ there are 2 distinct points forming orbit. These can be seen by slight peaks on the graph. For the larger values of $a$, the chaotic nature of the system can be seen. In those plots, the system seems to have an equal distribution of points across the entire range [0.1, 0.9] (which is very close to the domain for the map).

The final part examined were the largest eigenfunctions for the PF operator of the logistic map. A selection of their graphs can be seen in figure \ref{fig:logPF}. In these functions, the convergent points can begin to be seen. For instance, in the graphs for $a {<} 3$ there is a single peak near the value of the limit. In the graph for $a {=} 3.1, 3.5, 3.6$ multiple peaks and troughs can be seen near the points where the system oscillates. Finally, the graph becomes close to a flat line when the system becomes chaotic.

{\footnotesize
\begin{figure}[h!]
	\vspace{-1cm}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Spectrum/Midterm1_P2_Spectrum_a2_30.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Spectrum/Midterm1_P2_Spectrum_a2_40.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Spectrum/Midterm1_P2_Spectrum_a3_20.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Spectrum/Midterm1_P2_Spectrum_a3_50.png}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Spectrum/Midterm1_P2_Spectrum_a3_60.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Spectrum/Midterm1_P2_Spectrum_a3_70.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Spectrum/Midterm1_P2_Spectrum_a3_80.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Spectrum/Midterm1_P2_Spectrum_a3_90.png}
	\end{subfigure}
	
	\caption{Eigenvalues for the logistic map as $a$ varies.}
	\label{fig:logEigen}
\end{figure}}

{\footnotesize
\begin{figure}[h!]
	\vspace{-1cm}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Koopman/Midterm1_P2_Koopman_eig1_a2_00.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Koopman/Midterm1_P2_Koopman_eig1_a2_20.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Koopman/Midterm1_P2_Koopman_eig1_a3_00.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Koopman/Midterm1_P2_Koopman_eig1_a3_20.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Koopman/Midterm1_P2_Koopman_eig1_a3_60.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Koopman/Midterm1_P2_Koopman_eig1_a3_70.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Koopman/Midterm1_P2_Koopman_eig1_a3_80.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_Koopman/Midterm1_P2_Koopman_eig1_a3_90.png}
	\end{subfigure}
	
	\caption{Largest eigenfunction for the Koopman operator of the logistic map}
	\label{fig:logKoopman}
\end{figure}}

{\footnotesize
\begin{figure}[h!]
	\vspace{-1cm}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_PF/Midterm1_P2_PF_eig1_a2_30.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_PF/Midterm1_P2_PF_eig1_a2_40.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_PF/Midterm1_P2_PF_eig1_a3_00.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_PF/Midterm1_P2_PF_eig1_a3_10.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_PF/Midterm1_P2_PF_eig1_a3_50.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_PF/Midterm1_P2_PF_eig1_a3_60.png}
	\end{subfigure}
	
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_PF/Midterm1_P2_PF_eig1_a3_70.png}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{Problem2_PF/Midterm1_P2_PF_eig1_a3_90.png}
	\end{subfigure}
	
	\caption{Largest eigenfunction for the PF operator of the logistic map}
	\label{fig:logPF}
\end{figure}}


\end{document}
