% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: May 2, 2015
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
% These packages are all incorporated in the memoir class to one degree or another...

% Include custom commands
\usepackage{Unit-Declaration}
\usepackage{Sci-Formatting}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{}\rhead{}
\lfoot{EE 576 - Spring 2015}\cfoot{Final Project}\rfoot{\thepage}

\title{EE 576 Final Project: Altitude Controller for High Altitude Ballooning}
\date{May 5, 2015}
\author{Ian McInerney}


%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\graphicspath{{Images/}}

%%% END Article customizations

%%% The "real" document content comes below...

\begin{document}
\bibliographystyle{ieeetr}

\maketitle

\tableofcontents

\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

In this project a high altitude balloon system is created and then controlled using a digital control system. This balloon system is composed of two balloons: the primary balloon and a secondary balloon. The primary balloon is the major provider of lift for the payload, and the secondary balloon serves as a method of controlling the amount of lift generated. There is a compressor attached to the input of the secondary balloon, so that the overall air density inside can be changed, changing the net lift experienced by the system. This report will explore the development of a simplified non-linear model for the system, its subsequent linearization about the operating point of $21km$, and then the design of a full state feedback controller and observer to allow the balloon to change altitude.


\newpage

\section{System Physics Model}
\label{sec:physics}

This system is a more complex system than an ordinary high altitude balloon because it is composed of two distinct balloons. The main balloon is filled with a lifting gas (usually helium) on the ground, and is then sealed. This maintains a constant amount of lifting gas inside the balloon at all times. The second balloon is attached to a compressor system, and is filled with normal air throughout the flight. The second balloon is kept at a constant volume, so the density of the air inside it will change as the compressor operates.

\subsection{Simplifying Assumptions}

Due to the complexity of this model and the time frame allocated for this project, several simplifying assumptions for the physical system were made. Those assumptions are:
\begin{itemize}[noitemsep]
\item The compressor maintains a linear relationship between input voltage and output air velocity. (In reality though, the velocity will change based upon the pressure differential across the compressor).
\item The gravitational constant was assumed to be constant (9.81 $\sfrac{m}{s^2}$) during the entire flight. (In reality it will change due to the increased altitude).
\item The coefficient of drag was assumed to be constant throughout the flight. (In reality the coefficient changes based upon the atmospheric turbulence experienced).
\item The balloons were assumed to be impervious material, so no lifting gas can be lost through diffusion.
\item The affects of solar heating on the lifting gasses were ignored.
\end{itemize}

\subsection{Forces Acting on the Balloon}

For high altitude balloons, there are two main forces acting on each balloon during the flight: the buoyancy force and the drag force. The buoyancy force is the force lifting the balloon through the atmosphere, and the drag force is the force opposing the upwards movement (due to the air resistance the balloon encounters). These two forces are shown in figure \ref{fig:forces}.


\begin{figure}[h!]
\center
\resizebox{0.5\textwidth}{!} {
	\input{BalloonForces.tikz}
}
\caption{Forces acting on the balloon system.}
\label{fig:forces}
\end{figure}


\subsubsection{Balloon Lift}

The lift produced by the balloon is determined by using the principle of buoyancy, namely
$$F = (m_{displaced} - m_{lifting})g$$
This can then be adapted to the balloon lift in Earth's atmosphere by noting that $m_{displaced} = \rho_{a} V$, where $\rho_{a}$ is the atmospheric air density at altitude and $V$ is the volume of the balloon at that altitude.

Since the primary balloon is sealed at its neck, no helium can escape. That does two things: first it makes $m_{lifting}$ a constant value and second it makes the balloon volume change with altitude. The change in balloon volume is directly related to the density of ambient air through this ratio: $V_{new} = V_{0} \frac{\rho_{s}}{\rho_{a}}$, where $V_0$ is the volume on the ground and $\rho_s$ is the air density on the ground. The mass of the Helium used to lift the balloon can also be expressed using the balloon volume and surface air density: $m_{lifting} = \frac{4.0026V_0 \rho_s}{28.9644}$ \cite{wang2009}.

By combining the previous equations together, the overall lifting force of the primary balloon is the constant:
\begin{equation}
F_{L1} = (V_{1_0} \rho_{s} - \frac{4.0026V_{1_0} \rho_s}{28.9644})g = 0.8618 V_{1_0} \rho_{s} g
\label{eq:fl1}
\end{equation}

The secondary balloon has a different lifting force since it is being held at a constant volume, and the density of air inside will be changed using the compressor. This makes the secondary balloon's lift equation be:
\begin{equation}
F_{L2} = (V_{2} \rho_{a} - V_{2} \rho_{2})g
\label{eq:fl2}
\end{equation}
where $V_2$ is the constant volume of the second balloon and $\rho_{2}$ is the density of air inside that balloon.


\subsubsection{Balloon Drag}

As the balloon moves upward through the air, it encounters air resistance which creates a drag force. This drag force is nominally defined as:
$$ F_{D} = \frac{C_{dr} A_{r} \rho_{a} W_{r}^2}{2}$$
where $C_{dr}$ is the drag coefficient of the balloon, $A_{r}$ is the cross area of the balloon exposed to the air resistance, and $W_{r}$ is the rise rate of the balloon \cite{wang2009}.

From geometric principles, the cross sectional area of the balloon for the drag force is: \newline
$A_{r} = \pi \left( \frac{3V}{4\pi}\right)^{\sfrac{2}{3}}$ where $V$ is the balloon volume at altitude.

Normally, the coefficient of drag for the balloon will change based upon the Reynolds number of the balloon. The Reynolds number will change based upon the atmospheric turbulence and the ascent rate of the balloon, in a highly non-linear fashion (see \cite{gallice2011}). For this model though, the assumption supported by Wang in \cite{wang2009} will be used. He says that for altitudes above 5km, the soundings examined had a $C_{dr}$ of 0.4-0.5, so this model will use $C_{dr}=0.4$.

By placing the above equations together, the drag force for the primary balloon is:
\begin{equation}
F_{D1} = 0.2 \pi \left( \frac{3 V_{1_0} \rho_{s} }{4\pi}\right)^{\sfrac{2}{3}} \rho_{a}^{\sfrac{1}{3}} W_{r}^2
\label{eq:fd1}
\end{equation}

The drag force for the secondary balloon is:
\begin{equation}
F_{D2} = 0.2 \pi \left( \frac{3V_{2}}{4\pi}\right)^{\sfrac{2}{3}} \rho_{a} W_{r}^2
\label{eq:fd2}
\end{equation}


\subsection{Atmospheric Density at Altitude}

An important part of this model is the computation of the outside air density for the balloon at its current altitude. This problem is well researched in the aerospace literature and a closed-form equation is available.

The atmospheric density is dependent upon the atmospheric temperature, and that temperature is not constant throughout the atmosphere. There are two different temperature regions present in the atmosphere, isothermal regions and gradient regions. The distribution of these regions are shown in figure \ref{fig:atmosTempDistro}.

\begin{figure}[h!]
\center
\includegraphics[width=0.5\textwidth]{AtmosphereTemperatureDistribution.png}
\caption{Temperature distribution in the atmosphere. \cite{vtechLecture}}
\label{fig:atmosTempDistro}
\end{figure}

For each of these two regions, there is a distinct equation for computing the atmospheric density where the variables are given in table \ref{table:atmoDensityVar}\cite{vtechLecture}.

\begin{minipage}{0.5\textwidth}
Isothermal:
\begin{equation*}
\rho_a = \rho_1 e^{(h-h_1)\frac{-g_0}{RT}}
\end{equation*}
\end{minipage}
\begin{minipage}{0.5\textwidth}
Gradient:
\begin{equation*}
\rho_a = \rho_1 \left(\frac{T_1 + a(h-h_1)}{T_1}\right)^{\frac{-g_0}{aR}+1}
\end{equation*}
\end{minipage}

\begin{table}[h!]
\center
\caption{Variables used in atmospheric density calculation}
\label{table:atmoDensityVar}
\begin{tabular}{l|l}
$\rho_a$ & Calculated air density ($\sfrac{kg}{m^3}$)\\
$\rho_1$ & Air density at the last temperature corner ($\sfrac{kg}{m^3}$)\\
$h$ & Altitude at which to calculate air density (meters)\\
$h_1$ & Altitude at the last temperature corner (meters)\\
$g_0$ & Gravitational constant at altitude\\
$R$ & Gas constant of dry air ($287.058 \sfrac{J}{kg K}$)\\
$a$ & Slope of temperature change\\
$T$ & Temperature at the altitude (Kelvin)\\
$T_1$ & Temperature at the last temperature corner (Kelvin)
\end{tabular}
\end{table}

Since the balloon for this project is assumed to be performing maneuvers around 21km, the isothermal temperature profile can be used. This simplifies the computation into the single equation shown in equation \ref{eq:externalAirDensity}.

\begin{equation}
\rho_a = 0.367 e^{(h-11000)\frac{-9.81}{287.058 \cdot 216.66}}
\label{eq:externalAirDensity}
\end{equation}


\subsection{Density Of Secondary Balloon}

The density of the secondary balloon will change depending upon the command given to the compressor at its neck. For this model the compressor is idealized, meaning it has an output velocity proportional to the command and can work in both directions (moves air into and out of the balloon).

The compressor operation can be modeled using the principle of mass-flow rate \cite{NASAmfr}:
$$\dot{m} = \rho_{inlet} V_{outlet} A_{nozzle}$$
For this system, the output velocity is proportional to the commanded input and the inlet density is the density of the air at altitude. That makes the overall mass-flow equation become:
$$ \dot{m} = \rho_{a} A_{nozzle} K_{v} U_{motor} $$
To then determine the density of the balloon, use the relation $\rho = \frac{m}{V}$ which when differentiated gives $\dot{\rho} = \frac{\dot{m}}{V}$ for constant volume. That gives the final equation for the density inside the second balloon as:
\begin{equation}
\dot{\rho_{2}} = \frac{\rho_{a} A_{nozzle} K_{v}}{V_2}  U_{motor}
\label{eq:massFlow}
\end{equation}


\subsection{Non-Linear System Dynamics Model}

This system is defined by two differential equations. The first differential equation is constructed using Newton's Second Law on the balloon system:
$$m_t \dot{W_{r}} = \sum F = F_{L1} + F_{L2} - F_{D1} - F_{D2} - g m_{payload}$$
Then by substituting in the forces found in earlier sections (equations \ref{eq:fl1}, \ref{eq:fl2}, \ref{eq:fd1} and \ref{eq:fd2}), the complete differential equation is (note, $\rho_{a}$ has been left in for clarity):
\begin{equation}
m_t \dot{W_{r}} = 0.8618 V_{1_0} \rho_{s} g + V_{2} \rho_{a} g - V_{2} \rho_{2} g - 0.2 \pi \left( \frac{3 V_{1_0} \rho_{s} }{4\pi}\right)^{\sfrac{2}{3}} \rho_{a}^{\sfrac{1}{3}} W_{r}^2 - 0.2 \pi \left( \frac{3V_{2}}{4\pi}\right)^{\sfrac{2}{3}} \rho_{a} W_{r}^2 - g m_{payload}
\end{equation}

\noindent The second differential equation is the mass-flow equation given in equation \ref{eq:massFlow}.

\subsection{Linearized System Dynamics Model}

This balloon system is non-linear by nature, so before any controller design can be done it must be linearized. The states used for linearization can be seen in table \ref{tab:stateMeanings}. This system will be linearized about an equilibrium point such that its ascent rate will be zero. This directly eliminates the drag force terms, since they will contain $W_r$ terms that after the Jacobian is taken. Also, the total mass of this system system is $m_t = m_{payload} + m_{B1} + m_{B2}$, where $m_{payload}$ and $m_{B1}$ are constant but $m_{B2}$ changes with the density of balloon 2 according to $m_{B2} = \rho_{2} V_{2}$.

\begin{table}[h!]
\center
\begin{tabular}{l | l}
$x_{1} = h$ & The altitude of the balloon\\
$x_{2} = W_r$ & The ascent rate of the balloon\\
$x_{3} = \rho_2$ & The density of air inside balloon 2\\
$y_{1} = h$ & The altitude of the balloon\\
$u_{1} = U_{motor}$ & The motor input voltage
\end{tabular}
\caption{States, inputs and outputs for the system model.}
\label{tab:stateMeanings}
\end{table}

The resulting linearized A and B matrices in symbolic form are:

\begin{equation*}
A_c = 
\left[
\begin{array}{ccc}
0 & 1 & 0 \\
\frac{-57.88 \times 10^{-6} V_2 g}{m_t^{*}} e^{(h^{*}-11000)\frac{-9.81}{287.058 \cdot 216.66}}  & 0 &
\frac{V_{2}( 0.8618V_{1_0} \rho_{s} g + V_2 \rho_{a}^{*} g - V_2 \rho_{2}^{*} g - g m_{payload}) - V_{2} m_{t}^{*} g}{(m_{t}^{*})^2} \\
0 & 0 & 0
\end{array}
\right]
\end{equation*}

\begin{equation*}
B_c = \left[
\begin{array}{c}
0 \\
0 \\
\frac{\rho_a^{*} A_{nozzle} K_v}{V_2}
\end{array}
\right]
\end{equation*}

At the equilibrium point, the upward velocity will be zero so there are no drag forces, and the compressor input will be zero. That means the density inside the second balloon will be determined by the density in the primary balloon only.

After determining these two symbolic matrices, the actual system to be designed for can be found by plugging in the parameters shown in table \ref{tab:balloonParameters}. The actual state space matrices for this system are shown in equations \ref{ss:contA}, \ref{ss:contB}, \ref{ss:contC}, and \ref{ss:contD}.

\begin{table}[h!]
\center
\begin{tabular}{l | l | l}
$h^{*} = 21km$							& $V_1 = 2 m^3$ 					& $g = 9.81 \sfrac{m}{sec^2}$\\
$W_r^{*} = 0 \sfrac{m}{s}$				& $V_2 = 0.25 m^3$					& $A_{nozzle} = 1.96 \times 10^{-5} m^2$\\
$\rho_2^{*} = 2.5214 \sfrac{kg}{m^3}$	& $\rho_s = 1.225 \sfrac{kg}{m^3}$	& $K_v = 100$\\
$\rho_a^{*} = 0.0758 \sfrac{kg}{m^3}$ & $m_{payload} = 1.5 kg$
\end{tabular}
\caption{Parameters for the balloon system under study}
\label{tab:balloonParameters}
\end{table}

\begin{equation}
A_c = 
\left[
\begin{array}{ccc}
0 & 1 & 0 \\
-1.187 \times 10^{-5}  & 0 & -0.993 \\
0 & 0 & 0
\end{array}
\right]
\label{ss:contA}
\end{equation}

\begin{equation}
B_c = 
\left[
\begin{array}{c}
0 \\
0 \\
5.9530 \times 10^{-4}
\end{array}
\right]
\label{ss:contB}
\end{equation}

\begin{equation}
C = 
\left[
\begin{array}{ccc}
1 & 0 & 0
\end{array}
\right]
\label{ss:contC}
\end{equation}

\begin{equation}
D = 
\left[
\begin{array}{cc}
0
\end{array}
\right]
\label{ss:contD}
\end{equation}


\subsection{Discretization of System Model}
\label{sec:discrete}

Since the controller for this system would be implemented on a microcontroller system, the controller designed should be a digital controller. To perform the design on the digital controller, the continuous time plant model must be digitized. For this digitization, it was assumed that the controller would sample at $T=0.1$ seconds. This time period is equivalent to a GPS position signal received at 10Hz, which for a real system would provide the controller with the altitude measurement of the physical system.

The discretized state space matrices can be seen below, and will be referenced by $A_d$, $B_d$, $C_d$ and $D_d$ throughout the rest of this report.

\begin{align*}
A_d &= 
e^{A_c T}
= 
\left[
\begin{array}{ccc}
1 & 0.1 & -0.0050 \\
-1.1876 \times 10^{-6}  & 1 & -0.0993 \\
0 & 0 & 1
\end{array}
\right]
\\
B_d &= 
\int_{0}^{T} e^{A_c \tau} B_c d\tau
=
\left[
\begin{array}{c}
-9.855 \times 10^{-8} \\
-2.956 \times 10^{-6} \\
5.9530 \times 10^{-5}
\end{array}
\right]
\\
C_d &= C_c
=  
\left[
\begin{array}{ccc}
1 & 0 & 0
\end{array}
\right]
\\
D_d &= D_c
=
\left[
\begin{array}{c}
0
\end{array}
\right]
\end{align*}

\clearpage
\newpage


\section{System Stability}
\label{sec:stability}

The first test done on the system to determine its stability was to examine the open-loop system poles. For this discrete-time system there are three poles located at $1.0 \pm 0.0003 j$ and $1.00$. Each of these poles has a magnitude of approximately 1, making this system a marginally stable system. This stability can also be seen in a step response of the system, as shown in figure \ref{fig:openLoopResults}. In this step response, the step is applied halfway at time $t=5000$ seconds. Before that point the system maintains the altitude of 21km, however once the step enters the system it becomes unstable and crashes into the ground (note that the graph does show it going to negative altitude, however this is not possible in reality).

\begin{sidewaysfigure}[p!]
\center
\includegraphics[width=\textwidth]{Discrete_OpenLoopResponse.png}
\caption{Balloon system response to step inputs with no controller.}
\label{fig:openLoopResults}
\end{sidewaysfigure}

The next stability analysis performed on this system was a root locus analysis for a closed-loop system with simple proportional gain $K$. This root locus plot is shown in figure \ref{fig:rootLocus}. The three poles start bunched around the intersection of the unit circle and the real axis, and there is a zero located inside the unit circle and another outside the unit circle on the left-hand side of the imaginary axis. When a gain is applied, the three poles diverge with one moving outside the unit circle and the other two moving around inside. The two poles moving inside the unit circle eventually move towards the two zeros on the left-hand plane. Those two poles make little difference to this system though, since the first pole immediately leaves the unit circle. This pole causes instability for any $K > 0$. Because of this, the system cannot be controlled by simply closing the loop.

\begin{figure}[h!]
\center
\includegraphics[width=\textwidth]{RootLocus.png}
\caption{Root locus showing the system is unstable for $K > 0$.}
\label{fig:rootLocus}
\end{figure}

The final system property examined was the Bode plot for the open loop system (after a bilinear transform into the \~s domain). This plot can be seen in figure \ref{fig:bodePlot}. Based on this plot, the system has a phase margin of $\approx 90^{\circ}$ already, but has a bandwidth of only 0.08 $\sfrac{rad}{sec}$. This phase is maintained up through approximately 1 $\sfrac{rad}{sec}$, where the poles begin to pull the system phase down. In order to increase the system bandwidth though, the gain-crossover frequency must be modified. The best way of doing that while preserving the system phase margin is to use a proportional controller, however the root-locus plot has shown that this controller is ineffective at stabilizing the system.

\begin{figure}[h!]
\center
\includegraphics[width=\textwidth]{MarginPlot.png}
\caption{Bode plot for the open loop system}
\label{fig:bodePlot}
\end{figure}


\clearpage
\newpage

\section{Controller Design}
\label{sec:controller}

The controller designed for this system was a full state feedback controller using pole placement techniques. There were three main objectives for the controller to satisfy:
\begin{enumerate}[noitemsep]
\item Balloon should have an average velocity of $5\sfrac{m}{s}$ when climbing 1000m.
\item No more than 10\% overshoot
\item System should track reference input with unity gain
\end{enumerate}

Each of these objectives was then translated into meaningful controller parameters for the actual design process.

\subsection{Full State Feedback}
\label{sec:stateFeed}

The first step in designing the state feedback controller was to check the system controllability by constructing the system controllability matrix, shown in (\ref{matrix:ctrb}). This matrix is simply composed by stacking the A and B matrices according to $ctrb(A, B) = \left[\begin{array}{ccccc}
B & AB & A^2 B & ... & A^{n-1} B
\end{array} \right]$. The controllability of all the states can then be found by examining the matrix rank, and if it has column-rank equal to $n$ (where $n$ is the number of columns in the $A$ matrix), then the system is controllable and all states are affected by the inputs. For this system and the controllability matrix in (\ref{matrix:ctrb}), the rank is 3 (with $A$ being a 3x3 matrix), so the system is controllable.

\begin{equation}
ctrb(A_d, B_d) =
\left[
\begin{array}{ccc}
-9.85 \times 10^{-8} & -6.89 \times 10^{-7} & -1.87 \times 10^{-6}\\
-2.95 \times 10^{-6} & -8.87 \times 10^{-6} & -1.47 \times 10^{-5}\\
5.95 \times 10^{-5} & 5.95 \times 10^{-5} & 5.95 \times 10^{-5}
\end{array}
\right]
\label{matrix:ctrb}
\end{equation}

The next step in the controller design was to use the given controller characteristics and create the controller poles. Since this is a third order system, three poles needed to be created. One pole was set at 0.5 just by convenience, and the other two poles were made a conjugate pair. The rise velocity characteristic, moving no faster that 5$\sfrac{m}{s}$ over a 1000m space translates into a time constant for the pole pair. Knowing that the time constant is the time the system reaches 62.3\% of full scale, the time constant is therefore equivalent to the following:
$$
\tau = 0.632*\frac{1000m}{5 \sfrac{m}{s}} = 126.4 s
$$
The desired overshoot can then be directly mapped into the damping coefficient $\zeta$ using the following formula:
$$
\zeta = \sqrt{ \frac{ln^2 \left( \frac{M_p}{100} \right)} {\pi^2 + ln^2 \left( \frac{M_p}{100} \right)} }
\xrightarrow{M_p = 10} \zeta = 0.5912 \\
$$
By combining those two values ($\tau$ and $\zeta$) using the following two equations, the magnitude and angle of the system poles can be found.
$$
\tau = \frac{-T}{ln(r)} \rightarrow r = 0.9992
$$
$$
\zeta = \frac{-ln(r)}{\sqrt{ln^2(r) + \theta^2}} \rightarrow \theta = \pm 0.0010
$$
To then use those poles in the controller design, they were converted into rectangular coordinates of $z = 0.9992 \pm 0.0011 j$.

The three poles ($z = 0.5, 0.9992 \pm 0.0011 j$) were then used as arguments into the MATLAB place command. The place command accepts the $A_d$ and $B_d$ matrices and desired pole locations as inputs, and will output the full state feedback matrix $K$ that allows the system to achieve those closed-loop system poles. For this system, the controller gain matrix that achieves those closed-loop system poles was $K =
\left[
\begin{array}{ccc}
-1.412 & -133.96 & 8419.1 
\end{array}
\right]$.

This $K$ matrix was then entered into the Simulink model for the system and controller as the gain in the feedback loop. The system architecture used for simulation can be seen in figure \ref{fig:simFullState}. This simulation exposes the system to four step responses to mimic the commanded changes in altitude during a flight:
\begin{enumerate}[noitemsep]
\item $r = 21.0km$ for $ 0 < t < 2500 $
\item $r = 21.1km$ for $ 2500 < t < 5000 $
\item $r = 20.9km$ for $ 5000 < t < 7500 $
\item $r = 21.0km$ for $ 7500 < t < 10000 $
\end{enumerate}

Initially, the input gain was set to be unity ($N=1$) to just measure the response of the system and the controller. With the unity input gain, the system behaves as shown in figure \ref{fig:fullStateNoInputGain}. In the response with unity input gain, the system attempts to perform the maneuvers centered at $\approx -14km$ instead of the desired $21km$. This response shows that the third criterion of the controller (unity gain steady state reference tracking) is not met by strictly the $K$ matrix alone.

\begin{figure}[h!]
\center
\includegraphics[width=\textwidth]{FullState_NoObserver.png}
\caption{Simulink diagram for the system with full state feedback.}
\label{fig:simFullState}
\end{figure}

To implement the requirement for unity gain steady state reference tracking, the input gain $N$ had to be appropriately designed. To do this, the closed form equation
$$
\left[
\begin{array}{cc}
A-I & B\\
C & D
\end{array}
\right]^{-1}
\left[
\begin{array}{c}
0\\
I
\end{array}
\right]
=
\left[
\begin{array}{c}
N_1\\
N_2
\end{array}
\right]
$$
was used. This equation will create two separate gains, $N_1$ and $N_2$ which are related through $N = N_2 + KN_1$. For this system, the resulting gains were:
$$
\left.
\begin{array}{l}
N_1 = \left[
\begin{array}{c}
1\\
-5.03 \times 10^{-16}\\
-1.19 \times 10^{-5}
\end{array}\right]\\
N_2 = 0
\end{array}
\right\rbrace
\rightarrow
N = -1.5132 
$$

Note that the middle term of the $N_1$ matrix is very small (on the order of $10^{-16}$). This is probably numerical error during computation in MATLAB, so it can probably be set to 0 with no major affects on the system. These calculations showed that the closed loop system needs an input gain of $N = -1.5132$ to properly track the reference input with no steady state error. The resulting system with both the controller gain $K$ and input gain $N$ was simulated against the previously defined step responses, and produced the output in figure \ref{fig:fullStateResults}.

\begin{sidewaysfigure}[p!]
\center
\includegraphics[width=\textwidth]{Discrete_FullState_NoObserver_NoInputGain.png}
\caption{Balloon system response to step inputs. The system is being controlled by the full state feedback controller from section \ref{sec:stateFeed}, but contains unity input gain ($N = 1$).}
\label{fig:fullStateNoInputGain}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
\center
\includegraphics[width=\textwidth]{Discrete_FullState_NoObserver.png}
\caption{Balloon system response to step inputs. The system is being controlled by the full state feedback controller from section \ref{sec:stateFeed} and contains the calculated input gain ($N=-1.5132$)}
\label{fig:fullStateResults}
\end{sidewaysfigure}

\clearpage
\subsection{Full State Feedback with State Observer}
\label{sec:observer}

While the controller system derived in section \ref{sec:stateFeed} does meet the requirements for the system response, it is impractical to implement in the real world. Not all states in this system are physically measurable under the current assumptions. The current system only contains a GPS sensor to measure the altitude of the balloon system ($h$). While the ascent velocity ($W_r$) could be computed using the difference in altitudes, that would introduce more noise into the states. Additionally, there is no facility to measure the density of the air inside balloon 2 in this system. While a sensor could be added to measure the pressure inside the balloon, and then compute the density based upon that value, that system adds extra cost and weight to the overall system.

A more economical approach for this system would be to add a state observer to it, so then the discrete controller can sample each state using the previously derived model. The state observer would take the form shown in equations (\ref{eq:observer}). The variable $y$ would be the sample taken from the GPS device every sampling period, and then the state feedback controller can directly use the state variables $\hat{x}$ to perform its control computations. 
\begin{equation}
\begin{aligned}
\dot{\hat{x}} &= A \hat{x} + B u + L(y - \hat{y})\\
\hat{y} &= c \hat{x} + D u
\end{aligned}
\label{eq:observer}
\end{equation}

To begin the observer design, the system must be checked for observability. To do this, the observability matrix $\left[\begin{array}{ccccc}
C & C A & C A^2 & ... & C A^{n-1}
\end{array}\right]^T$
was constructed, and is shown in (\ref{matrix:obsv}). For the system to be observable, the observability matrix must be of rank $n$ (where $A$ is an $n$ x $n$ matrix). In this case, the observability matrix is rank 3, and $A$ is a 3x3 matrix, so the system is fully observable.
\begin{equation}
obsv(A_d, C_d) =
\left[
\begin{array}{ccc}
1.0 & 0 & 0\\
1.0 & 0.1 & -0.005\\
1.0 & 0.2 & -0.0199
\end{array}
\right]
\label{matrix:obsv}
\end{equation}

The observer design is very similar to the pole placement design done for the controller's $K$ matrix in section \ref{sec:stateFeed}. In this case though the poles were chose to have a time constant slightly slower than the sampling time ($\tau = 0.25$ seconds), and a $\zeta$ value equivalent to the controller's. This created the observer poles $z = 0.5, 0.7756 \pm 0.63 j$. The observer was then designed using the MATLAB place command, with $A^T$, $C^T$ and the desired observer poles as the input. This produced the observer gain matrix $L^T$ as its output, so $L$ was readily calculable. The final matrix calculated for the observer gain is:
\begin{equation*}
L =
\left[
\begin{array}{c}
1.2053\\
8.8044\\
-35.4231
\end{array}
\right]
\end{equation*}

The entire system, controller and observer was created in Simulink (shown in figure \ref{fig:simObserver}). It was exposed to the same step inputs as the controller in section \ref{sec:stateFeed}, and produced the output shown in figure \ref{fig:observerResults}. This system produces output that is equivalent to that of the full state feedback system in section \ref{sec:stateFeed}, however it can rely only on the GPS measurements for control.

\begin{figure}[h!]
\center
\includegraphics[width=\textwidth]{FullState_Observer.png}
\caption{Simulink diagram for the system with full state feedback and a state observer.}
\label{fig:simObserver}
\end{figure}

\begin{sidewaysfigure}[p!]
\center
\includegraphics[width=\textwidth]{Discrete_FullState_Observer.png}
\caption{Balloon system response to step inputs. The system is being controlled by the full state feedback controller from section \ref{sec:stateFeed} with the observer from section \ref{sec:observer}.}
\label{fig:observerResults}
\end{sidewaysfigure}
\clearpage


\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}

Overall, this balloon altitude system contains highly non-linear system dynamics so all controller design must be done around a known set of state variables. In this case, the system was examined around the altitude $21km$ and was found to be marginally stable, so any disturbance on the input or in the density state would destabilize the system. The controller was designed to meet three control objectives including rise-rate limitations, overshoot limitations, and reference input tracking. A full state feedback controller, with input gain and an observer was used to provide feedback control to this system, and stabilize it around the operating point. While this controller operates effectively around the operating point, any large variation in altitude from it will cause the system dynamics to exit the linearized region and the controller will not be tuned properly. Therefore this system would be an ideal candidate for a non-linear control system for large altitude changes.

\clearpage

\bibliography{references}

\clearpage

\appendix
\addcontentsline{toc}{section}{Appendices}

\section{MATLAB Code to Create Controller}
{\footnotesize
\lstinputlisting[language=matlab, frame=single, breaklines=tru, tabsize=4]{../MATLAB/StateSpaceCreation_NoGravityInput.m}}

\end{document}
