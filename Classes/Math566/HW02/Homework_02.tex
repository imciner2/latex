% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: September 13, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{listings}
\usepackage{enumitem}
% These packages are all incorporated in the memoir class to one degree or another...

% Include custom commands
\usepackage{Unit-Declaration}
\usepackage{Sci-Formatting}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{}\rhead{}
\lfoot{Math 566 - Fall 2016}\cfoot{Homework 2}\rfoot{\thepage}

\title{Math 566 - Homework 2}
\author{Ian McInerney}


%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%\graphicspath{{Images/}}

%%% END Article customizations

%%% The "real" document content comes below...

\begin{document}
	
\maketitle


\section*{Problem 1}

\subsection*{a}

Two bounded convex sets $C$ and $D$ where $C \cap D = \emptyset$ but there are no hyperplanes strictly separating them.
\begin{align*}
	C &= \{x: x_1^2 + x_2^2 + \cdots + x_d^2 < 1\}\\
	D &= \{x: x_1^2 + (x_2-2)^2 + \cdots + x_d^2 \leq 1\}
\end{align*}

Set $C$ is a bounded, non-closed set that doesn't include the points on the unit ball surrounding it (so it excludes $(0,1,0,\cdots,0)$ but is bounded by it). Set $D$ is bounded, and closed so that it does include points on the unit ball surrounding it (so it includes $(0,1,0,\cdots,0)$). This means that the sets do not have an intersection, but there is no hyperplane that can strictly separate them (because any separating hyperplane must pass through $(0,1,0,\cdots,0)$).

\subsection*{b}

Two closed convex sets $C$ and $D$ that cannot be strictly separated are:
\begin{align*}
C &= \{x: x_2 \leq 0\}\\
D &= \{x: x_1 x_2 \cdots x_d \geq 1\}
\end{align*}


\newpage

\section*{Problem 2}

On homework 1 I solved the primal version of the diet problem, and found a diet that cost \$4.30. The original problem is formulated as:
\begin{equation}
\begin{aligned}
& \text{minimize} & c^T x \\
& \text{subject to} & A^T x \geq b\\
& & x \geq 0\\
\end{aligned}
\label{eq:originalProblem}
\end{equation}
The dual of this problem is formulated as
\begin{equation}
\begin{aligned}
& \text{maximize} & b^T y \\
& \text{subject to} & A y \leq c\\
& & y \geq 0\\
\end{aligned}
\label{eq:originalProblem}
\end{equation}

The dual problem was solved using the CVX solver in MATLAB (Since the dual problem of an LP is a convex problem, CVX is an appropriate solver). The maximum cost found is \$4.30 with the nutrient amounts: $$y = [6.491\times 10^{-4}, 6.251\times 10^{-11}, 0.0096, 3.1191\times 10^{-4}, 0.0044, 1.1045\times 10^{-10}]$$. Those nutrient amounts stand for the maximum price per nutrient that is attainable without exceeding the price per serving of each food item (so therefore the maximum amount will give the same nutrients as the food item without exceeding the cost of the food item).

{\footnotesize\begin{lstlisting}[language=matlab,frame=single,tabsize=2]
%% Setup the diet problem

A = [180, 45, 4, 100, 0, 10;        % Canned Tuna
     0, 110, 1, 450, 0, 1;          % Banana
     80, 160, 0, 0, 45, 5;          % Lemon Yogurt
     55, 150, 6, 95, 0, 2;          % Crackers
     390, 260, 20, 0, 10, 8;        % Plain Bagel
     150, 100, 0, 0, 20, 7;         % Cheese Block
     0, 100, 2, 0, 2, 0;            % Red Apple
     110, 70, 4, 0, 4, 2;           % Bread
     50, 120, 0, 0, 0, 26;          % Chicken Breast Fillets
     30, 200, 2, 0, 4, 2;           % M&Ms
     1280, 450, 0, 0, 0, 23;        % Arby's Beef and Cheddar
     340, 150, 2, 0, 0, 2;          % Homestyle Popcorn
     120, 120, 0, 0, 30, 8;         % 2% Milk
     15, 150, 0, 0, 2, 1;           % Welch's Grape Juice
     65, 35, 0, 0, 0, 1];           % Baby Carrots

c = [0.50;        % Canned Tuna
     0.15;        % Banana
     0.55;        % Lemon Yogurt
     0.32;        % Crackers
     0.49;        % Plain Bagel
     0.29;        % Cheese Block
     1.39;        % Red Apple
     0.13;        % Bread
     1.85;        % Chicken Breast Fillets
     0.32;        % M&Ms
     3.99;        % Arby's Beef and Cheddar
     0.24;        % Homestyle Popcorn
     0.21;        % 2% Milk
     0.49;        % Welch's Grape Juice
     0.23];       % Baby Carrots

b = [3220;      % Sodium
     2400;      % Calories
     120;       % Iron
     1700;      % Potassium
     120;       % Calcium
     56];       % Protein



%% Create the CVX problem for the primal
cvx_begin
	variable x(15)

	minimize transpose(c)*x
	subject to
	A'*x >= b;
	x >= 0;
cvx_end


%% Create the CVX problem for the dual
cvx_begin
	variable y(6)

	maximize transpose(b)*y
	subject to
	A*y <= c;
	y >= 0;
cvx_end
\end{lstlisting}}

\newpage

\section*{Problem 3}

Given that a paper manufacturer has rolls of width 3 meters, they would like to create rolls of 135cm, 108cm, 93cm and 42cm without wasting much of the 3 meter rolls. There are 10 possible ways to cut up the 3 meter roll into these 4 subsequent rolls:

\begin{table}[h!]
	\begin{tabular}{|c|l|c|}
		\hline Roll \# & Dividends & Waste\\
		\hline $R_0$ & 2 - 1.35 & 0.30\\
		\hline $R_1$ & 1 - 1.35, 1 - 1.08, 1 - 0.42 & 0.15\\
		\hline $R_2$ & 1 - 1.35, 1 - 0.93, 1 - 0.42 & 0.30\\
		\hline $R_3$ & 2 - 1.08, 2 - 0.42 & 0.00\\
		\hline $R_4$ & 1 - 1.08, 2 - 0.93 & 0.06\\
		\hline $R_5$ & 1 - 1.08, 1 - 0.93, 2 - 0.42 & 0.15\\
		\hline $R_6$ & 3 - 0.93 & 0.21\\
		\hline $R_7$ & 2 - 0.93, 2 - 0.42 & 0.39\\
		\hline $R_8$ & 1 - 0.93, 4 - 0.42 & 0.39\\
		\hline $R_9$ & 7 - 0.42 & 0.06\\
		\hline
	\end{tabular}
\end{table}

There are then 10 variables for this optimization. Each variable represents how many 3m rolls to cut in that particular way. The overall objective is to get at least 97 135cm rolls, 610 108cm rolls, 395 93cm rolls, and 211 42cm rolls while minimizing the number of 3m rolls used. 

\begin{equation}
	\begin{aligned}
	& \text{minimize} & 1^T R \\
	& \text{subject to} & A^T R \geq b\\
	& & R_i \geq 0\\
	& & R_i \in \mathbb{Z}
	\end{aligned}
\end{equation}

where the following matrices are defined

\begin{minipage}{0.5\textwidth}
\begin{equation*}
A = \left[\begin{matrix}
2 & 0 & 0 & 0\\
1 & 1 & 0 & 1\\
1 & 0 & 1 & 1\\
0 & 2 & 0 & 2\\
0 & 1 & 2 & 0\\
0 & 1 & 1 & 2\\
0 & 0 & 3 & 0\\
0 & 0 & 2 & 2\\
0 & 0 & 1 & 4\\
0 & 0 & 0 & 7
\end{matrix}\right]
\end{equation*}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\begin{equation*}
	b = \left[\begin{matrix}
	97\\
	610\\
	395\\
	211
	\end{matrix}\right]
\end{equation*}
\end{minipage}

This problem was solved using APMonitor to do an integer program. The final result is that 453 3m rolls need to be used, and out of the 10 possible combinations to cut, only 4 are used: $R_0 = 48$, $R_2 = 1$, $R_3 = 207$, $R_4 = 197$, the rest of the variables are all 0.
{\footnotesize
\begin{lstlisting}[language=matlab,frame=single,tabsize=2,breaklines=true]
Model hs17
	Variables
		int_r[0]
		int_r[1]
		int_r[2]
		int_r[3]
		int_r[4]
		int_r[5]
		int_r[6]
		int_r[7]
		int_r[8]
		int_r[9]
	End Variables

	Equations
		2*int_r[0] + 1*int_r[1] + 1*int_r[2] >= 97
		1*int_r[1] + 2*int_r[3] + 1*int_r[4] + 1*int_r[5] >= 610
		1*int_r[2] + 2*int_r[4] + 3*int_r[6] + 2*int_r[7] + 1*int_r[8] >= 395
		1*int_r[1] + 1*int_r[2] + 2*int_r[3] + 2*int_r[5] + 2*int_r[7] + 4*int_r[8] + 7*int_r[9] >= 211
		int_r[0] >= 0
		int_r[1] >= 0
		int_r[2] >= 0
		int_r[3] >= 0
		int_r[4] >= 0
		int_r[5] >= 0
		int_r[6] >= 0
		int_r[7] >= 0
		int_r[8] >= 0
		int_r[9] >= 0


		minimize int_r[0] + int_r[1] + int_r[2] + int_r[3] + int_r[4] + int_r[5] + int_r[6] + int_r[7] + int_r[8] + int_r[9]
	End Equations
End Model
\end{lstlisting}}


{\footnotesize\begin{lstlisting}[frame=single]
Objective Value = 453.0

hs17.int_r[0] = 48.0
hs17.int_r[1] = 0.0
hs17.int_r[2] = 1.0
hs17.int_r[3] = 207.0
hs17.int_r[4] = 197.0
hs17.int_r[5] = 0.0
hs17.int_r[6] = 0.0
hs17.int_r[7] = 0.0
hs17.int_r[8] = 0.0
hs17.int_r[9] = 0.0
hs17.slk_1 = 0.0
hs17.slk_10 = 0.0
hs17.slk_11 = 0.0
hs17.slk_12 = 0.0
hs17.slk_13 = 0.0
hs17.slk_14 = 0.0
hs17.slk_2 = 1.0
hs17.slk_3 = 0.0
hs17.slk_4 = 204.0
hs17.slk_5 = 48.0
hs17.slk_6 = 0.0
hs17.slk_7 = 1.0
hs17.slk_8 = 207.0
hs17.slk_9 = 197.0
\end{lstlisting}}

\end{document}
