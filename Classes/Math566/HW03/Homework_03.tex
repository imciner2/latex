% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: September 20, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{listings}
\usepackage{enumitem}
% These packages are all incorporated in the memoir class to one degree or another...

% Include custom commands
\usepackage{Unit-Declaration}
\usepackage{Sci-Formatting}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{}\rhead{}
\lfoot{Math 566 - Fall 2016}\cfoot{Homework 3}\rfoot{\thepage}

\title{Math 566 - Homework 3}
\author{Ian McInerney}


%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%\graphicspath{{Images/}}

%%% END Article customizations

%%% The "real" document content comes below...

\begin{document}

\section*{Problem 2}

The given problem is to minimize the absolute residuals while fitting a linear equation to the given dataset. The normal formulation ca be seen in equation \ref{eq:originalProblem}.
\begin{equation}
\begin{aligned}
& \text{minimize} & \norm{ax+b-y}_1 \\
\end{aligned}
\label{eq:originalProblem}
\end{equation}

This problem can also be reformatted into a linear program by introducing new variables $t$. Those variables will represent the deviation of the estimated datapoint from the actual datapoint (as in $ax + b - y = t$). The goal of the optimization is then to make $t=0$, because that means that the estimate is precisely the measured value. This makes the cost function the sum of all the $t$ variables. Then introduce constraints for the deviation. To take into account the fact that the difference could be positive or negative, an inequality should be used that forces the deviation to be between $-t$ and $t$. This problem can be seen in equation \ref{eq:linearproblem}.
\begin{equation}
\begin{aligned}
& \text{minimize} & \sum_{i=1}^{n} t_i \\
& \text{subject to} & -t_i \leq ax_i + b - y_i \leq t\\
\end{aligned}
\label{eq:linearproblem}
\end{equation}

This problem was then entered into APMonitor for solving, the code below is the APMonitor input:
{\footnotesize\begin{lstlisting}[frame=single,tabsize=2]
Model hs46
  Variables
    t[1] = 1
    t[2] = 1
    t[3] = 1
    t[4] = 1
    t[5] = 1
    a = 1
    b = 1
  End Variables

  Equations
    a*53 + b - 90 <= t[1]
    a*53 + b - 90 >= -t[1]

    a*55 + b - 94 <= t[2]
    a*55 + b - 94 >= -t[2]

    a*59 + b - 95 <= t[3]
    a*59 + b - 95 >= -t[3]

    a*61.5 + b - 100 <= t[4]
    a*61.5 + b - 100 >= -t[4]

    a*61.5 + b - 105 <= t[5]
    a*61.5 + b - 105 >= -t[5]

    minimize t[1] + t[2] + t[3] + t[4] + t[5]
  End Equations
End Model
\end{lstlisting}}

The APMonitor output is the following:
{\footnotesize\begin{lstlisting}[frame=single]
Objective Value = 8.70588
	
hs46.a = 1.17647099495
hs46.b = 27.6470603943
hs46.slk_1 = 0.0
hs46.slk_10 = 0.0
hs46.slk_2 = 0.0
hs46.slk_3 = 3.29411792755
hs46.slk_4 = 0.0
hs46.slk_5 = 0.0
hs46.slk_6 = 4.11764717102
hs46.slk_7 = 0.0
hs46.slk_8 = 0.0
hs46.slk_9 = 10.0
hs46.t[1] = -1.11022302463e-16
hs46.t[2] = 1.64705896378
hs46.t[3] = 2.05882406235
hs46.t[4] = -2.22044604925e-15
hs46.t[5] = 5.0
\end{lstlisting}}
This gives the best-fit line as being $1.1764x + 27.647$.

\newpage

\section*{Problem 3}

The proposed optimization problem is supposed to schedule 13 classes into either 3 or 4 time slots, based upon the information provided by 54 students about which classes they want to take. The variables that will be used for this problem are binary variables, that indicate whether a class is being taught in the specified time slot (1 means it is being taught, 0 means it is not). The decision variables are stacked into a single vector. Using the notation $x_{ij}$ to mean the $j$th class in the $i$th timeslot, the decision vector is arraigned like so (note C:= number of classes):
\begin{equation*}
	x = \left[\begin{matrix} x_{1,1} & x_{1,2} & \cdots & x_{1,C} | x_{2,1} & x_{2,2} & \cdots & x_{2,C} | \cdots | x_{i,1} & x_{i,2} & \cdots & x_{i,C}\end{matrix}\right]^T
\end{equation*}

The objective function for this optimization is based upon trying to minimize the number of class conflicts (the number of classes that a student wants that are offered at the same time). The starting point for this function is shown below:
\begin{equation*}
	\left[\begin{matrix}
	\text{Student 1 Class Selection}\\
	\text{Student 2 Class Selection}\\
	\text{Student 3 Class Selection}\\
	\vdots\\
	\text{Student 54 Class Selection}
	\end{matrix}\right]
	\left[\begin{matrix}
	x_1\\
	x_2\\
	\vdots\\
	x_{13}
	\end{matrix}\right]
	=
	\left[\begin{matrix}
	\text{\# Classes 1}\\
	\text{\# Classes 2}\\
	\text{\# Classes 3}\\
	\vdots\\
	\text{\# Classes 54}\\
	\end{matrix}\right]
\end{equation*}
This function will produce a vector of length 54, with the $i$th element representing the number of classes student $i$ could possible enroll in during that time slot. The number of conflicts is just the class number minus 1, but since every row is subtracted it becomes a constant subtraction on the entire optimization problem and can just be ignored (the optimal point is not affected by it). A linear program requires that the objective function result in a scalar value, but the above function does not. To map the above function into a scalar, simply sum all the elements of the number of classes vector, producing the equation:
\begin{equation*}
1^T
\left[\begin{matrix}
\text{Student 1 Class Selection}\\
\text{Student 2 Class Selection}\\
\text{Student 3 Class Selection}\\
\vdots\\
\text{Student 54 Class Selection}
\end{matrix}\right]
\left[\begin{matrix}
x_1\\
x_2\\
\vdots\\
x_{13}
\end{matrix}\right]
=
1^T
\left[\begin{matrix}
\text{\# Classes 1}\\
\text{\# Classes 2}\\
\text{\# Classes 3}\\
\vdots\\
\text{\# Classes 54}\\
\end{matrix}\right]
\end{equation*}
This equation must be repeated for all time slots, and then the scalar values added together. Note that the class selection matrix is simply the matrix $A$ provided in the notes. The overall objective function for this optimization is (with T:= number of time slots, S:= number of students, C:= number of classes):
\begin{equation}
	f(x) = \sum_{i=1}^{T} \left(\sum_{j=1}^{C} x_{ik} \left(\sum_{k=1}^{S} a_{ik} \right)\right)
\end{equation}

This optimization problem on its own will not suffice, because clearly the optimal solution is just to not have any classes (then no conflicts can occur). Therefore some constraints are necessary to define the problem better:

\subsubsection*{Constraint 1}

The first constraint used is to force there to be at least 1 offering of the class. This constraint is formed by creating a matrix $F$ that has the following structure: $$F = [ I_C | I_C | I_C | \cdots| I_C]$$ (with T repetitions of $I_C$, the identity matrix of size $C\times C$). This matrix, when multiplied against the x vector, will create C constraints, each being the sum of the decision variables for that class. The formal constraint is:
\begin{equation}
	Fx \geq 1
\end{equation}

\subsubsection*{Constraint 2}

The second constraint used is to force there to be at least 1 class per timeslot. This constraint is created by forming a matrix $G$ that has the following structure:

\begin{equation*}
\setcounter{MaxMatrixCols}{25}
	G = \left[\begin{matrix}
	1 & 1 & 1 & \cdots & 1 & 0 & 0 & 0 & \cdots & 0 & 0 & 0 & \cdots \cdots & 0 & 0 & 0 & 0 & \cdots & 0 & 0\\
	0 & 0 & 0 & \cdots & 0 & 1 & 1 & 1 & \cdots & 1 & 0 & 0 & \cdots \cdots & 0 & 0 & 0 & 0 & \cdots & 0 & 0\\
	\vdots\\
	0 & 0 & 0 & \cdots & 0 & 0 & 0 & 0 & \cdots & 0 & 0 & 0 & \cdots \cdots & 1 & 0 & 0 & 0 & \cdots & 0 & 0\\
	0 & 0 & 0 & \cdots & 0 & 0 & 0 & 0 & \cdots & 0 & 0 & 0 & \cdots \cdots & 0 & 1 & 1 & 1 & \cdots & 1 & 1
	\end{matrix}	
	\right]
\end{equation*}

Each row contains a set of 13 1s, which multiply against every decision variable in a specific timeslot's place in the vector. Rows are then added so that each timeslot gets represented in the matrix. The overall constraint is:
\begin{equation}
	Gx \geq 1
\end{equation}

\subsubsection*{Constraint 3}

The third constraing used is to force the student to have at least 1 possible class per timeslot. This constraint is created by forming a matrix $H$ that has the following structure:

\begin{equation*}
	H = \left[\begin{matrix}
	A^T & 0 & \cdots & 0\\
	0 & A^T & \cdots & 0\\
	& & \ddots & \\
	0 & 0 & \cdots & A^T
	\end{matrix}\right]
\end{equation*}

This matrix is simply formed by doing the block diagonal of the matrix $A^T$, with the number of $A^T$ equal to the number of timeslots. The overall constraint is:
\begin{equation}
	Hx \geq 1
\end{equation}

These constraints all get put with the objective function, creating the overall optimization program:
\begin{equation}
\begin{aligned}
& \text{minimize} & \sum_{i=1}^{T} \left(\sum_{j=1}^{C} x_{ik} \left(\sum_{k=1}^{S} a_{ik} \right)\right) \\
& \text{subject to} & Fx \geq 1\\
& & Gx \geq 1\\
& & Hx \geq 1
\end{aligned}
\end{equation}
This was implemented inside MATLAB using the mixed integer linear program solver. Of note is the fact that all constraints were multiplied by -1 to make them into a less-than or equal to constraint instead of the greater-than.

\newpage
The following is the solution found for a 3-timeslot problem. Each row corresponds to a class, and each column corresponds to a timeslot.
\begin{verbatim}
     0     1     0
     0     0     1
     0     0     1
     0     0     1
     1     1     0
     0     1     1
     0     0     1
     0     0     1
     1     0     0
     1     1     0
     1     0     0
     0     1     0
     0     1     0
\end{verbatim}
This problem had an objective value of 335.

The following is the solution found for a 4-timeslot problem. Each row corresponds to a class, and each column corresponds to a timeslot.
\begin{verbatim}
     0    1    0    0
     0    0    1    1
     0    0    1    0
     0    1    0    0
     1    1    0    1
     0    1    1    0
     1    0    0    0
     0    1    0    0
     1    0    0    1
     1    1    0    0
     1    0    0    1
     0    0    1    0
     0    0    1    0
\end{verbatim}
This problem had an objective value of 426.

\newpage
{\footnotesize
\begin{lstlisting}[language=matlab,frame=single,tabsize=2,breaklines=true]
%% Solve the scheduling problem
% 13 classes, 54 students
A = [0,0,0,1,0,1,0,1,0,0,1,0,1,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,0,0,1,0,0,0,1,1,1,1,1,1,0,1,1,1,1,1,1,0,0,1,0,1,0,0;
     1,1,1,1,0,1,0,1,0,0,0,1,1,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,1,0,1,0,0,1,0,0,0,1,0,1,0,1,0,0,0,0,1,0,0,1,1,1,0;
     1,0,1,0,1,0,1,0,1,1,1,1,0,0,1,1,1,1,1,1,0,0,0,1,1,1,0,1,0,0,0,1,0,0,1,1,0,1,1,1,0,1,1,1,1,1,0,0,0,1,1,0,0,0;
     0,0,1,0,0,0,1,0,0,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,1,0,0,0,1,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0;
     1,0,0,1,0,1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,1,0,1,0,0,1,0,1,0,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0;
     0,1,1,1,1,0,0,0,1,1,1,1,1,0,1,0,1,1,0,1,0,0,0,0,0,1,0,0,0,1,1,0,0,1,1,0,1,1,1,1,0,0,0,1,1,1,1,0,0,1,0,1,0,1;
     0,0,1,1,0,1,1,0,0,1,1,1,0,1,1,0,1,1,0,1,0,1,1,0,0,1,1,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,1,1,0,1,1,0,0,1,0,1,1,0;
     1,0,0,0,0,1,1,0,1,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,0;
     0,1,0,0,0,1,1,0,1,0,1,0,0,0,0,1,1,1,0,1,0,1,1,1,1,1,0,0,0,0,1,0,1,0,0,0,1,0,1,1,0,0,1,1,1,0,1,0,0,0,0,1,0,1;
     1,1,1,1,0,1,0,1,0,0,0,1,1,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,1,0,1,0,0,1,0,0,0,1,0,1,0,1,0,0,0,0,1,0,0,1,1,1,0;
     1,0,1,0,1,0,1,0,1,1,1,1,0,0,1,1,1,1,1,1,0,0,0,0,1,1,0,1,0,0,0,1,0,0,1,1,0,1,1,1,0,1,1,1,1,1,0,0,0,1,1,0,0,0;
     0,0,1,0,0,0,1,0,0,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0;
     1,0,0,1,0,1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0];

numTimeSlots = 3;
numStudents = 54;

% Sum the rows to find out how many students requested the class
rowA = sum(A, 2);

% Stack the A row matrix according to the number of timeslots to use
objA = [];
for (i = 1:1:numTimeSlots)
  objA = [objA; rowA];
end
numVars = length(objA);

% Force the student to have at least 1 class per time slot
constraintA = [];
constraintb = [];
temp = [];
for (i = 1:1:numTimeSlots)
  temp = blkdiag(temp, A');
end
constraintA = -1*temp;
constraintb = -ones(numTimeSlots*numStudents,1);

% Force there to be at least 1 offering of each class
temp = [];
for (i = 1:1:numTimeSlots)
  temp = [temp, eye(13)];
end
constraintA = [constraintA; -1*temp];
constraintb = [constraintb;
               -1*ones(13,1) ];

% Force at least 1 class per timeslot
temp = [];
for (i = 1:1:numTimeSlots)
  basis = zeros(numTimeSlots,1);
  basis(i, 1) = 1;
  for (j = 1:1:13)
    temp = [temp, basis];
  end
end

constraintA = [constraintA;
               -1*temp];
constraintb = [constraintb;
               -1*ones(numTimeSlots,1)];


x = intlinprog(objA,...         % The vector for the cost function
               1:numVars, ...   % Make all the variables be integer
               constraintA,...      % The LHS of the inequality constraint
               constraintb,...      % The RHS of the inequality constraint
               [],...               % Not using the equality constraint
               [],...               % Not using the equality constraint
               zeros(1,numVars),... % Lower bound of 0 for the variables
               ones(1, numVars));   % Upper bound of 1 for the variables

schedule = reshape(x, 13, numTimeSlots)
\end{lstlisting}}




\end{document}
