% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: January 3, 2017
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
% These packages are all incorporated in the memoir class to one degree or another...

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

\newtheorem{proposition}{\textbf{Proposition}}[section]
\newtheorem{assumption}{\textbf{Assumption}}
\newtheorem{theorem}{\textbf{Theorem}}[section]
\newtheorem{lemma}[theorem]{\textbf{Lemma}}
\newtheorem{corollary}{\textbf{Corollary}}[theorem]

\newtheorem{example}{Example}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{datetime}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{Spectral Properties of the Prediction Matrices in Model Predictive Control}\rhead{}
\lfoot{\today\ \currenttime}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\bibliographystyle{IEEEtran}

%%% END Article customizations


\begin{document}

\section{Notation}

Let a matrix $A \in \mathbb{C}^{n \times m}$ with $n < m$ have $n$ singular values ordered such that $ \sigma_1 \geq \dots \geq \sigma_n \geq 0$.

\section{Preliminaries}

\subsection{Spectral Estimation}

\subsubsection{Lower Bounds}

\subsubsection{Upper Bounds}

%Bounding the largest eigenvalue/singular value of a matrix has been an ongoing research field over the past century. Many bounds have been derived from the simplest using row and column sums in \cite{Browne1930} (extended in \cite[p.~141]{Horn1994}), to more tighter bounds using the trace of a matrix in \cite{Rojo1998, Styan1983}.

The simplest upper bound was developed by Browne in 1930 \cite{Browne1930}, and presents the upper bound as the average of the largest row sum and the largest column sum. This result was extended to be an upper bound for the largest singular-value of a complex rectangular matrix in \cite[p.~141]{Horn1994}, which was stated as follows:
\begin{equation}
    \label{eq:upBound:HornAverage}
    \sigma_1(A) \leq \frac{1}{2} \left( \norm{A}_{\infty} + \norm{A}_1 \right)
\end{equation}

\subsection{Condition Number}

The condition number of the rectangular matrix $A$, denoted $\kappa(A)$ is defined as the ratio between the largest and smallest singular values, e.g.
\begin{equation}
\label{eq:condNum}
\kappa(A) = \frac{\sigma_1}{\sigma_n}
\end{equation}


\subsection{Toeplitz Matrices}

\begin{equation}
    \label{eq:toep:trigGenFunc}
    p(z) = \sum_{i=-\infty}^{\infty} F_i z^i
    \qquad
    \forall \{ z \in \mathbb{C} : \abs{z} = 1\}
\end{equation}


The block form of Proposition 1.13 in \cite[Section 1.5]{Bottcher1991} is restated here for completeness.
\begin{proposition}[{\cite[Section 1.5]{Bottcher1991}}]
    \label{prop:toep:upperLowerProduct}
    
    Let $H^{\infty}$ be the set of lower-triangular Toeplitz matrices, and $\bar{H}^{\infty}$ be the set of upper-triangular Toeplitz matrices defined formally as
    \begin{equation*}
    H^{\infty} := \{F\in L^{\infty}: F_n = 0 \quad \forall n < 0\}, \qquad \bar{H}^{\infty} := \{F\in L^{\infty}: F_n = 0 \quad \forall n < 0\}
    \end{equation*}
    If $A \in \bar{H}^{\infty}$, $B \in L^{\infty}$, $C \in H^{\infty}$, then
    \begin{equation*}
    T(ABC) = T(A)T(B)T(C)
    \end{equation*}
\end{proposition}

\subsubsection{Summation Results for the Generator Function}

In this analysis, two different norms of the symbol will be used, the 1-norm and the $\infty$-norm. For a constant matrix, those norms are defined as the largest absolute column sum and the largest absolute row sum respectively. This work will focus specifically on symbols defined by the trigonometric polynomial \eqref{eq:toep:trigGenFunc}.

\begin{lemma}
    \label{lemma:toep:genFuncNorms}
    For a matrix-valued trigonometric polynomial given by \eqref{eq:toep:trigGenFunc}, the following are upper bounds on its 1-norm and $\infty$-norm
    \begin{equation}
        \norm{p(z)}_{1} \leq \sum_{i=-\infty}^{\infty} \norm{F_i}_1
    \end{equation}
    \begin{equation}
        \norm{p(z)}_{\infty} \leq \sum_{i=-\infty}^{\infty} \norm{F_i}_{\infty}
    \end{equation}
\end{lemma}
\begin{proof}
    
    Begin with the definition of the norm for the trigonometric polynomial \eqref{eq:toep:trigGenFunc}:
    $$ \norm{p(z)}_1 = \norm{ \sum_{i=-\infty}^{\infty} F_i z^i }_1 $$
    
    Applying the triangle-inequality to the right-hand summation gives the inequality:
    $$ \norm{p(z)}_1 \leq \sum_{i=-\infty}^{\infty} \norm{ F_i z^i }_1 $$
    
    Rewritting the norm of the component matrices as the appropriate summation over the components, and substituting in $z = e^{j\theta}$ for the trigonometric polynomial variable gives
    \begin{subequations}
        \label{eq:norm:expanded}
        \begin{align}
            \norm{p(z)}_1 &\leq \sum_{i=-\infty}^{\infty}
            \left( \max_k
            \left\{ \sum_{l=1}^{n} \abs{ e^{j\theta} [F_i]_{lk} }
            \right\} \right)\\
            &\leq \sum_{i=-\infty}^{\infty}
            \left( \max_k
            \left\{ \sum_{l=1}^{n} \abs{ [F_i]_{lk} }
            \right\} \right)
        \end{align}
    \end{subequations}
    
    Rewritting the inner summation in terms of the matrix norm again leads to the final result
    $$ \norm{p(z)}_1 \leq \sum_{i=-\infty}^{\infty} \norm{ F_i}_1 $$
    
    The proof for the $\infty$-norm is similar, except the inner summation in \eqref{eq:norm:expanded} is over $k$ with the maximum over $l$.
\end{proof}
\begin{corollary}
    \label{cor:toep:genFuncUpperSingularValue}
    For a matrix-valued trigonometric polynomial given by \eqref{eq:toep:trigGenFunc}, the maximum singular value of the function over its entire range is upper bounded by
    \begin{equation}
        \sigma_1(p(z)) \leq \frac{1}{2} \sum_{i=-\infty}^{\infty}\left( \norm{F_i}_{\infty} + \norm{F_i}_{1}  \right)
    \end{equation}
\end{corollary}
\begin{proof}
    Using the bound in \eqref{eq:upBound:HornAverage}, the symbol's singular values can be upper bounded by
    \begin{equation*}
    \label{eq:fut:proof:funcUpperBound}
    \sigma_1(p(z)) \leq \frac{1}{2}\left( \norm{p(z)}_1 + \norm{p(z)}_{\infty} \right)
    \end{equation*}
    Then, substituting in the results from theorem \ref{thm:fut:singularValueUpperBound} gives the final inequality.
\end{proof}


\clearpage
\section{Dense Least-Squares QP Formulation}

One possible way of representing the prediction formulation is to only have variables for the possible future inputs, not the states (see e.g. \cite[p.~83]{Maciejowski2002}, \cite{Santin2017}).
 For this formulation, let the vector
$x = \begin{bmatrix}
x_1^T & x_2^T & \cdots & x_{N}^T
\end{bmatrix}^T$ represent the state variables
and
$u = \begin{bmatrix}
u_0^T & u_1^T & \cdots & u_{N-1}^T
\end{bmatrix}^T$ represent the control inputs.

To predict the future state values, the following equation is used
$$x_i = A^i x_0 + \sum_{k=1}^{i} A^{i-k}B u_{k-1} \qquad \forall i \in [1, N]$$
This equation can written in the form $x = E_{1} x_0 + G_{1} u$ with

\begin{equation}
E_{1} = \begin{bmatrix}
A\\
A^2\\
\vdots\\
A^N
\end{bmatrix}
\end{equation}

\begin{equation}
\label{eq:dense:predMatrix}
G_{1} = \begin{bmatrix}
B & 0 & 0 & & 0\\
AB & B & 0 & & 0\\
A^2b & AB & B & & 0 \\
\vdots & & & \ddots & \vdots\\
A^{N-1}B & A^{N-2}B & A^{N-3}B & \cdots & B
\end{bmatrix}
\end{equation}

The overall optimization problem to be solved is then
\begin{equation}
\label{eq:dense:qp}
\begin{array}{cc}
\underset{u}{\text{min}} \hspace{1em}
& u^T (G_1^T \hat{Q} G_1 + \hat{R}) u^T - (G_1^T \hat{Q} E_1 x_0)^T u
\end{array}
\end{equation}
where $\hat{Q} = I_N \otimes Q$ and $\hat{R} = I_N \otimes R$.

\subsection{Properties of the Prediction Matrix}

This section focuses on the study of matrix $G_1$ given by \eqref{eq:dense:predMatrix}, specifically on its structure and spectral properties.

Notice that matrix $G_1$ is a block lower-triangular Toeplitz matrix. This means that $G_1$ can be associated with an appropriate symbol (where the blocks of $G_1$ are the spectral coefficients of the function). Using the trigonometric function in \eqref{eq:toep:trigGenFunc} as a basis, the following function can be derived
\begin{equation*}
    p_1(z) = \sum_{i=0}^{\infty} A^iBz^{-i}
\end{equation*}
Through some algebraic manipulation, this symbol can be shown to be in the form of a matrix geometric series multiplied by $B$. The summation of the matrix geometric series is known in closed-form, simplifying the symbol to become
\begin{equation}
    \label{eq:dense:pred:genFunc}
    p_1(z) = (zI - A)^{-1}B 
    \qquad
    \forall \{ z \in \mathbb{C} : \abs{z} = 1\}
\end{equation}

\begin{theorem}
    \label{thm:dense:predMatrix:bounds}
    The singular values of the prediction matrix $G_1$ are bounded above by
    \begin{equation}
        \sigma_1(G_1) \leq \frac{ \norm{B}_2 }{1 - \norm{A}_2}
    \end{equation}
    provided that $A$ does not have a singular value equal to 1. In the case where $A$ has a singular value of 1, the bound becomes infinite, e.g. $\sigma_1(G_1) \leq \infty$.
\end{theorem}
\begin{proof}
    Start by deriving the upper bound for the singular values.
    
    Recall that the definition of the induced 2-norm on a matrix is $\norm{A}_2 = \sigma_1$. This means that 
    \begin{equation*}
        \sigma_1(p_1(z)) = \norm{p_1(z)}_2 = \norm{ (zI - A)^{-1}B }_2
    \end{equation*}
    By the submultiplicative property of the norm, that becomes
    \begin{equation*}
        \sigma_1(p_1(z)) \leq \norm{ (zI - A)^{-1}}_2 \norm{B}_2
    \end{equation*}
    By then using the upper limit of the matrix resolvant's norm
    \begin{equation*}
        \sigma_1(p_1(z)) \leq \frac{ \norm{B}_2 }{\abs{z} - \norm{A}_2}
    \end{equation*}
    Finally, recalling that $\abs{z} = 1$ by definition,
    \begin{equation*}
        \sigma_1(p_1(z)) \leq \frac{ \norm{B}_2 }{1 - \norm{A}_2}
    \end{equation*}
\end{proof}

\subsection{Properties of the QP Hessian}

The properties of the Hessian matrix of a QP is important to the convergence rate of numerical algorithms such as conjguate gradient, interior-point methods, etc. To this end, bounding the condition number and deriving estimates for the spectrum of the Hessian will allow for better estimates of algorithm convergence.

For the dense MPC formulation discussed here, the Hessian matrix is $H_1 = G_1^T \hat{Q} G_1 + \hat{R}$. A first try at analyzing this matrix might involve building the matrix products and finding the structure of $H_1$. This method can prove very difficult though, due to the long summations that appear and the dense structure of the matrix. An alternative approach is to examine the matrix $H_1$ through the theory of Toeplitz operators.

It is already known that the matrix $G_1$ is Toeplitz with the symbol given in \eqref{eq:dense:pred:genFunc}, and it is obvious that the matrix $G_1$ is lower-triangular as well (so therefore $G_1^T$ is upper-triangular). Additionally, the two other matrices $\hat{Q}$ and $\hat{R}$ are block Toeplitz (specifically block-diagonal)  with symbols $p_{\hat{Q}}(z) = Q$ and $p_{\hat{R}}(z) = R$ respectively.

Using these facts, the products and summations present in $H_1$ can be expressed as the product and summation of the Toeplitz matrices. The result in proposition \ref{prop:toep:upperLowerProduct} along with the fact that $T(A)+T(B) = T(A+B)$ \cite[Lemma 4.2]{Gutierrez-Gutierrez2012} allows for the matrix $H_1$ to be analyzed using the matrix-valued symbol
\begin{equation}
    p_{H_1}(z) = p_1(z)^T p_Q(z) p_1(z) + p_R(z) = B^T ((zI - A)^{-1})^* Q (zI-A)^{-1} B + R
    \label{eq:dense:hess:genFunc}
\end{equation}

\begin{theorem}
    \label{them:dense:hess:upperBound}
    The singular values of $H_1$ are bounded above by
    \begin{equation*}
        \sigma_1(H_1) \leq \left( \frac{\norm{B}_2}{1 - \norm{A}_2} \right)^2 \norm{Q}_2 + \norm{R}_2
    \end{equation*}
\end{theorem}
\begin{proof}
    This proof follows the same idea as the one for theorem \ref{thm:dense:predMatrix:bounds}.
    \begin{equation*}
        \sigma_1(H_1) \leq \norm{p_{H_1}(z)}_2 = \norm{ B^T ((zI - A)^{-1})^* Q (zI-A)^{-1} B + R }_2
    \end{equation*}
    By the sub-additive and sub-multiplicative properties of the matrix 2-norm, this becomes
    \begin{equation*}
        \sigma_1(H_1) \leq \norm{B^T}_2 \norm{((zI - A)^{-1})^*}_2 \norm{Q}_2 \norm{(zI-A)^{-1}}_2 \norm{B}_2 + \norm{R}_2
    \end{equation*}
    Note that for the matrix 2-norm $\norm{A}_2 = \norm{A^*}_2$ for a general matrix $A$, making
    \begin{equation*}
        \sigma_1(H_1) \leq \left(\norm{(zI-A)^{-1}}_2 \norm{B}_2\right)^2 \norm{Q}_2 + \norm{R}_2
    \end{equation*}
    Finally, by the definition of the norm of the resolvant used in the proof of theorem \ref{thm:dense:predMatrix:bounds}
    \begin{equation*}
        \sigma_1(H_1) \leq \left( \frac{\norm{B}_2}{1 - \norm{A}_2} \right)^2 \norm{Q}_2 + \norm{R}_2
    \end{equation*}
\end{proof}

\clearpage
\section{Constrained: Future-States \& Inputs}

\subsection{Overview}

Another way of representing the prediction matrix is to keep the future states as an optimization variable, defining the variable vector as
$z = \begin{bmatrix}
x_1^T & u_0^T & x_2^T & u_1^T & x_3^T & \cdots & x_{N}^T & u_{N-1}^T
\end{bmatrix}^T$ (see \cite[Section 11.3.1]{Borrelli2017}).
\footnote{Note that this is simply a permutation of the variable vector given in \cite[Section 11.3.1]{Borrelli2017}, which means that the resulting prediction matrix will be a column permutation of the original matrix. Since permutation matrices are orthogonal, the spectrum of a permuted matrix is the same as the original matrix. The ordering used here is more convenient for analysis than the one given in \cite{Borrelli2017}.}

The resulting prediction equation for this variable choice is $G_{2}z = E_{2} x_0$ with

\begin{equation}
    E_{2} = \begin{bmatrix}
    A\\
    0\\
    \vdots\\
    0
    \end{bmatrix}
\end{equation}

\begin{equation}
    \label{eq:future:predMatrix}
    G_{2} = \begin{bmatrix}
     I & -B &  0 &  0 &  0 &\cdots & 0 & 0 & 0 & 0\\
    -A &  0 &  I & -B &  0 & & 0 & 0 & 0 & 0\\
     0 &  0 & -A &  0 &  I & & 0 & 0 & 0 & 0\\
     0 &  0 &  0 &  0 & -A & & 0 & 0 & 0 & 0\\
    \vdots & & & & & \ddots & & & & \vdots\\
     0 &  0 &  0  & 0 & 0 & \cdots &  I & -B & 0 &  0\\
     0 &  0 &  0  & 0 & 0 & \cdots & -A &  0 & I & -B
    \end{bmatrix}
\end{equation}

Examining \eqref{eq:future:predMatrix} shows that it is a block-banded Toeplitz matrix with block
$F_0 {=} \begin{bmatrix}
I & -B
\end{bmatrix}$ on the diagonal, and block 
$F_{-1} {=} \begin{bmatrix}
-A & 0
\end{bmatrix}$ one below the diagonal. This means that $G_{2}$ has an associated matrix-valued symbol of
\begin{equation}
    \label{eq:future:GenFunc}
    p_{2}(z) = 
    \begin{bmatrix}
    I & -B
    \end{bmatrix}
    + z^{-1}
    \begin{bmatrix}
    -A & 0
    \end{bmatrix}
    \qquad
    \forall \{ z \in \mathbb{C} : \abs{z} = 1\}
\end{equation}

\subsection{Spectral Bounds}

An upper bound for the singular values of \eqref{eq:future:predMatrix} can be found by finding the maximum singular value of its symbol \eqref{eq:future:GenFunc} over its entire range since \eqref{eq:future:predMatrix} is Toeplitz. This leads to the following theorem

\begin{theorem}
    \label{thm:fut:singularValueUpperBound}
    The singular values of the prediction matrix $G_{2}$ are bounded above by
    \begin{equation*}
        \sigma_1(G_{2}) \leq \frac{1}{2}
        + \frac{1}{2} \left( \norm{B}_{\infty} + \max\{1,\norm{B}_1\}
        + \norm{A}_{\infty} + \norm{A}_1 \right)
    \end{equation*}
\end{theorem}
\begin{proof}
    Start by examining the upper bound for the singular values of the symbol.
    
    Since $p_{2}(z)$ in this case is defined by \eqref{eq:future:GenFunc} with blocks $F_0 {=} \begin{bmatrix}
    I & -B
    \end{bmatrix}$ and
    $F_{-1} {=} \begin{bmatrix}
    -A & 0
    \end{bmatrix}$,
    using the results of corollary \ref{cor:toep:genFuncUpperSingularValue} gives the relation
    \begin{equation*}
    \sigma_1(p_{2}(z)) \leq \frac{1}{2}\left(
       \norm{ \begin{bmatrix}  I & -B \end{bmatrix} }_1
     + \norm{ \begin{bmatrix}  I & -B \end{bmatrix} }_{\infty}
     + \norm{ \begin{bmatrix} -A & 0  \end{bmatrix} }_1
     + \norm{ \begin{bmatrix} -A & 0  \end{bmatrix} }_{\infty}
     \right)
    \end{equation*}
    Simplification of that gives the result
    \begin{equation}
    \label{eq:fut:proof:funcUpperBoundExpand}
    \sigma_1(p_{2}(z)) \leq \frac{1}{2}\left(
    \norm{ \begin{bmatrix}  I & -B \end{bmatrix} }_1
    + \norm{ \begin{bmatrix}  I & -B \end{bmatrix} }_{\infty}
    + \norm{ A }_1
    + \norm{ A }_{\infty}
    \right)
    \end{equation}
    Note that since the $\infty$-norm is the largest absolute row sum, $\norm{ \begin{bmatrix}  I & -B \end{bmatrix} }_{\infty} = 1 + \norm{B}_{\infty}$. Additionally since the 1-norm is the largest absolute column sum, 
    $\norm{ \begin{bmatrix}  I & -B \end{bmatrix} }_1 = max\{1,\norm{B}_1\}$.
    These facts along with \eqref{eq:fut:proof:funcUpperBoundExpand} give the upper bound of the symbol:
    \begin{equation*}
    \sigma_1(p_{2}(z)) \leq \frac{1}{2}
    + \frac{1}{2} \left( \norm{B}_{\infty} + \max\{1,\norm{B}_1\}
    + \norm{A}_{\infty} + \norm{A}_1 \right)
    \end{equation*}
    Since $\sigma_1(G_{2}) \leq \sigma_1(p_{2}(z))$ (by \cite[Theorem 4.3]{Gutierrez-Gutierrez2012}) the final bound is
    \begin{equation*}
    \sigma_1(G_{2}) \leq \frac{1}{2}
    + \frac{1}{2} \left( \norm{B}_{\infty} + \max\{1,\norm{B}_1\}
    + \norm{A}_{\infty} + \norm{A}_1 \right)
    \end{equation*}
    
\end{proof}

\begin{theorem}
    \label{thm:fut:singularValuesLowerBound}
    The smallest singular value of the prediction matrix $G_2$ lies in the interval
    \begin{equation*}
        max\{0, 1+s\} \leq \sigma_n \leq s + \sqrt{1 + \norm{B}_2^2}
    \end{equation*}
    where $s$ is the lower bound on the singular values of $A$ derived using theorem 3 in \cite{Johnson1989}, specifically
    \begin{equation*}
        s = \underset{1 \leq k \leq n}{min} \{ \abs{[A]_{kk}} - \frac{1}{2}(r_k(A) + c_k(A))\}
    \end{equation*}
\end{theorem}
\begin{proof}
    Start by examining the lower bound on $\sigma_n$. From \cite[Theorem 3]{Johnson1989}, the smallest singular value of a rectangular matrix $F$ can be bounded below by
    \begin{equation}
    \label{eq:fut:singValLower:proof1}
        \sigma_n(F) \geq \underset{1 \leq k \leq n}{min} \{ \abs{[F]_{kk}} - \frac{1}{2}(r_k(F) + c_k(F))\}
    \end{equation}
    Examining the matrix symbol $p_2(z)$, it can be seen that
    \begin{equation*}
        \begin{array}{ccc}
            \abs{[p_2(z)]_{kk}} = 1 + \abs{[A]_{kk}}, & r_k(p_2(z)) = r_k(A), & c_k(p_2(z)) = c_k(A)
        \end{array}
    \end{equation*}
    Making the full inequality in \eqref{eq:fut:singValLower:proof1} become
    \begin{equation*}
        \sigma_n \geq \underset{1 \leq k \leq n}{min} \{ 1 + \abs{[A]_{kk}} - \frac{1}{2}(r_k(A) + c_k(A))\}
    \end{equation*}
    Simplifying the expression and using $s$ as defined in the theorem changes the right-hand side to $1 + s$. It is also known that singular values must be positive, but the definition of $s$ could produce a number less than 1, therefore the full result of the lower bound
    \begin{equation*}
        \sigma_n \geq \min\{0, 1 + s\}
    \end{equation*}
    Turning to the upper bound, the smallest singular value of a sum of two matrices $F$ and $G$ can be expressed as $\sigma_n(F+G) \leq \sigma_n(F) + \sigma_1(G)$ \cite[Theorem 3.3.16 (a)]{Horn1994}. Applying that inequality to the function $p_2$ produces
    \begin{equation*}
        \sigma_n(p_2) = \sigma_n( F_0 + z^{-1}F_{-1}) \leq \sigma_n(z^{-1}F_{-1}) + \sigma_1(F_0)
    \end{equation*}
    Since $F_{-1} = z^{-1}\begin{bmatrix} -A & 0\end{bmatrix}$, and appending columns or rows of all zero to a matrix doesn't change the singular values (along with negating the entire matrix), $\sigma_n(F_{-1}) = \sigma_n(z^{-1}A)$. Using the lower-bound on the singular values from \cite[Theorem 3]{Johnson1989} for $\sigma_n(z^{-1}A)$ gives
    \begin{equation*}
        \sigma_n( p_2(z) ) \leq s + \sigma_1(F_0)
    \end{equation*}
    Next examine the matrix $F_0 = \begin{bmatrix} I & -B\end{bmatrix}.$ By definition, $\sigma_i(F_0) = \sqrt{ \lambda_i(F_0F_0^*)}$. In this case, $F_0F_0^* = I + B^*B$. It is known that $\lambda_i(I + B^*B) = 1 + \lambda_i(B^*B)$, which means that $\sigma_1(F_0) = \sqrt{1 + \sigma_1(B)^2}$. Substituting that into the inequality gives the final result of
    \begin{equation*}
        \sigma_n( p_2(z) ) \leq s + \sqrt{1 + \norm{B}^2_2}
    \end{equation*}
\end{proof}  



\clearpage
\section{Constrained: Current/Future-States \& Inputs}

\clearpage
\bibliography{references}

\end{document}
