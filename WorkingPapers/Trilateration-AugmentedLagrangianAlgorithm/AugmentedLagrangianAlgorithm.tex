% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: December 15, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,titlepage]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
% These packages are all incorporated in the memoir class to one degree or another...

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{datetime}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{Augmented Lagrangian-Based Algorithm for the Global Solution of the  Trilateration Problem}\rhead{}
\lfoot{\today\ \currenttime}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\bibliographystyle{IEEEtran}

%%% END Article customizations


\begin{document}

\section{Problem Forumlation}

The Range-based Least-Squares (R-LS) localization problem of trilateration (localizing using only distance-based measurements) can be formatted as a non-convex QCQP, shown in \eqref{eq:nonconvexQCQP}.

\begin{subequations}
\label{eq:nonconvexQCQP}
\begin{align}
\underset{\hat{x}, \hat{y}, \hat{z}, a_i}{\text{min}} \hspace{1em}
& a^{T} \Psi a \\
\text{subject to} \hspace{1em}
&
\norm{
    \begin{bmatrix} \hat{x}\\ \hat{y}\\ \hat{z} \end{bmatrix} - \begin{bmatrix} x_i\\ y_i\\ z_i \end{bmatrix}}^2 - (a_i {-} r_i)^2 = 0 \qquad \forall i = 1, 2, \dots, N
\end{align}
\end{subequations}

Finding the solution to this optimization problem is very difficult due to the non-convexity of the constraint set (e.g. quadratic equality constraints). The algorithm described in this paper sets out to solve \eqref{eq:nonconvexQCQP} using an Augmented Lagrangian-based approach that is also natively extendible to use in distributed computation.

Next, several modifications are made to problem \eqref{eq:nonconvexQCQP} resulting in problem \eqref{eq:modifiedProblem}. The first change made was to introduce position estimates for each of the measured distances, making constraints \eqref{eq:modifiedProblem_rCon} be completely parameterized by $i$. The next change was to introduce linear-equality constraints to force the different estimates to be the same (constraints \eqref{eq:modifiedProblem_xCon}, \eqref{eq:modifiedProblem_yCon}, and \eqref{eq:modifiedProblem_zCon}). This construction is commonly used in distributed computation frameworks, where each node in the network maintains its own estimate of the variables and the nodes jointly update the estimates until they all agree. 

\begin{subequations}
	\label{eq:modifiedProblem}
	\begin{align}
	\underset{\hat{x}_i, \hat{y}_i, \hat{z}_i, a_i}{\text{min}} \hspace{1em}
	& a^{T} \Psi a \label{eq:modifiedProblem_cost}\\ 
	\text{subject to} \hspace{1em}
	& L\hat{x} = 0 \label{eq:modifiedProblem_xCon}\\
	& L\hat{y} = 0 \label{eq:modifiedProblem_yCon}\\
	& L\hat{z} = 0 \label{eq:modifiedProblem_zCon}\\
	& \norm{
        \begin{bmatrix} \hat{x}\\ \hat{y}\\ \hat{z} \end{bmatrix} - \begin{bmatrix} x_i\\ y_i\\ z_i \end{bmatrix}
        }^2 = (a_i {-} r_i)^2 \quad \forall i = 1, 2, \dots, N \label{eq:modifiedProblem_rCon}
	\end{align}
\end{subequations}

Problem \eqref{eq:modifiedProblem} was transformed one more time into problem \eqref{eq:augmentedLagrangianProblem} by creating the partially-augmented Lagrangian with respect to the linear consensus constraints. This moved those constraints into the cost function using a quadratic penalty term, and added three Lagrange multipliers ($\alpha$, $\beta$ and $\gamma$). The quadratic-equality constraint was left as a constraint of the problem, which creates the constrained maximization-minimization problem shown in \eqref{eq:augmentedLagrangianProblem}.

\begin{subequations}
	\label{eq:augmentedLagrangianProblem}
	\begin{align}
	\underset{\alpha_i, \beta_i, \gamma_i}{\text{max}} \hspace{1em}
    \underset{\hat{x}_i, \hat{y}_i, \hat{z}_i, a_i}{\text{min}} \hspace{1em}
	& a^{T} \Psi a + \rho \left( \hat{x}^T L \hat{x} + \hat{y}^T L \hat{y} + \hat{z}^T L \hat{z}\right) + \alpha^{T} L \hat{x} + \beta^{T} L \hat{y} + \gamma^{T} L \hat{z}
    \label{eq:augmentedLagrangianProblem_cost}\\ 
	\text{subject to} \hspace{1em}
	&
	\norm{
        \begin{bmatrix} \hat{x}_i\\ \hat{y}_i\\ \hat{z}_i \end{bmatrix} - \begin{bmatrix} x_i\\ y_i\\ z_i \end{bmatrix}
        }^2 = (a_i {-} r_i)^2 \quad \forall i = 1, 2, \dots, N
        \label{eq:augmentedLagrangianProblem_rCon}
	\end{align}
\end{subequations}

\section{Overall Algorithm}

Problems of types similar to \eqref{eq:augmentedLagrangianProblem} (ones where there are both maximization and minimization of different variables) can be solved using an iterative 2-level method, the most common of which is the Augmented Lagrangian method.

The idea behind the Augmented Lagrangian method is to alternate between the maximization of some variables (usually the Lagrange multiplier variables, e.g. dual variables) and the minimization of the other variables (usually the original variables, e.g. primal variables). 

\subsection{Augmented Lagrangian Methods}

The most basic algorithms in optimization (such as steepest-descent, conjugate gradient, Newton's method, etc.) are natively formulated for solving unconstrained optimization problems, meaning the algorithm can freely visit any point in $\mathbb{R}^n$ in search of the optimal point. Many of the optimization problems that arise in engineering, physics, chemistry and other disciplines are constrained problems though.

One of the basic ways of solving a constrained optimization problem is to transform it into a similar unconstrained optimization problem. This has historically been done in 2 ways: Penalty function methods, and the Method of Multipliers. The Penalty function method takes the given constraints, and adds a penalty for violating them into the cost function of the problem.

\begin{algorithm}
\caption{General Augmented-Lagrangian Method}

\newcounter{stepCount}
\algrenewcommand\algorithmicfunction{
    \stepcounter{stepCount}\textbf{Step \arabic{stepCount} }
}
\algrenewcommand\algorithmicrequire{
    \textbf{Prerequisites:}
}
\begin{algorithmic}
\Require $k=0$, $\lambda_0 > 0$, $\rho_o > 0$
\Repeat
\Function{}{}
\State Find $x_k \in \mathbb{R}^n$ which is the solution to 
\State$$ \underset{x}{\text{minimize}} \hspace{1em} L_{\rho_k}(x, \lambda_k) $$
\EndFunction
\Function{}{}
\State Update the Lagrange multipliers
\State $$\lambda_{k+1} = \lambda_k + \alpha h(x_k)$$
\EndFunction
\Function{}{}
\State Update the penalty parameter using a set rule
\EndFunction
\Function{}{}
\State $k \gets k+1$
\EndFunction
\Until{ $h(x_k) < \text{Tolerance}$ } 
\end{algorithmic}

\end{algorithm}



\bibliography{references}

\end{document}
