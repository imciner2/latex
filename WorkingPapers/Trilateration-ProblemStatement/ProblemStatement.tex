% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: October 27, 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{datetime}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\bibliographystyle{IEEEtran}

%%% END Article customizations

\begin{document}

\begin{center}
	\section*{Trilateration Overview}
\end{center}

The problem of object localization can be broken into two distinct categories depending upon what data is available about the object's relative position. The two categories are:
\begin{itemize}[nolistsep, noitemsep]
	\item Triangulation - Which uses information about the direction to the object from the sensor
	\item Trilateration - Which uses information about the distance to the object from the sensor
\end{itemize}
The idea behind the trilateration systems is to use the fact that the sensor's position is known, and the distance from the sensor to the object is known. This creates a circular region around each sensor where the object can be, and by combining the information from multiple sensors the position of the object can be narrowed further. A 2D case is shown in figure \ref{fig:2dTrilaterationIdeal}. In this case the three sensors ($P1$ through $P3$) use their respective measurements ($r1$ through $r3$) to find the location of $Xp$.

\begin{figure}[h!]
	\center
	\includegraphics[scale=0.30]{Diagrams/IdealCircles.eps}
	\caption{Ideal 2D Trilateration problem.}
	\label{fig:2dTrilaterationIdeal}
\end{figure}

\subsection*{Linearization Method}

The simplest way of estimating the location of $Xp$ is to create an equation for every sensor that relates the measured distance to the radius between the object and the sensor, ie:
$$
(x - x_i)^2 + (y - y_i)^2 = r_i^2
$$
The set of all these equations will be a system of non-linear equations, the solution of which gives the estimated $x$ and $y$ position of the object.

This non-linear system is difficult to solve, so usually a linearized system is used instead of the non-linear system. This linear system becomes easily susceptible to noise though, so an exact solution may not exist (and usually does not). In practice many methods rely on finding a least-squares solution to the linear system as the solution method. Several groups have looked into augmenting this system for distributed use \cite{Reichenbach2006, Savvides2001, Ahmed2005}. Other work has looked into using dynamic weighting of the sensor's measurements before solving using the least-squares method \cite{Jiang2013, Li2015}. Some work has also been conducted into making the algorithm more robust to outliers \cite{Khalajmehrabadi2016}.


\subsection*{Kalman Filter Method}

The second method of estimating an object's position is using a Kalman filter to combine the sensor measurements, filter the sensor noise, and estimate the object's state vector (position, velocity, and acceleration). The Kalman filter approach is used mainly when an object is not stationary, ie moving along a trajectory. This is because the Kalman filter includes a physics model of the object's motion, so it is able to predict the movement of the object and use that to estimate its position \cite{Shareef2009}. There has also been some work in creating a distributed Kalman filter methodology to perform the object localization \cite{Alriksson2007, Yu2015}.

\begin{center}
	\section*{Proposed Trilateration Approach}
\end{center}

We propose an approach in the trilateration category. We utilize the distance between the object and the sensor along with information about the location of the sensor to determine the object's location in an environment. Many methods exist to measure the distance to an object, but the one we focus on is using information about a received RF signal. Using information about the received RF signal, an estimated distance measure can be calculated.

The RF signal strength maps nicely into circular or spherical contour lines around the sensor. The goal of our approach is to find the intersection of the contour spheres around each sensor. The 2D case is illustrated in figure \ref{fig:2dTrilaterationIdeal}. In this case there are 3 sensors, each with their own position and distance measurement. Three circles are created in the plane, and a point located at the intersection of the three circles is the estimated location of the object.


\begin{figure}[h!]
	\center
	\includegraphics[scale=0.25]{Diagrams/OverlappingCircles.eps}
	\caption{2D Trilateration problem where the distance estimates have errors.}
	\label{fig:2dTrilaterationOverlapping}
\end{figure}

In practical applications, the measured distance to the object will have an error, which makes the intersection into a feasible set instead of a single point. To overcome this we look at the error between the estimated position and the contour sphere (labeled as the $a$ vectors in figure \ref{fig:2dTrilaterationOverlapping}). 

This method can be formulated into the optimization problem shown in (\ref{eq:centralizedOptimProblem}). Where $P_i$ is the current location of node \textit{i}, and $r_i$ is the estimated distance between node \textit{i} and the observed object, and $\left[\begin{matrix} x & y & z \end{matrix}\right]^T$ is the estimated position of the object.

By formulating the trilateration problem in this manner, errors in the distance measurements are inherently included in the problem (as the $a_i$ variables in the constraints). The objective is to then minimize the $a$ vector in some sense. This approach does not require any information about the distribution of the distance errors, such as the covariances, which are required for other methods such as Kalman Filtering or Jacobi iteration.

\begin{equation}
\label{eq:centralizedOptimProblem}
\begin{matrix}
\text{min} & \norm{a}\\
\text{s.t.} &\\
& \norm{
	\left[\begin{matrix}
	x\\
	y\\
	z
	\end{matrix}\right]
	- P_i}_2^2 = (r_i + a_i)^2\\
\end{matrix}
\end{equation}

The problem posed in (\ref{eq:centralizedOptimProblem}) must be solved at a centralized point, only using the nodes for data acquisition. A better approach is to use the nodes as computation as well as data acquisition. To do this a network between the nodes is created and the object is localized using consensus. This approach is formulated using an optimization problem, shown in (\ref{eq:distributedOptimProblem}), where 
$ x = \left[\begin{matrix} x_1 & \cdots & x_i & \cdots & x_n \end{matrix}\right]^T$,
$ y = \left[\begin{matrix} y_1 & \cdots & y_i & \cdots & y_n \end{matrix}\right]^T$,
$ z = \left[\begin{matrix} z_1 & \cdots & z_i & \cdots & z_n \end{matrix}\right]^T$
are the estimates at node \textit{i} for the x, y, z, locations of the observed object and \textit{L} is the graph Laplacian matrix describing the node's communications network.

\begin{equation}
\label{eq:distributedOptimProblem}
	\begin{matrix}
	\text{min} & \norm{a}\\
	\text{s.t.} &\\
	& \norm{
		\left[\begin{matrix}
		x_i\\
		y_i\\
		z_i
		\end{matrix}\right]
		- P_i}_2^2 = (r_i + a_i)^2\\
	& Lx = 0\\
	& Ly = 0\\
	& Lz = 0
	\end{matrix}
\end{equation}

\bibliography{references.bib}

\end{document}
