\documentclass[11pt]{article}
\renewcommand{\baselinestretch}{1.05}
\usepackage{amsmath,amsthm,verbatim,amssymb,amsfonts,amscd, graphicx}
\usepackage{graphics}
\usepackage{lipsum} % Used for inserting dummy ^TLorem ipsum^T text into the template
\usepackage{fancyhdr} % Required for custom headers
\usepackage{lastpage} % Required to determine the last page for the footer
\usepackage{extramarks} % Required for headers and footers
\usepackage{datetime}
\usepackage{textcomp}
\usepackage{url}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage{hyperref}
\usepackage{wrapfig}
\usepackage{enumitem}
\usetikzlibrary{patterns,shapes,arrows,calc}

%\usepackage{draftwatermark}
%\SetWatermarkText{DRAFT - NOT FINAL}
%\SetWatermarkScale{3}

\newdateformat{monthyeardate}{%
  \monthname[\THEMONTH], \THEYEAR}
% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in 

%\linespread{1.1} % Line spacing

% Set up the header and footer
\pagestyle{fancy}
\lhead{\hmwkAuthorName} % Top left header
\chead{\hmwkClass:\ \hmwkTitle} % Top center header
\rhead{\firstxmark \monthyeardate\today} % Top right header
\lfoot{\lastxmark} % Bottom left footer
\cfoot{\textbf{Due on \hmwkDueDate, 23:00}} % Bottom center footer
\rfoot{Page\ \thepage\ of\ \pageref{LastPage}} % Bottom right footer
\renewcommand\headrulewidth{0.4pt} % Size of the header rule
\renewcommand\footrulewidth{0.4pt} % Size of the footer rule

\setlength\parindent{0pt} % Removes all indentation from paragraphs

%----------------------------------------------------------------------------------------
%	NAME AND CLASS SECTION
%----------------------------------------------------------------------------------------

\newcommand{\hmwkTitle}{Final Assignment} % Assignment title
\newcommand{\hmwkDueDate}{16 March 2018} % Due date
\newcommand{\hmwkReleaseData}{\today} % Release date
\newcommand{\hmwkClass}{Predictive Control} % Course/class
%\newcommand{\hmwkClassTime}{10:30am} % Class/lecture time
%\newcommand{\hmwkClassInstructor}{} % Teacher/lecturer
\newcommand{\hmwkAuthorName}{Dr Eric Kerrigan } % Your name

\begin{document}
\section*{Tasks}
\begin{itemize}
\item Create a real-time MPC system in Simulink to manoeuvre a laboratory-scale gantry crane inside a constrained region
\item Write a concise (one-page) report detailing your controller implementation and relevant design choices
\end{itemize}

\section*{Objective}

In this coursework, you will implement a closed-loop MPC system in the provided Simulink template. The controller should be designed to handle arbitrary constraint sets that can take one of 2 different shapes. Shape 1 (shown in Figure \ref{fig:shape1}) involves the crane moving inside a defined region from a starting point to a set target point. Shape 2 (shown in Figure \ref{fig:shape2}) involves the crane moving through an allowed region, turning $90^{\circ}$, then stopping at a point. For both shapes, the sides of the region represent constraints that ideally should not be violated by either the center of the cart position or the pendulum position. The end points for each line segment (e.g.\ the corners of the shape) will be provided to your controller at the beginning of the run, and then the shape will remain constant during the run.
\newline
\newline
The scoring for this assignment will be based on your controller's response to varying the shape sizes (e.g.\ narrowing the rectangles), and also how quickly the crane reaches the target point (with a maximum allowed time of 60 seconds). This assignment is worth 50 points total, 40 from the controller run in the Simulink software and 10 from the controller run on the lab hardware. The breakdown of the scoring for this assignment is:
\begin{table}[h!]
    \centering
    \caption{Scoring breakdown for the assignment}
    \begin{tabular}{c|c|c||c|}
             & Shape 1 & Shape 2 & Total\\
    \hline
    Software & 10 & 30 & 40\\
    \hline
    Hardware & 5 & 5 & 10\\
    \hline\hline
    Total	 & 15 & 35 & 50\\
    \hline
    \end{tabular}
\end{table}

If your controller does not run (e.g.\ errors out), or does not reach the target point, then you will not receive any marks for that shape.

\section*{Shapes}

\subsection*{Shape 1}

The first shape is composed of a single rectangle at an angle in the $xy$ plane, as shown in Figure \ref{fig:shape1}. The crane is allowed to move anywhere inside the rectangle, but must not leave it. The constraints are defined by the 4 corners of the rectangle, which will be provided to you inside a 4x2 matrix of the form
$$
    c = \begin{bmatrix}
    x_1 & y_1\\
    x_2 & y_2\\
    x_3 & y_3\\
    x_4 & y_4\\
    \end{bmatrix}
$$
The crane will start at the point $(x_s, y_s)$, and the target point will be located at the point $(x_t, y_t)$. The crane will be considered at the target point once the pendulum position and the cart position are within a circle of radius $\epsilon_t$ from the target point, and all rate states and inputs have decayed below a threshold $\epsilon_r$.

\begin{figure}[h!]
    \hspace{-6em}
    \begin{minipage}{0.49\textwidth}
        \begin{tikzpicture}
        % Create the hashed areas
        \path [pattern=north west lines, pattern color=red] (2,-2) -- (7,-2) -- (7,3) -- (2,-2);
        \path [pattern=north west lines, pattern color=red] (7,3) -- (5,5) -- (7,5) -- (7,3);
        \path [pattern=north west lines, pattern color=red] (0,0) -- (2,-2) -- (0,-2) -- (0,0);
        \path [pattern=north west lines, pattern color=red] (0,0) -- (5,5) -- (0,5) -- (0,0);
        
        % Draw the lines
        \draw [thick] (0,0) -- (5,5);
        \draw [thick] (5,5) -- (7,3);
        \draw [thick] (7,3) -- (2, -2);
        \draw [thick] (2,-2) -- (0,0);
        
        % Label the points
        \draw [fill] (0,0) circle [radius=0.1];
        \node [left] at (0,0) {$(x_1, y_1)$};
        
        \draw [fill] (5,5) circle [radius=0.1];
        \node [above] at (5,5) {$(x_2, y_2)$};
        
        \draw [fill] (7,3) circle [radius=0.1];
        \node [right] at (7,3) {$(x_3, y_3)$};
        
        \draw [fill] (2,-2) circle [radius=0.1];
        \node [below] at (2,-2) {$(x_4, y_4)$};
        
        % Label the target area
        \draw [fill=white] (5.5,3.5) circle [radius=0.2];
        \node [below] at (5.5,3.5) {$(x_t, y_t)$};
        
        % Label the starting point
        \draw [fill] (1.5,-0.5) circle [radius=0.1];
        \node [right] at (1.5,-0.5) {$(x_s, y_s)$};
        
        % Create the right angle markers
        \draw [dashed] (0.25, 0.25) -- (0.5, 0.0) -- (0.25,-0.25);
        \draw [dashed] (4.75, 4.75) -- (5.0, 4.5) -- (5.25, 4.75);
        \draw [dashed] (6.75, 2.75) -- (6.5, 3.0) -- (6.75, 3.25);
        \draw [dashed] (2.25,-1.75) -- (2.0,-1.5) -- (1.75,-1.75);
        \end{tikzpicture}
        \subcaption{Shape 1.}
        \label{fig:shape1}
    \end{minipage}\hspace{2em}
    \begin{minipage}{0.49\textwidth}
        \begin{tikzpicture}
        % Create the hashed areas
        \path [pattern=north west lines, pattern color=red] (1,1) -- (5,5) -- (1,5) -- (1,1);
        \path [pattern=north west lines, pattern color=red] (5,5) -- (9,1) -- (9,5) -- (5,5);
        \path [pattern=north west lines, pattern color=red] (9,1) -- (8,0) -- (9,0) -- (9,1);
        \path [pattern=north west lines, pattern color=red] (2,0) -- (5,3) -- (8,0) -- (2,0);
        \path [pattern=north west lines, pattern color=red] (1,1) -- (2,0) -- (1,0) -- (1,1);
        
        % Draw the lines
        \draw [thick] (1,1) -- (5,5);
        \draw [thick] (5,5) -- (9,1);
        \draw [thick] (9,1) -- (8, 0);
        \draw [thick] (8,0) -- (5,3);
        \draw [thick] (5,3) -- (2,0);
        \draw [thick] (2,0) -- (1,1);
        
        % Label the points
        \draw [fill] (1,1) circle [radius=0.1];
        \node [left] at (1,1) {$(x_1, y_1)$};
        
        \draw [fill] (5,5) circle [radius=0.1];
        \node [above] at (5,5) {$(x_2, y_2)$};
        
        \draw [fill] (9,1) circle [radius=0.1];
        \node [right] at (9,1) {$(x_3, y_3)$};
        
        \draw [fill] (8,0) circle [radius=0.1];
        \node [below] at (8,0) {$(x_4, y_4)$};
        
        \draw [fill] (5,3) circle [radius=0.1];
        \node [above] at (5,3) {$(x_5, y_5)$};
        
        \draw [fill] (2,0) circle [radius=0.1];
        \node [below] at (2,0) {$(x_6, y_6)$};
        
        % Label the target area
        \draw [fill=white] (8,1) circle [radius=0.2];
        \node [left, rotate=-45] at (8,1) {$(x_t, y_t)$};
        
        % Label the starting point
        \draw [fill] (2,1) circle [radius=0.1];
        \node [right, rotate=45] at (2,1) {$(x_s, y_s)$};
        
        % Create the right angle markers
        \draw [dashed] (1.25, 1.25) -- (1.5, 1.0) -- (1.25, 0.75);
        \draw [dashed] (4.75, 4.75) -- (5.0, 4.5) -- (5.25, 4.75);
        \draw [dashed] (8.75, 1.25) -- (8.5, 1.0) -- (8.75, 0.75);
        \draw [dashed] (7.75, 0.25) -- (8.0, 0.5) -- (8.25, 0.25);        
        \draw [dashed] (4.75, 2.75) -- (5.0, 2.5) -- (5.25, 2.75);        
        \draw [dashed] (2.25, 0.25) -- (2.0, 0.5) -- (1.75, 0.25);
        \end{tikzpicture}
        \subcaption{Shape 2}
        \label{fig:shape2}
    \end{minipage}
    \centering
    \caption{The possible shapes. The allowed region is in white, forbidden region in red.}
\end{figure}

\subsection*{Shape 2}

The second shape is composed of 2 rectangles, joined together at a $90^{\circ}$ angle, as shown in Figure \ref{fig:shape2}. The constraints are defined by the 6 corners, which will be provided to you inside a 6x2 matrix of the form
$$
c = \begin{bmatrix}
x_1 & y_1\\
x_2 & y_2\\
x_3 & y_3\\
x_4 & y_4\\
x_5 & y_5\\
x_6 & y_6
\end{bmatrix}
$$

The crane will start at the point $(x_s, y_s)$, and the target point will be located at the point $(x_t, y_t)$. The crane will be considered at the target point once the pendulum position and the cart position are within a circle of radius $\epsilon_t$ from the target point, and all rate states and inputs have decayed below a threshold $\epsilon_r$.
\newline

No intermediate points between the starting point and the target point will be provided, so it is up to your controller to create the path inside the allowed region.

\section*{Approach}

\subsection*{Physical System}

\begin{wrapfigure}{r}{0.3\textwidth}
    \centering
    \begin{tikzpicture}
    \draw (0,10) -- (5,10);
    \draw (2.5,10) -- (1,8);
    \draw [dashed](2.5,10) -- (2.5,8);
    \draw [->, dashed] (2.5,9) to [out=225, in=-45] (1.75,9);
    \draw [fill] (1,8) circle  [radius=0.1];
    \draw [fill] (2.5, 10) circle [radius=0.1];
    \node [above] at (2.5,10) {$x_c$};
    \node [left] at (1,8) {$x_p$};
    \node [above, rotate=60] at (1.75,9) {$l$};
    \node [above] at (2.1,8.3) {$\theta$};
    \end{tikzpicture}
    \caption{Pendulum and cart}
    \label{fig:pendulum}
\end{wrapfigure}
This assignment will use the laboratory-scale gantry crane that you have been using throughout the course (and which you derived the model for in assignment 1). The pendulum length will be fixed at 0.47m, and all constraints must be satisfied by both the pendulum and the cart positions. The pendulum is assumed to be a point-mass located at the end of a rigid string of length $l{=}0.47$m, as shown in Figure \ref{fig:pendulum}. Each angle is measured from the vertical line passing through the cart, with $\theta$ representing rotation about the $y$-axis (as shown in Figure \ref{fig:pendulum}), and $\psi$ representing the rotation about the $x$ axis.
\newline

The controller will run at a fixed rate of 20Hz ($T_s = \frac{1}{20}$s).

\subsection*{Framework provided:}

You will be provided with the following files on Cody Coursework for this assignment:

\begin{tabular}{p{6cm} p{10cm}}
    \textbf{FunctionTemplate\_S1.m} & Template for submission of code for shape 1\\
    \textbf{FunctionTemplate\_S2.m} & Template for submission of code for shape 2\\
\end{tabular}
\newline

Additional files are available on Blackboard for you to use when testing your design.

\begin{tabular}{p{6cm} p{10cm}}
    \textbf{SimscapeCrane\_ClosedLoop.slx} & Nonlinear Simscape model for testing the controllers\\
    \textbf{testMyDesign.m} & Script to test your closed-loop system \\
    \textbf{testMyDesign\_Linear.m} & Script to test your closed-loop system on a linear model \\
    \textbf{analyzeCourse.p} & Plot your trial against the constraint set \\
    \textbf{extractFuntions.m} & Helper function for the scripts
\end{tabular}
\newline

The two \textbf{FunctionTemplate\_S} files each contain the function declarations for all 4 functions you will be writing. Only make changes to the functions inside the template file, since all other locations (e.g.\ inside Simulink, inside the file with the function's name) will be overwritten every time you use \textbf{testMyDesign.m}.
\newline

To test your design on the nonlinear model, open the the \textbf{testMyDesign} script and select the appropriate shape number (e.g.\ for shape 1 set testShape=1, for shape 2 set testShape=2). Then, run the script. It will automatically load Simulink, run your controller, and display the resulting crane path and constraints.
\newline

To debug your design (e.g.\ using the Matlab debugger), use the \textbf{testMyDesign\_Linear} script. You can then place debug points at the desired lines in the functions inside the template file.

\subsection*{mySetup}

This function is where you will implement any controller generation/design tasks (e.g. matrix generation, gain programming, constraint generation, etc.). This function gets as inputs 5 variables: $c$, \textit{startingPoint}, \textit{targetPoint}, $\epsilon_r$, and $\epsilon_t$. $c$ contains the coordinates of the shape's corners, \textit{startingPoint} and \textit{targetPoint} contain the $(x,y)$ coordinate pair of the starting point and target point respectively, and $\epsilon_r$ and $\epsilon_t$ are the tolerance for meeting the target point and the rate's values at the end (as described above).
\newline

This function returns 1 variable, \textit{param}. This variable is a MATLAB structure that you define the fields of. This structure will be passed to the 3 functions in Simulink, so variables set inside this structure can be used inside your controller functions.
\newline

The following functions/data-files from previous assignments will be available to use inside your \textbf{mySetup} function to setup your control structure:

\begin{tabular}{p{6cm} p{10cm}}
    \textbf{SSmodelParams.mat} & System parameters\\
    \textbf{Params\_Simscape.mat} & System parameters\\
    \textbf{genCraneODE} & Crane system matrix generation (Assignment 1)\\
    \textbf{genPrediction} & Generate prediction matrices (Assignment 2)\\
    \textbf{genCostMatrices} & Generate cost function matrices (Assignment 2)\\
    \textbf{genRHC} & Generate static RHC matrix (Assignment 2)\\
    \textbf{genStageConstraints} & Generate stage contraints (Assignment 3)\\
    \textbf{genTrajectoryConstraints} & Generate the trajectory constraints (Assignment 3)\\
    \textbf{genConstraintMatrices} & Generate QP Constraint matrices (Assignment 3)\\
    \textbf{genSoftPadding} & Generate the padded QP matrices (Assignment 3)
\end{tabular}

\subsection*{Simulink Functions}

You will be responsible for writing 3 functions for each shape, which will run inside MATLAB function blocks of the Simulink model. Your function code inside these may only call MATLAB functions listed on this webpage: \href{https://uk.mathworks.com/help/simulink/ug/functions-supported-for-code-generation-alphabetical-list.html}{https://uk.mathworks.com/help/simulink/ug/functions-supported-for-code-generation-alphabetical-list.html}. If you attempt to call any other functions, then your controller will generate an error. Note that \textbf{mpcqpsolver} and \textbf{chol} are able to be used, but other optimization solvers, such as \textbf{quadprog} and \textbf{fmincon}, will not work.
\newline\newline
The architecture that you are provided inside the Simulink file is shown in Figure \ref{fig:bdiagram}. Every block will have a fixed size for the input and output vectors, which is described in the following sections. Additionally, every block will have \textit{param} as an input variable. This varaible is a structure created by you inside the setup function and can it be accessed inside these Simulink functions.
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
        \node [draw, rectangle, minimum height=3em, minimum width=3em, fill=yellow,name=gen] {\textbf{myTargetGenerator}};
        \node [draw, rectangle, minimum height=3em, minimum width=3em, fill=yellow,name=cont, right of=gen, node distance=13em] {\textbf{myMPController}};
        \node [draw, rectangle, minimum height=3em, minimum width=3em, fill=white,name=crane, right of=cont, node distance=10em] {Crane};
        \node [draw, rectangle, minimum height=3em, minimum width=3em, fill=yellow,name=est, below of=cont, node distance=5em] {\textbf{myStateEstimator}};
        \draw [->, line width=1.5pt] (cont) -- node[above]{u} (crane);
        \draw [->, line width=1.5pt] (cont.east) -- ($(cont.east)+(0.75,0)$) |- ($(est.east)+(0,0.25)$);
        \draw [->, line width=1.5pt] (crane.east) -- node[above]{y} ($(crane.east)+(0.75,0)$) |- ($(est.east)-(0,0.25)$);
        \draw [->, line width=1.5pt] ($(gen.east)+(0,0.25)$) -- node[above]{r} ($(cont.west)+(0,0.25)$);
        \draw [->, line width=1.5pt] (est.west) -- node[above]{$\hat{x}$} ($(est.west)-(0.5,0)$) -| ($(gen.west)-(0.5,0)$) -- (gen.west);
        \draw [->, line width=1.5pt] (est.west) -| ($(cont.west)-(0.75,0.25)$) -- ($(cont.west)-(0,0.25)$);
    \end{tikzpicture}
    \caption{Block diagram of the complete system. Yellow blocks are the functions you will write.}
    \label{fig:bdiagram}
\end{figure}

\subsubsection*{myStateEstimator}

If desired, you can implement a custom state-estimator/observer inside this function block. This function takes two vectors as its input: the current measurements from the crane, and the inputs that were supplied to the crane at the last sample time. There is a single output from the estimator.
\newline

The control input, $u$ is a 2x1 vector that has the following components: $
    u = \begin{bmatrix}
    u_x &
    u_y
    \end{bmatrix}^{T}
$.
The input containing the measured system variables is an 8x1 vector that has the following components:
$
    y = \begin{bmatrix}
    x_c &
    \dot{x}_c &
    y_c &
    \dot{y}_c &
    \theta &
    \dot{\theta} &
    \psi &
    \dot{\psi}
    \end{bmatrix}^{T}
$\newline

The output from this function is a 16x1 vector which connects to the \textbf{myTargetGenerator} and \textbf{MyMPController} blocks. By default, the measured system variables are passed through to the output in the same order, and the unused indices (9-16) are set to 0.

\subsubsection*{myTargetGenerator}

This function block is designed to contain the target generator for your controller. It has 1 input, which is the output from the state estimator, and 1 output to the controller block. The input is the 16x1 $\hat{x}$ vector from the state estimator. The output is a 10x1 vector $r$  (that you specify the structure of) that connects to the \textbf{myMPController} block. By default, this block does not have anything in it and the output is all 0.

\subsubsection*{myMPController}

This function block is designed to contain your MPC implementation. It has 2 inputs, the first is \textit{r} which is the 10x1 vector from the target generator, and the second is $\hat{x}$ which is the 16x1 output from the state estimator. It has 1 output, which is the 2x1 control signal $u$ that is given to the crane and to the state estimator in the next sample period. The control signal has the components: $
u = \begin{bmatrix}
u_x &
u_y
\end{bmatrix}^{T}
$.
By default, this block does not have anything in it and the output is all 0.



\section*{Submission}

You will be submitting 2 files to Cody Coursework for this assignment.

\vspace{1em}
\begin{tabular}{p{7cm} p{9cm}}
\textbf{Filename} &Description.\\
\hline
\textbf{FunctionTemplate\_S1.m}  & File containting all 4 functions for shape 1\\
\textbf{FunctionTemplate\_S2.m}  & File containting all 4 functions for shape 2\\
\end{tabular}
\newline

Cody Coursework does not do any marking of your design, it will only test to make sure your controller can run, and you are using the correct input and output sizes. It will supply a representative input, so it can catch MATLAB programming errors, but will not actually perform any validation of your design.

\section*{Hardware}

The majority of this lab is based on the software model provided to you in the closed-loop Simulink file. Your controller should be designed based upon that model, and also your report will be written based only on your design for the software model. No hardware portions should be discussed in the report.
\newline

In order for your controller to properly run on the hardware system, the computation time should be less than the sample period of the controller. This can be checked by running the \textbf{testMyDesign\_Linear} script, and examining the resulting computation time graph. This graph displays the amount of time taken to perform the computation in all 3 functions at each sample instant. If your controller computation takes longer than the sample period, it will not behave properly on the hardware system.
\newline

The hardware testing will use the code you submitted to Cody Coursework on the due date. You are then given a 30-minute timeslot with the hardware where minor tweaks/tuning to your controller can be done. The controller at the end of that 30-minute session will be used for grading the hardware portion of this assignment.

\section*{Report}

You are also required to write a short report detailing your solution to this assignment (only discuss the software portion). This report must not be more than 1 page (2-column format) in length, and cannot contain any figures, tables or appendices. Any report longer than 1 page or containing those items will receive no marks. A template will be provided on Blackboard for you to use when writing the report. Do not modify this template.
\newline

This report should describe your solution to the above assignment. Your report should include answers to the following questions:
\begin{itemize}[noitemsep]
    \item How did you formulate your optimal control problem?
    \item How did you include the constraints in the problem?
    \item How did you implement your control strategy?
    \item How did you deal with possible infeasibility?
    \item How did you address plant-model mismatch in your design?
\end{itemize}

Your report will be marked out of 20, following the rubric shown below.

\begin{table}[h!]
    \centering
    \caption{Marking rubric for the report}
    \begin{tabular}{l|c}
        \textbf{Element} & \textbf{Weighting}\\
        \hline
        Presentation & 4\\
        Language/Grammar & 4\\
        Clarity/Expression & 4\\
        Description of solution method & 8
    \end{tabular}
\end{table}

\end{document}