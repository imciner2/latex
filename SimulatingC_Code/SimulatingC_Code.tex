% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Ian McInerney
% Creation Date: January 26, 2017
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{letterpaper} % or letterpaper (US) or a5paper or....
\geometry{margin=1in} % for example, change the margins to 2 inches all round

%%% PACKAGES
%\include{Standard-Includes} % Include the standard packages used in every file I create
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{rotating}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{enumitem}
\usepackage{subcaption}
\usepackage{listings}
% These packages are all incorporated in the memoir class to one degree or another...

% Create a norm and abs environment
\usepackage{mathtools}
\usepackage{newfloat}

\usepackage{ifthen}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}

% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\usepackage{datetime}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customize the layout...
\renewcommand{\footrulewidth}{0.6pt}
\lhead{}
\chead{\ifthenelse{\value{page}=1}{}{Simulating C Code in Simulink}}
\rhead{}
\lfoot{Draft: \today\ \currenttime}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\bibliographystyle{IEEEtran}

%%% END Article customizations

\begin{document}

\begin{center}
	\Large\textbf{Simulating C Code in Simulink}
\end{center}

\section*{Introduction}

This document provides a step-by-step guide for simulating a C code function inside a Simulink block diagram. This functionality is useful for testing controllers and filters as they are implemented in the C code itself. This guide will walk through an example, simulating a simple test function written in C. The steps shown here are applicable for anyother C code block, with minor changes (such as filenames, input variables, output variables, etc.).

\vspace{0.5cm}
\noindent
\textbf{Note:} Simulating C code in Simulink requires the \textit{Simulink Coder} package.

\section*{Organization}

In order to use the simulation, 3 files are needed in the same directory:
\begin{enumerate}[noitemsep]
	\item \textbf{testFunction.slx} - The Simulink model for the simulation
	\item \textbf{testFunction.c} - C source file containing the function to simulate
	\item \textbf{testFunction.h} - Header file with function prototype for the function to simulate
\end{enumerate}

\section*{Simulink File}

\subsection*{Blocks}

In order to use the C coder in Simulink to execute a C code block, it must be called from inside a MATLAB function block. The MATLAB function block code would look something like this:

\begin{lstlisting}[language=matlab,frame=single,breaklines=true]
function [blockOutput1, blockOutput2] = fcn(input1, input2)

% These are the two variables that will be passed by reference
output1 = 0;
output2 = 2;

retVal = 0;

% This is the actual call to the function
retVal = coder.ceval('testFunction', input1, input2,...
                     coder.ref(output1),...
                     coder.wref(output2));

% Make the assignment for the block outputs
blockOutput1 = output1;
blockOutput2 = output2;
\end{lstlisting}

In this code, the actual C function is called inside the \textit{coder.ceval} function. This function takes the following arguments: 
\begin{enumerate}[noitemsep]
	\item The function name (as a string)
	\item The input arguments (in order)
\end{enumerate}
and will return the value returned by the C function.

The \textit{coder.ceval} function can take in 2 types of arguments: pass by value and pass by reference. To pass a variable by value, simply provide the variable to the \textit{coder.ceval} in the appropriate position. Passing a variable by reference is slightly more complicated, since there is no notion of a pointer in MATLAB functions.

There are 2 ways to pass a variable by reference:
\begin{itemize}[noitemsep]
	\item \textbf{Read only} - This is accomplished by calling \textbf{coder.rref(variableName)}. This variable can be used to pass in values, but cannot be used to get data back from the function. This is best used when the argument has the ``const'' keyword in it.
	\item \textbf{Write only} - This is accomplished by calling \textbf{coder.wref(variableName)}. This variable can be referenced only for writing to in the C code, and if its value is updated in the C code, that change is reflected in the MATLAB function.
	\item \textbf{Read/Write} - This is accomplished by calling \textbf{coder.ref(variableName)}. This variable can be referenced in the C code, and if its value is updated in the C code, that change is reflected in the MATLAB function.
\end{itemize}

The code block given above will create a MATLAB function block with 2 inputs and 2 outputs. It will call a function ``testFunction'', and pass in 4 variables:
\begin{itemize}[noitemsep,nolistsep]
	\item \textbf{input1}'s value (pass by value)
	\item \textbf{input2}'s value (pass by value)
	\item \textbf{output1}'s address (pass by reference, read and write)
	\item \textbf{output2}'s address (pass by reference, write only)
\end{itemize}

\subsection*{Settings}

The most important thing to remember when using C code in Simulink, is to think about the sampling interval of the C function. Since the C functions do not usually represent an ODE, they should have a sampling time specified inside the block parameters dialog box.

There are 2 main Simulink settings that must be modified in the Model Configuration Parameters dialog:
\begin{enumerate}
	\item \textbf{Header File:} Inside the Simulation Target $\rightarrow$ Custom Code menu there is an option in the upper window called ``Insert custon C code in genenerated'' labeled ``Header File.'' Inside here, enter the include directive for the header file of the C code. In this case:
	\begin{verbatim}
		#include "testFunction.h"
	\end{verbatim}
	\item \textbf{Source File:} Inside the Simulation Target $\rightarrow$ Custom Code menu, there is an option in the lower window called ``Include list of additional.'' Under the ``Source Files'' heading, enter the filename for the C source file. In this case:
	\begin{verbatim}
		testFunction.c
	\end{verbatim}
\end{enumerate}

These settings are shown in figure \ref{fig:settingsDialog}.

\begin{figure}
	\includegraphics[width=\linewidth]{images/SettingsDialog.png}
	\caption{Simulink Model Configuration Parameters dialog box showing necessary entries.}
	\label{fig:settingsDialog}
\end{figure}

\subsection*{Re-Running the Simulation}

The C code used in the \textit{coder.ceval} function is compiled into a MEX function. This MEX function is then loaded into memory and used as a shared library. The way Simulink access the library, it is persistent in memory across the simulation runs (it is not removed from memory). This means that any static varibles defined in the functions will not be reset on simulation start, and will instead take on the value they had after the last simulation. To fix this, simply unload the MEX files from memory by running the command
\begin{verbatim}
	clear mex
\end{verbatim}

This process can be automated by including that line inside the model's ``initFcn'' callback. To do that, do the following (final window is shown in figure \ref{fig:modelProperties}):

\begin{enumerate}[noitemsep]
	\item Right-click in the model 
	\item Click on Model Properties
	\item Select the Callback Functions tab
	\item Select InitFcn from the list
	\item Enter the clear command into the field on the right, then click ok.
\end{enumerate}

\begin{figure}
	\includegraphics[width=\linewidth]{images/ModelProperties.png}
	\caption{Model properties dialog box where the clear command can be inserted to run before each simulation}
	\label{fig:modelProperties}
\end{figure}

\clearpage
\newpage

\section*{C Source File}

The C source file contains the function that is to be simulated. For best results (aka. it is unknown how it behaves if you don't), follow these guidelines:

\begin{itemize}[noitemsep]
	\item The function may only return 1 double value (any other values to be returned must be through pointers given as parameters)
	\item The function should only take doubles or pointers to doubles as arguments
	\item The function should be C only (no C++)
	\item Any global variables or defines the function normally uses in-situ should be changed to either be an argument to the function or a static variable in the function
	\item Pointers can be used as arguments, and the underlying data can be modified by the C code and returned to MATLAB
	\item Comments must be in \textit{/* comment */} format, not \textit{// comment}
\end{itemize}

A sample C function that will simulate is shown below:
{\small
\begin{lstlisting}[language=c,frame=single, breaklines=true]
/**
 * This function is just a simple test function
 *
 * @param input1 - A random input value
 * @param input2 - A random input value
 * @param output1 - Pointer to an output variable
 * @param output2 - Pointer to an output variable
 * @return A return value
 */
double testFunction(double input1, double input2,
                    double *output1, double *output2) {
    /* This is a variable that will hold its value across runs
      (This is the proper way to do a feedback/forward in the C file) */
    static float holdOver = 0;

    /* Do some computations... */
    holdOver = holdOver + input1;

    /* These variables are passed in as pointers, so they can have a value assigned to them and it will appear back in Simulink (great for outputting multiple things)  */
    *output1 = input1 + input2 + *output1;
    *output2 = holdOver - input2;

    /* This will end the C function and pass the variable back to Simulink */
    return(holdOver);
}
\end{lstlisting}}

In this function, two variables are passed by value (\textit{input1} and \textit{input2}), and two variables are passed by reference (\textit{output1} and \textit{output2}). The two pass-by-value variables can be used in any computations in the code, but any changes to them will be lost when the function ends. The two pass-by-reference variables must be de-referenced before being used (since they are pointers), but they can be used for passing data both into and out of the function when reference appropriately from the MATLAB function block.

\newpage

\section*{C Header File}

The header file needed for simulation is basically a standard header file. It must contain the function prototype for the function. An example for the test function is shown below:

\begin{lstlisting}[language=c,frame=single]
#ifndef TEST_FUNCTION_H_
#define TEST_FUNCTION_H_

/**
 * This function is just a simple test function
 *
 * @param input1 - A random input value
 * @param input2 - A random input value
 * @param output1 - Pointer to an output variable
 * @param output2 - Pointer to an output variable
 * @return A return value
 */
double testFunction(double input1, double input2,
                    double *output1, double *output2);

#endif
\end{lstlisting}



\end{document}
